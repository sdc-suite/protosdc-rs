use protosdc_biceps::biceps::{
    context_association_mod, AbstractContextState, ContextAssociation, LocationContextState,
    PatientContextState,
};

use crate::common::mdib_description_modification::MdibDescriptionModification;
use crate::common::mdib_description_modifications::MdibDescriptionModifications;
use crate::common::mdib_pair::{
    ActivateOperationPair, AlertConditionPair, AlertSignalPair, AlertSystemPair, BatteryPair,
    ChannelPair, ClockPair, LocationContextPair, MdsPair, MultiStatePair, NumericMetricPair, Pair,
    PatientContextPair, RealTimeSampleArrayMetricPair, ScoPair, SetAlertStateOperationPair,
    SetComponentStateOperationPair, SetContextStateOperationPair, SetMetricStateOperationPair,
    SetStringOperationPair, SetValueOperationPair, SingleStatePair, StringMetricPair,
    SystemContextPair, VmdPair,
};
use crate::common::storage::mdib_storage::{MdibStorage, MdibStorageConstructor, MdibStorageImpl};
use crate::test_util::{handles, mdib_utils};

/// Creates a set of modifications with the following structure.
//
//     MDS_0
//       ALERT_SYSTEM_0
//         ALERT_CONDITION_0
//         ALERT_CONDITION_1
//         ALERT_SIGNAL_0
//         ALERT_SIGNAL_1
//       VMD_0
//         ALERT_SYSTEM_1
//         CHANNEL_0
//           METRIC_0 (numeric)
//           METRIC_1 (string)
//         CHANNEL_1
//       VMD_1
//       SYSTEM_CONTEXT_0
//         CONTEXT_DESCRIPTOR_0 (Pat)
//           CONTEXT_0
//           CONTEXT_1
//         CONTEXT_DESCRIPTOR_1 (Loc)
//           CONTEXT_2
//       SCO_0
//         OPERATION_0
//         OPERATION_1
//       CLOCK_0
//       BATTERY_0
//       BATTERY_1
//     MDS_1
//noinspection DuplicatedCode
pub fn mdib_modification_presets() -> MdibDescriptionModifications {
    let mut modifications = MdibDescriptionModifications::default();

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(MdsPair::from((
                mdib_utils::create_minimal_mds_descriptor(handles::MDS_0),
                mdib_utils::create_minimal_mds_state(handles::MDS_0),
            )))),
            parent: None,
        })
        .unwrap_or_else(|_| panic!("Could not add mds {}", handles::MDS_0));

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(AlertSystemPair::from((
                mdib_utils::create_minimal_alert_system_descriptor(handles::ALERT_SYSTEM_0),
                mdib_utils::create_minimal_alert_system_state(handles::ALERT_SYSTEM_0),
            )))),
            parent: Some(handles::MDS_0.to_string()),
        })
        .unwrap_or_else(|_| panic!("Could not add asy {}", handles::ALERT_SYSTEM_0));

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(AlertConditionPair::from((
                mdib_utils::create_minimal_alert_condition_descriptor(handles::ALERT_CONDITION_0),
                mdib_utils::create_minimal_alert_condition_state(handles::ALERT_CONDITION_0),
            )))),
            parent: Some(handles::ALERT_SYSTEM_0.to_string()),
        })
        .unwrap_or_else(|_| panic!("Could not add condition {}", handles::ALERT_CONDITION_0));

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(AlertConditionPair::from((
                mdib_utils::create_minimal_alert_condition_descriptor(handles::ALERT_CONDITION_1),
                mdib_utils::create_minimal_alert_condition_state(handles::ALERT_CONDITION_1),
            )))),
            parent: Some(handles::ALERT_SYSTEM_0.to_string()),
        })
        .unwrap_or_else(|_| panic!("Could not add condition {}", handles::ALERT_CONDITION_1));

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(AlertSignalPair::from((
                mdib_utils::create_minimal_alert_signal_descriptor(handles::ALERT_SIGNAL_0),
                mdib_utils::create_minimal_alert_signal_state(handles::ALERT_SIGNAL_0),
            )))),
            parent: Some(handles::ALERT_SYSTEM_0.to_string()),
        })
        .unwrap_or_else(|_| panic!("Could not add signal {}", handles::ALERT_SIGNAL_0));

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(AlertSignalPair::from((
                mdib_utils::create_minimal_alert_signal_descriptor(handles::ALERT_SIGNAL_1),
                mdib_utils::create_minimal_alert_signal_state(handles::ALERT_SIGNAL_1),
            )))),
            parent: Some(handles::ALERT_SYSTEM_0.to_string()),
        })
        .unwrap_or_else(|_| panic!("Could not add signal {}", handles::ALERT_SIGNAL_1));

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(VmdPair::from((
                mdib_utils::create_minimal_vmd_descriptor(handles::VMD_0),
                mdib_utils::create_minimal_vmd_state(handles::VMD_0),
            )))),
            parent: Some(handles::MDS_0.to_string()),
        })
        .unwrap_or_else(|_| panic!("Could not add vmd {}", handles::VMD_0));

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(AlertSystemPair::from((
                mdib_utils::create_minimal_alert_system_descriptor(handles::ALERT_SYSTEM_1),
                mdib_utils::create_minimal_alert_system_state(handles::ALERT_SYSTEM_1),
            )))),
            parent: Some(handles::VMD_0.to_string()),
        })
        .unwrap_or_else(|_| panic!("Could not add asy {}", handles::ALERT_SYSTEM_1));

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(ChannelPair::from((
                mdib_utils::create_minimal_channel_descriptor(handles::CHANNEL_0),
                mdib_utils::create_minimal_channel_state(handles::CHANNEL_0),
            )))),
            parent: Some(handles::VMD_0.to_string()),
        })
        .unwrap_or_else(|_| panic!("Could not add channel {}", handles::CHANNEL_0));

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(NumericMetricPair::from((
                mdib_utils::create_minimal_numeric_metric_descriptor(handles::METRIC_0),
                mdib_utils::create_minimal_numeric_metric_state(handles::METRIC_0),
            )))),
            parent: Some(handles::CHANNEL_0.to_string()),
        })
        .unwrap_or_else(|_| panic!("Could not add metric {}", handles::METRIC_0));

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(StringMetricPair::from((
                mdib_utils::create_minimal_string_metric_descriptor(handles::METRIC_1),
                mdib_utils::create_minimal_string_metric_state(handles::METRIC_1),
            )))),
            parent: Some(handles::CHANNEL_0.to_string()),
        })
        .unwrap_or_else(|_| panic!("Could not add metric {}", handles::METRIC_1));

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(ChannelPair::from((
                mdib_utils::create_minimal_channel_descriptor(handles::CHANNEL_1),
                mdib_utils::create_minimal_channel_state(handles::CHANNEL_1),
            )))),
            parent: Some(handles::VMD_0.to_string()),
        })
        .unwrap_or_else(|_| panic!("Could not add channel {}", handles::CHANNEL_1));

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(RealTimeSampleArrayMetricPair::from(
                (
                    mdib_utils::create_minimal_real_time_sample_array_metric_descriptor(
                        handles::METRIC_2,
                    ),
                    mdib_utils::create_minimal_real_time_sample_array_metric_state(
                        handles::METRIC_2,
                    ),
                ),
            ))),
            parent: Some(handles::CHANNEL_1.to_string()),
        })
        .unwrap_or_else(|_| panic!("Could not add metric {}", handles::METRIC_2));

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(VmdPair::from((
                mdib_utils::create_minimal_vmd_descriptor(handles::VMD_1),
                mdib_utils::create_minimal_vmd_state(handles::VMD_1),
            )))),
            parent: Some(handles::MDS_0.to_string()),
        })
        .unwrap_or_else(|_| panic!("Could not add vmd {}", handles::VMD_1));

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(SystemContextPair::from((
                mdib_utils::create_minimal_system_context_descriptor(handles::SYSTEM_CONTEXT_0),
                mdib_utils::create_minimal_system_context_state(handles::SYSTEM_CONTEXT_0),
            )))),
            parent: Some(handles::MDS_0.to_string()),
        })
        .unwrap_or_else(|_| panic!("Could not add system context {}", handles::SYSTEM_CONTEXT_0));

    // patient context data
    let pat1 = {
        let stuff = mdib_utils::create_minimal_patient_context_state(
            handles::CONTEXT_DESCRIPTOR_0,
            handles::CONTEXT_0,
        );

        PatientContextState {
            abstract_context_state: AbstractContextState {
                context_association_attr: Some(ContextAssociation {
                    enum_type: context_association_mod::EnumType::No,
                }),
                ..stuff.abstract_context_state
            },
            ..stuff
        }
    };

    let pat2 = {
        let stuff = mdib_utils::create_minimal_patient_context_state(
            handles::CONTEXT_DESCRIPTOR_0,
            handles::CONTEXT_1,
        );

        PatientContextState {
            abstract_context_state: AbstractContextState {
                context_association_attr: Some(ContextAssociation {
                    enum_type: context_association_mod::EnumType::No,
                }),
                ..stuff.abstract_context_state
            },
            ..stuff
        }
    };

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(MultiStatePair::from(PatientContextPair::from((
                mdib_utils::create_minimal_patient_context_descriptor(
                    handles::CONTEXT_DESCRIPTOR_0,
                ),
                vec![pat1, pat2],
            )))),
            parent: Some(handles::SYSTEM_CONTEXT_0.to_string()),
        })
        .unwrap_or_else(|_| {
            panic!(
                "Could not add patient context {}",
                handles::CONTEXT_DESCRIPTOR_0
            )
        });

    let loc1 = {
        let stuff = mdib_utils::create_minimal_location_context_state(
            handles::CONTEXT_DESCRIPTOR_1,
            handles::CONTEXT_2,
        );

        LocationContextState {
            abstract_context_state: AbstractContextState {
                context_association_attr: Some(ContextAssociation {
                    enum_type: context_association_mod::EnumType::Assoc,
                }),
                ..stuff.abstract_context_state
            },
            ..stuff
        }
    };

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(MultiStatePair::from(LocationContextPair::from((
                mdib_utils::create_minimal_location_context_descriptor(
                    handles::CONTEXT_DESCRIPTOR_1,
                ),
                loc1,
            )))),
            parent: Some(handles::SYSTEM_CONTEXT_0.to_string()),
        })
        .unwrap_or_else(|_| {
            panic!(
                "Could not add location context {}",
                handles::CONTEXT_DESCRIPTOR_0
            )
        });

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(ScoPair::from((
                mdib_utils::create_minimal_sco_descriptor(handles::SCO_0),
                mdib_utils::create_minimal_sco_state(handles::SCO_0),
            )))),
            parent: Some(handles::MDS_0.to_string()),
        })
        .unwrap_or_else(|_| panic!("Could not add sco {}", handles::SCO_0));

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(ActivateOperationPair::from((
                mdib_utils::create_minimal_activate_operation_descriptor(
                    handles::OPERATION_0,
                    handles::MDS_0,
                ),
                mdib_utils::create_minimal_activate_operation_state(handles::OPERATION_0),
            )))),
            parent: Some(handles::SCO_0.to_string()),
        })
        .unwrap_or_else(|_| panic!("Could not add activate {}", handles::OPERATION_0));

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(SetAlertStateOperationPair::from((
                mdib_utils::create_minimal_set_alert_state_operation_descriptor(
                    handles::OPERATION_1,
                    handles::MDS_0,
                ),
                mdib_utils::create_minimal_set_alert_state_operation_state(handles::OPERATION_1),
            )))),
            parent: Some(handles::SCO_0.to_string()),
        })
        .unwrap_or_else(|_| panic!("Could not add set alert state {}", handles::OPERATION_1));

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(SetComponentStateOperationPair::from(
                (
                    mdib_utils::create_minimal_set_component_state_operation_descriptor(
                        handles::OPERATION_2,
                        handles::MDS_0,
                    ),
                    mdib_utils::create_minimal_set_component_state_operation_state(
                        handles::OPERATION_2,
                    ),
                ),
            ))),
            parent: Some(handles::SCO_0.to_string()),
        })
        .unwrap_or_else(|_| panic!("Could not add set component state {}", handles::OPERATION_2));

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(SetContextStateOperationPair::from((
                mdib_utils::create_minimal_set_context_state_operation_descriptor(
                    handles::OPERATION_3,
                    handles::MDS_0,
                ),
                mdib_utils::create_minimal_set_context_state_operation_state(handles::OPERATION_3),
            )))),
            parent: Some(handles::SCO_0.to_string()),
        })
        .unwrap_or_else(|_| panic!("Could not add set context state {}", handles::OPERATION_3));

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(SetMetricStateOperationPair::from((
                mdib_utils::create_minimal_set_metric_state_operation_descriptor(
                    handles::OPERATION_4,
                    handles::MDS_0,
                ),
                mdib_utils::create_minimal_set_metric_state_operation_state(handles::OPERATION_4),
            )))),
            parent: Some(handles::SCO_0.to_string()),
        })
        .unwrap_or_else(|_| panic!("Could not add set metric state {}", handles::OPERATION_4));

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(SetStringOperationPair::from((
                mdib_utils::create_minimal_set_string_operation_descriptor(
                    handles::OPERATION_5,
                    handles::METRIC_1,
                ),
                mdib_utils::create_minimal_set_string_operation_state(handles::OPERATION_5),
            )))),
            parent: Some(handles::SCO_0.to_string()),
        })
        .unwrap_or_else(|_| panic!("Could not add set string {}", handles::OPERATION_5));

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(SetValueOperationPair::from((
                mdib_utils::create_minimal_set_value_operation_descriptor(
                    handles::OPERATION_6,
                    handles::MDS_0,
                ),
                mdib_utils::create_minimal_set_value_operation_state(handles::OPERATION_6),
            )))),
            parent: Some(handles::SCO_0.to_string()),
        })
        .unwrap_or_else(|_| panic!("Could not add set value {}", handles::OPERATION_5));

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(ClockPair::from((
                mdib_utils::create_minimal_clock_descriptor(handles::CLOCK_0),
                mdib_utils::create_minimal_clock_state(handles::CLOCK_0),
            )))),
            parent: Some(handles::MDS_0.to_string()),
        })
        .unwrap_or_else(|_| panic!("Could not add clock {}", handles::CLOCK_0));

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(BatteryPair::from((
                mdib_utils::create_minimal_battery_descriptor(handles::BATTERY_0),
                mdib_utils::create_minimal_battery_state(handles::BATTERY_0),
            )))),
            parent: Some(handles::MDS_0.to_string()),
        })
        .unwrap_or_else(|_| panic!("Could not add battery {}", handles::BATTERY_0));

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(BatteryPair::from((
                mdib_utils::create_minimal_battery_descriptor(handles::BATTERY_1),
                mdib_utils::create_minimal_battery_state(handles::BATTERY_1),
            )))),
            parent: Some(handles::MDS_0.to_string()),
        })
        .unwrap_or_else(|_| panic!("Could not add battery {}", handles::BATTERY_1));

    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::from(SingleStatePair::from(MdsPair::from((
                mdib_utils::create_minimal_mds_descriptor(handles::MDS_1),
                mdib_utils::create_minimal_mds_state(handles::MDS_1),
            )))),
            parent: None,
        })
        .unwrap_or_else(|_| panic!("Could not add mds {}", handles::MDS_1));

    modifications
}

pub fn base_storage() -> MdibStorageImpl {
    let mut storage = MdibStorageImpl::new("wat:yolo".to_string(), false);

    let modifications = mdib_modification_presets();

    storage
        .apply_description(
            storage.mdib_version().inc(),
            Some(storage.md_description_version() + 1),
            Some(storage.md_state_version() + 1),
            modifications,
        )
        .expect("write failed");

    storage
}
