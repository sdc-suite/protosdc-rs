use protosdc_biceps::biceps::{
    localized_text_width_mod, LocalizedText, LocalizedTextContent, LocalizedTextRef,
    LocalizedTextWidth, ReferencedVersion, VersionCounter,
};
use protosdc_biceps::types::ProtoLanguage;

pub struct Initial;
pub struct LocalizationServiceRef {
    reference: String,
    lang: Option<String>,
    version: u64,
    width: Option<LocalizedTextWidth>,
}

pub struct NoRef {
    lang: Option<String>,
    text: String,
    width: Option<LocalizedTextWidth>,
}

#[derive(Clone, Debug)]
pub struct LocalizedTextBuilder<Kind> {
    kind: Kind,
}

impl LocalizedTextBuilder<Initial> {
    pub fn create() -> Self {
        Self { kind: Initial }
    }

    pub fn localization_service(
        self,
        reference: String,
        version: u64,
    ) -> LocalizedTextBuilder<LocalizationServiceRef> {
        LocalizedTextBuilder {
            kind: LocalizationServiceRef {
                reference,
                lang: None,
                version,
                width: None,
            },
        }
    }

    pub fn no_service(self, text: impl Into<String>) -> LocalizedTextBuilder<NoRef> {
        LocalizedTextBuilder {
            kind: NoRef {
                lang: None,
                text: text.into(),
                width: None,
            },
        }
    }
}

impl LocalizedTextBuilder<LocalizationServiceRef> {
    pub fn width(mut self, width: localized_text_width_mod::EnumType) -> Self {
        self.kind.width = Some(LocalizedTextWidth { enum_type: width });
        self
    }

    pub fn lang(mut self, lang: impl Into<String>) -> Self {
        self.kind.lang = Some(lang.into());
        self
    }
}

impl LocalizedTextBuilder<NoRef> {
    pub fn width(mut self, width: localized_text_width_mod::EnumType) -> Self {
        self.kind.width = Some(LocalizedTextWidth { enum_type: width });
        self
    }

    pub fn lang(mut self, lang: impl Into<String>) -> Self {
        self.kind.lang = Some(lang.into());
        self
    }
}

impl LocalizedTextBuilder<LocalizationServiceRef> {
    pub fn build(self) -> LocalizedText {
        LocalizedText {
            localized_text_content: Default::default(),
            ref_attr: Some(LocalizedTextRef {
                string: self.kind.reference,
            }),
            lang_attr: self.kind.lang.map(|it| ProtoLanguage { language: it }),
            version_attr: Some(ReferencedVersion {
                version_counter: VersionCounter {
                    unsigned_long: self.kind.version,
                },
            }),
            text_width_attr: None,
        }
    }
}

impl LocalizedTextBuilder<NoRef> {
    pub fn build(self) -> LocalizedText {
        LocalizedText {
            localized_text_content: LocalizedTextContent {
                string: self.kind.text,
            },
            ref_attr: None,
            lang_attr: self.kind.lang.map(|it| ProtoLanguage { language: it }),
            version_attr: None,
            text_width_attr: None,
        }
    }
}
