use std::fmt::Debug;
use std::sync::Arc;

use async_trait::async_trait;
use tokio::sync::{mpsc, Mutex, RwLock};

use protosdc_biceps::biceps::AbstractContextStateOneOf;

use crate::common::access::mdib_access::{MdibAccess, MdibAccessEvent, ObservableMdib};
use crate::common::channel_publisher::ChannelPublisher;
use crate::common::descriptor_child_remover::DescriptorChildRemover;
use crate::common::mdib_description_modification::MdibDescriptionModification;
use crate::common::mdib_description_modifications::MdibDescriptionModifications;
use crate::common::mdib_entity::MdibEntity;
use crate::common::mdib_state_modifications::MdibStateModifications;
use crate::common::mdib_version::MdibVersion;
use crate::common::preprocessing::{
    DescriptionPreprocessor, DummyProcessor, PreprocessingError, StatePreprocessor,
};
use crate::common::storage::mdib_storage::{
    MdibStorage, MdibStorageConstructor, WriteDescriptionResult, WriteError, WriteStateResult,
};
use crate::common::write_util;

// both are not transmitted and cannot be known
const MD_STATE_VERSION: Option<u64> = Some(0);
const MD_DESCRIPTION_VERSION: Option<u64> = Some(0);

const MDIB_ACCESS_EVENT_CHANNEL_SIZE: usize = 1000;

/// Consumer-side read-only access to the mdib.
#[async_trait]
pub trait RemoteMdibAccess: MdibAccess + ObservableMdib {}

/// Consumer-side write access to the mdib that must not be used in an application, as writing
/// changes to this must only occur through reports.
#[async_trait]
pub trait InternalRemoteMdibAccess: RemoteMdibAccess {
    /// Processes the description modifications object, stores the data internally and trigger an event.
    ///
    /// Data is passed through without any checks to the MDIB storage.
    ///
    /// # Arguments
    ///
    /// * `mdib_version`: mdib version of the set of changes
    /// * `description_modifications`: a set of insertions, updates and deletes.
    async fn write_description(
        &mut self,
        mdib_version: MdibVersion,
        description_modifications: MdibDescriptionModifications,
    ) -> Result<WriteDescriptionResult, WriteError>;

    /// Processes the state modifications object, stores the data internally and triggers an event.
    ///
    /// Data is passed through without any checks to the MDIB storage.
    ///
    /// # Arguments
    ///
    /// * `mdib_version`: mdib version of the set of changes
    /// * `state_modifications`: a set of state updates.
    async fn write_states(
        &mut self,
        mdib_version: MdibVersion,
        state_modifications: MdibStateModifications,
    ) -> Result<WriteStateResult, WriteError>;
}

#[derive(Debug)]
pub enum RemoteMdibDescriptionPreprocessor<T: MdibStorage> {
    DescriptorChildRemover(DescriptorChildRemover<T>),
}

impl<T: MdibStorage> DescriptionPreprocessor<T> for RemoteMdibDescriptionPreprocessor<T> {
    fn before_first_modification(
        &mut self,
        mdib_storage: &T,
        modifications: Vec<MdibDescriptionModification>,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError> {
        match self {
            RemoteMdibDescriptionPreprocessor::DescriptorChildRemover(dp) => {
                dp.before_first_modification(mdib_storage, modifications)
            }
        }
    }

    fn after_last_modification(
        &mut self,
        mdib_storage: &T,
        modifications: Vec<MdibDescriptionModification>,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError> {
        match self {
            RemoteMdibDescriptionPreprocessor::DescriptorChildRemover(dp) => {
                dp.after_last_modification(mdib_storage, modifications)
            }
        }
    }

    fn process(
        &mut self,
        mdib_storage: &T,
        modifications: Vec<MdibDescriptionModification>,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError> {
        match self {
            RemoteMdibDescriptionPreprocessor::DescriptorChildRemover(dp) => {
                dp.process(mdib_storage, modifications)
            }
        }
    }
}

#[derive(Debug)]
pub struct RemoteMdibAccessImpl<T, DESC, STATE>
where
    T: MdibStorage,
    DESC: DescriptionPreprocessor<T>,
    STATE: StatePreprocessor<T>,
{
    mdib_access: Arc<RwLock<T>>,
    channel_publisher: Arc<Mutex<ChannelPublisher<MdibAccessEvent>>>,
    description_preprocessors: Arc<Mutex<DESC>>,
    state_preprocessors: Arc<Mutex<STATE>>,
}

// this needs to be implemented manually, as the derived complains about bounds not satisfied
impl<T, DESC, STATE> Clone for RemoteMdibAccessImpl<T, DESC, STATE>
where
    T: MdibStorage,
    DESC: DescriptionPreprocessor<T>,
    STATE: StatePreprocessor<T>,
{
    //noinspection DuplicatedCode
    fn clone(&self) -> Self {
        Self {
            mdib_access: self.mdib_access.clone(),
            channel_publisher: self.channel_publisher.clone(),
            description_preprocessors: self.description_preprocessors.clone(),
            state_preprocessors: self.state_preprocessors.clone(),
        }
    }
}

pub type DefaultRemoteDescriptorPreprocessingSegment<T> = DescriptorChildRemover<T>;
pub type DefaultRemoteStatePreprocessingSegment<T> = DummyProcessor<T>;

impl<T>
    RemoteMdibAccessImpl<
        T,
        DefaultRemoteDescriptorPreprocessingSegment<T>,
        DefaultRemoteStatePreprocessingSegment<T>,
    >
where
    T: MdibStorageConstructor,
{
    pub fn new(
        mdib_version: MdibVersion,
    ) -> RemoteMdibAccessImpl<
        T,
        DefaultRemoteDescriptorPreprocessingSegment<T>,
        DefaultRemoteStatePreprocessingSegment<T>,
    > {
        Self::new_with_context_option(mdib_version, false)
    }

    pub fn new_with_context_option(
        mdib_version: MdibVersion,
        remove_not_associated_context_states: bool,
    ) -> RemoteMdibAccessImpl<
        T,
        DefaultRemoteDescriptorPreprocessingSegment<T>,
        DefaultRemoteStatePreprocessingSegment<T>,
    > {
        Self::new_with_context_option_custom_preprocessors(
            mdib_version,
            remove_not_associated_context_states,
            DescriptorChildRemover::default(),
            DummyProcessor::default(),
        )
    }
}

impl<T, DESC, STATE> RemoteMdibAccessImpl<T, DESC, STATE>
where
    T: MdibStorageConstructor,
    DESC: DescriptionPreprocessor<T>,
    STATE: StatePreprocessor<T>,
{
    pub fn new_custom_preprocessors(
        mdib_version: MdibVersion,
        description_preprocessors: DESC,
        state_preprocessors: STATE,
    ) -> RemoteMdibAccessImpl<T, DESC, STATE> {
        Self::new_with_context_option_custom_preprocessors(
            mdib_version,
            false,
            description_preprocessors,
            state_preprocessors,
        )
    }

    pub fn new_with_context_option_custom_preprocessors(
        mdib_version: MdibVersion,
        remove_not_associated_context_states: bool,
        description_preprocessors: DESC,
        state_preprocessors: STATE,
    ) -> RemoteMdibAccessImpl<T, DESC, STATE> {
        RemoteMdibAccessImpl {
            mdib_access: Arc::new(RwLock::new(T::new_from_mdib_version(
                mdib_version,
                remove_not_associated_context_states,
            ))),
            channel_publisher: Arc::new(Mutex::new(ChannelPublisher::new(
                MDIB_ACCESS_EVENT_CHANNEL_SIZE,
            ))),
            description_preprocessors: Arc::new(Mutex::new(description_preprocessors)),
            state_preprocessors: Arc::new(Mutex::new(state_preprocessors)),
        }
    }
}

#[async_trait]
impl<T, DESC, STATE> MdibAccess for RemoteMdibAccessImpl<T, DESC, STATE>
where
    T: MdibStorage + Send + Sync,
    DESC: DescriptionPreprocessor<T> + Send + Sync,
    STATE: StatePreprocessor<T> + Send + Sync,
{
    async fn mdib_version(&self) -> MdibVersion {
        let acc = self.mdib_access.read().await;
        acc.mdib_version()
    }

    async fn md_description_version(&self) -> u64 {
        let acc = self.mdib_access.read().await;
        acc.md_description_version()
    }

    async fn md_state_version(&self) -> u64 {
        let acc = self.mdib_access.read().await;
        acc.md_state_version()
    }

    async fn entity(&self, handle: &str) -> Option<MdibEntity> {
        let acc = self.mdib_access.read().await;
        acc.entity(handle)
    }

    async fn root_entities(&self) -> Vec<MdibEntity> {
        let acc = self.mdib_access.read().await;
        acc.root_entities()
    }

    async fn context_state(&self, handle: &str) -> Option<AbstractContextStateOneOf> {
        let acc = self.mdib_access.read().await;
        acc.context_state(handle)
    }

    async fn context_states(&self) -> Vec<AbstractContextStateOneOf> {
        let acc = self.mdib_access.read().await;
        acc.context_states()
    }

    async fn entities_by_type<F>(&self, filter: F) -> Vec<MdibEntity>
    where
        F: 'static + Fn(&MdibEntity) -> bool + Send + Sync,
    {
        let acc = self.mdib_access.read().await;
        acc.entities_by_type(filter)
    }

    async fn children_by_type<F>(&self, handle: &str, filter: F) -> Vec<MdibEntity>
    where
        F: 'static + Fn(&MdibEntity) -> bool + Send + Sync,
    {
        let acc = self.mdib_access.read().await;
        acc.children_by_type(handle, filter)
    }
}

#[async_trait]
impl<T, DESC, STATE> ObservableMdib for RemoteMdibAccessImpl<T, DESC, STATE>
where
    T: MdibStorage + Send + Sync + Debug,
    DESC: DescriptionPreprocessor<T> + Send + Sync,
    STATE: StatePreprocessor<T> + Send + Sync,
{
    //noinspection DuplicatedCode
    async fn subscribe(&self) -> mpsc::Receiver<MdibAccessEvent> {
        self.channel_publisher.lock().await.subscribe().await
    }

    async fn unsubscribe_all(&self) {
        self.channel_publisher.lock().await.unsubscribe_all().await
    }
}

#[async_trait]
impl<T, DESC, STATE> InternalRemoteMdibAccess for RemoteMdibAccessImpl<T, DESC, STATE>
where
    T: MdibStorage + Send + Sync + Debug,
    DESC: DescriptionPreprocessor<T> + Send + Sync,
    STATE: StatePreprocessor<T> + Send + Sync,
{
    async fn write_description(
        &mut self,
        mdib_version: MdibVersion,
        description_modifications: MdibDescriptionModifications,
    ) -> Result<WriteDescriptionResult, WriteError> {
        // lock early
        let mut access = self.mdib_access.write().await;

        write_util::write_description(
            description_modifications,
            mdib_version,
            move |storage, desc, version| {
                storage.apply_description(version, MD_DESCRIPTION_VERSION, MD_STATE_VERSION, desc)
            },
            &*self.channel_publisher.lock().await,
            &mut *self.description_preprocessors.lock().await,
            &mut *access,
        )
        .await
    }

    async fn write_states(
        &mut self,
        mdib_version: MdibVersion,
        state_modifications: MdibStateModifications,
    ) -> Result<WriteStateResult, WriteError> {
        // lock early
        let mut access = self.mdib_access.write().await;

        write_util::write_states(
            state_modifications,
            mdib_version,
            move |storage, state, version| storage.apply_state(version, MD_STATE_VERSION, state),
            &*self.channel_publisher.lock().await,
            &mut *self.state_preprocessors.lock().await,
            &mut access,
        )
        .await
    }
}

#[async_trait]
impl<T, DESC, STATE> RemoteMdibAccess for RemoteMdibAccessImpl<T, DESC, STATE>
where
    T: MdibStorage + Send + Sync + Debug,
    DESC: DescriptionPreprocessor<T> + Send + Sync,
    STATE: StatePreprocessor<T> + Send + Sync,
{
}

//noinspection DuplicatedCode
#[cfg(test)]
mod test {
    use std::time::Duration;

    use env_logger;
    use log::info;
    use tokio_stream::wrappers::ReceiverStream;
    use tokio_stream::StreamExt;

    use protosdc_biceps::biceps::{
        context_association_mod, AbstractContextState, AbstractContextStateOneOf,
        AbstractDeviceComponentStateOneOf, AbstractMetricStateOneOf, ContextAssociation,
        MdsDescriptor, MdsState, NumericMetricState, PatientContextState,
    };
    use protosdc_biceps::types::ProtoLanguage;

    use crate::common::access::mdib_access::{MdibAccess, MdibAccessEvent, ObservableMdib};
    use crate::common::biceps_util::{ContextState, Descriptor, State};
    use crate::common::mdib_description_modification::MdibDescriptionModification;
    use crate::common::mdib_description_modifications::MdibDescriptionModifications;
    use crate::common::mdib_entity::{Entity, EntityBase, MdibEntity};
    use crate::common::mdib_pair::Pair;
    use crate::common::mdib_state_modifications::MdibStateModifications;
    use crate::common::mdib_version::MdibVersionBuilder;
    use crate::common::storage::mdib_storage::{MdibStorageImpl, WriteError};
    use crate::consumer::access::remote_mdib_access::{
        DefaultRemoteDescriptorPreprocessingSegment, DefaultRemoteStatePreprocessingSegment,
        InternalRemoteMdibAccess, RemoteMdibAccessImpl,
    };
    use crate::test_util::mdib::mdib_modification_presets;
    use crate::test_util::{handles, mdib_utils};

    fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[tokio::test]
    async fn test_initial_description_insert(
    ) -> Result<(), tokio::sync::broadcast::error::RecvError> {
        init();
        let mdib_version = MdibVersionBuilder::default().seq("wat:abc").create();

        let mut mdib_access =
            RemoteMdibAccessImpl::<MdibStorageImpl, _, _>::new(mdib_version.clone());
        let mut receiver = mdib_access.subscribe().await;

        let modifications = mdib_modification_presets();
        let event_fut = receiver.recv();

        mdib_access
            .write_description(mdib_version.inc(), modifications)
            .await
            .expect("Description write failed");
        mdib_access.unsubscribe_all().await;

        let event = event_fut.await.unwrap();

        match event {
            MdibAccessEvent::DescriptionModification {
                inserted,
                updated,
                deleted,
                ..
            } => {
                assert!(!inserted.is_empty());
                assert!(updated.is_empty());
                assert!(deleted.is_empty());
            }
            _ => panic!("Unexpected event type"),
        };

        if receiver.recv().await.is_some() {
            panic!("Should not be Some")
        }

        Ok(())
    }

    #[tokio::test]
    async fn test_description_update_of_existing_entities() {
        init();

        let mut mdib_access = base_remote_mdib(false).await;

        let mut receiver = mdib_access.subscribe().await;
        let event_fut = receiver.recv();

        let mut modifications = MdibDescriptionModifications::default();

        modifications
            .add(MdibDescriptionModification::Update {
                pair: Pair::from((
                    mdib_utils::create_minimal_mds_descriptor(handles::MDS_0),
                    mdib_utils::create_minimal_mds_state(handles::MDS_0),
                )),
            })
            .unwrap_or_else(|_| panic!("Could not update {}", handles::MDS_0));

        modifications
            .add(MdibDescriptionModification::Update {
                pair: Pair::from((
                    mdib_utils::create_minimal_numeric_metric_descriptor(handles::METRIC_0),
                    mdib_utils::create_minimal_numeric_metric_state(handles::METRIC_0),
                )),
            })
            .unwrap_or_else(|_| panic!("Could not update {}", handles::METRIC_0));

        modifications
            .add(MdibDescriptionModification::Update {
                pair: Pair::from((
                    mdib_utils::create_minimal_patient_context_descriptor(
                        handles::CONTEXT_DESCRIPTOR_0,
                    ),
                    vec![
                        mdib_utils::create_minimal_patient_context_state(
                            handles::CONTEXT_DESCRIPTOR_0,
                            handles::CONTEXT_0,
                        ),
                        mdib_utils::create_minimal_patient_context_state(
                            handles::CONTEXT_DESCRIPTOR_0,
                            handles::CONTEXT_1,
                        ),
                    ],
                )),
            })
            .unwrap_or_else(|_| panic!("Could not update {}", handles::CONTEXT_DESCRIPTOR_0));

        mdib_access
            .write_description(mdib_access.mdib_version().await.inc(), modifications)
            .await
            .expect("Could not write description");

        let event = event_fut.await;

        let mdib_access_event = event.unwrap();
        assert!(matches!(
            mdib_access_event,
            MdibAccessEvent::DescriptionModification { .. }
        ));

        if let MdibAccessEvent::DescriptionModification {
            inserted,
            updated,
            deleted,
            ..
        } = mdib_access_event
        {
            assert_eq!(0, inserted.len());
            assert_eq!(3, updated.len());
            assert_eq!(0, deleted.len());
        }
    }

    #[tokio::test]
    async fn test_description_delete() {
        init();

        let mut mdib_access = base_remote_mdib(false).await;

        let mut receiver = mdib_access.subscribe().await;
        let event_fut = receiver.recv();

        let mut modifications = MdibDescriptionModifications::default();

        modifications
            .add(MdibDescriptionModification::Delete {
                handle: handles::CONTEXT_DESCRIPTOR_0.to_string(),
            })
            .expect("Could not add deletion");

        mdib_access
            .write_description(mdib_access.mdib_version().await.inc(), modifications)
            .await
            .expect("Could not write description");

        let event = event_fut.await;

        let mdib_access_event = event.unwrap();
        assert!(matches!(
            mdib_access_event,
            MdibAccessEvent::DescriptionModification { .. }
        ));

        if let MdibAccessEvent::DescriptionModification {
            inserted,
            updated,
            deleted,
            ..
        } = mdib_access_event
        {
            assert_eq!(0, inserted.len());
            assert_eq!(0, updated.len());
            assert_eq!(1, deleted.len());

            assert_eq!(
                handles::CONTEXT_DESCRIPTOR_0.to_string(),
                deleted.first().unwrap().entity.handle()
            );
        }

        assert!(mdib_access
            .entity(handles::CONTEXT_DESCRIPTOR_0)
            .await
            .is_none())
    }

    #[tokio::test]
    async fn test_existing_state_update() -> Result<(), tokio::sync::broadcast::error::RecvError> {
        init();

        let mut mdib_access = base_remote_mdib(false).await;

        let receiver = mdib_access.subscribe().await;

        // collect all events in a task, because why not?
        let events_future = tokio::spawn(async {
            let mut ev = vec![];
            let mut strm = ReceiverStream::new(receiver);
            while let Some(item) = strm.next().await {
                ev.push(item)
            }
            ev
        });

        let expected_mds0_lang = "de".to_string();
        let expected_mds1_lang = "en-DE".to_string();
        let expected_avg_period = Duration::from_secs(3600);
        let expected_association = Some(ContextAssociation {
            enum_type: context_association_mod::EnumType::Dis,
        });

        let new_mds0 = AbstractDeviceComponentStateOneOf::MdsState(MdsState {
            lang_attr: Some(ProtoLanguage {
                language: expected_mds0_lang.clone(),
            }),
            ..mdib_utils::create_minimal_mds_state(handles::MDS_0)
        });

        let new_mds1 = AbstractDeviceComponentStateOneOf::MdsState(MdsState {
            lang_attr: Some(ProtoLanguage {
                language: expected_mds1_lang.clone(),
            }),
            ..mdib_utils::create_minimal_mds_state(handles::MDS_1)
        });

        let new_metric = AbstractMetricStateOneOf::NumericMetricState(NumericMetricState {
            active_averaging_period_attr: Some(expected_avg_period.into()),
            ..mdib_utils::create_minimal_numeric_metric_state(handles::METRIC_0)
        });

        let new_context = AbstractContextStateOneOf::PatientContextState(PatientContextState {
            abstract_context_state: AbstractContextState {
                context_association_attr: expected_association.clone(),
                ..mdib_utils::create_minimal_patient_context_state(
                    handles::CONTEXT_DESCRIPTOR_0,
                    handles::CONTEXT_1,
                )
                .abstract_context_state
            },
            ..mdib_utils::create_minimal_patient_context_state(
                handles::CONTEXT_DESCRIPTOR_0,
                handles::CONTEXT_1,
            )
        });

        mdib_access
            .write_states(
                mdib_access.mdib_version().await.inc(),
                MdibStateModifications::Component {
                    component_states: vec![new_mds0, new_mds1],
                },
            )
            .await
            .expect("write failed");

        mdib_access
            .write_states(
                mdib_access.mdib_version().await.inc(),
                MdibStateModifications::Metric {
                    metric_states: vec![new_metric],
                },
            )
            .await
            .expect("write failed");

        mdib_access
            .write_states(
                mdib_access.mdib_version().await.inc(),
                MdibStateModifications::Context {
                    context_states: vec![new_context],
                },
            )
            .await
            .expect("write failed");

        mdib_access.unsubscribe_all().await;

        let events = events_future.await.expect("Could not get events");

        assert_eq!(3, events.len());

        assert!(matches!(
            events.first().unwrap(),
            MdibAccessEvent::ComponentStateModification { .. }
        ));
        assert!(matches!(
            events.get(1).unwrap(),
            MdibAccessEvent::MetricStateModification { .. }
        ));
        assert!(matches!(
            events.get(2).unwrap(),
            MdibAccessEvent::ContextStateModification { .. }
        ));

        // verify mds
        match events.first().unwrap() {
            MdibAccessEvent::ComponentStateModification { states, .. } => {
                assert_eq!(2, states.len());
                for (source_mds, mds_states) in states {
                    // one mds "per mds"
                    assert_eq!(1, mds_states.len());
                    for state in mds_states {
                        match state {
                            AbstractDeviceComponentStateOneOf::MdsState(mds) => {
                                if mds.descriptor_handle() == handles::MDS_0 {
                                    assert_eq!(
                                        Some(ProtoLanguage {
                                            language: expected_mds0_lang.clone()
                                        }),
                                        mds.lang_attr
                                    );
                                    assert_eq!(handles::MDS_0, source_mds);
                                } else {
                                    assert_eq!(
                                        Some(ProtoLanguage {
                                            language: expected_mds1_lang.clone()
                                        }),
                                        mds.lang_attr
                                    );
                                    assert_eq!(handles::MDS_1, source_mds);
                                }
                            }
                            _ => panic!("Unexpected state type"),
                        }
                    }
                }
            }
            _ => panic!("Unexpected modification type"),
        };

        let mds_entity0 = mdib_access
            .entity(handles::MDS_0)
            .await
            .unwrap_or_else(|| panic!("Missing entity {}", handles::MDS_0));

        let mds_entity1 = mdib_access
            .entity(handles::MDS_1)
            .await
            .unwrap_or_else(|| panic!("Missing entity {}", handles::MDS_1));

        if let Entity::Mds(mds) = mds_entity0.entity {
            assert_eq!(
                Some(ProtoLanguage {
                    language: expected_mds0_lang.clone()
                }),
                mds.pair.state.lang_attr
            )
        } else {
            panic!("Mds missing")
        };

        if let Entity::Mds(mds) = mds_entity1.entity {
            assert_eq!(
                Some(ProtoLanguage {
                    language: expected_mds1_lang.clone()
                }),
                mds.pair.state.lang_attr
            )
        } else {
            panic!("Mds missing")
        };

        match events.get(1).unwrap() {
            MdibAccessEvent::MetricStateModification { states, .. } => {
                assert_eq!(1, states.len());
                for (source_mds, mds_states) in states {
                    assert_eq!(1, mds_states.len());
                    assert_eq!(handles::MDS_0, source_mds);
                    for state in mds_states {
                        match state {
                            AbstractMetricStateOneOf::NumericMetricState(num) => {
                                assert_eq!(
                                    Some(expected_avg_period.into()),
                                    num.active_averaging_period_attr
                                );
                            }
                            _ => panic!("Unexpected state type"),
                        }
                    }
                }
            }
            _ => panic!("Unexpected modification type"),
        }

        let metric_entity0 = mdib_access
            .entity(handles::METRIC_0)
            .await
            .unwrap_or_else(|| panic!("Missing entity {}", handles::METRIC_0));

        if let Entity::NumericMetric(metric) = metric_entity0.entity {
            assert_eq!(
                Some(expected_avg_period.into()),
                metric.pair.state.active_averaging_period_attr
            )
        } else {
            panic!("NumericMetric missing")
        };

        match events.get(2).unwrap() {
            MdibAccessEvent::ContextStateModification {
                updated_context_states,
                removed_context_states,
                ..
            } => {
                assert_eq!(1, updated_context_states.len());
                assert_eq!(0, removed_context_states.len());
                for (source_mds, mds_states) in updated_context_states {
                    assert_eq!(1, mds_states.len());
                    assert_eq!(handles::MDS_0, source_mds);
                    for state in mds_states {
                        match state {
                            AbstractContextStateOneOf::PatientContextState(ctx) => {
                                assert_eq!(
                                    expected_association,
                                    ctx.abstract_context_state.context_association_attr
                                );
                            }
                            _ => panic!("Unexpected state type"),
                        }
                    }
                }
            }
            _ => panic!("Unexpected modification type"),
        }

        let ctx_state = mdib_access
            .context_state(handles::CONTEXT_1)
            .await
            .unwrap_or_else(|| panic!("Missing context {}", handles::CONTEXT_1));

        if let AbstractContextStateOneOf::PatientContextState(metric) = ctx_state {
            assert_eq!(
                expected_association,
                metric.abstract_context_state.context_association_attr
            )
        } else {
            panic!("PatientContextState missing")
        };

        Ok(())
    }

    #[tokio::test]
    async fn test_reinsert_entities() {
        init();

        let mut mdib_access = base_remote_mdib(false).await;

        {
            let mut modifications = MdibDescriptionModifications::default();
            modifications
                .add(MdibDescriptionModification::Delete {
                    handle: handles::METRIC_0.to_string(),
                })
                .expect("Could not add deletion");

            modifications
                .add(MdibDescriptionModification::Delete {
                    handle: handles::CONTEXT_DESCRIPTOR_0.to_string(),
                })
                .expect("Could not add deletion");

            mdib_access
                .write_description(mdib_access.mdib_version().await.inc(), modifications)
                .await
                .expect("Could not write description");
        }

        let mut subscription = mdib_access.subscribe().await;

        let mut modifications = MdibDescriptionModifications::default();
        modifications
            .add(MdibDescriptionModification::Insert {
                pair: Pair::from((
                    mdib_utils::create_minimal_numeric_metric_descriptor(handles::METRIC_0),
                    mdib_utils::create_minimal_numeric_metric_state(handles::METRIC_0),
                )),
                parent: Some(handles::CHANNEL_0.to_string()),
            })
            .unwrap_or_else(|_| panic!("Could not add metric {}", handles::METRIC_0));

        // patient context data
        let pat2 = {
            let stuff = mdib_utils::create_minimal_patient_context_state(
                handles::CONTEXT_DESCRIPTOR_0,
                handles::CONTEXT_1,
            );

            PatientContextState {
                abstract_context_state: AbstractContextState {
                    context_association_attr: Some(ContextAssociation {
                        enum_type: context_association_mod::EnumType::No,
                    }),
                    ..stuff.abstract_context_state
                },
                ..stuff
            }
        };

        modifications
            .add(MdibDescriptionModification::Insert {
                pair: Pair::from((
                    mdib_utils::create_minimal_patient_context_descriptor(
                        handles::CONTEXT_DESCRIPTOR_0,
                    ),
                    pat2,
                )),
                parent: Some(handles::SYSTEM_CONTEXT_0.to_string()),
            })
            .unwrap_or_else(|_| {
                panic!(
                    "Could not add patient context {}",
                    handles::CONTEXT_DESCRIPTOR_0
                )
            });

        let write_result = mdib_access
            .write_description(mdib_access.mdib_version().await.inc(), modifications)
            .await
            .expect("Could not write description");

        mdib_access.unsubscribe_all().await;

        let event0 = subscription.recv().await.expect("Event0 missing");

        let check_metric_entity = |entity: MdibEntity| {
            assert!(matches!(entity.entity, Entity::NumericMetric { .. }));
            if let Entity::NumericMetric(metric) = entity.entity {
                assert_eq!(0, metric.pair.descriptor.version());
                assert_eq!(0, metric.pair.state.descriptor_version());
                assert_eq!(0, metric.pair.state.version());
            }
        };

        // check metric
        check_metric_entity(write_result.inserted.first().unwrap().clone());

        if let MdibAccessEvent::DescriptionModification {
            inserted,
            updated,
            deleted,
            ..
        } = event0.clone()
        {
            check_metric_entity(inserted.first().unwrap().clone());
            assert_eq!(0, updated.len());
            assert_eq!(0, deleted.len())
        }

        check_metric_entity(mdib_access.entity(handles::METRIC_0).await.unwrap());

        let check_context_entity = |entity: MdibEntity| {
            assert!(matches!(entity.entity, Entity::PatientContext { .. }));
            if let Entity::PatientContext(metric) = entity.entity {
                assert_eq!(0, metric.pair.descriptor.version());
                assert_eq!(1, metric.pair.states.len());
                assert_eq!(0, metric.pair.states.first().unwrap().descriptor_version());
                assert_eq!(0, metric.pair.states.first().unwrap().version());
            }
        };

        // check context
        check_context_entity(write_result.inserted.get(1).unwrap().clone());

        if let MdibAccessEvent::DescriptionModification {
            inserted,
            updated,
            deleted,
            ..
        } = event0
        {
            check_context_entity(inserted.get(1).unwrap().clone());
            assert_eq!(0, updated.len());
            assert_eq!(0, deleted.len())
        }

        check_context_entity(
            mdib_access
                .entity(handles::CONTEXT_DESCRIPTOR_0)
                .await
                .unwrap(),
        );
    }

    #[tokio::test]
    async fn test_update_only_subset_of_context_entity() {
        init();

        let mut mdib_access = base_remote_mdib(false).await;
        let mut receiver = mdib_access.subscribe().await;

        let write_result = mdib_access
            .write_states(
                mdib_access.mdib_version().await.inc(),
                MdibStateModifications::Context {
                    context_states: vec![mdib_utils::create_minimal_patient_context_state(
                        handles::CONTEXT_DESCRIPTOR_0,
                        handles::CONTEXT_1,
                    )
                    .into_abstract_context_state_one_of()],
                },
            )
            .await;

        assert!(write_result.is_ok());

        mdib_access.unsubscribe_all().await;

        let event = receiver.recv().await.expect("Subscription failed");

        let expected_state = mdib_utils::create_minimal_patient_context_state(
            handles::CONTEXT_DESCRIPTOR_0,
            handles::CONTEXT_1,
        )
        .into_abstract_context_state_one_of();

        match event {
            MdibAccessEvent::ContextStateModification {
                updated_context_states,
                removed_context_states,
                ..
            } => {
                assert_eq!(0, removed_context_states.len());
                assert_eq!(
                    &expected_state,
                    updated_context_states
                        .get(handles::MDS_0)
                        .expect("no mds1")
                        .first()
                        .expect("No context state update")
                )
            }
            _ => panic!("Wrong kind of modification"),
        }
    }

    #[tokio::test]
    async fn test_deletion_and_reinsertion_of_context_state() {
        init();

        let mut mdib_access = base_remote_mdib(true).await;
        let mut receiver = mdib_access.subscribe().await;

        let no_assoc = Some(ContextAssociation {
            enum_type: context_association_mod::EnumType::No,
        });
        let assoc = Some(ContextAssociation {
            enum_type: context_association_mod::EnumType::Assoc,
        });

        assert!(mdib_access
            .context_state(handles::CONTEXT_1)
            .await
            .is_none());

        // delete context
        let no_assoc_context =
            AbstractContextStateOneOf::PatientContextState(PatientContextState {
                abstract_context_state: AbstractContextState {
                    context_association_attr: no_assoc.clone(),
                    ..mdib_utils::create_minimal_patient_context_state(
                        handles::CONTEXT_DESCRIPTOR_0,
                        handles::CONTEXT_1,
                    )
                    .abstract_context_state
                },
                ..mdib_utils::create_minimal_patient_context_state(
                    handles::CONTEXT_DESCRIPTOR_0,
                    handles::CONTEXT_1,
                )
            });

        mdib_access
            .write_states(
                mdib_access.mdib_version().await.inc(),
                MdibStateModifications::Context {
                    context_states: vec![no_assoc_context],
                },
            )
            .await
            .expect("write failed");

        let event = receiver.recv().await.expect("Event broken");
        assert!(matches!(
            event,
            MdibAccessEvent::ContextStateModification { .. }
        ));

        assert!(mdib_access
            .context_state(handles::CONTEXT_1)
            .await
            .is_none());

        // insert context
        let new_context = AbstractContextStateOneOf::PatientContextState(PatientContextState {
            abstract_context_state: AbstractContextState {
                context_association_attr: assoc.clone(),
                ..mdib_utils::create_minimal_patient_context_state(
                    handles::CONTEXT_DESCRIPTOR_0,
                    handles::CONTEXT_1,
                )
                .abstract_context_state
            },
            ..mdib_utils::create_minimal_patient_context_state(
                handles::CONTEXT_DESCRIPTOR_0,
                handles::CONTEXT_1,
            )
        });

        mdib_access
            .write_states(
                mdib_access.mdib_version().await.inc(),
                MdibStateModifications::Context {
                    context_states: vec![new_context],
                },
            )
            .await
            .expect("write failed");

        let event = receiver.recv().await.expect("Event broken");
        assert!(matches!(
            event,
            MdibAccessEvent::ContextStateModification { .. }
        ));

        assert!(mdib_access
            .context_state(handles::CONTEXT_1)
            .await
            .is_some());
    }

    // #[tokio::test]
    // async fn test_fail_on_inserting_existing_handle() {
    //
    //
    // }

    #[tokio::test]
    async fn test_fail_on_update_unknown_entity() {
        init();

        let mut mdib_access = base_remote_mdib(false).await;
        let mut receiver = mdib_access.subscribe().await;

        let mut modifications = MdibDescriptionModifications::default();
        modifications
            .add(MdibDescriptionModification::Update {
                pair: Pair::from((
                    mdib_utils::create_minimal_numeric_metric_descriptor(handles::METRIC_3),
                    mdib_utils::create_minimal_numeric_metric_state(handles::METRIC_3),
                )),
            })
            .unwrap_or_else(|_| panic!("Could not add metric {}", handles::METRIC_3));

        let write_result = mdib_access
            .write_description(mdib_access.mdib_version().await.inc(), modifications)
            .await;

        match write_result {
            Ok(_) => {
                panic!("Not ok!")
            }
            Err(err) => {
                assert_eq!(
                    WriteError::EntityMissing {
                        handle: handles::METRIC_3.to_string()
                    },
                    err
                )
            }
        }

        mdib_access.unsubscribe_all().await;

        if receiver.recv().await.is_some() {
            panic!("Should not be Some")
        }
    }

    #[tokio::test]
    async fn test_fail_on_wrong_insertion_order() {
        let mdib_version = MdibVersionBuilder::default().seq("wat:abc").create();

        let mut mdib_access =
            RemoteMdibAccessImpl::<MdibStorageImpl, _, _>::new_with_context_option(
                mdib_version.clone(),
                false,
            );
        let mut receiver = mdib_access.subscribe().await;

        let mut modifications = MdibDescriptionModifications::default();

        modifications
            .add(MdibDescriptionModification::Insert {
                pair: Pair::from((
                    mdib_utils::create_minimal_vmd_descriptor(handles::VMD_0),
                    mdib_utils::create_minimal_vmd_state(handles::VMD_0),
                )),
                parent: Some(handles::MDS_0.to_string()),
            })
            .unwrap_or_else(|_| panic!("Could not add vmd {}", handles::VMD_0));

        modifications
            .add(MdibDescriptionModification::Insert {
                pair: Pair::from((
                    mdib_utils::create_minimal_mds_descriptor(handles::MDS_0),
                    mdib_utils::create_minimal_mds_state(handles::MDS_0),
                )),
                parent: None,
            })
            .unwrap_or_else(|_| panic!("Could not add mds {}", handles::MDS_0));

        let write_result = mdib_access
            .write_description(mdib_access.mdib_version().await.inc(), modifications)
            .await;

        match write_result {
            Ok(_) => {
                panic!("Not ok!")
            }
            Err(err) => {
                assert_eq!(
                    WriteError::ParentEntityMissing {
                        parent: handles::MDS_0.to_string(),
                        handle: handles::VMD_0.to_string()
                    },
                    err
                )
            }
        }

        mdib_access.unsubscribe_all().await;

        if receiver.recv().await.is_some() {
            panic!("Should not be Some")
        }
    }

    #[tokio::test]
    async fn test_descriptor_child_remover() {
        let mdib_version = MdibVersionBuilder::default().seq("wat:abc").create();

        let mut mdib_access =
            RemoteMdibAccessImpl::<MdibStorageImpl, _, _>::new_with_context_option(
                mdib_version.clone(),
                false,
            );

        let mut modifications = MdibDescriptionModifications::default();
        let vmd_desc = mdib_utils::create_minimal_vmd_descriptor(handles::VMD_0);
        modifications
            .add(MdibDescriptionModification::Insert {
                pair: Pair::from((
                    MdsDescriptor {
                        vmd: vec![vmd_desc],
                        ..mdib_utils::create_minimal_mds_descriptor(handles::MDS_0)
                    },
                    mdib_utils::create_minimal_mds_state(handles::MDS_0),
                )),
                parent: None,
            })
            .unwrap_or_else(|_| panic!("Could not add mds {}", handles::MDS_0));

        mdib_access
            .write_description(mdib_access.mdib_version().await.inc(), modifications)
            .await
            .expect("Write failed for no reason");

        match mdib_access
            .entity(handles::MDS_0)
            .await
            .expect("Mds missing")
            .entity
        {
            Entity::Mds(mds) => {
                assert!(mds.pair.descriptor.vmd.is_empty())
            }
            _ => panic!("Wrong entity type"),
        }
    }

    async fn base_remote_mdib(
        remove_not_associated_context_states: bool,
    ) -> RemoteMdibAccessImpl<
        MdibStorageImpl,
        DefaultRemoteDescriptorPreprocessingSegment<MdibStorageImpl>,
        DefaultRemoteStatePreprocessingSegment<MdibStorageImpl>,
    > {
        info!("Creating base remote mdib");
        let mdib_version = MdibVersionBuilder::default().seq("urn:abc").create();

        let mut mdib_access = RemoteMdibAccessImpl::new_with_context_option(
            mdib_version.clone(),
            remove_not_associated_context_states,
        );

        let modifications = mdib_modification_presets();
        mdib_access
            .write_description(mdib_version.inc(), modifications)
            .await
            .expect("Description write failed");

        mdib_access
    }
}
