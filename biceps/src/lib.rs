extern crate core;

pub mod common {
    pub mod biceps_util;
    pub mod builders;
    pub mod channel_publisher;
    pub mod constants;
    pub mod mdib_entity;
    pub mod mdib_entity_util;
    pub mod mdib_pair;
    pub mod mdib_version;

    pub mod storage {
        #[macro_use]
        pub mod mdib_storage;
    }

    pub mod access {
        pub mod mdib_access;
    }

    pub mod context {
        pub mod context_identification_mapper;
        pub mod fallback_instance_identifier;
        pub mod location_detail_query_mapper;
        pub mod uri;
    }

    pub mod preprocessing;

    pub mod event_distributor;
    pub mod write_util;

    pub mod mdib_description_modification;
    pub mod mdib_description_modifications;
    pub mod mdib_state_modifications;

    pub mod mdib_tree_validator;

    pub mod descriptor_child_remover;
}

pub mod consumer {
    pub mod access {
        pub mod remote_mdib_access;
    }
}

pub mod provider {
    pub mod access {
        pub mod local_mdib_access;
    }
    mod preprocessing {
        pub(crate) mod cardinality_checker;
        pub(crate) mod duplicate_context_state_handle_handler;
        pub(crate) mod duplicate_descriptor_handler;
        pub(crate) mod type_change_checker;
        pub(crate) mod type_consistency_checker;
        pub(crate) mod version_handler;
    }
}

pub mod utilities {
    pub mod localized_text;
    pub mod modifications;
}

#[cfg(any(feature = "test-utilities", test))]
pub mod test_util {
    pub mod handles;
    pub mod mdib;
    pub mod mdib_utils;
}
