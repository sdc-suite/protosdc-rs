use log::error;
use thiserror::Error;

use crate::common::mdib_description_modification::MdibDescriptionModification;

#[derive(PartialEq, Eq, Debug, Error)]
pub enum ModificationError {
    #[error("Duplicate handles were detected: {handles:?}")]
    DuplicateHandle { handles: Vec<String> },
}

/// Container for a set of description modifications.
#[derive(Default, Debug)]
pub struct MdibDescriptionModifications {
    modifications: Vec<MdibDescriptionModification>,
    handles: Vec<String>,
}

impl MdibDescriptionModifications {
    pub fn into_vec(self) -> Vec<MdibDescriptionModification> {
        self.modifications
    }

    /// Adds a new description modification to the set of modifications.
    ///
    /// Returns a [ModificationError] in case a duplicate is detected in the set, e.g.
    /// deleting and inserting the same descriptor in one set.
    ///
    /// # Arguments
    ///
    /// * `modification`:
    ///
    /// returns: Result<(), ModificationError>
    pub fn add(
        &mut self,
        modification: MdibDescriptionModification,
    ) -> Result<(), ModificationError> {
        match self.duplicate_detection(&modification) {
            Ok(_) => {
                self.push_modification(modification);
                Ok(())
            }
            Err(e) => {
                error!("{}", e);
                Err(e)
            }
        }
    }

    pub fn add_all(
        &mut self,
        modifications: Vec<MdibDescriptionModification>,
    ) -> Result<(), ModificationError> {
        // check for input data collisions
        let handles: Vec<String> = modifications
            .iter()
            .map(|it| it.descriptor_handle())
            .collect();
        let mut unique_handles = handles.clone();
        unique_handles.sort();
        unique_handles.dedup();

        if handles.len() != unique_handles.len() {
            // duplicates
            let dups = handles
                .iter()
                .filter(|handle| unique_handles.contains(handle))
                .cloned()
                .collect();
            return Err(ModificationError::DuplicateHandle { handles: dups });
        }

        // check for present data collisions
        let collisions: Vec<String> = modifications
            .iter()
            .map(|it| self.duplicate_detection(it))
            .filter_map(|it| match it {
                Ok(_) => None,
                Err(e) => Some(match e {
                    ModificationError::DuplicateHandle { handles } => handles,
                }),
            })
            .fold(vec![], |mut x, y| {
                x.extend(y);
                x
            });

        if !collisions.is_empty() {
            return Err(ModificationError::DuplicateHandle {
                handles: collisions,
            });
        }

        for x in modifications {
            self.push_modification(x)
        }

        Ok(())
    }

    fn push_modification(&mut self, modification: MdibDescriptionModification) {
        self.handles.push(modification.descriptor_handle());
        self.modifications.push(modification)
    }

    fn duplicate_detection(
        &mut self,
        modification: &MdibDescriptionModification,
    ) -> Result<(), ModificationError> {
        let handle = modification.descriptor_handle();

        match self.handles.contains(&handle) {
            true => Err(ModificationError::DuplicateHandle {
                handles: vec![handle.to_string()],
            }),
            false => Ok(()),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::common::mdib_description_modification::MdibDescriptionModification;
    use crate::common::mdib_description_modifications::{
        MdibDescriptionModifications, ModificationError,
    };
    use crate::common::mdib_pair::Pair;
    use crate::test_util::mdib_utils;

    #[test]
    fn test_no_error() -> anyhow::Result<()> {
        let mut modification = MdibDescriptionModifications::default();

        let test_handle = "TestSchmest";
        let test_handle2 = "TestSchmest2";

        let del = MdibDescriptionModification::Delete {
            handle: test_handle.to_string(),
        };
        let del2 = MdibDescriptionModification::Delete {
            handle: test_handle2.to_string(),
        };
        modification.add_all(vec![del, del2])?;
        Ok(())
    }

    #[test]
    fn test_duplicate_detection_single() {
        let mut modification = MdibDescriptionModifications::default();

        let test_handle = "TestSchmest";

        {
            let del = MdibDescriptionModification::Delete {
                handle: test_handle.to_string(),
            };
            assert_eq!(Ok(()), modification.add(del));
        }
        {
            let del = MdibDescriptionModification::Delete {
                handle: test_handle.to_string(),
            };
            assert_eq!(
                Err(ModificationError::DuplicateHandle {
                    handles: vec![test_handle.to_string()]
                }),
                modification.add(del)
            );
        }
        {
            let del = MdibDescriptionModification::Delete {
                handle: test_handle.to_string(),
            };
            assert_eq!(
                Err(ModificationError::DuplicateHandle {
                    handles: vec![test_handle.to_string()]
                }),
                modification.add_all(vec![del])
            );
        }
        {
            let ins = MdibDescriptionModification::Insert {
                pair: Pair::from((
                    mdib_utils::create_minimal_numeric_metric_descriptor(test_handle),
                    mdib_utils::create_minimal_numeric_metric_state(test_handle),
                )),
                parent: None,
            };
            assert_eq!(
                Err(ModificationError::DuplicateHandle {
                    handles: vec![test_handle.to_string()]
                }),
                modification.add(ins)
            );
        }
    }

    #[test]
    fn test_duplicate_detection_multiple() {
        let mut modification = MdibDescriptionModifications::default();

        let test_handle = "TestSchmest";

        let del = MdibDescriptionModification::Delete {
            handle: test_handle.to_string(),
        };
        let del2 = MdibDescriptionModification::Delete {
            handle: test_handle.to_string(),
        };
        assert_eq!(
            Err(ModificationError::DuplicateHandle {
                handles: vec![test_handle.to_string(), test_handle.to_string()]
            }),
            modification.add_all(vec![del, del2])
        );
    }
}
