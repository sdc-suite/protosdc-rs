use paste::paste;
use thiserror::Error;

use protosdc_biceps::biceps::{AbstractDescriptorOneOf, AbstractStateOneOf};

use crate::common::mdib_entity::{
    ActivateOperation, AlertCondition, AlertSignal, AlertSystem, Battery, Channel, Clock,
    DistributionSampleArrayMetric, EnsembleContext, Entity, EnumStringMetric, LimitAlertCondition,
    LocationContext, MdibEntity, Mds, MeansContext, NumericMetric, OperatorContext, PatientContext,
    RealTimeSampleArrayMetric, Sco, SetAlertStateOperation, SetComponentStateOperation,
    SetContextStateOperation, SetMetricStateOperation, SetStringOperation, SetValueOperation,
    StringMetric, SystemContext, Vmd, WorkflowContext,
};
use crate::common::mdib_pair::*;
use crate::common::mdib_version::MdibVersion;

#[derive(Debug, PartialEq, Eq, Error)]
pub enum CreationError {
    #[error("Descriptor and states did not match in cardinality, types, ...")]
    TypeMismatch,
    #[error("Mandatory parent is missing")]
    ParentRequired,
    #[error("No children are allowed for an element")]
    NoChildrenAllowed,
    #[error("Tried to create an abstract type, unsupported.")]
    AbstractType,
}

/// Creates an mdib entity for an entity without children and a single state, raising an error
/// if there is a child
///
/// # Arguments
///
/// * `struct_name`: name of the [Entity] to create
/// * `state_type`: [AbstractStateOneOf] variant of the state
/// * `desc`: reference to the [AbstractDescriptorOneOf] for the entity
/// * `par`: reference to the [Option<String>] parent handle, must be present, error otherwise
/// * `st`: reference to the [Vec<AbstractStateOneOf>] with a single state inside
/// * `ch`: reference to the [Vec<String>] of child handles which must be empty, error otherwise
/// * `mv`: reference to the [MdibVersion] in which the entity was last changed (or created in this case)
macro_rules! entity_no_children {
    ($struct_name:ident, $state_type: ident, $desc:ident, $par:ident, $st:ident, $ch:ident, $mv:ident, $mds:ident) => {{
        // parent is required
        if $par.is_none() {
            return Err(CreationError::ParentRequired);
        }

        // state is required
        if $st.len() != 1 {
            return Err(CreationError::TypeMismatch);
        }

        // children forbidden
        if !$ch.is_empty() {
            return Err(CreationError::NoChildrenAllowed);
        }

        match $st.pop() {
            Some(AbstractStateOneOf::$state_type(state)) => {
                paste! {
                    let ent = $struct_name {
                        pair: [<$struct_name Pair>] {
                            descriptor: $desc,
                            state,
                        },
                        parent: $par.unwrap(),
                        mds: $mds,
                    };
                }

                Ok(MdibEntity {
                    last_changed: $mv,
                    entity: Entity::$struct_name(Box::new(ent)),
                })
            }
            _ => Err(CreationError::TypeMismatch),
        }
    }};
}

/// Creates an mdib entity for an entity with children and a single state.
///
/// # Arguments
///
/// * `struct_name`: name of the [Entity] to create
/// * `state_type`: [AbstractStateOneOf] variant of the state
/// * `desc`: reference to the [AbstractDescriptorOneOf] for the entity
/// * `par`: reference to the [Option<String>] parent handle, must be present, error otherwise
/// * `st`: reference to the [Vec<AbstractStateOneOf>] with a single state inside
/// * `ch`: reference to the [Vec<String>] of child handles
/// * `mv`: reference to the [MdibVersion] in which the entity was last changed (or created in this case)
macro_rules! entity_children {
    ($struct_name:ident, $state_type: ident, $desc:ident, $par:ident, $st:ident, $ch:ident, $mv:ident, $mds:ident) => {{
        // parent is required
        if $par.is_none() {
            return Err(CreationError::ParentRequired);
        }

        // state is required
        if $st.len() != 1 {
            return Err(CreationError::TypeMismatch);
        }

        match $st.pop() {
            Some(AbstractStateOneOf::$state_type(state)) => {
                paste! {
                    let ent = $struct_name {
                        pair: [<$struct_name Pair>] {
                            descriptor: $desc,
                            state,
                        },
                        parent: $par.unwrap(),
                        children: $ch,
                        mds: $mds
                    };
                }

                Ok(MdibEntity {
                    last_changed: $mv,
                    entity: Entity::$struct_name(Box::new(ent)),
                })
            }
            _ => Err(CreationError::TypeMismatch),
        }
    }};
}

/// Creates an mdib entity for an entity representing a context
///
/// # Arguments
///
/// * `struct_name`: name of the [Entity] to create
/// * `state_type`: [AbstractStateOneOf] variant of the state
/// * `desc`: reference to the [AbstractDescriptorOneOf] for the entity
/// * `par`: reference to the [Option<String>] parent handle, must be present, error otherwise
/// * `st`: reference to the [Vec<AbstractStateOneOf>]
/// * `mv`: reference to the [MdibVersion] in which the entity was last changed (or created in this case)
macro_rules! entity_context {
    ($struct_name:ident, $state_type: ident, $desc:ident, $par:ident, $st:ident, $mv:ident, $mds:ident) => {{
        // parent is required
        if $par.is_none() {
            return Err(CreationError::ParentRequired);
        }

        // consume states vec, type checking on the way
        let mut mapped_states = vec![];
        for st in $st {
            match st {
                AbstractStateOneOf::$state_type(s) => mapped_states.push(s),
                _ => return Err(CreationError::TypeMismatch),
            }
        }

        paste! {
            let ent = $struct_name {
                pair: [<$struct_name Pair>] {
                    descriptor: $desc,
                    states: mapped_states,
                },
                parent: $par.unwrap(),
                mds: $mds
            };
        }

        Ok(MdibEntity {
            last_changed: $mv,
            entity: Entity::$struct_name(Box::new(ent)),
        })
    }};
}

pub(crate) fn create_mdib_entity(
    descriptor: AbstractDescriptorOneOf,
    mut states: Vec<AbstractStateOneOf>,
    mdib_version: MdibVersion,
    parent: Option<String>,
    children: Vec<String>,
    mds: String,
) -> Result<MdibEntity, CreationError> {
    match descriptor {
        AbstractDescriptorOneOf::AlertConditionDescriptor(desc) => {
            entity_no_children!(
                AlertCondition,
                AlertConditionState,
                desc,
                parent,
                states,
                children,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::LimitAlertConditionDescriptor(desc) => {
            entity_no_children!(
                LimitAlertCondition,
                LimitAlertConditionState,
                desc,
                parent,
                states,
                children,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::AlertSignalDescriptor(desc) => {
            entity_no_children!(
                AlertSignal,
                AlertSignalState,
                desc,
                parent,
                states,
                children,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::AlertSystemDescriptor(desc) => {
            entity_children!(
                AlertSystem,
                AlertSystemState,
                desc,
                parent,
                states,
                children,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::ClockDescriptor(desc) => {
            entity_no_children!(
                Clock,
                ClockState,
                desc,
                parent,
                states,
                children,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::BatteryDescriptor(desc) => {
            entity_no_children!(
                Battery,
                BatteryState,
                desc,
                parent,
                states,
                children,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::ChannelDescriptor(desc) => {
            entity_children!(
                Channel,
                ChannelState,
                desc,
                parent,
                states,
                children,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::ScoDescriptor(desc) => {
            entity_children!(
                Sco,
                ScoState,
                desc,
                parent,
                states,
                children,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::SystemContextDescriptor(desc) => {
            entity_children!(
                SystemContext,
                SystemContextState,
                desc,
                parent,
                states,
                children,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::VmdDescriptor(desc) => {
            entity_children!(
                Vmd,
                VmdState,
                desc,
                parent,
                states,
                children,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::SetStringOperationDescriptor(desc) => {
            entity_no_children!(
                SetStringOperation,
                SetStringOperationState,
                desc,
                parent,
                states,
                children,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::SetComponentStateOperationDescriptor(desc) => {
            entity_no_children!(
                SetComponentStateOperation,
                SetComponentStateOperationState,
                desc,
                parent,
                states,
                children,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::SetAlertStateOperationDescriptor(desc) => {
            entity_no_children!(
                SetAlertStateOperation,
                SetAlertStateOperationState,
                desc,
                parent,
                states,
                children,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::SetMetricStateOperationDescriptor(desc) => {
            entity_no_children!(
                SetMetricStateOperation,
                SetMetricStateOperationState,
                desc,
                parent,
                states,
                children,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::SetContextStateOperationDescriptor(desc) => {
            entity_no_children!(
                SetContextStateOperation,
                SetContextStateOperationState,
                desc,
                parent,
                states,
                children,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::ActivateOperationDescriptor(desc) => {
            entity_no_children!(
                ActivateOperation,
                ActivateOperationState,
                desc,
                parent,
                states,
                children,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::SetValueOperationDescriptor(desc) => {
            entity_no_children!(
                SetValueOperation,
                SetValueOperationState,
                desc,
                parent,
                states,
                children,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::NumericMetricDescriptor(desc) => {
            entity_no_children!(
                NumericMetric,
                NumericMetricState,
                desc,
                parent,
                states,
                children,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::DistributionSampleArrayMetricDescriptor(desc) => {
            entity_no_children!(
                DistributionSampleArrayMetric,
                DistributionSampleArrayMetricState,
                desc,
                parent,
                states,
                children,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::RealTimeSampleArrayMetricDescriptor(desc) => {
            entity_no_children!(
                RealTimeSampleArrayMetric,
                RealTimeSampleArrayMetricState,
                desc,
                parent,
                states,
                children,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::StringMetricDescriptor(desc) => {
            entity_no_children!(
                StringMetric,
                StringMetricState,
                desc,
                parent,
                states,
                children,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::EnumStringMetricDescriptor(desc) => {
            entity_no_children!(
                EnumStringMetric,
                EnumStringMetricState,
                desc,
                parent,
                states,
                children,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::MeansContextDescriptor(desc) => {
            entity_context!(
                MeansContext,
                MeansContextState,
                desc,
                parent,
                states,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::WorkflowContextDescriptor(desc) => {
            entity_context!(
                WorkflowContext,
                WorkflowContextState,
                desc,
                parent,
                states,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::PatientContextDescriptor(desc) => {
            entity_context!(
                PatientContext,
                PatientContextState,
                desc,
                parent,
                states,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::OperatorContextDescriptor(desc) => {
            entity_context!(
                OperatorContext,
                OperatorContextState,
                desc,
                parent,
                states,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::EnsembleContextDescriptor(desc) => {
            entity_context!(
                EnsembleContext,
                EnsembleContextState,
                desc,
                parent,
                states,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::LocationContextDescriptor(desc) => {
            entity_context!(
                LocationContext,
                LocationContextState,
                desc,
                parent,
                states,
                mdib_version,
                mds
            )
        }
        AbstractDescriptorOneOf::MdsDescriptor(desc) => {
            // state is required
            if states.len() != 1 {
                return Err(CreationError::TypeMismatch);
            }

            match states.pop() {
                Some(AbstractStateOneOf::MdsState(state)) => {
                    let ent = Mds {
                        pair: MdsPair {
                            descriptor: desc,
                            state,
                        },
                        children,
                    };

                    Ok(MdibEntity {
                        last_changed: mdib_version,
                        entity: Entity::Mds(Box::new(ent)),
                    })
                }
                _ => Err(CreationError::TypeMismatch),
            }
        }
        _ => Err(CreationError::AbstractType),
    }
}

macro_rules! replace_children {
    ($struct_name:ident, $field_name:ident, $entity:ident, $children:ident, $last_changed:ident) => {{
        MdibEntity {
            last_changed: $last_changed,
            entity: Entity::$struct_name(Box::new($struct_name {
                children: $children,
                ..*$field_name
            })),
        }
    }};
}

/// Replaces the child handles in an entity which supports children, or returns the original
/// entity otherwise.
///
/// # Arguments
///
/// * `entity`: to replace the children in
/// * `new_children`: for the entity
///
/// returns: MdibEntity
pub fn replace_children(entity: MdibEntity, new_children: Vec<String>) -> MdibEntity {
    match entity {
        MdibEntity {
            last_changed,
            entity: Entity::AlertSystem(asy),
        } => replace_children!(AlertSystem, asy, entity, new_children, last_changed),
        MdibEntity {
            last_changed,
            entity: Entity::Mds(mds),
        } => replace_children!(Mds, mds, entity, new_children, last_changed),
        MdibEntity {
            last_changed,
            entity: Entity::Sco(sco),
        } => replace_children!(Sco, sco, entity, new_children, last_changed),
        MdibEntity {
            last_changed,
            entity: Entity::SystemContext(sc),
        } => replace_children!(SystemContext, sc, entity, new_children, last_changed),
        MdibEntity {
            last_changed,
            entity: Entity::Vmd(vmd),
        } => replace_children!(Vmd, vmd, entity, new_children, last_changed),
        MdibEntity {
            last_changed,
            entity: Entity::Channel(chan),
        } => replace_children!(Channel, chan, entity, new_children, last_changed),
        _ => entity,
    }
}

macro_rules! replace_single_descriptor_and_state {
    ($entity:ident, $struct_name:ident, $descriptor_t: ident, $state_t:ident, $descriptor:ident, $states:ident, $entity_field:ident, $last_changed:ident) => {{
        // descriptor type
        match $descriptor {
            AbstractDescriptorOneOf::$descriptor_t(desc) => {
                // single state is required
                if $states.len() != 1 {
                    return Err(CreationError::TypeMismatch);
                }
                let state = match $states.pop() {
                    Some(AbstractStateOneOf::$state_t(state)) => state,
                    _ => return Err(CreationError::TypeMismatch),
                };
                Ok(MdibEntity {
                    entity: Entity::$struct_name(Box::new($struct_name {
                        pair: (desc, state).into(),
                        ..*$entity_field
                    })),
                    last_changed: $last_changed,
                })
            }
            _ => return Err(CreationError::TypeMismatch),
        }
    }};
}

macro_rules! replace_multi_descriptor_and_state {
    ($entity:ident, $struct_name:ident, $descriptor_t: ident, $state_t:ident, $descriptor:ident, $states:ident, $entity_field:ident, $last_changed:ident) => {{
        // descriptor type
        match $descriptor {
            AbstractDescriptorOneOf::$descriptor_t(desc) => {
                // consume states vec, type checking on the way
                let mut mapped_states = vec![];
                for st in $states {
                    match st {
                        AbstractStateOneOf::$state_t(s) => mapped_states.push(s),
                        _ => return Err(CreationError::TypeMismatch),
                    }
                }
                Ok(MdibEntity {
                    entity: Entity::$struct_name(Box::new($struct_name {
                        pair: (desc, mapped_states).into(),
                        ..*$entity_field
                    })),
                    last_changed: $last_changed,
                })
            }
            _ => return Err(CreationError::TypeMismatch),
        }
    }};
}

/// Replaces the descriptor and the state in an [MdibEntity].
///
/// # Arguments
///
/// * `entity`: to replace descriptor and state(s) in
/// * `descriptor`: replacement descriptor
/// * `states`: replacement state(s)
///
/// returns: Result<MdibEntity, CreationError>
pub fn replace_descriptor_and_states(
    entity: MdibEntity,
    descriptor: AbstractDescriptorOneOf,
    mut states: Vec<AbstractStateOneOf>,
) -> Result<MdibEntity, CreationError> {
    match entity {
        MdibEntity {
            last_changed,
            entity: Entity::StringMetric(ent),
        } => {
            replace_single_descriptor_and_state!(
                entity,
                StringMetric,
                StringMetricDescriptor,
                StringMetricState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::EnumStringMetric(ent),
        } => {
            replace_single_descriptor_and_state!(
                entity,
                EnumStringMetric,
                EnumStringMetricDescriptor,
                EnumStringMetricState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::NumericMetric(ent),
        } => {
            replace_single_descriptor_and_state!(
                entity,
                NumericMetric,
                NumericMetricDescriptor,
                NumericMetricState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::RealTimeSampleArrayMetric(ent),
        } => {
            replace_single_descriptor_and_state!(
                entity,
                RealTimeSampleArrayMetric,
                RealTimeSampleArrayMetricDescriptor,
                RealTimeSampleArrayMetricState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::DistributionSampleArrayMetric(ent),
        } => {
            replace_single_descriptor_and_state!(
                entity,
                DistributionSampleArrayMetric,
                DistributionSampleArrayMetricDescriptor,
                DistributionSampleArrayMetricState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::AlertSystem(ent),
        } => {
            replace_single_descriptor_and_state!(
                entity,
                AlertSystem,
                AlertSystemDescriptor,
                AlertSystemState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::AlertCondition(ent),
        } => {
            replace_single_descriptor_and_state!(
                entity,
                AlertCondition,
                AlertConditionDescriptor,
                AlertConditionState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::LimitAlertCondition(ent),
        } => {
            replace_single_descriptor_and_state!(
                entity,
                LimitAlertCondition,
                LimitAlertConditionDescriptor,
                LimitAlertConditionState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::AlertSignal(ent),
        } => {
            replace_single_descriptor_and_state!(
                entity,
                AlertSignal,
                AlertSignalDescriptor,
                AlertSignalState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::Mds(ent),
        } => {
            replace_single_descriptor_and_state!(
                entity,
                Mds,
                MdsDescriptor,
                MdsState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::Vmd(ent),
        } => {
            replace_single_descriptor_and_state!(
                entity,
                Vmd,
                VmdDescriptor,
                VmdState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::Channel(ent),
        } => {
            replace_single_descriptor_and_state!(
                entity,
                Channel,
                ChannelDescriptor,
                ChannelState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::Sco(ent),
        } => {
            replace_single_descriptor_and_state!(
                entity,
                Sco,
                ScoDescriptor,
                ScoState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::Battery(ent),
        } => {
            replace_single_descriptor_and_state!(
                entity,
                Battery,
                BatteryDescriptor,
                BatteryState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::Clock(ent),
        } => {
            replace_single_descriptor_and_state!(
                entity,
                Clock,
                ClockDescriptor,
                ClockState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::SystemContext(ent),
        } => {
            replace_single_descriptor_and_state!(
                entity,
                SystemContext,
                SystemContextDescriptor,
                SystemContextState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::SetValueOperation(ent),
        } => {
            replace_single_descriptor_and_state!(
                entity,
                SetValueOperation,
                SetValueOperationDescriptor,
                SetValueOperationState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::SetStringOperation(ent),
        } => {
            replace_single_descriptor_and_state!(
                entity,
                SetStringOperation,
                SetStringOperationDescriptor,
                SetStringOperationState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::SetAlertStateOperation(ent),
        } => {
            replace_single_descriptor_and_state!(
                entity,
                SetAlertStateOperation,
                SetAlertStateOperationDescriptor,
                SetAlertStateOperationState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::SetMetricStateOperation(ent),
        } => {
            replace_single_descriptor_and_state!(
                entity,
                SetMetricStateOperation,
                SetMetricStateOperationDescriptor,
                SetMetricStateOperationState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::SetComponentStateOperation(ent),
        } => {
            replace_single_descriptor_and_state!(
                entity,
                SetComponentStateOperation,
                SetComponentStateOperationDescriptor,
                SetComponentStateOperationState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::SetContextStateOperation(ent),
        } => {
            replace_single_descriptor_and_state!(
                entity,
                SetContextStateOperation,
                SetContextStateOperationDescriptor,
                SetContextStateOperationState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::ActivateOperation(ent),
        } => {
            replace_single_descriptor_and_state!(
                entity,
                ActivateOperation,
                ActivateOperationDescriptor,
                ActivateOperationState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::PatientContext(ent),
        } => {
            replace_multi_descriptor_and_state!(
                entity,
                PatientContext,
                PatientContextDescriptor,
                PatientContextState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::LocationContext(ent),
        } => {
            replace_multi_descriptor_and_state!(
                entity,
                LocationContext,
                LocationContextDescriptor,
                LocationContextState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::EnsembleContext(ent),
        } => {
            replace_multi_descriptor_and_state!(
                entity,
                EnsembleContext,
                EnsembleContextDescriptor,
                EnsembleContextState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::WorkflowContext(ent),
        } => {
            replace_multi_descriptor_and_state!(
                entity,
                WorkflowContext,
                WorkflowContextDescriptor,
                WorkflowContextState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::MeansContext(ent),
        } => {
            replace_multi_descriptor_and_state!(
                entity,
                MeansContext,
                MeansContextDescriptor,
                MeansContextState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::OperatorContext(ent),
        } => {
            replace_multi_descriptor_and_state!(
                entity,
                OperatorContext,
                OperatorContextDescriptor,
                OperatorContextState,
                descriptor,
                states,
                ent,
                last_changed
            )
        }
    }
}

macro_rules! replace_single_state {
    ($entity:ident, $struct_name:ident, $state_t:ident, $states:ident, $entity_field:ident, $last_changed:ident) => {{
        // single state is required
        if $states.len() != 1 {
            return Err(CreationError::TypeMismatch);
        }
        let state = match $states.pop() {
            Some(AbstractStateOneOf::$state_t(state)) => state,
            _ => return Err(CreationError::TypeMismatch),
        };
        paste! {
            Ok(MdibEntity {
                entity: Entity::$struct_name(Box::new($struct_name {
                    pair: [<$struct_name Pair>] {
                        state,
                        ..$entity_field.pair
                    },
                    ..*$entity_field
                })),
                last_changed: $last_changed,
            })
        }
    }};
}

macro_rules! replace_multi_state {
    ($entity:ident, $struct_name:ident, $state_t:ident, $states:ident, $entity_field:ident, $last_changed:ident) => {{
        // consume states vec, type checking on the way
        let mut mapped_states = vec![];
        for st in $states {
            match st {
                AbstractStateOneOf::$state_t(s) => mapped_states.push(s),
                _ => return Err(CreationError::TypeMismatch),
            }
        }
        paste! {
            Ok(MdibEntity {
                entity: Entity::$struct_name(Box::new($struct_name {
                    pair: [<$struct_name Pair>] {
                        states: mapped_states,
                        ..$entity_field.pair
                    },
                    ..*$entity_field
                })),
                last_changed: $last_changed,
            })
        }
    }};
}

/// Replaces the states in an [MdibEntity].
///
/// # Arguments
///
/// * `entity`: to replace state(s) in
/// * `states`: replacement state(s)
///
/// returns: Result<MdibEntity, CreationError>
pub fn replace_states(
    entity: MdibEntity,
    mut states: Vec<AbstractStateOneOf>,
) -> Result<MdibEntity, CreationError> {
    match entity {
        MdibEntity {
            last_changed,
            entity: Entity::StringMetric(ent),
        } => {
            replace_single_state!(
                entity,
                StringMetric,
                StringMetricState,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::EnumStringMetric(ent),
        } => {
            replace_single_state!(
                entity,
                EnumStringMetric,
                EnumStringMetricState,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::NumericMetric(ent),
        } => {
            replace_single_state!(
                entity,
                NumericMetric,
                NumericMetricState,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::RealTimeSampleArrayMetric(ent),
        } => {
            replace_single_state!(
                entity,
                RealTimeSampleArrayMetric,
                RealTimeSampleArrayMetricState,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::DistributionSampleArrayMetric(ent),
        } => {
            replace_single_state!(
                entity,
                DistributionSampleArrayMetric,
                DistributionSampleArrayMetricState,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::AlertSystem(ent),
        } => {
            replace_single_state!(
                entity,
                AlertSystem,
                AlertSystemState,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::AlertCondition(ent),
        } => {
            replace_single_state!(
                entity,
                AlertCondition,
                AlertConditionState,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::LimitAlertCondition(ent),
        } => {
            replace_single_state!(
                entity,
                LimitAlertCondition,
                LimitAlertConditionState,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::AlertSignal(ent),
        } => {
            replace_single_state!(
                entity,
                AlertSignal,
                AlertSignalState,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::Mds(ent),
        } => {
            replace_single_state!(entity, Mds, MdsState, states, ent, last_changed)
        }
        MdibEntity {
            last_changed,
            entity: Entity::Vmd(ent),
        } => {
            replace_single_state!(entity, Vmd, VmdState, states, ent, last_changed)
        }
        MdibEntity {
            last_changed,
            entity: Entity::Channel(ent),
        } => {
            replace_single_state!(entity, Channel, ChannelState, states, ent, last_changed)
        }
        MdibEntity {
            last_changed,
            entity: Entity::Sco(ent),
        } => {
            replace_single_state!(entity, Sco, ScoState, states, ent, last_changed)
        }
        MdibEntity {
            last_changed,
            entity: Entity::Battery(ent),
        } => {
            replace_single_state!(entity, Battery, BatteryState, states, ent, last_changed)
        }
        MdibEntity {
            last_changed,
            entity: Entity::Clock(ent),
        } => {
            replace_single_state!(entity, Clock, ClockState, states, ent, last_changed)
        }
        MdibEntity {
            last_changed,
            entity: Entity::SystemContext(ent),
        } => {
            replace_single_state!(
                entity,
                SystemContext,
                SystemContextState,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::SetValueOperation(ent),
        } => {
            replace_single_state!(
                entity,
                SetValueOperation,
                SetValueOperationState,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::SetStringOperation(ent),
        } => {
            replace_single_state!(
                entity,
                SetStringOperation,
                SetStringOperationState,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::SetAlertStateOperation(ent),
        } => {
            replace_single_state!(
                entity,
                SetAlertStateOperation,
                SetAlertStateOperationState,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::SetMetricStateOperation(ent),
        } => {
            replace_single_state!(
                entity,
                SetMetricStateOperation,
                SetMetricStateOperationState,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::SetComponentStateOperation(ent),
        } => {
            replace_single_state!(
                entity,
                SetComponentStateOperation,
                SetComponentStateOperationState,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::SetContextStateOperation(ent),
        } => {
            replace_single_state!(
                entity,
                SetContextStateOperation,
                SetContextStateOperationState,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::ActivateOperation(ent),
        } => {
            replace_single_state!(
                entity,
                ActivateOperation,
                ActivateOperationState,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::PatientContext(ent),
        } => {
            replace_multi_state!(
                entity,
                PatientContext,
                PatientContextState,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::LocationContext(ent),
        } => {
            replace_multi_state!(
                entity,
                LocationContext,
                LocationContextState,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::EnsembleContext(ent),
        } => {
            replace_multi_state!(
                entity,
                EnsembleContext,
                EnsembleContextState,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::WorkflowContext(ent),
        } => {
            replace_multi_state!(
                entity,
                WorkflowContext,
                WorkflowContextState,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::MeansContext(ent),
        } => {
            replace_multi_state!(
                entity,
                MeansContext,
                MeansContextState,
                states,
                ent,
                last_changed
            )
        }
        MdibEntity {
            last_changed,
            entity: Entity::OperatorContext(ent),
        } => {
            replace_multi_state!(
                entity,
                OperatorContext,
                OperatorContextState,
                states,
                ent,
                last_changed
            )
        }
    }
}
