use ambassador::{delegatable_trait, delegate_to_remote_methods, Delegate};
use derive_more::From;
use paste::paste;
use std::ops::Deref;

use protosdc_biceps::biceps::{
    AbstractContextStateOneOf, AbstractDescriptor, AbstractDescriptorOneOf, AbstractState,
    AbstractStateOneOf,
};

use crate::common::biceps_util::{Descriptor, MultiState, State};
use crate::common::mdib_pair::*;
use crate::common::mdib_version::MdibVersion;

#[delegatable_trait]
pub trait EntityBase {
    /// Returns the Mds handle the entity is a child of, or the Mds handle in case it's the Mds.
    fn mds(&self) -> String;

    /// Returns the descriptor handle of the entity
    fn handle(&self) -> String;

    /// Returns the version(s) of the entity, see [MdibEntityVersion]
    fn version(&self) -> MdibEntityVersion;
}

/// MdibEntity represents a single Descriptor-State combination sharing the same descriptor handle.
#[derive(Clone, Debug)]
pub struct MdibEntity {
    pub last_changed: MdibVersion,
    pub entity: Entity,
}

/// Enum to represent versions for the two different variants of states, single state and
/// multi-state.
pub enum MdibEntityStateVersion {
    /// The version for a single state entity.
    SingleStateVersion(u64),
    /// A vec of multi-state handles and their respective state version.
    MultiStateVersion(Vec<(String, u64)>),
}

/// Represents the version of an [MdibEntity].
///
/// The descriptor version is always a single entry, while the state version depends on whether
/// the entity represents a single or a multi state. See [MdibEntityStateVersion].
pub struct MdibEntityVersion {
    pub descriptor_version: u64,
    pub state_version: MdibEntityStateVersion,
}

impl MdibEntity {
    /// Returns the handle of the parent entity in the mdib, if present.
    pub fn resolve_parent(&self) -> Option<String> {
        match &self.entity {
            Entity::StringMetric(x) => Some(x.parent.clone()),
            Entity::EnumStringMetric(x) => Some(x.parent.clone()),
            Entity::NumericMetric(x) => Some(x.parent.clone()),
            Entity::RealTimeSampleArrayMetric(x) => Some(x.parent.clone()),
            Entity::DistributionSampleArrayMetric(x) => Some(x.parent.clone()),
            Entity::AlertSystem(x) => Some(x.parent.clone()),
            Entity::AlertCondition(x) => Some(x.parent.clone()),
            Entity::LimitAlertCondition(x) => Some(x.parent.clone()),
            Entity::AlertSignal(x) => Some(x.parent.clone()),
            Entity::Vmd(x) => Some(x.parent.clone()),
            Entity::Channel(x) => Some(x.parent.clone()),
            Entity::Sco(x) => Some(x.parent.clone()),
            Entity::Battery(x) => Some(x.parent.clone()),
            Entity::Clock(x) => Some(x.parent.clone()),
            Entity::SystemContext(x) => Some(x.parent.clone()),
            Entity::SetValueOperation(x) => Some(x.parent.clone()),
            Entity::SetStringOperation(x) => Some(x.parent.clone()),
            Entity::SetAlertStateOperation(x) => Some(x.parent.clone()),
            Entity::SetMetricStateOperation(x) => Some(x.parent.clone()),
            Entity::SetComponentStateOperation(x) => Some(x.parent.clone()),
            Entity::SetContextStateOperation(x) => Some(x.parent.clone()),
            Entity::ActivateOperation(x) => Some(x.parent.clone()),
            Entity::PatientContext(x) => Some(x.parent.clone()),
            Entity::LocationContext(x) => Some(x.parent.clone()),
            Entity::EnsembleContext(x) => Some(x.parent.clone()),
            Entity::WorkflowContext(x) => Some(x.parent.clone()),
            Entity::MeansContext(x) => Some(x.parent.clone()),
            Entity::OperatorContext(x) => Some(x.parent.clone()),
            Entity::Mds(_) => None,
        }
    }

    /// Returns the handles of the children of the entity in the mdib.
    pub fn resolve_children(&self) -> Option<Vec<String>> {
        match &self.entity {
            Entity::AlertSystem(x) => Some(x.children.clone()),
            Entity::Mds(x) => Some(x.children.clone()),
            Entity::Sco(x) => Some(x.children.clone()),
            Entity::SystemContext(x) => Some(x.children.clone()),
            Entity::Vmd(x) => Some(x.children.clone()),
            Entity::Channel(x) => Some(x.children.clone()),
            _ => None,
        }
    }

    /// Returns the context states of the entity, if it is a multi-state.
    ///
    /// When called on a single state entity, the list is always empty.
    pub fn resolve_context_states(&self) -> Vec<AbstractContextStateOneOf> {
        match &self.entity {
            Entity::EnsembleContext(x) => x
                .pair
                .states
                .iter()
                .map(|x| AbstractContextStateOneOf::EnsembleContextState(x.clone()))
                .collect(),
            Entity::MeansContext(x) => x
                .pair
                .states
                .iter()
                .map(|x| AbstractContextStateOneOf::MeansContextState(x.clone()))
                .collect(),
            Entity::LocationContext(x) => x
                .pair
                .states
                .iter()
                .map(|x| AbstractContextStateOneOf::LocationContextState(x.clone()))
                .collect(),
            Entity::OperatorContext(x) => x
                .pair
                .states
                .iter()
                .map(|x| AbstractContextStateOneOf::OperatorContextState(x.clone()))
                .collect(),
            Entity::PatientContext(x) => x
                .pair
                .states
                .iter()
                .map(|x| AbstractContextStateOneOf::PatientContextState(x.clone()))
                .collect(),
            Entity::WorkflowContext(x) => x
                .pair
                .states
                .iter()
                .map(|x| AbstractContextStateOneOf::WorkflowContextState(x.clone()))
                .collect(),
            _ => vec![],
        }
    }

    /// Returns true if the entity represents a context.
    pub fn is_context(&self) -> bool {
        matches!(
            &self.entity,
            Entity::EnsembleContext(_)
                | Entity::MeansContext(_)
                | Entity::LocationContext(_)
                | Entity::OperatorContext(_)
                | Entity::PatientContext(_)
                | Entity::WorkflowContext(_)
        )
    }

    /// Returns the single state if the entity represents a single-state entity, None otherwise.
    pub fn resolve_single_state(&self) -> Option<AbstractStateOneOf> {
        match &self.entity {
            Entity::StringMetric(it) => {
                Some(AbstractStateOneOf::StringMetricState(it.pair.state.clone()))
            }
            Entity::EnumStringMetric(it) => Some(AbstractStateOneOf::EnumStringMetricState(
                it.pair.state.clone(),
            )),
            Entity::NumericMetric(it) => Some(AbstractStateOneOf::NumericMetricState(
                it.pair.state.clone(),
            )),
            Entity::RealTimeSampleArrayMetric(it) => Some(
                AbstractStateOneOf::RealTimeSampleArrayMetricState(it.pair.state.clone()),
            ),
            Entity::DistributionSampleArrayMetric(it) => Some(
                AbstractStateOneOf::DistributionSampleArrayMetricState(it.pair.state.clone()),
            ),
            Entity::AlertSystem(it) => {
                Some(AbstractStateOneOf::AlertSystemState(it.pair.state.clone()))
            }
            Entity::AlertCondition(it) => Some(AbstractStateOneOf::AlertConditionState(
                it.pair.state.clone(),
            )),
            Entity::LimitAlertCondition(it) => Some(AbstractStateOneOf::LimitAlertConditionState(
                it.pair.state.clone(),
            )),
            Entity::AlertSignal(it) => {
                Some(AbstractStateOneOf::AlertSignalState(it.pair.state.clone()))
            }
            Entity::Mds(it) => Some(AbstractStateOneOf::MdsState(it.pair.state.clone())),
            Entity::Vmd(it) => Some(AbstractStateOneOf::VmdState(it.pair.state.clone())),
            Entity::Channel(it) => Some(AbstractStateOneOf::ChannelState(it.pair.state.clone())),
            Entity::Sco(it) => Some(AbstractStateOneOf::ScoState(it.pair.state.clone())),
            Entity::Battery(it) => Some(AbstractStateOneOf::BatteryState(it.pair.state.clone())),
            Entity::Clock(it) => Some(AbstractStateOneOf::ClockState(it.pair.state.clone())),
            Entity::SystemContext(it) => Some(AbstractStateOneOf::SystemContextState(
                it.pair.state.clone(),
            )),
            Entity::SetValueOperation(it) => Some(AbstractStateOneOf::SetValueOperationState(
                it.pair.state.clone(),
            )),
            Entity::SetStringOperation(it) => Some(AbstractStateOneOf::SetStringOperationState(
                it.pair.state.clone(),
            )),
            Entity::SetAlertStateOperation(it) => Some(
                AbstractStateOneOf::SetAlertStateOperationState(it.pair.state.clone()),
            ),
            Entity::SetMetricStateOperation(it) => Some(
                AbstractStateOneOf::SetMetricStateOperationState(it.pair.state.clone()),
            ),
            Entity::SetComponentStateOperation(it) => Some(
                AbstractStateOneOf::SetComponentStateOperationState(it.pair.state.clone()),
            ),
            Entity::SetContextStateOperation(it) => Some(
                AbstractStateOneOf::SetContextStateOperationState(it.pair.state.clone()),
            ),
            Entity::ActivateOperation(it) => Some(AbstractStateOneOf::ActivateOperationState(
                it.pair.state.clone(),
            )),
            _ => None,
        }
    }

    /// Returns the list of all states for the entity.
    ///
    /// If the entity is a single-state entity, the list always contains a single entry.
    pub fn resolve_states(&self) -> Vec<AbstractStateOneOf> {
        match &self.entity {
            Entity::StringMetric(it) => {
                vec![AbstractStateOneOf::StringMetricState(it.pair.state.clone())]
            }
            Entity::EnumStringMetric(it) => {
                vec![AbstractStateOneOf::EnumStringMetricState(
                    it.pair.state.clone(),
                )]
            }
            Entity::NumericMetric(it) => {
                vec![AbstractStateOneOf::NumericMetricState(
                    it.pair.state.clone(),
                )]
            }
            Entity::RealTimeSampleArrayMetric(it) => {
                vec![AbstractStateOneOf::RealTimeSampleArrayMetricState(
                    it.pair.state.clone(),
                )]
            }
            Entity::DistributionSampleArrayMetric(it) => {
                vec![AbstractStateOneOf::DistributionSampleArrayMetricState(
                    it.pair.state.clone(),
                )]
            }
            Entity::AlertSystem(it) => {
                vec![AbstractStateOneOf::AlertSystemState(it.pair.state.clone())]
            }
            Entity::AlertCondition(it) => {
                vec![AbstractStateOneOf::AlertConditionState(
                    it.pair.state.clone(),
                )]
            }
            Entity::LimitAlertCondition(it) => vec![AbstractStateOneOf::LimitAlertConditionState(
                it.pair.state.clone(),
            )],
            Entity::AlertSignal(it) => {
                vec![AbstractStateOneOf::AlertSignalState(it.pair.state.clone())]
            }
            Entity::Mds(it) => vec![AbstractStateOneOf::MdsState(it.pair.state.clone())],
            Entity::Vmd(it) => vec![AbstractStateOneOf::VmdState(it.pair.state.clone())],
            Entity::Channel(it) => vec![AbstractStateOneOf::ChannelState(it.pair.state.clone())],
            Entity::Sco(it) => vec![AbstractStateOneOf::ScoState(it.pair.state.clone())],
            Entity::Battery(it) => vec![AbstractStateOneOf::BatteryState(it.pair.state.clone())],
            Entity::Clock(it) => vec![AbstractStateOneOf::ClockState(it.pair.state.clone())],
            Entity::SystemContext(it) => {
                vec![AbstractStateOneOf::SystemContextState(
                    it.pair.state.clone(),
                )]
            }
            Entity::SetValueOperation(it) => {
                vec![AbstractStateOneOf::SetValueOperationState(
                    it.pair.state.clone(),
                )]
            }
            Entity::SetStringOperation(it) => vec![AbstractStateOneOf::SetStringOperationState(
                it.pair.state.clone(),
            )],
            Entity::SetAlertStateOperation(it) => {
                vec![AbstractStateOneOf::SetAlertStateOperationState(
                    it.pair.state.clone(),
                )]
            }
            Entity::SetMetricStateOperation(it) => {
                vec![AbstractStateOneOf::SetMetricStateOperationState(
                    it.pair.state.clone(),
                )]
            }
            Entity::SetComponentStateOperation(it) => {
                vec![AbstractStateOneOf::SetComponentStateOperationState(
                    it.pair.state.clone(),
                )]
            }
            Entity::SetContextStateOperation(it) => {
                vec![AbstractStateOneOf::SetContextStateOperationState(
                    it.pair.state.clone(),
                )]
            }
            Entity::ActivateOperation(it) => {
                vec![AbstractStateOneOf::ActivateOperationState(
                    it.pair.state.clone(),
                )]
            }
            Entity::EnsembleContext(x) => x
                .pair
                .states
                .iter()
                .map(|x| AbstractStateOneOf::EnsembleContextState(x.clone()))
                .collect(),
            Entity::MeansContext(x) => x
                .pair
                .states
                .iter()
                .map(|x| AbstractStateOneOf::MeansContextState(x.clone()))
                .collect(),
            Entity::LocationContext(x) => x
                .pair
                .states
                .iter()
                .map(|x| AbstractStateOneOf::LocationContextState(x.clone()))
                .collect(),
            Entity::OperatorContext(x) => x
                .pair
                .states
                .iter()
                .map(|x| AbstractStateOneOf::OperatorContextState(x.clone()))
                .collect(),
            Entity::PatientContext(x) => x
                .pair
                .states
                .iter()
                .map(|x| AbstractStateOneOf::PatientContextState(x.clone()))
                .collect(),
            Entity::WorkflowContext(x) => x
                .pair
                .states
                .iter()
                .map(|x| AbstractStateOneOf::WorkflowContextState(x.clone()))
                .collect(),
        }
    }
}

/// Represents a descriptor-state entity-pair in the mdib.
#[derive(Delegate, From, Clone, Debug)]
#[delegate(EntityBase)]
#[allow(dead_code)]
pub enum Entity {
    StringMetric(Box<StringMetric>),
    EnumStringMetric(Box<EnumStringMetric>),
    NumericMetric(Box<NumericMetric>),
    RealTimeSampleArrayMetric(Box<RealTimeSampleArrayMetric>),
    DistributionSampleArrayMetric(Box<DistributionSampleArrayMetric>),
    AlertSystem(Box<AlertSystem>),
    AlertCondition(Box<AlertCondition>),
    LimitAlertCondition(Box<LimitAlertCondition>),
    AlertSignal(Box<AlertSignal>),
    Mds(Box<Mds>),
    Vmd(Box<Vmd>),
    Channel(Box<Channel>),
    Sco(Box<Sco>),
    Battery(Box<Battery>),
    Clock(Box<Clock>),
    SystemContext(Box<SystemContext>),
    SetValueOperation(Box<SetValueOperation>),
    SetStringOperation(Box<SetStringOperation>),
    SetAlertStateOperation(Box<SetAlertStateOperation>),
    SetMetricStateOperation(Box<SetMetricStateOperation>),
    SetComponentStateOperation(Box<SetComponentStateOperation>),
    SetContextStateOperation(Box<SetContextStateOperation>),
    ActivateOperation(Box<ActivateOperation>),
    PatientContext(Box<PatientContext>),
    LocationContext(Box<LocationContext>),
    EnsembleContext(Box<EnsembleContext>),
    WorkflowContext(Box<WorkflowContext>),
    MeansContext(Box<MeansContext>),
    OperatorContext(Box<OperatorContext>),
}

#[delegate_to_remote_methods]
#[delegate(EntityBase, target_ref = "deref")]
impl<M: ?Sized + EntityBase> EntityBase for Box<M> {
    fn deref(&self) -> &M;
}

impl From<Entity> for Pair {
    fn from(value: Entity) -> Self {
        match value {
            Entity::StringMetric(x) => (*x).into(),
            Entity::EnumStringMetric(x) => (*x).into(),
            Entity::NumericMetric(x) => (*x).into(),
            Entity::RealTimeSampleArrayMetric(x) => (*x).into(),
            Entity::DistributionSampleArrayMetric(x) => (*x).into(),
            Entity::AlertSystem(x) => (*x).into(),
            Entity::AlertCondition(x) => (*x).into(),
            Entity::LimitAlertCondition(x) => (*x).into(),
            Entity::AlertSignal(x) => (*x).into(),
            Entity::Mds(x) => (*x).into(),
            Entity::Vmd(x) => (*x).into(),
            Entity::Channel(x) => (*x).into(),
            Entity::Sco(x) => (*x).into(),
            Entity::Battery(x) => (*x).into(),
            Entity::Clock(x) => (*x).into(),
            Entity::SystemContext(x) => (*x).into(),
            Entity::SetValueOperation(x) => (*x).into(),
            Entity::SetStringOperation(x) => (*x).into(),
            Entity::SetAlertStateOperation(x) => (*x).into(),
            Entity::SetMetricStateOperation(x) => (*x).into(),
            Entity::SetComponentStateOperation(x) => (*x).into(),
            Entity::SetContextStateOperation(x) => (*x).into(),
            Entity::ActivateOperation(x) => (*x).into(),
            Entity::PatientContext(x) => (*x).into(),
            Entity::LocationContext(x) => (*x).into(),
            Entity::EnsembleContext(x) => (*x).into(),
            Entity::WorkflowContext(x) => (*x).into(),
            Entity::MeansContext(x) => (*x).into(),
            Entity::OperatorContext(x) => (*x).into(),
        }
    }
}

macro_rules! entity_get_abstract_descriptor {
    { $($single_field:ident),*; and $($multi_field:ident),* } => {
        /// Returns the descriptor for an entity.
        pub fn get_abstract_descriptor(&self) -> AbstractDescriptorOneOf {
            match self {
                $(
                    Entity::$single_field(ent) => ent.pair.descriptor.clone().into_abstract_descriptor_one_of(),
                )*
                $(
                    Entity::$multi_field(ent) => ent.pair.descriptor.clone().into_abstract_descriptor_one_of(),
                )*
            }
        }
    }
}

impl Entity {
    all_pairs!(entity_get_abstract_descriptor);
    /// Returns the descriptor for an entity as a reference.
    pub fn get_abstract_descriptor_ref(&self) -> &AbstractDescriptor {
        match self {
            Entity::StringMetric(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_metric_descriptor
                    .abstract_descriptor
            }
            Entity::EnumStringMetric(ent) => {
                &ent.pair
                    .descriptor
                    .string_metric_descriptor
                    .abstract_metric_descriptor
                    .abstract_descriptor
            }
            Entity::NumericMetric(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_metric_descriptor
                    .abstract_descriptor
            }
            Entity::RealTimeSampleArrayMetric(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_metric_descriptor
                    .abstract_descriptor
            }
            Entity::DistributionSampleArrayMetric(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_metric_descriptor
                    .abstract_descriptor
            }
            Entity::AlertSystem(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_alert_descriptor
                    .abstract_descriptor
            }
            Entity::AlertCondition(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_alert_descriptor
                    .abstract_descriptor
            }
            Entity::LimitAlertCondition(ent) => {
                &ent.pair
                    .descriptor
                    .alert_condition_descriptor
                    .abstract_alert_descriptor
                    .abstract_descriptor
            }
            Entity::AlertSignal(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_alert_descriptor
                    .abstract_descriptor
            }
            Entity::Mds(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_complex_device_component_descriptor
                    .abstract_device_component_descriptor
                    .abstract_descriptor
            }
            Entity::Vmd(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_complex_device_component_descriptor
                    .abstract_device_component_descriptor
                    .abstract_descriptor
            }
            Entity::Channel(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_device_component_descriptor
                    .abstract_descriptor
            }
            Entity::Sco(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_device_component_descriptor
                    .abstract_descriptor
            }
            Entity::Battery(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_device_component_descriptor
                    .abstract_descriptor
            }
            Entity::Clock(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_device_component_descriptor
                    .abstract_descriptor
            }
            Entity::SystemContext(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_device_component_descriptor
                    .abstract_descriptor
            }
            Entity::SetValueOperation(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_operation_descriptor
                    .abstract_descriptor
            }
            Entity::SetStringOperation(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_operation_descriptor
                    .abstract_descriptor
            }
            Entity::SetAlertStateOperation(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_set_state_operation_descriptor
                    .abstract_operation_descriptor
                    .abstract_descriptor
            }
            Entity::SetMetricStateOperation(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_set_state_operation_descriptor
                    .abstract_operation_descriptor
                    .abstract_descriptor
            }
            Entity::SetComponentStateOperation(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_set_state_operation_descriptor
                    .abstract_operation_descriptor
                    .abstract_descriptor
            }
            Entity::SetContextStateOperation(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_set_state_operation_descriptor
                    .abstract_operation_descriptor
                    .abstract_descriptor
            }
            Entity::ActivateOperation(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_set_state_operation_descriptor
                    .abstract_operation_descriptor
                    .abstract_descriptor
            }
            Entity::PatientContext(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_context_descriptor
                    .abstract_descriptor
            }
            Entity::LocationContext(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_context_descriptor
                    .abstract_descriptor
            }
            Entity::EnsembleContext(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_context_descriptor
                    .abstract_descriptor
            }
            Entity::WorkflowContext(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_context_descriptor
                    .abstract_descriptor
            }
            Entity::MeansContext(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_context_descriptor
                    .abstract_descriptor
            }
            Entity::OperatorContext(ent) => {
                &ent.pair
                    .descriptor
                    .abstract_context_descriptor
                    .abstract_descriptor
            }
        }
    }

    /// Returns the state for an entity as a reference.
    ///
    /// Multi-states return None.
    pub fn get_abstract_state_ref(&self) -> Option<&AbstractState> {
        match self {
            Entity::StringMetric(ent) => Some(&ent.pair.state.abstract_metric_state.abstract_state),
            Entity::EnumStringMetric(ent) => Some(
                &ent.pair
                    .state
                    .string_metric_state
                    .abstract_metric_state
                    .abstract_state,
            ),
            Entity::NumericMetric(ent) => {
                Some(&ent.pair.state.abstract_metric_state.abstract_state)
            }
            Entity::RealTimeSampleArrayMetric(ent) => {
                Some(&ent.pair.state.abstract_metric_state.abstract_state)
            }
            Entity::DistributionSampleArrayMetric(ent) => {
                Some(&ent.pair.state.abstract_metric_state.abstract_state)
            }
            Entity::AlertSystem(ent) => Some(&ent.pair.state.abstract_alert_state.abstract_state),
            Entity::AlertCondition(ent) => {
                Some(&ent.pair.state.abstract_alert_state.abstract_state)
            }
            Entity::LimitAlertCondition(ent) => Some(
                &ent.pair
                    .state
                    .alert_condition_state
                    .abstract_alert_state
                    .abstract_state,
            ),
            Entity::AlertSignal(ent) => Some(&ent.pair.state.abstract_alert_state.abstract_state),
            Entity::Mds(ent) => Some(
                &ent.pair
                    .state
                    .abstract_complex_device_component_state
                    .abstract_device_component_state
                    .abstract_state,
            ),
            Entity::Vmd(ent) => Some(
                &ent.pair
                    .state
                    .abstract_complex_device_component_state
                    .abstract_device_component_state
                    .abstract_state,
            ),
            Entity::Channel(ent) => Some(
                &ent.pair
                    .state
                    .abstract_device_component_state
                    .abstract_state,
            ),
            Entity::Sco(ent) => Some(
                &ent.pair
                    .state
                    .abstract_device_component_state
                    .abstract_state,
            ),
            Entity::Battery(ent) => Some(
                &ent.pair
                    .state
                    .abstract_device_component_state
                    .abstract_state,
            ),
            Entity::Clock(ent) => Some(
                &ent.pair
                    .state
                    .abstract_device_component_state
                    .abstract_state,
            ),
            Entity::SystemContext(ent) => Some(
                &ent.pair
                    .state
                    .abstract_device_component_state
                    .abstract_state,
            ),
            Entity::SetValueOperation(ent) => {
                Some(&ent.pair.state.abstract_operation_state.abstract_state)
            }
            Entity::SetStringOperation(ent) => {
                Some(&ent.pair.state.abstract_operation_state.abstract_state)
            }
            Entity::SetAlertStateOperation(ent) => {
                Some(&ent.pair.state.abstract_operation_state.abstract_state)
            }
            Entity::SetMetricStateOperation(ent) => {
                Some(&ent.pair.state.abstract_operation_state.abstract_state)
            }
            Entity::SetComponentStateOperation(ent) => {
                Some(&ent.pair.state.abstract_operation_state.abstract_state)
            }
            Entity::SetContextStateOperation(ent) => {
                Some(&ent.pair.state.abstract_operation_state.abstract_state)
            }
            Entity::ActivateOperation(ent) => {
                Some(&ent.pair.state.abstract_operation_state.abstract_state)
            }
            Entity::PatientContext(_) => None,
            Entity::LocationContext(_) => None,
            Entity::EnsembleContext(_) => None,
            Entity::WorkflowContext(_) => None,
            Entity::MeansContext(_) => None,
            Entity::OperatorContext(_) => None,
        }
    }
}

macro_rules! generate_entity_base_single_state {
    ($struct_name:ident) => {
        impl EntityBase for $struct_name {
            fn mds(&self) -> String {
                self.mds.clone()
            }

            fn handle(&self) -> String {
                self.pair.descriptor.handle()
            }

            fn version(&self) -> MdibEntityVersion {
                MdibEntityVersion {
                    descriptor_version: self.pair.descriptor.version(),
                    state_version: MdibEntityStateVersion::SingleStateVersion(
                        self.pair.state.version(),
                    ),
                }
            }
        }
    };
}

macro_rules! generate_entity_base_multi_state {
    ($struct_name:ident) => {
        impl EntityBase for $struct_name {
            fn mds(&self) -> String {
                self.mds.clone()
            }

            fn handle(&self) -> String {
                self.pair.descriptor.handle()
            }

            fn version(&self) -> MdibEntityVersion {
                MdibEntityVersion {
                    descriptor_version: self.pair.descriptor.version(),
                    state_version: MdibEntityStateVersion::MultiStateVersion(
                        self.pair
                            .states
                            .iter()
                            .map(|state| (state.state_handle(), state.version()))
                            .collect(),
                    ),
                }
            }
        }
    };
}

macro_rules! generate_container_common {
    ($struct_name:ident, $descriptor_type:ident, $state_type: ident) => {
        generate_entity_base_single_state!($struct_name);
        generate_into_pair_single!($struct_name, $descriptor_type, $state_type);
    };
}

macro_rules! generate_into_pair_single {
    ($struct_name:ident, $descriptor_type:ident, $state_type: ident) => {
        paste! {
            impl From<$struct_name> for Pair {
                fn from(value: $struct_name) -> Self {
                    Self::SingleStatePair(SingleStatePair::$struct_name(Box::new(value.pair)))
                }
            }
        }
    };
}

macro_rules! generate_into_pair_multi {
    ($struct_name:ident, $descriptor_type:ident, $state_type: ident) => {
        paste! {
            impl From<$struct_name> for Pair {
                fn from(value: $struct_name) -> Self {
                    Pair::MultiStatePair(MultiStatePair::$struct_name(Box::new(value.pair)))
                }
            }
        }
    };
}

/// Generates an entity container with a descriptor and a single state.
macro_rules! generate_container {
    ($struct_name:ident, $descriptor_type:ident, $state_type: ident) => {
        paste! {
            #[allow(dead_code)]
            #[derive(Clone, Debug)]
            pub struct $struct_name {
                pub pair: [<$struct_name Pair>],
                pub parent: String,
                pub mds: String,
            }
        }

        generate_container_common!($struct_name, $descriptor_type, $state_type);
    };
}

/// Generates an entity container with a descriptor and a single state.
macro_rules! generate_container_children {
    ($struct_name:ident, $descriptor_type:ident, $state_type: ident) => {
        generate_container_common!($struct_name, $descriptor_type, $state_type);

        paste! {
            #[allow(dead_code)]
            #[derive(Clone, Debug)]
            pub struct $struct_name {
                pub pair: [<$struct_name Pair>],
                pub parent: String,
                pub children: Vec<String>,
                pub mds: String,
            }
        }
    };
}

/// Generates an entity container with a descriptor and multiple states.
macro_rules! generate_context_container {
    ($struct_name:ident, $descriptor_type:ident, $state_type: ident) => {
        paste! {
            #[allow(dead_code)]
            #[derive(Clone, Debug)]
            pub struct $struct_name {
                pub pair: [<$struct_name Pair>],
                pub parent: String,
                pub mds: String,
            }
        }
        generate_entity_base_multi_state!($struct_name);
        generate_into_pair_multi!($struct_name, $descriptor_type, $state_type);
    };
}

macro_rules! generate_containers {
    { $($field:ident),* } => {
        $(
             paste! {
                generate_container!($field, [<$field Descriptor>], [<$field State>]);
            }
        )*
    }
}

macro_rules! generate_containers_children {
    { $($field:ident),* } => {
        $(
             paste! {
                generate_container_children!($field, [<$field Descriptor>], [<$field State>]);
            }
        )*
    }
}

macro_rules! generate_containers_context {
    { $($field:ident),* } => {
        $(
             paste! {
                generate_context_container!($field, [<$field Descriptor>], [<$field State>]);
            }
        )*
    }
}

// metrics
generate_containers!(
    StringMetric,
    EnumStringMetric,
    NumericMetric,
    RealTimeSampleArrayMetric,
    DistributionSampleArrayMetric
);
// alerts
generate_containers!(AlertCondition, LimitAlertCondition, AlertSignal);
generate_containers_children!(AlertSystem);

// components
generate_containers_children!(Vmd, Channel, Sco, SystemContext);
generate_containers!(Battery, Clock);

// operations
generate_containers!(
    SetValueOperation,
    SetStringOperation,
    SetAlertStateOperation,
    SetMetricStateOperation,
    SetComponentStateOperation,
    SetContextStateOperation,
    ActivateOperation
);

// contexts
generate_containers_context!(
    PatientContext,
    LocationContext,
    EnsembleContext,
    WorkflowContext,
    MeansContext,
    OperatorContext
);

// components
// MDS is the only one without parent
#[allow(dead_code)]
#[derive(Clone, Debug)]
pub struct Mds {
    pub pair: MdsPair,
    pub children: Vec<String>,
}

impl EntityBase for Mds {
    fn mds(&self) -> String {
        self.handle()
    }

    fn handle(&self) -> String {
        self.pair.descriptor.handle()
    }

    fn version(&self) -> MdibEntityVersion {
        MdibEntityVersion {
            descriptor_version: self.pair.descriptor.version(),
            state_version: MdibEntityStateVersion::SingleStateVersion(self.pair.state.version()),
        }
    }
}

impl From<Mds> for Pair {
    fn from(value: Mds) -> Self {
        Self::SingleStatePair(SingleStatePair::Mds(Box::new(value.pair)))
    }
}

#[cfg(test)]
mod tests {
    use protosdc_biceps::biceps::{
        AbstractDescriptor, AbstractMetricDescriptor, AbstractMetricState, AbstractState,
        StringMetricDescriptor, StringMetricState,
    };

    use crate::common::biceps_util::{Descriptor, State};
    use crate::common::mdib_entity::{AlertSystem, Entity, EntityBase, StringMetric};
    use crate::common::mdib_pair::{AlertSystemPair, StringMetricPair};
    use crate::test_util::mdib_utils;

    #[test]
    fn test_string_metric_eq() {
        let desc = mdib_utils::create_minimal_string_metric_descriptor("eeh");
        let desc2 = desc.clone();

        assert_eq!(desc, desc2)
    }

    #[test]
    fn test_string_metric_neq() {
        let handle1 = "handle1";
        let handle2 = "handle2";
        let desc = mdib_utils::create_minimal_string_metric_descriptor(handle1);
        let desc2 = mdib_utils::create_minimal_string_metric_descriptor(handle2);

        assert_ne!(desc, desc2)
    }

    #[test]
    fn test_string_metric_handle() {
        let descriptor_handle = "TestHandle";
        let descriptor_version: u64 = 1333336;

        let orig_desc = mdib_utils::create_minimal_string_metric_descriptor(descriptor_handle);
        let orig_state = mdib_utils::create_minimal_string_metric_state(descriptor_handle);

        let desc = StringMetricDescriptor {
            abstract_metric_descriptor: AbstractMetricDescriptor {
                abstract_descriptor: AbstractDescriptor {
                    descriptor_version_attr: mdib_utils::create_version_counter(Some(
                        descriptor_version,
                    )),
                    ..orig_desc.abstract_metric_descriptor.abstract_descriptor
                },
                ..orig_desc.abstract_metric_descriptor
            },
        };

        let state = StringMetricState {
            abstract_metric_state: AbstractMetricState {
                abstract_state: AbstractState {
                    descriptor_version_attr: mdib_utils::create_referenced_version(Some(
                        descriptor_version,
                    )),
                    ..orig_state.abstract_metric_state.abstract_state
                },
                ..orig_state.abstract_metric_state
            },
            ..orig_state
        };

        let string_metric = StringMetric {
            pair: StringMetricPair {
                descriptor: desc,
                state,
            },
            parent: "whatever".to_string(),
            mds: "some_mds".to_string(),
        };

        assert_eq!(descriptor_handle, string_metric.handle());
        assert_eq!(descriptor_version, string_metric.pair.descriptor.version());
        assert_eq!(
            descriptor_version,
            string_metric.pair.state.descriptor_version()
        );

        let et = Entity::StringMetric(Box::new(string_metric));

        assert_eq!(descriptor_handle, et.handle());
    }

    #[test]
    fn test_alert_system_has_children() {
        let handle = "AS_Handle";
        let desc = mdib_utils::create_minimal_alert_system_descriptor(handle);
        let state = mdib_utils::create_minimal_alert_system_state(handle);

        let children = vec!["child1".to_string(), "child2".to_string()];

        let mut asy = AlertSystem {
            pair: AlertSystemPair {
                descriptor: desc,
                state,
            },
            parent: "yolo".to_string(),
            children: children.clone(),
            mds: "some_mds".to_string(),
        };

        assert_eq!(children, asy.children);

        asy.children.reverse();
        assert_ne!(children, asy.children);
    }
}
