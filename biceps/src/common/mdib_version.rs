use std::cmp::Ordering;
use std::fmt::{Display, Formatter};

use protosdc_biceps::biceps::{MdibVersionGroup, VersionCounter};
use protosdc_biceps::types::ProtoUri;

/// Implied MdibVersionGroup/@MdibVersion according to IEEE 11073-10207:2017
const IMPLIED_VERSION: u64 = 0;

/// This violates IEEE 11073-10207:2017 as there is no implied InstanceId. However this is
/// definitely an oversight.
/// TODO: Review this for new BICEPS revision.
const IMPLIED_INSTANCE_ID: u64 = 0;

#[derive(Clone, Debug, PartialEq)]
pub struct MdibVersion {
    pub sequence_id: String,
    pub instance_id: u64,
    pub version: u64,
}

impl MdibVersion {
    /// Compares two MdibVersions.
    ///
    /// As the MDIB version is a triple consisting of at least one non-trivially comparable data type,
    /// it cannot be compared using the usual [cmp](std::cmp).
    ///
    /// Instead, there are rules that define what to expect, depending on the data of the triple.
    /// Since there are triples which simply cannot be ordered, an Optional ordering is returned.
    ///
    /// The ruleset is:
    /// - None if not comparable
    /// - [Less](Ordering::Less) if
    ///   - `self.instance_id < other.instance_id`
    ///   - `self.instance_id == other.instance_id && self.version < other.version`
    /// - [Equal](Ordering::Equal) if
    ///   - `self.instance_id == other.instance_id && self.version == other.version`
    /// - [Equal](Ordering::Greater) if
    ///   - `self.instance_id > other.instance_id`
    ///   - `self.instance_id == other.instance_id && self.version > other.version`
    pub fn cmp_to(&self, other: &MdibVersion) -> Option<Ordering> {
        if self.sequence_id != other.sequence_id {
            // not comparable
            return None;
        }

        Some(match &self.instance_id.cmp(&other.instance_id) {
            Ordering::Less => Ordering::Less,
            Ordering::Greater => Ordering::Greater,
            Ordering::Equal => self.version.cmp(&other.version),
        })
    }

    /// Creates a copy of the current MdibVersion with version incremented by one.
    pub(crate) fn inc(&self) -> MdibVersion {
        MdibVersion {
            version: &self.version + 1,
            ..self.clone()
        }
    }
}

impl Display for MdibVersion {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Mdib(version={}, sequence={}, instance={})",
            self.version, self.sequence_id, self.instance_id
        )
    }
}

impl From<MdibVersion> for MdibVersionGroup {
    fn from(value: MdibVersion) -> Self {
        MdibVersionGroup {
            mdib_version_attr: Some(VersionCounter {
                unsigned_long: value.version,
            }),
            sequence_id_attr: ProtoUri {
                uri: value.sequence_id,
            },
            instance_id_attr: Some(value.instance_id),
        }
    }
}

impl From<&MdibVersion> for MdibVersionGroup {
    fn from(value: &MdibVersion) -> Self {
        MdibVersionGroup {
            mdib_version_attr: Some(VersionCounter {
                unsigned_long: value.version,
            }),
            sequence_id_attr: ProtoUri {
                uri: value.sequence_id.clone(),
            },
            instance_id_attr: Some(value.instance_id),
        }
    }
}

impl From<MdibVersionGroup> for MdibVersion {
    fn from(group: MdibVersionGroup) -> Self {
        MdibVersion {
            sequence_id: group.sequence_id_attr.uri,
            instance_id: group.instance_id_attr.unwrap_or(0),
            version: group.mdib_version_attr.map_or(0, |it| it.unsigned_long),
        }
    }
}

pub struct RequiresSequenceId {}
pub struct Buildable {
    sequence_id: String,
    instance_id: Option<u64>,
    version: Option<u64>,
}

/// Simple builder to create [MdibVersion].
#[derive(Default)]
pub struct MdibVersionBuilder<State>(State);

impl Default for MdibVersionBuilder<RequiresSequenceId> {
    fn default() -> Self {
        Self(RequiresSequenceId {})
    }
}

impl MdibVersionBuilder<RequiresSequenceId> {
    /// Sets the sequence id in the builder.
    pub fn seq(self, url: impl Into<String>) -> MdibVersionBuilder<Buildable> {
        MdibVersionBuilder(Buildable {
            sequence_id: url.into(),
            instance_id: None,
            version: None,
        })
    }
}

impl MdibVersionBuilder<Buildable> {
    /// Sets the instance id in the builder.
    pub fn ins(self, arg: u64) -> MdibVersionBuilder<Buildable> {
        Self(Buildable {
            instance_id: Some(arg),
            ..self.0
        })
    }

    /// Sets the version in the builder.
    pub fn ver(self, arg: u64) -> MdibVersionBuilder<Buildable> {
        Self(Buildable {
            version: Some(arg),
            ..self.0
        })
    }

    /// Creates a new [MdibVersion].
    ///
    /// Default values for instance and version are [IMPLIED_INSTANCE_ID] and [IMPLIED_VERSION]
    /// respectively
    pub fn create(self) -> MdibVersion {
        MdibVersion {
            sequence_id: self.0.sequence_id, // must be present
            instance_id: self.0.instance_id.unwrap_or(IMPLIED_INSTANCE_ID),
            version: self.0.version.unwrap_or(IMPLIED_VERSION),
        }
    }
}

#[cfg(test)]
mod tests {
    use std::cmp::Ordering;

    use anyhow::Result;

    use crate::common::mdib_version::{MdibVersionBuilder, IMPLIED_INSTANCE_ID, IMPLIED_VERSION};

    const SOME_SEQUENCE_ID: &str = "urn:string:SomeExampleInstanceId";

    fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[test]
    fn test_create_version() -> Result<()> {
        init();

        let expected_version = 2;
        let expected_sequence = "wat:Yolo".to_string();
        let expected_instance = 1333333337;

        let ver = MdibVersionBuilder::default()
            .seq(expected_sequence.clone())
            .ver(expected_version)
            .ins(expected_instance)
            .create();

        assert_eq!(expected_version, ver.version);
        assert_eq!(expected_sequence, ver.sequence_id);
        assert_eq!(expected_instance, ver.instance_id);
        Ok(())
    }

    #[test]
    fn test_implied_version() -> Result<()> {
        init();

        let expected_sequence = "wat:TheExpectedVersion".to_string();

        let ver = MdibVersionBuilder::default()
            .seq(expected_sequence.clone())
            .create();

        assert_eq!(expected_sequence, ver.sequence_id);
        assert_eq!(IMPLIED_VERSION, ver.version);
        assert_eq!(IMPLIED_INSTANCE_ID, ver.instance_id);
        Ok(())
    }

    #[test]
    fn test_comparisons_equal() {
        init();

        let ver1 = MdibVersionBuilder::default().seq(SOME_SEQUENCE_ID).create();

        let ver2 = MdibVersionBuilder::default().seq(SOME_SEQUENCE_ID).create();

        assert_eq!(Some(Ordering::Equal), ver1.cmp_to(&ver2))
    }

    #[test]
    fn test_comparisons_not_comparable() -> Result<()> {
        init();

        let ver1 = MdibVersionBuilder::default().seq(SOME_SEQUENCE_ID).create();

        let ver2 = MdibVersionBuilder::default().seq("wat:Whatever").create();

        assert_eq!(None, ver1.cmp_to(&ver2));
        Ok(())
    }

    #[test]
    fn test_comparisons_less() {
        init();

        {
            let ver1 = MdibVersionBuilder::default()
                .seq(SOME_SEQUENCE_ID)
                .ver(2)
                .create();

            let ver2 = MdibVersionBuilder::default()
                .seq(SOME_SEQUENCE_ID)
                .ver(3)
                .create();

            assert_eq!(Some(Ordering::Less), ver1.cmp_to(&ver2))
        }
        {
            let ver1 = MdibVersionBuilder::default()
                .seq(SOME_SEQUENCE_ID)
                .ins(2)
                .create();

            let ver2 = MdibVersionBuilder::default()
                .seq(SOME_SEQUENCE_ID)
                .ins(4)
                .create();

            assert_eq!(Some(Ordering::Less), ver1.cmp_to(&ver2))
        }
        {
            let ver1 = MdibVersionBuilder::default()
                .seq(SOME_SEQUENCE_ID)
                .ins(2)
                .ver(4)
                .create();

            let ver2 = MdibVersionBuilder::default()
                .seq(SOME_SEQUENCE_ID)
                .ins(4)
                .ver(1)
                .create();

            assert_eq!(Some(Ordering::Less), ver1.cmp_to(&ver2))
        }
    }

    #[test]
    fn test_comparisons_greater() {
        init();

        {
            let ver1 = MdibVersionBuilder::default()
                .seq(SOME_SEQUENCE_ID)
                .ver(4)
                .create();

            let ver2 = MdibVersionBuilder::default()
                .seq(SOME_SEQUENCE_ID)
                .ver(3)
                .create();

            assert_eq!(Some(Ordering::Greater), ver1.cmp_to(&ver2))
        }
        {
            let ver1 = MdibVersionBuilder::default()
                .seq(SOME_SEQUENCE_ID)
                .ins(5)
                .create();

            let ver2 = MdibVersionBuilder::default()
                .seq(SOME_SEQUENCE_ID)
                .ins(4)
                .create();

            assert_eq!(Some(Ordering::Greater), ver1.cmp_to(&ver2))
        }
        {
            let ver1 = MdibVersionBuilder::default()
                .seq(SOME_SEQUENCE_ID)
                .ins(5)
                .ver(1)
                .create();

            let ver2 = MdibVersionBuilder::default()
                .seq(SOME_SEQUENCE_ID)
                .ins(4)
                .ver(2)
                .create();

            assert_eq!(Some(Ordering::Greater), ver1.cmp_to(&ver2))
        }
    }

    #[test]
    fn test_inc() {
        init();

        let ver1 = MdibVersionBuilder::default().seq(SOME_SEQUENCE_ID).create();

        let ver2 = ver1.inc();

        assert_eq!(ver1.instance_id, ver2.instance_id);
        assert_eq!(ver1.sequence_id, ver2.sequence_id);
        assert_eq!(ver1.version + 1, ver2.version)
    }
}
