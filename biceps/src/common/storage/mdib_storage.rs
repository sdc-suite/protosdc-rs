use std::collections::HashMap;

use log::{debug, error, log_enabled, Level};
use thiserror::Error;

pub use protosdc_biceps::biceps::AbstractDescriptorOneOf;
use protosdc_biceps::biceps::{
    context_association_mod, AbstractAlertStateOneOf, AbstractContextStateOneOf,
    AbstractDeviceComponentStateOneOf, AbstractMetricStateOneOf, AbstractOperationStateOneOf,
    AbstractStateOneOf, ContextAssociation, RealTimeSampleArrayMetricState,
};

use crate::common::biceps_util::State;
use crate::common::biceps_util::{ContextState, Descriptor, MultiState};
use crate::common::mdib_description_modification::MdibDescriptionModification;
use crate::common::mdib_description_modifications::{
    MdibDescriptionModifications, ModificationError,
};
pub use crate::common::mdib_entity::{Entity, MdibEntity};
use crate::common::mdib_entity::{EntityBase, MdibEntityVersion};
use crate::common::mdib_entity_util;
use crate::common::mdib_entity_util::CreationError;
use crate::common::mdib_state_modifications::{
    MdibStateModificationResults, MdibStateModifications,
};
use crate::common::mdib_version::{MdibVersion, MdibVersionBuilder};
use crate::common::preprocessing::PreprocessingError;

pub struct WriteDescriptionResult {
    pub mdib_version: MdibVersion,
    pub inserted: Vec<MdibEntity>,
    pub updated: Vec<MdibEntity>,
    pub deleted: Vec<MdibEntity>,
}

pub struct WriteStateResult {
    pub mdib_version: MdibVersion,
    pub states: MdibStateModificationResults,
}

#[derive(Debug, PartialEq, Error, Eq)]
pub enum WriteError {
    #[error("A parent handle is required for entity with handle {handle:?}")]
    ParentHandleRequired { handle: String },
    #[error("Parent entity {parent:?} for entity {handle:?} is missing")]
    ParentEntityMissing { handle: String, parent: String },
    #[error("Required entity {handle:?} is missing")]
    EntityMissing { handle: String },
    #[error("Parent entity {parent} didn't list child {child} while updating child, inconsistent")]
    ParentEntityDoesntListChild { parent: String, child: String },
    #[error(transparent)]
    MdibEntityError(#[from] CreationError),
    #[error(transparent)]
    PreprocessingError(#[from] PreprocessingError),
    #[error(transparent)]
    ModificationError(#[from] ModificationError),
    #[error("An unknown error occurred")]
    Unknown,
}

const ASSOCIATED: Option<ContextAssociation> = Some(ContextAssociation {
    enum_type: context_association_mod::EnumType::Assoc,
});
const NOT_ASSOCIATED: Option<ContextAssociation> = Some(ContextAssociation {
    enum_type: context_association_mod::EnumType::No,
});

/// Filter rule to filter [MdibEntity] by a single [Entity] type
#[macro_export]
macro_rules! entity_filter {
    ($struct_name:ident) => {{
        |ent: &$crate::common::mdib_entity::MdibEntity| match ent.entity {
            $crate::common::mdib_entity::Entity::$struct_name(_) => true,
            _ => false,
        }
    }};
}

/// Filter rule to filter [MdibEntity] by multiple [Entity] types
#[macro_export]
macro_rules! entity_filter_multi {
        ($($struct_name:ident),+) => {{
|ent: &$crate::common::mdib_entity::MdibEntity| {
    match ent.entity {
        $(
        $crate::common::mdib_entity::Entity::$struct_name(_) => true,
        )+
        _ => false
    }
}
    }}
}

/// Filter rule to filter [MdibEntity] by the type of the [Entity]'s [AbstractDescriptorOneOf] type
#[macro_export]
macro_rules! entity_filter_by_descriptor {
        ($desc:expr) => {
        match $desc {
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::AlertConditionDescriptor(_) => $crate::entity_filter!(AlertCondition),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::LimitAlertConditionDescriptor(_) => $crate::entity_filter!(LimitAlertCondition),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::AlertSignalDescriptor(_) => $crate::entity_filter!(AlertSignal),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::AlertSystemDescriptor(_) => $crate::entity_filter!(AlertSystem),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::ClockDescriptor(_) => $crate::entity_filter!(Clock),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::BatteryDescriptor(_) => $crate::entity_filter!(Battery),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::ChannelDescriptor(_) => $crate::entity_filter!(Channel),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::ScoDescriptor(_) => $crate::entity_filter!(Sco),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::SystemContextDescriptor(_) => $crate::entity_filter!(SystemContext),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::VmdDescriptor(_) => $crate::entity_filter!(Vmd),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::MdsDescriptor(_) => $crate::entity_filter!(Mds),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::SetStringOperationDescriptor(_) => $crate::entity_filter!(SetStringOperation),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::SetComponentStateOperationDescriptor(_) => $crate::entity_filter!(SetComponentStateOperation),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::SetAlertStateOperationDescriptor(_) => $crate::entity_filter!(SetAlertStateOperation),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::SetMetricStateOperationDescriptor(_) => $crate::entity_filter!(SetMetricStateOperation),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::SetContextStateOperationDescriptor(_) => $crate::entity_filter!(SetContextStateOperation),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::ActivateOperationDescriptor(_) => $crate::entity_filter!(ActivateOperation),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::SetValueOperationDescriptor(_) => $crate::entity_filter!(SetValueOperation),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::NumericMetricDescriptor(_) => $crate::entity_filter!(NumericMetric),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::DistributionSampleArrayMetricDescriptor(_) => $crate::entity_filter!(DistributionSampleArrayMetric),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::RealTimeSampleArrayMetricDescriptor(_) => $crate::entity_filter!(RealTimeSampleArrayMetric),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::StringMetricDescriptor(_) => $crate::entity_filter!(StringMetric),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::EnumStringMetricDescriptor(_) => $crate::entity_filter!(EnumStringMetric),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::MeansContextDescriptor(_) => $crate::entity_filter!(MeansContext),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::WorkflowContextDescriptor(_) => $crate::entity_filter!(WorkflowContext),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::PatientContextDescriptor(_) => $crate::entity_filter!(PatientContext),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::OperatorContextDescriptor(_) => $crate::entity_filter!(OperatorContext),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::EnsembleContextDescriptor(_) => $crate::entity_filter!(EnsembleContext),
            ::protosdc_biceps::biceps::AbstractDescriptorOneOf::LocationContextDescriptor(_) => $crate::entity_filter!(LocationContext),
            // no entities for abstract stuff!
            _ => |_: &$crate::common::mdib_entity::MdibEntity| {false},
        }
    }
}

/// Filter rule to filter [MdibEntity] by the type of the [Entity] [Pair] type
#[macro_export]
macro_rules! entity_filter_by_pair {
    ($desc:expr) => {
        match $desc {
            $crate::common::mdib_pair::Pair::SingleStatePair(pair2) => {
                $crate::entity_filter_by_single_state_pair!(pair2)
            }
            $crate::common::mdib_pair::Pair::MultiStatePair(pair2) => {
                $crate::entity_filter_by_multi_state_pair!(pair2)
            }
        }
    };
}

/// Filter rule to filter [MdibEntity] by the type of the [Entity] [SingleStatePair] type
#[macro_export]
macro_rules! entity_filter_by_single_state_pair {
    ($desc:expr) => {
        match $desc {
            $crate::common::mdib_pair::SingleStatePair::AlertCondition { .. } => {
                $crate::entity_filter!(AlertCondition)
            }
            $crate::common::mdib_pair::SingleStatePair::LimitAlertCondition { .. } => {
                $crate::entity_filter!(LimitAlertCondition)
            }
            $crate::common::mdib_pair::SingleStatePair::AlertSignal { .. } => {
                $crate::entity_filter!(AlertSignal)
            }
            $crate::common::mdib_pair::SingleStatePair::AlertSystem { .. } => {
                $crate::entity_filter!(AlertSystem)
            }
            $crate::common::mdib_pair::SingleStatePair::Clock { .. } => {
                $crate::entity_filter!(Clock)
            }
            $crate::common::mdib_pair::SingleStatePair::Battery { .. } => {
                $crate::entity_filter!(Battery)
            }
            $crate::common::mdib_pair::SingleStatePair::Channel { .. } => {
                $crate::entity_filter!(Channel)
            }
            $crate::common::mdib_pair::SingleStatePair::Sco { .. } => $crate::entity_filter!(Sco),
            $crate::common::mdib_pair::SingleStatePair::SystemContext { .. } => {
                $crate::entity_filter!(SystemContext)
            }
            $crate::common::mdib_pair::SingleStatePair::Vmd { .. } => $crate::entity_filter!(Vmd),
            $crate::common::mdib_pair::SingleStatePair::Mds { .. } => $crate::entity_filter!(Mds),
            $crate::common::mdib_pair::SingleStatePair::SetStringOperation { .. } => {
                $crate::entity_filter!(SetStringOperation)
            }
            $crate::common::mdib_pair::SingleStatePair::SetComponentStateOperation { .. } => {
                $crate::entity_filter!(SetComponentStateOperation)
            }
            $crate::common::mdib_pair::SingleStatePair::SetAlertStateOperation { .. } => {
                $crate::entity_filter!(SetAlertStateOperation)
            }
            $crate::common::mdib_pair::SingleStatePair::SetMetricStateOperation { .. } => {
                $crate::entity_filter!(SetMetricStateOperation)
            }
            $crate::common::mdib_pair::SingleStatePair::SetContextStateOperation { .. } => {
                $crate::entity_filter!(SetContextStateOperation)
            }
            $crate::common::mdib_pair::SingleStatePair::ActivateOperation { .. } => {
                $crate::entity_filter!(ActivateOperation)
            }
            $crate::common::mdib_pair::SingleStatePair::SetValueOperation { .. } => {
                $crate::entity_filter!(SetValueOperation)
            }
            $crate::common::mdib_pair::SingleStatePair::NumericMetric { .. } => {
                $crate::entity_filter!(NumericMetric)
            }
            $crate::common::mdib_pair::SingleStatePair::DistributionSampleArrayMetric {
                ..
            } => {
                $crate::entity_filter!(DistributionSampleArrayMetric)
            }
            $crate::common::mdib_pair::SingleStatePair::RealTimeSampleArrayMetric { .. } => {
                $crate::entity_filter!(RealTimeSampleArrayMetric)
            }
            $crate::common::mdib_pair::SingleStatePair::StringMetric { .. } => {
                $crate::entity_filter!(StringMetric)
            }
            $crate::common::mdib_pair::SingleStatePair::EnumStringMetric { .. } => {
                $crate::entity_filter!(EnumStringMetric)
            }
        }
    };
}

/// Filter rule to filter [MdibEntity] by the type of the [Entity] [MultiStatePair] type
#[macro_export]
macro_rules! entity_filter_by_multi_state_pair {
    ($desc:expr) => {
        match $desc {
            $crate::common::mdib_pair::MultiStatePair::MeansContext { .. } => {
                $crate::entity_filter!(MeansContext)
            }
            $crate::common::mdib_pair::MultiStatePair::WorkflowContext { .. } => {
                $crate::entity_filter!(WorkflowContext)
            }
            $crate::common::mdib_pair::MultiStatePair::PatientContext { .. } => {
                $crate::entity_filter!(PatientContext)
            }
            $crate::common::mdib_pair::MultiStatePair::OperatorContext { .. } => {
                $crate::entity_filter!(OperatorContext)
            }
            $crate::common::mdib_pair::MultiStatePair::EnsembleContext { .. } => {
                $crate::entity_filter!(EnsembleContext)
            }
            $crate::common::mdib_pair::MultiStatePair::LocationContext { .. } => {
                $crate::entity_filter!(LocationContext)
            }
        }
    };
}

pub trait MdibStorageConstructor: MdibStorage {
    /// Creates a new empty MdibStorage with default parameters.
    ///
    /// MdStateVersion = 0
    /// MdDescriptionVersion = 0
    ///
    /// # Arguments
    ///
    /// * `mdib_version`: initial version of the mdib in the storage
    /// * `remove_not_associated_context_states`: whether to remove not associated states instead
    ///                                           of keeping them in storage
    ///
    /// returns: MdibStorage
    ///
    /// # Examples
    ///
    /// ```
    ///  # use biceps::common::storage::mdib_storage::{MdibStorageConstructor, MdibStorageImpl};
    ///  # use biceps::common::mdib_version::MdibVersionBuilder;
    ///  let ver = MdibVersionBuilder::default()
    ///      .seq("prefix:sequence_id")
    ///      .create();
    ///  MdibStorageImpl::new_from_mdib_version(ver, true);
    /// ```
    fn new_from_mdib_version(
        mdib_version: MdibVersion,
        remove_not_associated_context_states: bool,
    ) -> Self;

    /// Creates a new empty MdibStorage with default parameters.
    ///
    /// MdStateVersion = 0
    /// MdDescriptionVersion = 0
    ///
    /// # Arguments
    ///
    /// * `mdib_version`: initial version of the mdib in the storage
    /// * `remove_not_associated_context_states`: see [Self::new_from_mdib_version]
    ///
    /// # Examples
    ///
    /// ```
    ///  # use biceps::common::storage::mdib_storage::{MdibStorageConstructor, MdibStorageImpl};
    ///  MdibStorageImpl::new("prefix:whatever", false);
    /// ```
    fn new(sequence_id: impl Into<String>, remove_not_associated_context_states: bool) -> Self;
}

/// HashMap-based access to [`MdibEntity`] instances.
///
/// The trait provides read and write access. Note that MdibStorage is not
///  - meant to be thread-safe
///  - performing MDIB consistency checks. Bring your own validation.
pub trait MdibStorage {
    /// The latest stored MDIB version.
    fn mdib_version(&self) -> MdibVersion;

    /// The latest stored medical device description version.
    fn md_description_version(&self) -> u64;

    /// The latest stored medical device state version.
    fn md_state_version(&self) -> u64;

    /// Returns an entity for the given handle, if available.
    ///
    /// # Arguments
    ///
    /// * `handle`: the descriptor handle to retrieve the entity for
    fn entity(&self, handle: &str) -> Option<MdibEntity>;

    /// Returns an entity for the given handle, if available.
    ///
    /// # Arguments
    ///
    /// * `handle`: the descriptor handle to retrieve the entity for
    fn entity_version(&self, handle: &str) -> Option<MdibEntityVersion>;

    /// Returns all available root entities (i.e. Mds).
    fn root_entities(&self) -> Vec<MdibEntity>;

    /// Returns a context state for the given handle, if available.
    ///
    /// # Arguments
    ///
    /// * `handle`: the state handle of the context state to retrieve
    fn context_state(&self, handle: &str) -> Option<AbstractContextStateOneOf>;

    /// Returns all available context states.
    fn context_states(&self) -> Vec<AbstractContextStateOneOf>;

    /// Finds all entities matching the given type filter.
    ///
    /// # Arguments
    ///
    /// * `filter`: filter to execute for search, see [`crate::entity_filter`]
    fn entities_by_type<F: 'static + Fn(&MdibEntity) -> bool>(&self, filter: F) -> Vec<MdibEntity>;

    /// Finds all child entities for a handle matching the given type filter.
    ///
    /// # Arguments
    ///
    /// * `handle`: handle of the parent descriptor
    /// * `filter`: filter to execute for search, see [`crate::entity_filter`]
    fn children_by_type<F: 'static + Fn(&MdibEntity) -> bool>(
        &self,
        handle: &str,
        filter: F,
    ) -> Vec<MdibEntity>;

    /// Applies the given description modifications to the storage.
    ///
    /// # Arguments
    ///
    /// * `mdib_version`: new version to be stored
    /// * `md_description_version`: new description version
    /// * `md_state_version`: new state version
    /// * `description_modifications`: modifications to be applied to the storage
    fn apply_description(
        &mut self,
        mdib_version: MdibVersion,
        md_description_version: Option<u64>,
        md_state_version: Option<u64>,
        description_modifications: MdibDescriptionModifications,
    ) -> Result<WriteDescriptionResult, WriteError>;

    /// Applies the given state modifications to the storage.
    ///
    /// # Arguments
    ///
    /// * `mdib_version`: new version to be stored
    /// * `md_state_version`: new state version
    /// * `state_modifications`: modifications to be applied to the storage
    fn apply_state(
        &mut self,
        mdib_version: MdibVersion,
        md_state_version: Option<u64>,
        state_modifications: MdibStateModifications,
    ) -> Result<WriteStateResult, WriteError>;
}

#[derive(Debug)]
pub struct MdibStorageImpl {
    mdib_version: MdibVersion,
    md_description_version: u64,
    md_state_version: u64,
    entities: HashMap<String, MdibEntity>,
    context_map: HashMap<String, String>,
    remove_not_associated_context_states: bool,
}

macro_rules! entity_arm {
    ($enum_type:ident, $enum_field:ident, $state_type:ident, $self:ident) => {{
        let mut updated = HashMap::<String, Vec<$state_type>>::new();

        for state in $enum_field {
            let mdib_entity = match $self.entities.remove(&state.descriptor_handle()) {
                None => {
                    return Err(WriteError::EntityMissing {
                        handle: state.descriptor_handle(),
                    })
                }
                Some(ent) => ent,
            };

            let new_entity = mdib_entity_util::replace_states(
                mdib_entity,
                vec![state.clone().into_abstract_state_one_of()],
            )?;

            let vec = updated.entry(new_entity.entity.mds()).or_default();
            vec.push(state);

            $self
                .entities
                .insert(new_entity.entity.handle(), new_entity);
        }

        Ok(WriteStateResult {
            mdib_version: $self.mdib_version.clone(),
            states: MdibStateModificationResults::$enum_type {
                $enum_field: updated,
            },
        })
    }};
}

impl MdibStorageImpl {
    // removes all not associated context types from storage if remove_not_associated_context_states was set
    fn remove_not_associated_contexts(
        &self,
        states: Vec<AbstractStateOneOf>,
    ) -> Vec<AbstractStateOneOf> {
        if !self.remove_not_associated_context_states {
            return states;
        }

        states
            .into_iter()
            .filter(|state| match state {
                AbstractStateOneOf::OperatorContextState(s) => {
                    s.context_association() == ASSOCIATED
                }
                AbstractStateOneOf::EnsembleContextState(s) => {
                    s.context_association() == ASSOCIATED
                }
                AbstractStateOneOf::WorkflowContextState(s) => {
                    s.context_association() == ASSOCIATED
                }
                AbstractStateOneOf::PatientContextState(s) => s.context_association() == ASSOCIATED,
                AbstractStateOneOf::LocationContextState(s) => {
                    s.context_association() == ASSOCIATED
                }
                AbstractStateOneOf::MeansContextState(s) => s.context_association() == ASSOCIATED,
                _ => true,
            })
            .collect()
    }

    fn find_parent_mds(
        &self,
        inserted_entities: &[MdibEntity],
        parent_handle: String,
        new_entity_handle: String,
    ) -> Result<String, WriteError> {
        // find parent entity
        let parent_entity = match inserted_entities
            .iter()
            .find(|it| it.entity.handle() == parent_handle)
        {
            Some(inserted_entity) => Some(inserted_entity),
            None => self.entities.get(&parent_handle),
        }
        .ok_or(WriteError::ParentEntityMissing {
            parent: parent_handle,
            handle: new_entity_handle.clone(),
        })?;

        match &parent_entity.entity {
            Entity::Mds(mds) => Ok(mds.handle()),
            other => match parent_entity.resolve_parent() {
                None => Err(WriteError::ParentHandleRequired {
                    handle: other.handle(),
                }),
                Some(grand_parent_handle) => {
                    self.find_parent_mds(inserted_entities, grand_parent_handle, new_entity_handle)
                }
            },
        }
    }

    fn insert_entity(
        &mut self,
        parent: Option<String>,
        descriptor: AbstractDescriptorOneOf,
        states: Vec<AbstractStateOneOf>,
        inserted_entities: &mut Vec<MdibEntity>,
    ) -> Result<(), WriteError> {
        // find parent mds for entity
        let mds_handle = match &descriptor {
            AbstractDescriptorOneOf::MdsDescriptor(mds_descriptor) => mds_descriptor.handle(),
            _ => {
                // traverse parents until mds is found
                self.find_parent_mds(
                    inserted_entities,
                    parent.clone().ok_or(WriteError::ParentHandleRequired {
                        handle: descriptor.handle(),
                    })?,
                    descriptor.handle(),
                )?
            }
        };

        let entity_storage = match mdib_entity_util::create_mdib_entity(
            descriptor.clone(),
            states,
            self.mdib_version.clone(),
            parent.clone(),
            vec![],
            mds_handle,
        ) {
            Ok(x) => x,
            Err(x) => return Err(WriteError::MdibEntityError(x)),
        };

        match parent {
            None => {
                if let AbstractDescriptorOneOf::MdsDescriptor(_) = descriptor {
                } else {
                    return Err(WriteError::ParentHandleRequired {
                        handle: descriptor.handle(),
                    });
                }
            }
            Some(p) => match self.entities.remove_entry(&p) {
                None => {
                    return Err(WriteError::ParentEntityMissing {
                        handle: descriptor.handle(),
                        parent: p,
                    });
                }
                Some((key, parent_entity)) => {
                    if let Some(mut children) = parent_entity.resolve_children() {
                        children.push(entity_storage.entity.handle());
                        self.entities.insert(
                            key,
                            mdib_entity_util::replace_children(parent_entity, children),
                        );
                    }
                }
            },
        };

        // add clone to inserted
        let entity_for_tracking = entity_storage.clone();
        debug!("Inserting entity: {:?}", &entity_storage);
        self.entities
            .insert(entity_storage.entity.handle(), entity_storage);

        // Update context handle to descriptor handle map
        entity_for_tracking
            .resolve_context_states()
            .iter()
            .for_each(|it| {
                self.context_map
                    .insert(it.state_handle(), entity_for_tracking.entity.handle());
            });

        inserted_entities.push(entity_for_tracking);

        Ok(())
    }

    fn delete_entity(
        &mut self,
        modification: MdibDescriptionModification,
        deleted_entities: &mut Vec<MdibEntity>,
    ) -> Result<(), WriteError> {
        let handle = modification.descriptor_handle();

        let entity_to_delete = match self.entities.get(&handle) {
            None => return Err(WriteError::EntityMissing { handle }),
            Some(e) => e,
        };

        // update parent
        match entity_to_delete.resolve_parent() {
            None => {}
            Some(parent_handle) => {
                if let Some(parent_entity_to_update) = self.entities.get(&parent_handle) {
                    let updated_children = match parent_entity_to_update.resolve_children() {
                        None => {
                            return Err(WriteError::ParentEntityDoesntListChild {
                                parent: parent_handle,
                                child: entity_to_delete.entity.get_abstract_descriptor().handle(),
                            })
                        }
                        Some(children) => children.into_iter().filter(|it| it != &handle).collect(),
                    };

                    let updated_entity = mdib_entity_util::replace_children(
                        parent_entity_to_update.clone(),
                        updated_children,
                    );
                    debug!(
                        "Updating parent entity with handle {}, removing {} from children",
                        parent_handle, handle
                    );
                    self.entities.insert(parent_handle, updated_entity);
                } else {
                    error!(
                        "Parent entity with handle {} for deleted child {} missing",
                        parent_handle, handle
                    );
                    return Err(WriteError::EntityMissing {
                        handle: parent_handle,
                    });
                }
            }
        }

        let removed_entity = match self.entities.remove(&handle) {
            None => return Err(WriteError::EntityMissing { handle }),
            Some(entity) => entity,
        };

        // remove any lingering contexts
        removed_entity
            .resolve_context_states()
            .iter()
            .for_each(|state| {
                debug!("Removing context state {} from map", &state.state_handle());
                self.context_map.remove(&state.state_handle());
            });

        debug!(
            "Removed entity with handle {}",
            removed_entity.entity.handle()
        );
        deleted_entities.push(removed_entity);

        Ok(())
    }

    fn update_entity(
        &mut self,
        descriptor: AbstractDescriptorOneOf,
        states: Vec<AbstractStateOneOf>,
        updated_entities: &mut Vec<MdibEntity>,
    ) -> Result<(), WriteError> {
        let handle = descriptor.handle();
        let entity_to_update = match self.entities.remove(&handle) {
            None => return Err(WriteError::EntityMissing { handle }),
            Some(e) => e,
        };

        let updated_entity =
            mdib_entity_util::replace_descriptor_and_states(entity_to_update, descriptor, states)?;

        // Update context states, we might've dropped some here
        self.update_context_map(&updated_entity.resolve_context_states());

        updated_entities.push(updated_entity.clone());
        debug!("Updated entity {}", handle);
        self.entities.insert(handle, updated_entity);

        Ok(())
    }

    fn update_context_map(&mut self, states: &[AbstractContextStateOneOf]) {
        for state in states {
            if self.remove_not_associated_context_states
                && (state.context_association().is_none()
                    || state.context_association() == NOT_ASSOCIATED)
            {
                self.context_map.remove(&state.state_handle());
            }
        }
    }
}

impl MdibStorageConstructor for MdibStorageImpl {
    fn new_from_mdib_version(
        mdib_version: MdibVersion,
        remove_not_associated_context_states: bool,
    ) -> Self {
        MdibStorageImpl {
            mdib_version,
            md_description_version: 0,
            md_state_version: 0,
            entities: HashMap::new(),
            context_map: HashMap::new(),
            remove_not_associated_context_states,
        }
    }

    fn new(sequence_id: impl Into<String>, remove_not_associated_context_states: bool) -> Self {
        let ver = MdibVersionBuilder::default().seq(sequence_id).create();
        Self::new_from_mdib_version(ver, remove_not_associated_context_states)
    }
}

impl MdibStorage for MdibStorageImpl {
    fn mdib_version(&self) -> MdibVersion {
        self.mdib_version.clone()
    }

    fn md_description_version(&self) -> u64 {
        self.md_description_version
    }

    fn md_state_version(&self) -> u64 {
        self.md_state_version
    }

    fn entity(&self, handle: &str) -> Option<MdibEntity> {
        self.entities.get(handle).cloned()
    }

    fn entity_version(&self, handle: &str) -> Option<MdibEntityVersion> {
        self.entities
            .get(handle)
            .map(|entity| entity.entity.version())
    }

    fn root_entities(&self) -> Vec<MdibEntity> {
        self.entities_by_type(entity_filter!(Mds))
    }

    fn context_state(&self, handle: &str) -> Option<AbstractContextStateOneOf> {
        match self.context_map.get(handle) {
            None => None,
            Some(entity_handle) => match self.entities.get(entity_handle) {
                None => {
                    error!(
                        "Inconsistent context state map, {} pointing to missing entity {}",
                        handle, entity_handle
                    );
                    None
                }
                Some(entity) => entity
                    .resolve_context_states()
                    .into_iter()
                    .find(|it| it.state_handle() == handle)
                    .or(None),
            },
        }
    }

    fn context_states(&self) -> Vec<AbstractContextStateOneOf> {
        self.entities
            .iter()
            .map(|it| it.1)
            .filter(|it| it.is_context())
            .map(|it| it.resolve_context_states())
            .fold(vec![], |mut a, b| {
                a.extend(b);
                a
            })
    }

    fn entities_by_type<F: 'static + Fn(&MdibEntity) -> bool>(&self, filter: F) -> Vec<MdibEntity> {
        self.entities
            .iter()
            .filter(|ent| filter(ent.1))
            .map(|ent| ent.1.clone())
            .collect()
    }

    fn children_by_type<F: 'static + Fn(&MdibEntity) -> bool>(
        &self,
        handle: &str,
        filter: F,
    ) -> Vec<MdibEntity> {
        self.entities_by_type(filter)
            .iter()
            .filter(|ent| match ent.resolve_parent() {
                None => false,
                Some(p) => handle == p,
            })
            .cloned()
            .collect()
    }

    fn apply_description(
        &mut self,
        mdib_version: MdibVersion,
        md_description_version: Option<u64>,
        md_state_version: Option<u64>,
        description_modifications: MdibDescriptionModifications,
    ) -> Result<WriteDescriptionResult, WriteError> {
        self.mdib_version = mdib_version;
        if let Some(ver) = md_description_version {
            self.md_description_version = ver
        }
        if let Some(ver) = md_state_version {
            self.md_state_version = ver
        }

        let mut inserted_entities: Vec<MdibEntity> = Vec::new();
        let mut updated_entities: Vec<MdibEntity> = Vec::new();
        let mut deleted_entities: Vec<MdibEntity> = Vec::new();
        // TODO: Track updated parents?
        // let mut updated_parent_entities: Vec<String> = Vec::new();

        for modification in description_modifications.into_vec() {
            match modification {
                MdibDescriptionModification::Insert { pair, parent } => {
                    let split: (AbstractDescriptorOneOf, Vec<AbstractStateOneOf>) = pair.into();
                    let sanitized_states = self.remove_not_associated_contexts(split.1);
                    self.insert_entity(parent, split.0, sanitized_states, &mut inserted_entities)?;
                }
                MdibDescriptionModification::Update { pair, .. } => {
                    let split: (AbstractDescriptorOneOf, Vec<AbstractStateOneOf>) = pair.into();
                    let sanitized_states = self.remove_not_associated_contexts(split.1);
                    self.update_entity(split.0, sanitized_states, &mut updated_entities)?;
                }
                MdibDescriptionModification::Delete { .. } => {
                    self.delete_entity(modification, &mut deleted_entities)?;
                }
            }
        }

        Ok(WriteDescriptionResult {
            mdib_version: self.mdib_version.clone(),
            inserted: inserted_entities,
            updated: updated_entities,
            deleted: deleted_entities,
        })
    }

    fn apply_state(
        &mut self,
        mdib_version: MdibVersion,
        md_state_version: Option<u64>,
        state_modifications: MdibStateModifications,
    ) -> Result<WriteStateResult, WriteError> {
        self.mdib_version = mdib_version;
        if let Some(ver) = md_state_version {
            self.md_state_version = ver
        }

        match state_modifications {
            MdibStateModifications::Context { context_states } => {
                let mut removed_context_states =
                    HashMap::<String, Vec<AbstractContextStateOneOf>>::new();
                let mut updated_context_states =
                    HashMap::<String, Vec<AbstractContextStateOneOf>>::new();

                for context_state in context_states {
                    let mdib_entity = match self.entities.remove(&context_state.descriptor_handle())
                    {
                        None => {
                            return Err(WriteError::EntityMissing {
                                handle: context_state.descriptor_handle(),
                            })
                        }
                        Some(ent) => ent,
                    };

                    // let mut new_states: Vec<AbstractStateOneOf> = Vec::new();
                    let state_handle = context_state.state_handle();
                    let entity_context_states = mdib_entity.resolve_context_states();

                    let mut present_states: Vec<AbstractContextStateOneOf> = entity_context_states
                        .into_iter()
                        .filter(|it| it.state_handle() != state_handle)
                        .collect();

                    let not_associated = context_state.context_association().is_none()
                        || context_state.context_association() == NOT_ASSOCIATED;
                    let do_store = !(self.remove_not_associated_context_states && not_associated);

                    let existing_index = present_states
                        .iter()
                        .position(|it| it.state_handle() == state_handle);

                    if do_store {
                        if log_enabled!(Level::Debug) {
                            match &existing_index {
                                None => debug!("Adding new multi-state {}", state_handle),
                                Some(_) => debug!("Updating existing multi-state {}", state_handle),
                            }
                        }
                        updated_context_states
                            .entry(mdib_entity.entity.mds())
                            .or_default()
                            .push(context_state.clone().into_abstract_context_state_one_of());
                        match existing_index {
                            None => present_states.push(context_state),
                            Some(idx) => present_states[idx] = context_state,
                        }
                    } else if not_associated {
                        debug!(
                            "Not storing not associated context state {} for descriptor {}",
                            &context_state.state_handle(),
                            &context_state.descriptor_handle()
                        );
                        match existing_index {
                            None => {
                                // don't care, nothing to remove, nothing to communicate
                            }
                            Some(idx) => removed_context_states
                                .entry(mdib_entity.entity.mds())
                                .or_default()
                                .push(present_states.remove(idx)),
                        }
                    }

                    // update contexts
                    mdib_entity.resolve_context_states().iter().for_each(|it| {
                        self.context_map.remove(&it.state_handle());
                    });

                    let new_entity = mdib_entity_util::replace_states(
                        mdib_entity,
                        present_states
                            .into_iter()
                            .map(|it| it.into_abstract_state_one_of())
                            .collect(),
                    )?;

                    new_entity.resolve_context_states().iter().for_each(|it| {
                        self.context_map
                            .insert(it.state_handle(), it.descriptor_handle());
                    });

                    self.entities.insert(new_entity.entity.handle(), new_entity);
                }

                Ok(WriteStateResult {
                    mdib_version: self.mdib_version.clone(),
                    states: MdibStateModificationResults::Context {
                        updated_context_states,
                        removed_context_states,
                    },
                })
            }
            MdibStateModifications::Alert { alert_states } => {
                entity_arm!(Alert, alert_states, AbstractAlertStateOneOf, self)
            }
            MdibStateModifications::Metric { metric_states } => {
                entity_arm!(Metric, metric_states, AbstractMetricStateOneOf, self)
            }
            MdibStateModifications::Component { component_states } => entity_arm!(
                Component,
                component_states,
                AbstractDeviceComponentStateOneOf,
                self
            ),
            MdibStateModifications::Operation { operation_states } => entity_arm!(
                Operation,
                operation_states,
                AbstractOperationStateOneOf,
                self
            ),
            MdibStateModifications::Waveform { waveform_states } => {
                let mut updated = Vec::<RealTimeSampleArrayMetricState>::new();

                for state in waveform_states {
                    let mdib_entity = match self.entities.remove(&state.descriptor_handle()) {
                        None => {
                            return Err(WriteError::EntityMissing {
                                handle: state.descriptor_handle(),
                            })
                        }
                        Some(ent) => ent,
                    };

                    let new_entity = mdib_entity_util::replace_states(
                        mdib_entity,
                        vec![state.clone().into_abstract_state_one_of()],
                    )?;

                    let new_entity_handle = new_entity.entity.handle();
                    self.entities.insert(new_entity_handle.clone(), new_entity);

                    updated.push(state);
                }

                Ok(WriteStateResult {
                    mdib_version: self.mdib_version.clone(),
                    states: MdibStateModificationResults::Waveform {
                        waveform_states: updated,
                    },
                })
            }
        }
    }
}

//noinspection DuplicatedCode
#[cfg(test)]
pub mod test {
    use std::collections::{HashMap, HashSet};

    use anyhow::Result;
    use itertools::Itertools;
    use protosdc_biceps::biceps::{
        AbstractContextDescriptor, AbstractContextState, AbstractContextStateOneOf,
        AbstractDescriptor, AbstractMultiState, AbstractState, LocationContextDescriptor,
        LocationContextState, LocationDetail,
    };

    use crate::common::biceps_util::Descriptor;
    use crate::common::mdib_description_modification::MdibDescriptionModification;
    use crate::common::mdib_description_modifications::MdibDescriptionModifications;
    use crate::common::mdib_entity::{AlertSystem, Entity, EntityBase, MdibEntity};
    use crate::common::mdib_pair::Pair;
    use crate::common::mdib_state_modifications::MdibStateModifications;
    use crate::common::mdib_version::MdibVersionBuilder;
    use crate::common::storage::mdib_storage::{
        MdibStorage, MdibStorageConstructor, MdibStorageImpl,
    };
    use crate::test_util::mdib::mdib_modification_presets;
    use crate::test_util::{handles, mdib_utils};

    #[test]
    fn test_entities_by_type_works() -> Result<()> {
        let mut entities = HashMap::new();

        {
            let asy = AlertSystem {
                pair: (
                    mdib_utils::create_minimal_alert_system_descriptor(handles::ALERT_SYSTEM_0),
                    mdib_utils::create_minimal_alert_system_state(handles::ALERT_SYSTEM_0),
                )
                    .into(),
                parent: "yolo".to_string(),
                children: vec!["child1".to_string(), "child2".to_string()],
                mds: "yolo_mds".to_string(),
            };

            let entity = MdibEntity {
                last_changed: MdibVersionBuilder::default().seq("wat:frup").create(),
                entity: Entity::AlertSystem(Box::new(asy)),
            };
            entities.insert(entity.entity.handle(), entity);
        }

        {
            let asy = AlertSystem {
                pair: (
                    mdib_utils::create_minimal_alert_system_descriptor(handles::ALERT_SYSTEM_1),
                    mdib_utils::create_minimal_alert_system_state(handles::ALERT_SYSTEM_1),
                )
                    .into(),
                parent: "yolo".to_string(),
                children: vec!["child1".to_string(), "child2".to_string()],
                mds: "yolo_mds".to_string(),
            };

            let entity = MdibEntity {
                last_changed: MdibVersionBuilder::default().seq("wat:frup").create(),
                entity: Entity::AlertSystem(Box::new(asy)),
            };
            entities.insert(entity.entity.handle(), entity);
        }

        let mut storage = MdibStorageImpl::new("wat:whatever".to_string(), true);
        for entity in entities {
            storage.entities.insert(entity.0, entity.1);
        }

        {
            let retrieved = storage.entities_by_type(entity_filter!(AlertSystem));
            assert_eq!(2, retrieved.len())
        }
        {
            let retrieved = storage.entities_by_type(entity_filter!(StringMetric));
            assert_eq!(0, retrieved.len())
        }
        {
            let retrieved = storage.entities_by_type(entity_filter_by_descriptor!(
                mdib_utils::create_minimal_alert_system_descriptor(handles::ALERT_SYSTEM_1)
                    .into_abstract_descriptor_one_of()
            ));

            assert_eq!(2, retrieved.len())
        }
        {
            let retrieved = storage.entities_by_type(entity_filter_by_pair!(Pair::from((
                mdib_utils::create_minimal_alert_system_descriptor(handles::ALERT_SYSTEM_1),
                mdib_utils::create_minimal_alert_system_state(handles::ALERT_SYSTEM_1)
            ))));
            assert_eq!(2, retrieved.len())
        }

        Ok(())
    }

    #[test]
    fn test_descriptors_insertion() -> Result<()> {
        let mut storage = MdibStorageImpl::new("urn:eeeh".to_string(), false);

        assert!(storage.entity(handles::MDS_0).is_none());

        assert!(storage.entity(handles::ALERT_SYSTEM_0).is_none());
        assert!(storage.entity(handles::ALERT_CONDITION_0).is_none());
        assert!(storage.entity(handles::ALERT_CONDITION_1).is_none());
        assert!(storage.entity(handles::ALERT_SIGNAL_0).is_none());
        assert!(storage.entity(handles::ALERT_SIGNAL_1).is_none());

        assert!(storage.entity(handles::VMD_0).is_none());

        assert!(storage.entity(handles::CHANNEL_0).is_none());
        assert!(storage.entity(handles::METRIC_0).is_none());
        assert!(storage.entity(handles::METRIC_1).is_none());

        assert!(storage.entity(handles::CHANNEL_1).is_none());

        assert!(storage.entity(handles::VMD_1).is_none());

        assert!(storage.entity(handles::SYSTEM_CONTEXT_0).is_none());

        assert!(storage.entity(handles::CONTEXT_DESCRIPTOR_0).is_none());
        assert!(storage.context_state(handles::CONTEXT_0).is_none());
        assert!(storage.context_state(handles::CONTEXT_1).is_none());

        assert!(storage.entity(handles::CONTEXT_DESCRIPTOR_1).is_none());
        assert!(storage.context_state(handles::CONTEXT_2).is_none());

        assert!(storage.entity(handles::SCO_0).is_none());
        assert!(storage.entity(handles::OPERATION_0).is_none());
        assert!(storage.entity(handles::OPERATION_1).is_none());

        assert!(storage.entity(handles::CLOCK_0).is_none());
        assert!(storage.entity(handles::BATTERY_0).is_none());
        assert!(storage.entity(handles::BATTERY_1).is_none());

        apply_description(&mut storage);

        assert!(storage.entity(handles::MDS_0).is_some());
        assert!(storage
            .entity(handles::MDS_0)
            .unwrap()
            .resolve_children()
            .unwrap()
            .contains(&handles::SYSTEM_CONTEXT_0.to_string()));

        assert!(storage.entity(handles::ALERT_SYSTEM_0).is_some());
        assert!(storage.entity(handles::ALERT_CONDITION_0).is_some());
        assert!(storage.entity(handles::ALERT_CONDITION_1).is_some());
        assert!(storage.entity(handles::ALERT_SIGNAL_0).is_some());
        assert!(storage.entity(handles::ALERT_SIGNAL_1).is_some());

        assert!(storage.entity(handles::VMD_0).is_some());

        assert!(storage.entity(handles::CHANNEL_0).is_some());
        assert!(storage.entity(handles::METRIC_0).is_some());
        assert!(storage.entity(handles::METRIC_1).is_some());

        assert!(storage.entity(handles::CHANNEL_1).is_some());

        assert!(storage.entity(handles::VMD_1).is_some());

        assert!(storage.entity(handles::SYSTEM_CONTEXT_0).is_some());
        assert_eq!(
            Some(handles::MDS_0.to_string()),
            storage
                .entity(handles::SYSTEM_CONTEXT_0)
                .unwrap()
                .resolve_parent()
        );

        assert!(storage.entity(handles::CONTEXT_DESCRIPTOR_0).is_some());
        assert!(storage.context_state(handles::CONTEXT_0).is_some());
        assert!(storage.context_state(handles::CONTEXT_1).is_some());

        assert!(storage.entity(handles::CONTEXT_DESCRIPTOR_1).is_some());
        assert!(storage.context_state(handles::CONTEXT_2).is_some());

        assert!(storage.entity(handles::SCO_0).is_some());
        assert!(storage.entity(handles::OPERATION_0).is_some());
        assert!(storage.entity(handles::OPERATION_1).is_some());

        assert!(storage.entity(handles::CLOCK_0).is_some());
        assert!(storage.entity(handles::BATTERY_0).is_some());
        assert!(storage.entity(handles::BATTERY_1).is_some());

        assert_eq!(2, storage.root_entities().len());
        Ok(())
    }

    #[test]
    fn test_descriptor_deletion() -> Result<()> {
        let mut storage = MdibStorageImpl::new("urn:eeeh".to_string(), false);
        apply_description(&mut storage);

        assert!(storage.entity(handles::CONTEXT_DESCRIPTOR_0).is_some());
        assert!(storage.entity(handles::CONTEXT_DESCRIPTOR_1).is_some());

        assert!(storage.context_state(handles::CONTEXT_0).is_some());
        assert!(storage.context_state(handles::CONTEXT_1).is_some());
        assert!(storage.context_state(handles::CONTEXT_2).is_some());

        // delete a context state
        let handle_to_delete = handles::CONTEXT_DESCRIPTOR_1.to_string();

        let mut modifications = MdibDescriptionModifications::default();

        modifications
            .add(MdibDescriptionModification::Delete {
                handle: handle_to_delete.clone(),
            })
            .unwrap_or_else(|_| panic!("Could not remove {}", handle_to_delete.clone()));

        storage
            .apply_description(
                storage.mdib_version.inc(),
                Some(storage.md_description_version + 1),
                Some(storage.md_state_version + 1),
                modifications,
            )
            .expect("Could not apply delete to mdib");

        assert!(storage.entity(handles::CONTEXT_DESCRIPTOR_0).is_some());
        assert!(storage.entity(handles::CONTEXT_DESCRIPTOR_1).is_none());

        assert!(storage.context_state(handles::CONTEXT_0).is_some());
        assert!(storage.context_state(handles::CONTEXT_1).is_some());
        assert!(storage.context_state(handles::CONTEXT_2).is_none());

        let system_context_children = storage
            .entity(handles::SYSTEM_CONTEXT_0)
            .unwrap()
            .resolve_children()
            .unwrap();
        assert!(!system_context_children.contains(&handle_to_delete));
        assert!(system_context_children.contains(&handles::CONTEXT_DESCRIPTOR_0.to_string()));
        Ok(())
    }

    #[test]
    fn test_descriptor_update() -> Result<()> {
        let mut storage = MdibStorageImpl::new("urn:eeeh".to_string(), false);
        apply_description(&mut storage);
        let new_version = 4;

        assert!(storage.entity(handles::CONTEXT_DESCRIPTOR_0).is_some());
        assert!(storage.entity(handles::CONTEXT_DESCRIPTOR_1).is_some());

        assert!(storage.context_state(handles::CONTEXT_0).is_some());
        assert!(storage.context_state(handles::CONTEXT_1).is_some());
        assert!(storage.context_state(handles::CONTEXT_2).is_some());

        // get current context state
        let location_context_entity = storage.entity(handles::CONTEXT_DESCRIPTOR_1).unwrap();
        assert!(&location_context_entity.is_context());

        // modify descriptor
        if let Entity::LocationContext(ctx) = &location_context_entity.entity {
            let desc = &ctx.pair.descriptor;
            let states = &ctx.pair.states;

            assert!(desc
                .abstract_context_descriptor
                .abstract_descriptor
                .descriptor_version_attr
                .is_none());

            let new_desc = LocationContextDescriptor {
                abstract_context_descriptor: AbstractContextDescriptor {
                    abstract_descriptor: AbstractDescriptor {
                        descriptor_version_attr: mdib_utils::create_version_counter(Some(
                            new_version,
                        )),
                        ..desc.abstract_context_descriptor.abstract_descriptor.clone()
                    },
                },
            };

            // for vanity, update the version in the state too
            let new_states: Vec<LocationContextState> = states
                .iter()
                .map(|it| LocationContextState {
                    abstract_context_state: AbstractContextState {
                        abstract_multi_state: AbstractMultiState {
                            abstract_state: AbstractState {
                                descriptor_version_attr: mdib_utils::create_referenced_version(
                                    Some(new_version),
                                ),
                                ..it.abstract_context_state
                                    .abstract_multi_state
                                    .abstract_state
                                    .clone()
                            },
                            ..it.abstract_context_state.abstract_multi_state.clone()
                        },
                        ..it.abstract_context_state.clone()
                    },
                    ..it.clone()
                })
                .collect();

            let mut modifications = MdibDescriptionModifications::default();

            modifications
                .add(MdibDescriptionModification::Update {
                    pair: (new_desc, new_states).into(),
                })
                .expect("Could not add update");

            storage
                .apply_description(
                    storage.mdib_version.inc(),
                    Some(storage.md_description_version + 1),
                    Some(storage.md_state_version + 1),
                    modifications,
                )
                .expect("Could not apply update to mdib");
        } else {
            panic!("Was not a location, what?")
        }

        let new_location_context_entity = storage.entity(handles::CONTEXT_DESCRIPTOR_1).unwrap();
        assert!(&new_location_context_entity.is_context());

        if let Entity::LocationContext(ctx) = &new_location_context_entity.entity {
            let desc = &ctx.pair.descriptor;
            let desc_version = desc
                .abstract_context_descriptor
                .abstract_descriptor
                .descriptor_version_attr
                .as_ref()
                .unwrap()
                .unsigned_long;

            assert_eq!(new_version, desc_version);
        } else {
            panic!("Was not a location, what?")
        }
        Ok(())
    }

    #[test]
    fn test_descriptor_update_drop_context() -> Result<()> {
        let mut storage = MdibStorageImpl::new("urn:eeeh".to_string(), true);
        apply_description(&mut storage);
        let new_version = 4;

        assert!(storage.entity(handles::CONTEXT_DESCRIPTOR_0).is_some());
        assert!(storage.entity(handles::CONTEXT_DESCRIPTOR_1).is_some());

        assert!(storage.context_state(handles::CONTEXT_0).is_none());
        assert!(storage.context_state(handles::CONTEXT_1).is_none());
        assert!(storage.context_state(handles::CONTEXT_2).is_some());

        // get current context state
        let location_context_entity = storage.entity(handles::CONTEXT_DESCRIPTOR_1).unwrap();
        assert!(&location_context_entity.is_context());
        assert_eq!(&1, &location_context_entity.resolve_context_states().len());

        // modify descriptor
        if let Entity::LocationContext(ctx) = &location_context_entity.entity {
            let desc = &ctx.pair.descriptor;
            let states = &ctx.pair.states;

            assert!(desc
                .abstract_context_descriptor
                .abstract_descriptor
                .descriptor_version_attr
                .is_none());

            let new_desc = LocationContextDescriptor {
                abstract_context_descriptor: AbstractContextDescriptor {
                    abstract_descriptor: AbstractDescriptor {
                        descriptor_version_attr: mdib_utils::create_version_counter(Some(
                            new_version,
                        )),
                        ..desc.abstract_context_descriptor.abstract_descriptor.clone()
                    },
                },
            };

            // disassociate context on the way
            let new_states: Vec<LocationContextState> = states
                .iter()
                .map(|it| LocationContextState {
                    abstract_context_state: AbstractContextState {
                        abstract_multi_state: AbstractMultiState {
                            abstract_state: AbstractState {
                                descriptor_version_attr: mdib_utils::create_referenced_version(
                                    Some(new_version),
                                ),
                                ..it.abstract_context_state
                                    .abstract_multi_state
                                    .abstract_state
                                    .clone()
                            },
                            ..it.abstract_context_state.abstract_multi_state.clone()
                        },
                        context_association_attr: None,
                        ..it.abstract_context_state.clone()
                    },
                    ..it.clone()
                })
                .collect();

            let mut modifications = MdibDescriptionModifications::default();

            modifications
                .add(MdibDescriptionModification::Update {
                    pair: (new_desc, new_states).into(),
                })
                .expect("Could not add update");

            storage
                .apply_description(
                    storage.mdib_version.inc(),
                    Some(storage.md_description_version + 1),
                    Some(storage.md_state_version + 1),
                    modifications,
                )
                .expect("Could not apply update to mdib");
        } else {
            panic!("Was not a location, what?")
        }

        let new_location_context_entity = storage.entity(handles::CONTEXT_DESCRIPTOR_1).unwrap();
        assert!(&new_location_context_entity.is_context());

        // context state was dropped
        assert!(storage.context_state(handles::CONTEXT_2).is_none());
        assert!(new_location_context_entity
            .resolve_context_states()
            .is_empty());

        Ok(())
    }

    #[test]
    fn test_context_states() -> Result<()> {
        let mut storage = MdibStorageImpl::new("urn:eeeh".to_string(), false);
        apply_description(&mut storage);

        let ctx = storage.context_states();

        assert_eq!(3, ctx.len());
        Ok(())
    }

    // ensure that we only have distinct entities in our entity map
    fn entities_distinct(storage: &MdibStorageImpl) -> Result<()> {
        let handles: Vec<String> = storage
            .entities
            .values()
            .map(|it| it.entity.handle())
            .collect_vec();

        let distinct_handles: HashSet<String> = HashSet::from_iter(handles.iter().cloned());

        assert_eq!(
            handles.len(),
            distinct_handles.len(),
            "Distinct handles size do not match handles size"
        );

        Ok(())
    }

    #[test]
    fn test_update_context_state() -> Result<()> {
        let mut storage = MdibStorageImpl::new("urn:eeeh".to_string(), false);
        apply_description(&mut storage);

        entities_distinct(&storage)?;

        let loc_ctx_handle_to_update = handles::CONTEXT_2;
        let new_room = "Somewhere";

        let ctx = storage
            .context_state(loc_ctx_handle_to_update)
            .expect("Location missing, what?");

        let loc_ctx = match ctx {
            AbstractContextStateOneOf::LocationContextState(loc) => loc,
            _ => panic!("Was no location?"),
        };

        let new_loc_ctx = LocationContextState {
            location_detail: Some(LocationDetail {
                extension_element: None,
                po_c_attr: None,
                room_attr: Some(new_room.to_string()),
                bed_attr: None,
                facility_attr: None,
                building_attr: None,
                floor_attr: None,
            }),
            ..loc_ctx
        };

        let modification = MdibStateModifications::Context {
            context_states: vec![AbstractContextStateOneOf::LocationContextState(new_loc_ctx)],
        };

        storage
            .apply_state(
                storage.mdib_version.inc(),
                Some(storage.md_state_version + 1),
                modification,
            )
            .expect("Could not apply states");

        {
            let new_ctx = storage
                .context_state(loc_ctx_handle_to_update)
                .expect("Location missing, what?");

            let new_loc_ctx = match new_ctx {
                AbstractContextStateOneOf::LocationContextState(loc) => loc,
                _ => panic!("Was no location?"),
            };

            assert_eq!(
                new_room.to_string(),
                new_loc_ctx.location_detail.unwrap().room_attr.unwrap()
            )
        }

        entities_distinct(&storage)?;
        Ok(())
    }

    fn apply_description<T: MdibStorage>(storage: &mut T) {
        let modifications = mdib_modification_presets();

        storage
            .apply_description(
                storage.mdib_version().inc(),
                Some(storage.md_description_version() + 1),
                Some(storage.md_state_version() + 1),
                modifications,
            )
            .expect("Could not apply changes");
    }
}
