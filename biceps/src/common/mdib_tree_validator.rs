use crate::common::mdib_pair::{MultiStatePair, Pair, SingleStatePair};
use protosdc_biceps::biceps::AbstractDescriptorOneOf;

/// Returns true if the type of the provided descriptor is allowed to occur more than once per
/// parent.
pub fn is_many_allowed(child: &Pair) -> bool {
    !matches!(
        child,
        Pair::SingleStatePair(SingleStatePair::AlertSystem(_))
            | Pair::SingleStatePair(SingleStatePair::Sco(_))
            | Pair::SingleStatePair(SingleStatePair::SystemContext(_))
            | Pair::MultiStatePair(MultiStatePair::PatientContext(_))
            | Pair::MultiStatePair(MultiStatePair::LocationContext(_))
    )
}

/// Returns true if the parent is a valid type for the child.
pub fn is_valid_parent(parent: &AbstractDescriptorOneOf, child: &Pair) -> bool {
    matches!(
        (parent, child),
        (
            AbstractDescriptorOneOf::MdsDescriptor { .. },
            Pair::SingleStatePair(SingleStatePair::Vmd { .. })
        ) | (
            AbstractDescriptorOneOf::VmdDescriptor { .. },
            Pair::SingleStatePair(SingleStatePair::Channel { .. })
        ) | (
            AbstractDescriptorOneOf::ChannelDescriptor { .. },
            Pair::SingleStatePair(SingleStatePair::NumericMetric { .. })
        ) | (
            AbstractDescriptorOneOf::ChannelDescriptor { .. },
            Pair::SingleStatePair(SingleStatePair::StringMetric { .. })
        ) | (
            AbstractDescriptorOneOf::ChannelDescriptor { .. },
            Pair::SingleStatePair(SingleStatePair::EnumStringMetric { .. })
        ) | (
            AbstractDescriptorOneOf::ChannelDescriptor { .. },
            Pair::SingleStatePair(SingleStatePair::RealTimeSampleArrayMetric { .. })
        ) | (
            AbstractDescriptorOneOf::ChannelDescriptor { .. },
            Pair::SingleStatePair(SingleStatePair::DistributionSampleArrayMetric { .. })
        ) | (
            AbstractDescriptorOneOf::MdsDescriptor { .. },
            Pair::SingleStatePair(SingleStatePair::AlertSystem { .. })
        ) | (
            AbstractDescriptorOneOf::VmdDescriptor { .. },
            Pair::SingleStatePair(SingleStatePair::AlertSystem { .. })
        ) | (
            AbstractDescriptorOneOf::AlertSystemDescriptor { .. },
            Pair::SingleStatePair(SingleStatePair::AlertCondition { .. })
        ) | (
            AbstractDescriptorOneOf::AlertSystemDescriptor { .. },
            Pair::SingleStatePair(SingleStatePair::LimitAlertCondition { .. })
        ) | (
            AbstractDescriptorOneOf::AlertSystemDescriptor { .. },
            Pair::SingleStatePair(SingleStatePair::AlertSignal { .. })
        ) | (
            AbstractDescriptorOneOf::MdsDescriptor { .. },
            Pair::SingleStatePair(SingleStatePair::Sco { .. })
        ) | (
            AbstractDescriptorOneOf::VmdDescriptor { .. },
            Pair::SingleStatePair(SingleStatePair::Sco { .. })
        ) | (
            AbstractDescriptorOneOf::ScoDescriptor { .. },
            Pair::SingleStatePair(SingleStatePair::ActivateOperation { .. })
        ) | (
            AbstractDescriptorOneOf::ScoDescriptor { .. },
            Pair::SingleStatePair(SingleStatePair::SetAlertStateOperation { .. })
        ) | (
            AbstractDescriptorOneOf::ScoDescriptor { .. },
            Pair::SingleStatePair(SingleStatePair::SetComponentStateOperation { .. })
        ) | (
            AbstractDescriptorOneOf::ScoDescriptor { .. },
            Pair::SingleStatePair(SingleStatePair::SetContextStateOperation { .. })
        ) | (
            AbstractDescriptorOneOf::ScoDescriptor { .. },
            Pair::SingleStatePair(SingleStatePair::SetMetricStateOperation { .. })
        ) | (
            AbstractDescriptorOneOf::ScoDescriptor { .. },
            Pair::SingleStatePair(SingleStatePair::SetStringOperation { .. })
        ) | (
            AbstractDescriptorOneOf::ScoDescriptor { .. },
            Pair::SingleStatePair(SingleStatePair::SetValueOperation { .. })
        ) | (
            AbstractDescriptorOneOf::MdsDescriptor { .. },
            Pair::SingleStatePair(SingleStatePair::Clock { .. })
        ) | (
            AbstractDescriptorOneOf::MdsDescriptor { .. },
            Pair::SingleStatePair(SingleStatePair::Battery { .. })
        ) | (
            AbstractDescriptorOneOf::MdsDescriptor { .. },
            Pair::SingleStatePair(SingleStatePair::SystemContext { .. })
        ) | (
            AbstractDescriptorOneOf::SystemContextDescriptor { .. },
            Pair::MultiStatePair(MultiStatePair::PatientContext { .. })
        ) | (
            AbstractDescriptorOneOf::SystemContextDescriptor { .. },
            Pair::MultiStatePair(MultiStatePair::LocationContext { .. })
        ) | (
            AbstractDescriptorOneOf::SystemContextDescriptor { .. },
            Pair::MultiStatePair(MultiStatePair::MeansContext { .. })
        ) | (
            AbstractDescriptorOneOf::SystemContextDescriptor { .. },
            Pair::MultiStatePair(MultiStatePair::WorkflowContext { .. })
        ) | (
            AbstractDescriptorOneOf::SystemContextDescriptor { .. },
            Pair::MultiStatePair(MultiStatePair::EnsembleContext { .. })
        ) | (
            AbstractDescriptorOneOf::SystemContextDescriptor { .. },
            Pair::MultiStatePair(MultiStatePair::OperatorContext { .. })
        )
    )
}
