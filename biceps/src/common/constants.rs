/// The implied version used in BICEPS for almost all instances of [VersionCounter]
pub const IMPLIED_VERSION: u64 = 0;
