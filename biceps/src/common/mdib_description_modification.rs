use crate::common::mdib_pair::{MdibPair, Pair};

/// A description modification containing the insert, updated or deleted descriptor as well
/// as the state(s).
#[derive(Debug)]
pub enum MdibDescriptionModification {
    /// Inserted descriptor, state(s) and the handle of parent descriptor, if applicable
    Insert { pair: Pair, parent: Option<String> },

    /// Updated descriptor and state(s)
    Update { pair: Pair },

    /// Handle of the descriptor which was deleted
    Delete { handle: String },
}

impl MdibDescriptionModification {
    /// Handle of the affected descriptor.
    pub fn descriptor_handle(&self) -> String {
        match &self {
            MdibDescriptionModification::Insert { pair, .. } => pair.handle(),
            MdibDescriptionModification::Update { pair, .. } => pair.handle(),
            MdibDescriptionModification::Delete { handle, .. } => handle.clone(),
        }
    }

    /// Parent handle of the descriptor, None if MDS or not an insert.
    pub fn parent(&self) -> Option<String> {
        match &self {
            MdibDescriptionModification::Insert { parent, .. } => parent.clone(),
            _ => None,
        }
    }
}
