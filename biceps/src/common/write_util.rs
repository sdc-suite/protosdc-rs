use std::time::Instant;

use log::debug;

use crate::common::access::mdib_access::MdibAccessEvent;
use crate::common::channel_publisher::ChannelPublisher;
use crate::common::event_distributor;
use crate::common::mdib_description_modifications::MdibDescriptionModifications;
use crate::common::mdib_state_modifications::MdibStateModifications;
use crate::common::mdib_version::MdibVersion;
use crate::common::preprocessing::{DescriptionPreprocessor, StatePreprocessor};
use crate::common::storage::mdib_storage::{
    MdibStorage, WriteDescriptionResult, WriteError, WriteStateResult,
};

/// Executes a description write on an mdib, publishes the changes.
///
/// # Arguments
///
/// * `description_modifications`: to provide to the write call
/// * `write_call`: to write the changes into the mdib
/// * `channel_publisher`: to distribute the changes to
/// * `description_preprocessor`: to verify the changes for correctness and apply necessary changes
/// * `mdib_access`: to write the description changes to
///
/// returns: Result<WriteDescriptionResult, WriteError>
pub async fn write_description<U, V, T>(
    description_modifications: MdibDescriptionModifications,
    mdib_version: MdibVersion,
    mut write_call: U,
    channel_publisher: &ChannelPublisher<MdibAccessEvent>,
    description_preprocessor: &mut V,
    mdib_access: &mut T,
) -> Result<WriteDescriptionResult, WriteError>
where
    U: FnMut(
        &mut T,
        MdibDescriptionModifications,
        MdibVersion,
    ) -> Result<WriteDescriptionResult, WriteError>,
    V: DescriptionPreprocessor<T>,
    T: MdibStorage,
{
    let start_time = Instant::now();
    debug!("Start writing description");

    let preprocessed_modifications = match preprocess_description_modifications(
        mdib_access,
        description_preprocessor,
        description_modifications,
    )
    .await
    {
        Ok(it) => it,
        Err(err) => return Err(err),
    };

    let modification_result = write_call(mdib_access, preprocessed_modifications, mdib_version);

    let time = start_time.elapsed();
    match &modification_result {
        Ok(res) => {
            debug!("Successful write took {}ns", &time.as_nanos());
            debug!("New mdib version {}", res.mdib_version)
        }
        Err(_) => {
            debug!("Failed write took {}ns", &time.as_nanos())
        }
    }

    // Send event in case of success and we have a channel
    if let Ok(result) = &modification_result {
        event_distributor::send_description_modification_event(
            channel_publisher,
            result.mdib_version.clone(),
            result.inserted.clone(),
            result.updated.clone(),
            result.deleted.clone(),
        )
        .await;
    }

    modification_result
}

/// Executes a state write on an mdib, publishes the changes.
///
/// # Arguments
///
/// * `state_modifications`: to provide to the write call
/// * `write_call`: to write the changes into the mdib
/// * `channel_publisher`: to distribute the changes to
/// * `state_preprocessor`: to verify the changes for correctness and apply necessary changes
/// * `mdib_access`: to write the state changes to
///
/// returns: Result<WriteDescriptionResult, WriteError>
pub async fn write_states<U, V, T>(
    state_modifications: MdibStateModifications,
    mdib_version: MdibVersion,
    mut write_call: U,
    channel_publisher: &ChannelPublisher<MdibAccessEvent>,
    state_preprocessor: &mut V,
    mdib_access: &mut T,
) -> Result<WriteStateResult, WriteError>
where
    U: FnMut(&mut T, MdibStateModifications, MdibVersion) -> Result<WriteStateResult, WriteError>,
    V: StatePreprocessor<T>,
    T: MdibStorage,
{
    let start_time = Instant::now();
    debug!("Start writing states");

    let preprocessed_modifications =
        preprocess_state_modifications(mdib_access, state_preprocessor, state_modifications)
            .await?;

    let modification_result = write_call(mdib_access, preprocessed_modifications, mdib_version);

    let time = start_time.elapsed();
    match &modification_result {
        Ok(res) => {
            debug!("Successful write took {}ns", &time.as_nanos());
            debug!("New mdib version {}", res.mdib_version)
        }
        Err(_) => {
            debug!("Failed write took {}ns", &time.as_nanos())
        }
    }

    if let Ok(result) = &modification_result {
        event_distributor::send_state_event(
            channel_publisher,
            result.mdib_version.clone(),
            result.states.clone(),
        )
        .await;
    }

    modification_result
}

async fn preprocess_description_modifications<T, V>(
    mdib_access: &mut T,
    description_preprocessor: &mut V,
    description_modifications: MdibDescriptionModifications,
) -> Result<MdibDescriptionModifications, WriteError>
where
    T: MdibStorage,
    V: DescriptionPreprocessor<T>,
{
    let before_first = description_preprocessor
        .before_first_modification(mdib_access, description_modifications.into_vec())?;
    let process = description_preprocessor.process(mdib_access, before_first)?;
    let after_last = description_preprocessor.after_last_modification(mdib_access, process)?;

    let mut new_mods = MdibDescriptionModifications::default();
    new_mods.add_all(after_last)?;
    Ok(new_mods)
}

async fn preprocess_state_modifications<T, V>(
    mdib_access: &mut T,
    state_preprocessor: &mut V,
    state_modifications: MdibStateModifications,
) -> Result<MdibStateModifications, WriteError>
where
    T: MdibStorage,
    V: StatePreprocessor<T>,
{
    let before_first =
        state_preprocessor.before_first_modification(mdib_access, state_modifications)?;
    let process = state_preprocessor.process(mdib_access, before_first)?;
    Ok(state_preprocessor.after_last_modification(mdib_access, process)?)
}
