use std::fmt::Debug;
use std::sync::Arc;
use std::time::Duration;

use futures::stream::{self, StreamExt};
use futures::TryStreamExt;
use log::{debug, trace};
use thiserror::Error;
use tokio::sync::{mpsc, Mutex};

pub const DEFAULT_CHANNEL_SIZE: usize = 1;

#[derive(Debug, Error, PartialEq, Eq)]
pub enum ChannelPublisherError {
    #[error("Sending timed out")]
    Timeout,
}

/// Publisher which distributes events to all subscribers
#[derive(Debug)]
pub struct ChannelPublisher<T>
where
    T: Send + Sync + Debug,
{
    senders: Mutex<Vec<mpsc::Sender<T>>>,
    channel_size: usize,
    send_timeout: Duration,
}

impl<T> ChannelPublisher<T>
where
    T: Send + Sync + Debug + Clone,
{
    pub fn new(channel_size: usize) -> Self {
        ChannelPublisher {
            channel_size,
            send_timeout: Duration::from_secs(5),
            senders: Mutex::new(vec![]),
        }
    }

    /// Returns a receiver providing subscription events.
    pub async fn subscribe(&self) -> mpsc::Receiver<T> {
        let (tx, rx) = mpsc::channel(self.channel_size);

        debug!("New subscriber for {:?}", self);
        let mut locked_senders = self.senders.lock().await;
        locked_senders.push(tx);
        trace!("New subscriber list: {:?}", locked_senders);

        rx
    }

    /// Closes all currently running subscriptions.
    pub async fn unsubscribe_all(&self) {
        debug!("Dropping all senders: {:?}", self);
        self.senders.lock().await.clear();
    }

    /// Sends data to all current subscribers.
    ///
    /// If sending fails or times out, the subscription will be removed.
    pub async fn send(&self, data: T) {
        let mut locked_senders = self.senders.lock().await;

        let arc_data = Arc::new(data);
        let sender_stream = stream::iter(locked_senders.to_owned());
        let filtered_senders: Vec<mpsc::Sender<T>> = sender_stream
            .filter_map(|sender| async {
                {
                    let internal_arc = arc_data.clone();
                    let internal_data = &*internal_arc;
                    match sender
                        .send_timeout(internal_data.clone(), self.send_timeout)
                        .await
                    {
                        Ok(_) => Some(sender),
                        Err(err) => {
                            debug!("Send failed: {}, lost {:?}", err, internal_data);
                            None
                        }
                    }
                }
            })
            .collect::<Vec<_>>()
            .await;
        *locked_senders = filtered_senders;
    }

    /// Sends data to all current subscribers.
    ///
    /// If sending fails or times out, an error is returned.
    pub async fn try_send(&self, data: T) -> Result<(), ChannelPublisherError> {
        let locked_senders = self.senders.lock().await;

        let arc_data = Arc::new(data);
        let sender_stream = stream::iter(&*locked_senders);
        let result: Result<Vec<()>, ChannelPublisherError> = sender_stream
            .then(|sender| async {
                {
                    let internal_arc = arc_data.clone();
                    let internal_data = &*internal_arc;
                    match sender
                        .send_timeout(internal_data.clone(), self.send_timeout)
                        .await
                    {
                        Ok(_) => Ok(()),
                        Err(_) => Err(ChannelPublisherError::Timeout),
                    }
                }
            })
            .try_collect()
            .await;
        result?;
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use std::ops::Mul;
    use std::time::Duration;

    use env_logger;

    use crate::common::channel_publisher::{
        ChannelPublisher, ChannelPublisherError, DEFAULT_CHANNEL_SIZE,
    };

    fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[tokio::test]
    async fn test_send() {
        init();

        let publisher = ChannelPublisher::new(DEFAULT_CHANNEL_SIZE);
        {
            let mut receiver = publisher.subscribe().await;

            for test_value in 0..50 {
                publisher.send(test_value).await;
                assert_eq!(test_value, receiver.recv().await.unwrap());
            }
        }

        let test_value = 123;
        let mut receiver2 = publisher.subscribe().await;
        {
            assert_eq!(2, publisher.senders.lock().await.len());
        }
        // drops the sender
        publisher.send(test_value).await;
        {
            assert_eq!(1, publisher.senders.lock().await.len());
        }
        // other receiver still receives
        assert_eq!(test_value, receiver2.recv().await.unwrap());
    }

    #[tokio::test]
    async fn test_timeout() {
        init();

        let timeout = Duration::from_millis(100);
        let mut publisher = ChannelPublisher::new(DEFAULT_CHANNEL_SIZE);
        publisher.send_timeout = timeout;

        // send
        {
            let mut receiver = publisher.subscribe().await;
            assert_eq!(1, publisher.senders.lock().await.len());
            publisher.send(1).await;
            publisher.send(2).await;

            assert_eq!(0, publisher.senders.lock().await.len());
            assert_eq!(1, receiver.recv().await.unwrap());
            assert!(receiver.recv().await.is_none());
        }
        // try_send
        {
            let mut receiver = publisher.subscribe().await;
            assert_eq!(1, publisher.senders.lock().await.len());
            let send1 = publisher.try_send(1).await;
            let send2 = publisher.try_send(2).await;

            tokio::time::sleep(timeout.mul(2)).await;

            assert_eq!(Ok(()), send1);
            assert_eq!(Err(ChannelPublisherError::Timeout), send2);
            assert_eq!(1, publisher.senders.lock().await.len());
            assert_eq!(1, receiver.recv().await.unwrap());
        }
    }
}
