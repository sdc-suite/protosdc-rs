use crate::common::context::uri::{decode_pchar, encode_pchar, UriUtilError};
use const_format::concatcp;
use lazy_static::lazy_static;
use log::error;
use protosdc_biceps::biceps::instance_identifier_mod::{ExtensionAttr, RootAttr};
use protosdc_biceps::biceps::InstanceIdentifier;
use protosdc_biceps::types::ProtoUri;
use regex::Regex;
use thiserror::Error;

pub const NULL_FLAVOR_ROOT: &str = "biceps.uri.unk";
pub const SCHEME_PREFIX: &str = "sdc.ctxt.";

const ALLOWED_CHARS: &str = "[a-zA-Z0-9\\-._~!$&'()*+,;=:@]";
const P_CHAR: &str = concatcp!("(?:(?:%[a-fA-F0-9]{2})+|(?:", ALLOWED_CHARS, ")+)");
const SEGMENT_NZ_REGEX: &str = concatcp!(P_CHAR, "+");
const SEGMENT_REGEX: &str = concatcp!(P_CHAR, "*");

const ROOT_GROUP: &str = "root";
const EXTENSION_GROUP: &str = "extension";
const INSTANCE_IDENTIFIER_PATTERN: &str = concatcp!(
    "/(?P<",
    ROOT_GROUP,
    ">",
    SEGMENT_NZ_REGEX,
    ")/",
    "(?P<",
    EXTENSION_GROUP,
    ">",
    SEGMENT_REGEX,
    ")$"
);

const SCHEME_SEGMENT: &str = "(?i:[a-z][a-z0-9+-.]*)";
const SCHEME_VALIDATOR_PATTERN: &str = concatcp!("^", SCHEME_SEGMENT, "$");

const CTX_LOC: &str = concatcp!(SCHEME_PREFIX, "loc");
const CTX_PAT: &str = concatcp!(SCHEME_PREFIX, "pat");
const CTX_ENS: &str = concatcp!(SCHEME_PREFIX, "ens");
const CTX_WFL: &str = concatcp!(SCHEME_PREFIX, "wfl");
const CTX_OPR: &str = concatcp!(SCHEME_PREFIX, "opr");
const CTX_MNS: &str = concatcp!(SCHEME_PREFIX, "mns");

lazy_static! {
    pub(crate) static ref INSTANCE_IDENTIFIER_PARSER: Regex =
        Regex::new(INSTANCE_IDENTIFIER_PATTERN).unwrap();
    pub(crate) static ref SCHEME_VALIDATOR: Regex = Regex::new(SCHEME_VALIDATOR_PATTERN).unwrap();
}

#[derive(Debug, PartialEq)]
pub enum ContextSource {
    Location,
    Patient,
    Ensemble,
    Workflow,
    Operator,
    Means,
}

impl ContextSource {
    fn source_string(&self) -> &str {
        match self {
            ContextSource::Location => CTX_LOC,
            ContextSource::Patient => CTX_PAT,
            ContextSource::Ensemble => CTX_ENS,
            ContextSource::Workflow => CTX_WFL,
            ContextSource::Operator => CTX_OPR,
            ContextSource::Means => CTX_MNS,
        }
    }
}

impl TryFrom<&str> for ContextSource {
    type Error = ContextIdentificationMapperError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        Ok(match value {
            CTX_LOC => Self::Location,
            CTX_PAT => Self::Patient,
            CTX_ENS => Self::Ensemble,
            CTX_WFL => Self::Workflow,
            CTX_OPR => Self::Operator,
            CTX_MNS => Self::Means,
            _ => return Err(ContextIdentificationMapperError::InvalidScheme),
        })
    }
}

#[derive(Debug, Error)]
pub enum ContextIdentificationMapperError {
    #[error(transparent)]
    UriUtil(#[from] UriUtilError),
    #[error("Instance identifier had no extension and no root")]
    InstanceIdentifierWithoutExtension,
    #[error("Invalid Scheme")]
    InvalidScheme,
    #[error("Unexpected scheme, was {actual} but expected {expected}")]
    UnexpectedScheme { expected: String, actual: String },
    #[error(transparent)]
    UrlParse(#[from] url::ParseError),
    #[error("Unknown Error")]
    Other,
    #[error("Invalid encoding of InstanceIdentifier in the URI")]
    InvalidEncoding,
}

pub fn from_instance_identifier(
    instance_identifier: &InstanceIdentifier,
    context_source: ContextSource,
) -> Result<String, ContextIdentificationMapperError> {
    let root = match &instance_identifier.root_attr {
        None => NULL_FLAVOR_ROOT.to_string(),
        Some(it) => encode_pchar(&it.any_u_r_i.uri, false),
    };
    let extension = match &instance_identifier.extension_attr {
        None => "".to_string(),
        Some(it) => encode_pchar(&it.string, false),
    };

    let result = format!("{}:/{}/{}", context_source.source_string(), root, extension);

    // validate
    from_uri(&result, context_source)?;

    Ok(result)
}

pub fn from_uri(
    uri: &str,
    expected_context_source: ContextSource,
) -> Result<InstanceIdentifier, ContextIdentificationMapperError> {
    // In case the input will be changed in the future
    let source_string = expected_context_source.source_string();
    if !SCHEME_VALIDATOR.is_match(source_string) {
        error!("{source_string} has an invalid scheme");
        return Err(ContextIdentificationMapperError::InvalidScheme);
    }

    let parsed = url::Url::parse(uri)?;

    let context_source: ContextSource = parsed.scheme().try_into()?;

    if context_source != expected_context_source {
        return Err(ContextIdentificationMapperError::UnexpectedScheme {
            actual: parsed.scheme().to_string(),
            expected: expected_context_source.source_string().to_string(),
        });
    }

    let matched_identifier = INSTANCE_IDENTIFIER_PARSER.captures(parsed.path());
    match matched_identifier {
        None => Err(ContextIdentificationMapperError::InvalidEncoding),
        Some(matched) => {
            // these must exist now
            let root = matched.name(ROOT_GROUP).unwrap();
            let extension = matched.name(EXTENSION_GROUP).unwrap();

            let decoded_root = decode_pchar(root.as_str())?;
            let decoded_extension = decode_pchar(extension.as_str())?;

            Ok(InstanceIdentifier {
                extension_element: None,
                r#type: None,
                identifier_name: vec![],
                root_attr: match decoded_root.as_str() {
                    NULL_FLAVOR_ROOT => None,
                    _ => Some(RootAttr {
                        any_u_r_i: ProtoUri { uri: decoded_root },
                    }),
                },
                extension_attr: match decoded_extension.is_empty() {
                    true => None,
                    false => Some(ExtensionAttr {
                        string: decoded_extension,
                    }),
                },
            })
        }
    }
}

#[cfg(test)]
mod test {
    use crate::common::builders::InstanceIdentifierBuilder;
    use crate::common::context::context_identification_mapper::{
        from_instance_identifier, from_uri, ContextSource,
    };
    use protosdc_biceps::biceps::InstanceIdentifier;

    pub fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    fn create_instance_identifier(
        root: Option<&str>,
        extension: Option<&str>,
    ) -> InstanceIdentifier {
        InstanceIdentifierBuilder::default()
            .identifier_root_opt(root)
            .identifier_extension_opt(extension)
            .build()
    }

    #[test]
    fn test_from_instance_identifier() -> anyhow::Result<()> {
        init();
        {
            let actual_uri = from_instance_identifier(
                &create_instance_identifier(None, None),
                ContextSource::Location,
            )?;
            let expected_uri = "sdc.ctxt.loc:/biceps.uri.unk/";
            assert_eq!(expected_uri, &actual_uri);
        }
        {
            let actual_uri = from_instance_identifier(
                &create_instance_identifier(Some("http://root"), None),
                ContextSource::Location,
            )?;
            let expected_uri = "sdc.ctxt.loc:/http:%2F%2Froot/";
            assert_eq!(expected_uri, &actual_uri);
        }
        {
            let actual_uri = from_instance_identifier(
                &create_instance_identifier(Some("http://root"), Some("extension")),
                ContextSource::Patient,
            )?;
            let expected_uri = "sdc.ctxt.pat:/http:%2F%2Froot/extension";
            assert_eq!(expected_uri, &actual_uri);
        }
        {
            let actual_uri = from_instance_identifier(
                &create_instance_identifier(Some("http://root"), Some("ext/enÖsion?")),
                ContextSource::Ensemble,
            )?;
            let expected_uri = "sdc.ctxt.ens:/http:%2F%2Froot/ext%2Fen%C3%96sion%3F";
            assert_eq!(expected_uri, &actual_uri);
        }

        Ok(())
    }

    fn compare(expected: &InstanceIdentifier, actual: &InstanceIdentifier) {
        assert_eq!(expected.root_attr, actual.root_attr);
        assert_eq!(expected.extension_attr, actual.extension_attr);
    }

    #[test]
    fn test_from_uri() -> anyhow::Result<()> {
        {
            let actual_identifier =
                from_uri("sdc.ctxt.loc:/biceps.uri.unk/", ContextSource::Location)?;
            let expected_identifier = create_instance_identifier(None, None);
            compare(&expected_identifier, &actual_identifier)
        }
        {
            let actual_identifier =
                from_uri("sdc.ctxt.loc:/http%3A%2F%2Froot/", ContextSource::Location)?;
            let expected_identifier = create_instance_identifier(Some("http://root"), None);
            compare(&expected_identifier, &actual_identifier)
        }
        {
            let actual_identifier = from_uri(
                "sdc.ctxt.pat:/http%3A%2F%2Froot/extension",
                ContextSource::Patient,
            )?;
            let expected_identifier =
                create_instance_identifier(Some("http://root"), Some("extension"));
            compare(&expected_identifier, &actual_identifier)
        }
        {
            let actual_identifier = from_uri(
                "sdc.ctxt.ens:/http%3A%2F%2Froot/ext%2Fen%C3%96sion%3F",
                ContextSource::Ensemble,
            )?;
            let expected_identifier =
                create_instance_identifier(Some("http://root"), Some("ext/enÖsion?"));
            compare(&expected_identifier, &actual_identifier)
        }
        {
            let actual_identifier = from_uri(
                "sdc.ctxt.ens:/http%3A%2F%2Froot/ext%2Fen%C3%96sion%3F?query#fragment",
                ContextSource::Ensemble,
            )?;
            let expected_identifier =
                create_instance_identifier(Some("http://root"), Some("ext/enÖsion?"));
            compare(&expected_identifier, &actual_identifier)
        }

        {
            assert!(from_uri(
                "sdc.ctxt.loc:/http%3A%2F%2Froot/ext%2Fen%C3%96sion%3F",
                ContextSource::Patient
            )
            .is_err())
        }
        {
            assert!(from_uri(
                "sdc.ctxt.loc:/http%3A%2F%2Froot//ext%2Fen%C3%96sion%3F",
                ContextSource::Location
            )
            .is_err())
        }
        {
            assert!(from_uri(
                "sdc.ctxt.loc://http%3A%2F%2Froot/ext%2Fen%C3%96sion%3F",
                ContextSource::Location
            )
            .is_err())
        }
        {
            assert!(from_uri(
                "sdc.ctxt.loc://ext%2Fen%C3%96sion%3F",
                ContextSource::Location
            )
            .is_err())
        }
        // TODO: These two inputs are used in sdc-ri and are failing a match against the
        //  URI regex used by ri. However, parsing using url::Url seems to work, so these don't fail
        //  Evaluate why that is and if it's bad.
        // {
        //     assert!(from_uri("sdc.ctxt.loc:/a/b?ä?", ContextSource::Location).is_err())
        // }
        // {
        //     assert!(from_uri("sdc.ctxt.loc:/a/ä", ContextSource::Location).is_err())
        // }
        {
            assert!(from_uri("sdc.ctxt.loc:/a#/b", ContextSource::Location).is_err())
        }
        Ok(())
    }
}
