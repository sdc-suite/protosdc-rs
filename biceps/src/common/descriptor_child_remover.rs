use std::fmt::Debug;
use std::marker::PhantomData;

use crate::common::mdib_description_modification::MdibDescriptionModification;
use crate::common::mdib_pair::{
    AlertSystemPair, ChannelPair, MdsPair, Pair, ScoPair, SingleStatePair, SystemContextPair,
    VmdPair,
};
use crate::common::preprocessing::{DescriptionPreprocessor, PreprocessingError};
use crate::common::storage::mdib_storage::MdibStorage;
use protosdc_biceps::biceps::{
    AbstractComplexDeviceComponentDescriptor, AlertSystemDescriptor, ChannelDescriptor,
    MdsDescriptor, ScoDescriptor, SystemContextDescriptor, VmdDescriptor,
};

/// [DescriptionPreprocessor] which removes child descriptors from entities.
#[derive(Debug)]
pub struct DescriptorChildRemover<T: MdibStorage> {
    phantom: PhantomData<T>,
}

impl<T: MdibStorage> Default for DescriptorChildRemover<T> {
    fn default() -> Self {
        DescriptorChildRemover {
            phantom: Default::default(),
        }
    }
}

impl<T: MdibStorage> DescriptorChildRemover<T> {
    // replaces all children with empty vecs which have zero allocation
    fn process_pair(pair: Pair) -> Pair {
        match pair {
            Pair::SingleStatePair(SingleStatePair::AlertSystem(p)) => {
                Pair::SingleStatePair(SingleStatePair::AlertSystem(Box::new(AlertSystemPair {
                    descriptor: AlertSystemDescriptor {
                        // create new empty vec with capacity
                        alert_condition: Vec::with_capacity(0),
                        alert_signal: Vec::with_capacity(0),
                        ..p.descriptor
                    },
                    ..*p
                })))
            }
            Pair::SingleStatePair(SingleStatePair::Channel(p)) => {
                Pair::SingleStatePair(SingleStatePair::Channel(Box::new(ChannelPair {
                    descriptor: ChannelDescriptor {
                        metric: Vec::with_capacity(0),
                        ..p.descriptor
                    },
                    ..*p
                })))
            }
            Pair::SingleStatePair(SingleStatePair::Sco(p)) => {
                Pair::SingleStatePair(SingleStatePair::Sco(Box::new(ScoPair {
                    descriptor: ScoDescriptor {
                        operation: Vec::with_capacity(0),
                        ..p.descriptor
                    },
                    ..*p
                })))
            }
            Pair::SingleStatePair(SingleStatePair::SystemContext(p)) => Pair::SingleStatePair(
                SingleStatePair::SystemContext(Box::new(SystemContextPair {
                    descriptor: SystemContextDescriptor {
                        ensemble_context: Vec::with_capacity(0),
                        location_context: None,
                        means_context: Vec::with_capacity(0),
                        operator_context: Vec::with_capacity(0),
                        patient_context: None,
                        workflow_context: Vec::with_capacity(0),
                        ..p.descriptor
                    },
                    ..*p
                })),
            ),
            Pair::SingleStatePair(SingleStatePair::Vmd(p)) => {
                Pair::SingleStatePair(SingleStatePair::Vmd(Box::new(VmdPair {
                    descriptor: VmdDescriptor {
                        channel: Vec::with_capacity(0),
                        abstract_complex_device_component_descriptor:
                            AbstractComplexDeviceComponentDescriptor {
                                alert_system: None,
                                sco: None,
                                ..p.descriptor.abstract_complex_device_component_descriptor
                            },
                        ..p.descriptor
                    },
                    ..*p
                })))
            }
            Pair::SingleStatePair(SingleStatePair::Mds(p)) => {
                Pair::SingleStatePair(SingleStatePair::Mds(Box::new(MdsPair {
                    descriptor: MdsDescriptor {
                        vmd: Vec::with_capacity(0),
                        battery: Vec::with_capacity(0),
                        clock: None,
                        system_context: None,
                        abstract_complex_device_component_descriptor:
                            AbstractComplexDeviceComponentDescriptor {
                                alert_system: None,
                                sco: None,
                                ..p.descriptor.abstract_complex_device_component_descriptor
                            },
                        ..p.descriptor
                    },
                    ..*p
                })))
            }
            _ => pair,
        }
    }
}

impl<T: MdibStorage> DescriptionPreprocessor<T> for DescriptorChildRemover<T> {
    fn before_first_modification(
        &mut self,
        _: &T,
        modifications: Vec<MdibDescriptionModification>,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError>
    where
        T: MdibStorage,
    {
        Ok(modifications)
    }

    fn after_last_modification(
        &mut self,
        _: &T,
        modifications: Vec<MdibDescriptionModification>,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError>
    where
        T: MdibStorage,
    {
        Ok(modifications)
    }

    fn process(
        &mut self,
        _: &T,
        modifications: Vec<MdibDescriptionModification>,
    ) -> Result<Vec<MdibDescriptionModification>, PreprocessingError>
    where
        T: MdibStorage,
    {
        let result = modifications
            .into_iter()
            .map(|modification| match modification {
                MdibDescriptionModification::Insert { pair, parent } => {
                    MdibDescriptionModification::Insert {
                        pair: (Self::process_pair(pair)),
                        parent,
                    }
                }
                MdibDescriptionModification::Update { pair } => {
                    MdibDescriptionModification::Update {
                        pair: (Self::process_pair(pair)),
                    }
                }
                MdibDescriptionModification::Delete { .. } => modification,
            })
            .collect();

        Ok(result)
    }
}
