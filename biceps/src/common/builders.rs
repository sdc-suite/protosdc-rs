use protosdc_biceps::biceps::instance_identifier_mod::{ExtensionAttr, RootAttr};
use protosdc_biceps::biceps::{CodedValue, Extension, InstanceIdentifier, LocalizedText};
use protosdc_biceps::types::ProtoUri;

#[derive(Debug, Default)]
pub struct InstanceIdentifierBuilder {
    extension_element: Option<Extension>,
    r#type: Option<CodedValue>,
    identifier_name: Vec<LocalizedText>,
    root_attr: Option<RootAttr>,
    extension_attr: Option<ExtensionAttr>,
}

impl InstanceIdentifierBuilder {
    pub fn ext_extension(mut self, extension: Extension) -> Self {
        self.extension_element = Some(extension);
        self
    }

    pub fn identifier_type(mut self, identifier_type: CodedValue) -> Self {
        self.r#type = Some(identifier_type);
        self
    }

    pub fn identifier_name(mut self, name: LocalizedText) -> Self {
        self.identifier_name.push(name);
        self
    }

    pub fn identifier_root(mut self, root: impl Into<String>) -> Self {
        self.root_attr = Some(RootAttr {
            any_u_r_i: ProtoUri { uri: root.into() },
        });
        self
    }

    pub fn identifier_root_opt(mut self, root: Option<impl Into<String>>) -> Self {
        self.root_attr = root.map(|it| RootAttr {
            any_u_r_i: ProtoUri { uri: it.into() },
        });
        self
    }

    pub fn identifier_extension(mut self, extension: impl Into<String>) -> Self {
        self.extension_attr = Some(ExtensionAttr {
            string: extension.into(),
        });
        self
    }

    pub fn identifier_extension_opt(mut self, extension: Option<impl Into<String>>) -> Self {
        self.extension_attr = extension.map(|it| ExtensionAttr { string: it.into() });
        self
    }

    pub fn build(self) -> InstanceIdentifier {
        InstanceIdentifier {
            extension_element: self.extension_element,
            r#type: self.r#type,
            identifier_name: self.identifier_name,
            root_attr: self.root_attr,
            extension_attr: self.extension_attr,
        }
    }
}
