use protosdc_biceps::biceps::{
    AbstractAlertStateOneOf, AbstractContextStateOneOf, AbstractDeviceComponentStateOneOf,
    AbstractMetricStateOneOf, AbstractOperationStateOneOf, AbstractStateOneOf,
    RealTimeSampleArrayMetricState,
};
use std::collections::HashMap;

use crate::common::biceps_util::State;

/// Describes a set of modified states in an mdib.
///
/// The states are grouped by the type of report they will result in to restrict modifications
/// which would violate BICEPS.
#[derive(Clone, Debug)]
pub enum MdibStateModificationResults {
    Alert {
        alert_states: HashMap<String, Vec<AbstractAlertStateOneOf>>,
    },

    Component {
        component_states: HashMap<String, Vec<AbstractDeviceComponentStateOneOf>>,
    },

    Context {
        removed_context_states: HashMap<String, Vec<AbstractContextStateOneOf>>,
        updated_context_states: HashMap<String, Vec<AbstractContextStateOneOf>>,
    },

    Metric {
        metric_states: HashMap<String, Vec<AbstractMetricStateOneOf>>,
    },

    Operation {
        operation_states: HashMap<String, Vec<AbstractOperationStateOneOf>>,
    },

    Waveform {
        waveform_states: Vec<RealTimeSampleArrayMetricState>,
    },
}

/// Describes a set of modified states in an mdib.
///
/// The states are grouped by the type of report they will result in to restrict modifications
/// which would violate BICEPS.
#[derive(Clone, Debug)]
pub enum MdibStateModifications {
    Alert {
        alert_states: Vec<AbstractAlertStateOneOf>,
    },

    Component {
        component_states: Vec<AbstractDeviceComponentStateOneOf>,
    },

    Context {
        context_states: Vec<AbstractContextStateOneOf>,
    },

    Metric {
        metric_states: Vec<AbstractMetricStateOneOf>,
    },

    Operation {
        operation_states: Vec<AbstractOperationStateOneOf>,
    },

    Waveform {
        waveform_states: Vec<RealTimeSampleArrayMetricState>,
    },
}

impl MdibStateModifications {
    pub fn states(self) -> Vec<AbstractStateOneOf> {
        match self {
            MdibStateModifications::Alert { alert_states } => alert_states
                .into_iter()
                .map(|it| it.into_abstract_state_one_of())
                .collect(),
            MdibStateModifications::Component { component_states } => component_states
                .into_iter()
                .map(|it| it.into_abstract_state_one_of())
                .collect(),
            MdibStateModifications::Context { context_states } => context_states
                .into_iter()
                .map(|it| it.into_abstract_state_one_of())
                .collect(),
            MdibStateModifications::Metric { metric_states } => metric_states
                .into_iter()
                .map(|it| it.into_abstract_state_one_of())
                .collect(),
            MdibStateModifications::Operation { operation_states } => operation_states
                .into_iter()
                .map(|it| it.into_abstract_state_one_of())
                .collect(),
            MdibStateModifications::Waveform { waveform_states } => waveform_states
                .into_iter()
                .map(|it| it.into_abstract_state_one_of())
                .collect(),
        }
    }

    pub fn len(&self) -> usize {
        match self {
            MdibStateModifications::Alert { alert_states } => alert_states.len(),
            MdibStateModifications::Component { component_states } => component_states.len(),
            MdibStateModifications::Context { context_states } => context_states.len(),
            MdibStateModifications::Metric { metric_states } => metric_states.len(),
            MdibStateModifications::Operation { operation_states } => operation_states.len(),
            MdibStateModifications::Waveform { waveform_states } => waveform_states.len(),
        }
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }
}
