use std::fmt::Debug;
use std::sync::{Arc, Mutex};

use async_trait::async_trait;
use futures::future::BoxFuture;
use log::trace;
use tokio::sync::{broadcast, mpsc, oneshot, Mutex as TokioMutex, RwLock, RwLockReadGuard};

use crate::chain_descriptor_segment;
use protosdc_biceps::biceps::AbstractContextStateOneOf;

use crate::common::access::mdib_access::{MdibAccess, MdibAccessEvent, ObservableMdib};
use crate::common::channel_publisher::ChannelPublisher;
use crate::common::descriptor_child_remover::DescriptorChildRemover;
use crate::common::mdib_description_modifications::MdibDescriptionModifications;
use crate::common::mdib_entity::MdibEntity;
use crate::common::mdib_state_modifications::MdibStateModifications;
use crate::common::mdib_version::MdibVersion;
use crate::common::preprocessing::{
    DescriptionPreprocessor, DescriptionPreprocessorSegment, StatePreprocessor,
    StatePreprocessorSegment,
};
use crate::common::storage::mdib_storage::{
    MdibStorage, MdibStorageConstructor, WriteDescriptionResult, WriteError, WriteStateResult,
};
use crate::common::write_util;
use crate::provider::preprocessing::cardinality_checker::CardinalityChecker;
use crate::provider::preprocessing::duplicate_context_state_handle_handler::DuplicateContextStateHandleHandler;
use crate::provider::preprocessing::duplicate_descriptor_handler::DuplicateDescriptorHandler;
use crate::provider::preprocessing::type_change_checker::{
    TypeChangeChecker, TypeChangeCheckerInterior,
};
use crate::provider::preprocessing::type_consistency_checker::TypeConsistencyChecker;
use crate::provider::preprocessing::version_handler::{VersionHandler, VersionHandlerInterior};

const MDIB_ACCESS_EVENT_CHANNEL_SIZE: usize = 1000;

pub enum LocalMdibAccessRequest {
    WriteDescription {
        description_modifications: MdibDescriptionModifications,
        response: oneshot::Sender<Result<WriteDescriptionResult, WriteError>>,
    },
    WriteStates {
        state_modifications: MdibStateModifications,
        response: oneshot::Sender<Result<WriteStateResult, WriteError>>,
    },
    Subscribe {
        response: oneshot::Sender<Option<broadcast::Receiver<MdibAccessEvent>>>,
    },
    UnsubscribeAll {
        response: oneshot::Sender<()>,
    },
}

/// Provider-side read-write access to the mdib.
#[async_trait]
pub trait LocalMdibAccess: MdibAccess + ObservableMdib {
    /// Processes the description modifications object, stores the data internally and trigger an event.
    ///
    /// Data is passed through without any checks to the MDIB storage.
    ///
    /// # Arguments
    ///
    /// * `description_modifications`: a set of insertions, updates and deletes.
    async fn write_description(
        &mut self,
        description_modifications: MdibDescriptionModifications,
    ) -> Result<WriteDescriptionResult, WriteError>;

    /// Processes the state modifications object, stores the data internally and triggers an event.
    ///
    /// Data is passed through without any checks to the MDIB storage.
    ///
    /// # Arguments
    ///
    /// * `state_modifications`: a set of state updates.
    async fn write_states(
        &mut self,
        state_modifications: MdibStateModifications,
    ) -> Result<WriteStateResult, WriteError>;
}

/// Ability to execute read-transactions for an mdib to retrieve a consistent state.
#[async_trait]
pub trait LocalMdibAccessReadTransaction {
    type Access<'a>: MdibAccess + Send + Sync
    where
        Self: 'a;
    /// Executes a closure within a single read transaction, providing a lock-free `MdibAccess`
    async fn read_transaction<'b, 'a: 'b, G>(
        &'a self,
        // see https://users.rust-lang.org/t/lifetime-may-not-live-long-enough-for-an-async-closure/62489
        // for BoxFuture reasons
        func: impl FnOnce(Self::Access<'b>) -> BoxFuture<'b, G> + Send + Sync,
    ) -> G
    where
        Self: 'b;
}

#[derive(Debug)]
pub struct LocalMdibAccessImpl<T, DESC, STATE>
where
    T: MdibStorage,
    DESC: DescriptionPreprocessor<T>,
    STATE: StatePreprocessor<T>,
{
    mdib_access: Arc<RwLock<T>>,
    channel_publisher: Arc<TokioMutex<ChannelPublisher<MdibAccessEvent>>>,
    description_preprocessors: Arc<TokioMutex<DESC>>,
    state_preprocessors: Arc<TokioMutex<STATE>>,
}

// this needs to be implemented manually, as the derived complains about bounds not satisfied
impl<T, DESC, STATE> Clone for LocalMdibAccessImpl<T, DESC, STATE>
where
    T: MdibStorage,
    DESC: DescriptionPreprocessor<T>,
    STATE: StatePreprocessor<T>,
{
    //noinspection DuplicatedCode
    fn clone(&self) -> Self {
        Self {
            mdib_access: self.mdib_access.clone(),
            channel_publisher: self.channel_publisher.clone(),
            description_preprocessors: self.description_preprocessors.clone(),
            state_preprocessors: self.state_preprocessors.clone(),
        }
    }
}

pub type DefaultLocalDescriptionPreprocessors<T> = DescriptionPreprocessorSegment<
    DescriptorChildRemover<T>,
    DescriptionPreprocessorSegment<
        DuplicateDescriptorHandler<T>,
        DescriptionPreprocessorSegment<
            CardinalityChecker<T>,
            DescriptionPreprocessorSegment<
                TypeConsistencyChecker<T>,
                DescriptionPreprocessorSegment<TypeChangeChecker<T>, VersionHandler<T>, T>,
                T,
            >,
            T,
        >,
        T,
    >,
    T,
>;

pub type DefaultLocalStatePreprocessors<T> = StatePreprocessorSegment<
    DuplicateContextStateHandleHandler<T>,
    StatePreprocessorSegment<VersionHandler<T>, TypeChangeChecker<T>, T>,
    T,
>;

impl<T, DESC, STATE> LocalMdibAccessImpl<T, DESC, STATE>
where
    T: MdibStorageConstructor + Send + Sync,
    DESC: DescriptionPreprocessor<T>,
    STATE: StatePreprocessor<T>,
{
    pub fn new_custom_preprocessors(
        mdib_version: MdibVersion,
        description_preprocessors: DESC,
        state_preprocessors: STATE,
    ) -> LocalMdibAccessImpl<T, DESC, STATE> {
        Self::new_with_context_option_custom_preprocessors(
            mdib_version,
            false,
            description_preprocessors,
            state_preprocessors,
        )
    }

    pub fn new_with_context_option_custom_preprocessors(
        mdib_version: MdibVersion,
        remove_not_associated_context_states: bool,
        description_preprocessors: DESC,
        state_preprocessors: STATE,
    ) -> LocalMdibAccessImpl<T, DESC, STATE> {
        LocalMdibAccessImpl {
            mdib_access: Arc::new(RwLock::new(T::new_from_mdib_version(
                mdib_version,
                remove_not_associated_context_states,
            ))),
            channel_publisher: Arc::new(TokioMutex::new(ChannelPublisher::new(
                MDIB_ACCESS_EVENT_CHANNEL_SIZE,
            ))),
            description_preprocessors: Arc::new(TokioMutex::new(description_preprocessors)),
            state_preprocessors: Arc::new(TokioMutex::new(state_preprocessors)),
        }
    }
}

impl<T>
    LocalMdibAccessImpl<
        T,
        DefaultLocalDescriptionPreprocessors<T>,
        DefaultLocalStatePreprocessors<T>,
    >
where
    T: MdibStorageConstructor + Send + Sync,
{
    pub fn new(
        mdib_version: MdibVersion,
    ) -> LocalMdibAccessImpl<
        T,
        DefaultLocalDescriptionPreprocessors<T>,
        DefaultLocalStatePreprocessors<T>,
    > {
        Self::new_with_context_option(mdib_version, false)
    }

    pub fn new_with_context_option(
        mdib_version: MdibVersion,
        remove_not_associated_context_states: bool,
    ) -> LocalMdibAccessImpl<
        T,
        DefaultLocalDescriptionPreprocessors<T>,
        DefaultLocalStatePreprocessors<T>,
    > {
        let version_handler_interior = Arc::new(Mutex::new(VersionHandlerInterior::default()));
        let type_change_checker_interior =
            Arc::new(Mutex::new(TypeChangeCheckerInterior::default()));

        let default_description_preprocessors = chain_descriptor_segment!(
            DescriptorChildRemover::default(),
            DuplicateDescriptorHandler::default(),
            CardinalityChecker::default(),
            TypeConsistencyChecker::default(),
            TypeChangeChecker::new(type_change_checker_interior.clone()),
            VersionHandler::new(version_handler_interior.clone()),
        );

        let default_state_preprocessors = StatePreprocessorSegment::create(
            DuplicateContextStateHandleHandler::default(),
            StatePreprocessorSegment::create(
                VersionHandler::new(version_handler_interior),
                TypeChangeChecker::new(type_change_checker_interior),
            ),
        );

        Self::new_with_context_option_custom_preprocessors(
            mdib_version,
            remove_not_associated_context_states,
            default_description_preprocessors,
            default_state_preprocessors,
        )
    }
}

#[async_trait]
impl<T, DESC, STATE> MdibAccess for LocalMdibAccessImpl<T, DESC, STATE>
where
    T: MdibStorage + Send + Sync,
    DESC: DescriptionPreprocessor<T> + Send + Sync,
    STATE: StatePreprocessor<T> + Send + Sync,
{
    async fn mdib_version(&self) -> MdibVersion {
        self.mdib_access.read().await.mdib_version()
    }

    async fn md_description_version(&self) -> u64 {
        {
            let data = self.mdib_access.read().await;
            data.md_description_version()
        }
    }

    async fn md_state_version(&self) -> u64 {
        self.mdib_access.read().await.md_state_version()
    }

    async fn entity(&self, handle: &str) -> Option<MdibEntity> {
        self.mdib_access.read().await.entity(handle)
    }

    async fn root_entities(&self) -> Vec<MdibEntity> {
        self.mdib_access.read().await.root_entities()
    }

    async fn context_state(&self, handle: &str) -> Option<AbstractContextStateOneOf> {
        self.mdib_access.read().await.context_state(handle)
    }

    async fn context_states(&self) -> Vec<AbstractContextStateOneOf> {
        self.mdib_access.read().await.context_states()
    }

    async fn entities_by_type<F>(&self, filter: F) -> Vec<MdibEntity>
    where
        F: 'static + Fn(&MdibEntity) -> bool + Send + Sync,
    {
        self.mdib_access.read().await.entities_by_type(filter)
    }

    async fn children_by_type<F>(&self, handle: &str, filter: F) -> Vec<MdibEntity>
    where
        F: 'static + Fn(&MdibEntity) -> bool + Send + Sync,
    {
        self.mdib_access
            .read()
            .await
            .children_by_type(handle, filter)
    }
}

#[async_trait]
impl<T, DESC, STATE> ObservableMdib for LocalMdibAccessImpl<T, DESC, STATE>
where
    T: MdibStorage + Send + Sync + Debug,
    DESC: DescriptionPreprocessor<T> + Send + Sync,
    STATE: StatePreprocessor<T> + Send + Sync,
{
    //noinspection DuplicatedCode
    async fn subscribe(&self) -> mpsc::Receiver<MdibAccessEvent> {
        self.channel_publisher.lock().await.subscribe().await
    }

    async fn unsubscribe_all(&self) {
        self.channel_publisher.lock().await.unsubscribe_all().await
    }
}

#[async_trait]
impl<T, DESC, STATE> LocalMdibAccess for LocalMdibAccessImpl<T, DESC, STATE>
where
    T: MdibStorage + Send + Sync + Debug,
    DESC: DescriptionPreprocessor<T> + Send + Sync,
    STATE: StatePreprocessor<T> + Send + Sync,
{
    async fn write_description(
        &mut self,
        description_modifications: MdibDescriptionModifications,
    ) -> Result<WriteDescriptionResult, WriteError> {
        // lock early
        let mut access = self.mdib_access.write().await;

        write_util::write_description(
            description_modifications,
            access.mdib_version().inc(),
            move |storage, desc, version| {
                storage.apply_description(
                    version,
                    Some(storage.md_description_version() + 1),
                    Some(storage.md_state_version() + 1),
                    desc,
                )
            },
            &*self.channel_publisher.lock().await,
            &mut *self.description_preprocessors.lock().await,
            &mut access,
        )
        .await
    }

    async fn write_states(
        &mut self,
        state_modifications: MdibStateModifications,
    ) -> Result<WriteStateResult, WriteError> {
        // lock early
        let mut access = self.mdib_access.write().await;

        write_util::write_states(
            state_modifications,
            access.mdib_version().inc(),
            move |storage, state, version| {
                storage.apply_state(version, Some(storage.md_state_version() + 1), state)
            },
            &*self.channel_publisher.lock().await,
            &mut *self.state_preprocessors.lock().await,
            &mut access,
        )
        .await
    }
}

#[async_trait]
impl<T, DESC, STATE> LocalMdibAccessReadTransaction for LocalMdibAccessImpl<T, DESC, STATE>
where
    T: MdibStorage + Send + Sync + Debug,
    DESC: DescriptionPreprocessor<T> + Send + Sync,
    STATE: StatePreprocessor<T> + Send + Sync,
{
    type Access<'a>
        = LocalMdibAccessReadTransactionImpl<'a, T>
    where
        Self: 'a;

    async fn read_transaction<'b, 'a: 'b, G>(
        &'a self,
        func: impl FnOnce(Self::Access<'b>) -> BoxFuture<'b, G> + Send + Sync,
    ) -> G
    where
        Self: 'b,
    {
        let access = self.mdib_access.read().await; // lock here
        let facade = LocalMdibAccessReadTransactionImpl { lock: access };
        let result = func(facade).await;
        trace!("read_transaction finished");
        result
    }
}

pub struct LocalMdibAccessReadTransactionImpl<'a, T>
where
    T: MdibStorage + Send + Sync + Debug,
{
    lock: RwLockReadGuard<'a, T>,
}

#[async_trait]
impl<T: MdibStorage + Send + Sync + Debug> MdibAccess
    for LocalMdibAccessReadTransactionImpl<'_, T>
{
    async fn mdib_version(&self) -> MdibVersion {
        self.lock.mdib_version()
    }

    async fn md_description_version(&self) -> u64 {
        self.lock.md_description_version()
    }

    async fn md_state_version(&self) -> u64 {
        self.lock.md_state_version()
    }

    async fn entity(&self, handle: &str) -> Option<MdibEntity> {
        self.lock.entity(handle)
    }

    async fn root_entities(&self) -> Vec<MdibEntity> {
        self.lock.root_entities()
    }

    async fn context_state(&self, handle: &str) -> Option<AbstractContextStateOneOf> {
        self.lock.context_state(handle)
    }

    async fn context_states(&self) -> Vec<AbstractContextStateOneOf> {
        self.lock.context_states()
    }

    async fn entities_by_type<F>(&self, filter: F) -> Vec<MdibEntity>
    where
        F: 'static + Fn(&MdibEntity) -> bool + Send + Sync,
    {
        self.lock.entities_by_type(filter)
    }

    async fn children_by_type<F>(&self, handle: &str, filter: F) -> Vec<MdibEntity>
    where
        F: 'static + Fn(&MdibEntity) -> bool + Send + Sync,
    {
        self.lock.children_by_type(handle, filter)
    }
}

//noinspection DuplicatedCode
#[cfg(test)]
mod test {
    use std::time::Duration;

    use env_logger;
    use futures::FutureExt;
    use log::{error, info};
    use tokio::time::timeout;
    use tokio_stream::wrappers::ReceiverStream;
    use tokio_stream::StreamExt;

    use protosdc_biceps::biceps::{
        context_association_mod, AbstractContextState, AbstractContextStateOneOf,
        AbstractDeviceComponentStateOneOf, AbstractMetricStateOneOf, ContextAssociation,
        MdsDescriptor, MdsState, NumericMetricState, PatientContextState, ReferencedVersion,
        VersionCounter,
    };
    use protosdc_biceps::types::ProtoLanguage;

    use crate::common::access::mdib_access::{MdibAccess, MdibAccessEvent, ObservableMdib};
    use crate::common::biceps_util::{ContextState, Descriptor, State};
    use crate::common::mdib_description_modification::MdibDescriptionModification;
    use crate::common::mdib_description_modifications::MdibDescriptionModifications;
    use crate::common::mdib_entity::{Entity, EntityBase, MdibEntity};
    use crate::common::mdib_pair::Pair;
    use crate::common::mdib_state_modifications::MdibStateModifications;
    use crate::common::mdib_version::MdibVersionBuilder;
    use crate::common::preprocessing::PreprocessingError;
    use crate::common::storage::mdib_storage::{MdibStorageImpl, WriteError};
    use crate::provider::access::local_mdib_access::{
        DefaultLocalDescriptionPreprocessors, DefaultLocalStatePreprocessors, LocalMdibAccess,
        LocalMdibAccessImpl, LocalMdibAccessReadTransaction,
    };
    use crate::test_util::mdib::mdib_modification_presets;
    use crate::test_util::{handles, mdib_utils};

    fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[tokio::test]
    async fn test_initial_description_insert(
    ) -> Result<(), tokio::sync::broadcast::error::RecvError> {
        init();
        let mdib_version = MdibVersionBuilder::default().seq("wat:abc").create();

        let mut mdib_access = LocalMdibAccessImpl::<
            MdibStorageImpl,
            DefaultLocalDescriptionPreprocessors<_>,
            DefaultLocalStatePreprocessors<_>,
        >::new(mdib_version.clone());
        let mut receiver = mdib_access.subscribe().await;

        let modifications = mdib_modification_presets();
        let event_fut = receiver.recv();

        mdib_access
            .write_description(modifications)
            .await
            .expect("Description write failed");
        mdib_access.unsubscribe_all().await;

        let event = event_fut.await.unwrap();

        match event {
            MdibAccessEvent::DescriptionModification {
                inserted,
                updated,
                deleted,
                ..
            } => {
                assert_eq!(mdib_modification_presets().into_vec().len(), inserted.len());
                assert!(updated.is_empty());
                assert!(deleted.is_empty());
            }
            _ => panic!("Unexpected event type"),
        };

        if receiver.recv().await.is_some() {
            panic!("Should not be Some")
        }

        Ok(())
    }

    #[tokio::test]
    async fn test_description_update_of_existing_entities() {
        init();

        let mut mdib_access = base_local_mdib(false).await;

        let mut receiver = mdib_access.subscribe().await;
        let event_fut = receiver.recv();

        let mut modifications = MdibDescriptionModifications::default();

        modifications
            .add(MdibDescriptionModification::Update {
                pair: Pair::from((
                    mdib_utils::create_minimal_mds_descriptor(handles::MDS_0),
                    mdib_utils::create_minimal_mds_state(handles::MDS_0),
                )),
            })
            .unwrap_or_else(|_| panic!("Could not update {}", handles::MDS_0));

        modifications
            .add(MdibDescriptionModification::Update {
                pair: Pair::from((
                    mdib_utils::create_minimal_numeric_metric_descriptor(handles::METRIC_0),
                    mdib_utils::create_minimal_numeric_metric_state(handles::METRIC_0),
                )),
            })
            .unwrap_or_else(|_| panic!("Could not update {}", handles::METRIC_0));

        modifications
            .add(MdibDescriptionModification::Update {
                pair: Pair::from((
                    mdib_utils::create_minimal_patient_context_descriptor(
                        handles::CONTEXT_DESCRIPTOR_0,
                    ),
                    vec![
                        mdib_utils::create_minimal_patient_context_state(
                            handles::CONTEXT_DESCRIPTOR_0,
                            handles::CONTEXT_0,
                        ),
                        mdib_utils::create_minimal_patient_context_state(
                            handles::CONTEXT_DESCRIPTOR_0,
                            handles::CONTEXT_1,
                        ),
                    ],
                )),
            })
            .unwrap_or_else(|_| panic!("Could not update {}", handles::CONTEXT_DESCRIPTOR_0));

        mdib_access
            .write_description(modifications)
            .await
            .expect("Could not write description");

        let event = event_fut.await;

        let mdib_access_event = event.unwrap();
        assert!(matches!(
            mdib_access_event,
            MdibAccessEvent::DescriptionModification { .. }
        ));

        if let MdibAccessEvent::DescriptionModification {
            inserted,
            updated,
            deleted,
            ..
        } = mdib_access_event
        {
            assert_eq!(0, inserted.len());
            assert_eq!(3, updated.len());
            assert_eq!(0, deleted.len());
        }
    }

    #[tokio::test]
    async fn test_description_delete() {
        init();

        let mut mdib_access = base_local_mdib(false).await;

        let mut receiver = mdib_access.subscribe().await;
        let event_fut = receiver.recv();

        let mut modifications = MdibDescriptionModifications::default();

        modifications
            .add(MdibDescriptionModification::Delete {
                handle: handles::CONTEXT_DESCRIPTOR_0.to_string(),
            })
            .expect("Could not add deletion");

        mdib_access
            .write_description(modifications)
            .await
            .expect("Could not write description");

        let event = event_fut.await;

        let mdib_access_event = event.unwrap();
        assert!(matches!(
            mdib_access_event,
            MdibAccessEvent::DescriptionModification { .. }
        ));

        if let MdibAccessEvent::DescriptionModification {
            inserted,
            updated,
            deleted,
            ..
        } = mdib_access_event
        {
            assert_eq!(0, inserted.len());
            assert_eq!(1, updated.len()); // parent version update
            assert_eq!(1, deleted.len());

            assert_eq!(
                handles::CONTEXT_DESCRIPTOR_0.to_string(),
                deleted.first().unwrap().entity.handle()
            );
        }

        assert!(mdib_access
            .entity(handles::CONTEXT_DESCRIPTOR_0)
            .await
            .is_none())
    }

    #[tokio::test]
    async fn test_existing_state_update() -> Result<(), tokio::sync::broadcast::error::RecvError> {
        init();

        let mut mdib_access = base_local_mdib(false).await;

        let receiver = mdib_access.subscribe().await;

        // collect all events in a task, because why not?
        let events_future = tokio::spawn(async {
            let mut ev = vec![];
            let mut strm = ReceiverStream::new(receiver);
            while let Some(item) = strm.next().await {
                ev.push(item)
            }
            ev
        });

        let expected_mds0_lang = "de".to_string();
        let expected_mds1_lang = "en-DE".to_string();
        let expected_avg_period = Duration::from_secs(3600);
        let expected_association = Some(ContextAssociation {
            enum_type: context_association_mod::EnumType::Dis,
        });

        let new_mds0 = AbstractDeviceComponentStateOneOf::MdsState(MdsState {
            lang_attr: Some(ProtoLanguage {
                language: expected_mds0_lang.clone(),
            }),
            ..mdib_utils::create_minimal_mds_state(handles::MDS_0)
        });

        let new_mds1 = AbstractDeviceComponentStateOneOf::MdsState(MdsState {
            lang_attr: Some(ProtoLanguage {
                language: expected_mds1_lang.clone(),
            }),
            ..mdib_utils::create_minimal_mds_state(handles::MDS_1)
        });

        let new_metric = AbstractMetricStateOneOf::NumericMetricState(NumericMetricState {
            active_averaging_period_attr: Some(expected_avg_period.into()),
            ..mdib_utils::create_minimal_numeric_metric_state(handles::METRIC_0)
        });

        let new_context = AbstractContextStateOneOf::PatientContextState(PatientContextState {
            abstract_context_state: AbstractContextState {
                context_association_attr: expected_association.clone(),
                ..mdib_utils::create_minimal_patient_context_state(
                    handles::CONTEXT_DESCRIPTOR_0,
                    handles::CONTEXT_1,
                )
                .abstract_context_state
            },
            ..mdib_utils::create_minimal_patient_context_state(
                handles::CONTEXT_DESCRIPTOR_0,
                handles::CONTEXT_1,
            )
        });

        mdib_access
            .write_states(MdibStateModifications::Component {
                component_states: vec![new_mds0, new_mds1],
            })
            .await
            .expect("write failed");

        mdib_access
            .write_states(MdibStateModifications::Metric {
                metric_states: vec![new_metric],
            })
            .await
            .expect("write failed");

        mdib_access
            .write_states(MdibStateModifications::Context {
                context_states: vec![new_context],
            })
            .await
            .expect("write failed");

        mdib_access.unsubscribe_all().await;

        let events = events_future.await.expect("Could not get events");

        assert_eq!(3, events.len());

        assert!(matches!(
            events.first().unwrap(),
            MdibAccessEvent::ComponentStateModification { .. }
        ));
        assert!(matches!(
            events.get(1).unwrap(),
            MdibAccessEvent::MetricStateModification { .. }
        ));
        assert!(matches!(
            events.get(2).unwrap(),
            MdibAccessEvent::ContextStateModification { .. }
        ));

        // verify mds
        match events.first().unwrap() {
            MdibAccessEvent::ComponentStateModification { states, .. } => {
                assert_eq!(2, states.len());
                for (source_mds, mds_states) in states {
                    // one mds "per mds"
                    assert_eq!(1, mds_states.len());
                    for state in mds_states {
                        match state {
                            AbstractDeviceComponentStateOneOf::MdsState(mds) => {
                                if mds.descriptor_handle() == handles::MDS_0 {
                                    assert_eq!(
                                        Some(ProtoLanguage {
                                            language: expected_mds0_lang.clone()
                                        }),
                                        mds.lang_attr
                                    );
                                    assert_eq!(handles::MDS_0, source_mds);
                                } else {
                                    assert_eq!(
                                        Some(ProtoLanguage {
                                            language: expected_mds1_lang.clone()
                                        }),
                                        mds.lang_attr
                                    );
                                    assert_eq!(handles::MDS_1, source_mds);
                                }
                            }
                            _ => panic!("Unexpected state type"),
                        }
                    }
                }
            }
            _ => panic!("Unexpected modification type"),
        };

        let mds_entity0 = mdib_access
            .entity(handles::MDS_0)
            .await
            .unwrap_or_else(|| panic!("Missing entity {}", handles::MDS_0));

        let mds_entity1 = mdib_access
            .entity(handles::MDS_1)
            .await
            .unwrap_or_else(|| panic!("Missing entity {}", handles::MDS_1));

        if let Entity::Mds(mds) = mds_entity0.entity {
            assert_eq!(
                Some(ProtoLanguage {
                    language: expected_mds0_lang.clone()
                }),
                mds.pair.state.lang_attr
            )
        } else {
            panic!("Mds missing")
        };

        if let Entity::Mds(mds) = mds_entity1.entity {
            assert_eq!(
                Some(ProtoLanguage {
                    language: expected_mds1_lang.clone()
                }),
                mds.pair.state.lang_attr
            )
        } else {
            panic!("Mds missing")
        };

        match events.get(1).unwrap() {
            MdibAccessEvent::MetricStateModification { states, .. } => {
                assert_eq!(1, states.len());
                for (source_mds, mds_states) in states {
                    assert_eq!(handles::MDS_0, source_mds);
                    assert_eq!(1, mds_states.len());
                    for state in mds_states {
                        match state {
                            AbstractMetricStateOneOf::NumericMetricState(num) => {
                                assert_eq!(
                                    Some(expected_avg_period.into()),
                                    num.active_averaging_period_attr
                                );
                            }
                            _ => panic!("Unexpected state type"),
                        }
                    }
                }
            }
            _ => panic!("Unexpected modification type"),
        }

        let metric_entity0 = mdib_access
            .entity(handles::METRIC_0)
            .await
            .unwrap_or_else(|| panic!("Missing entity {}", handles::METRIC_0));

        if let Entity::NumericMetric(metric) = metric_entity0.entity {
            assert_eq!(
                Some(expected_avg_period.into()),
                metric.pair.state.active_averaging_period_attr
            )
        } else {
            panic!("NumericMetric missing")
        };

        match events.get(2).unwrap() {
            MdibAccessEvent::ContextStateModification {
                updated_context_states,
                removed_context_states,
                ..
            } => {
                assert_eq!(1, updated_context_states.len());
                assert_eq!(0, removed_context_states.len());
                for (source_mds, mds_states) in updated_context_states {
                    assert_eq!(1, mds_states.len());
                    assert_eq!(handles::MDS_0, source_mds);
                    for state in mds_states {
                        match state {
                            AbstractContextStateOneOf::PatientContextState(ctx) => {
                                assert_eq!(
                                    expected_association,
                                    ctx.abstract_context_state.context_association_attr
                                );
                            }
                            _ => panic!("Unexpected state type"),
                        }
                    }
                }
            }
            _ => panic!("Unexpected modification type"),
        }

        let ctx_state = mdib_access
            .context_state(handles::CONTEXT_1)
            .await
            .unwrap_or_else(|| panic!("Missing context {}", handles::CONTEXT_1));

        if let AbstractContextStateOneOf::PatientContextState(metric) = ctx_state {
            assert_eq!(
                expected_association,
                metric.abstract_context_state.context_association_attr
            )
        } else {
            panic!("PatientContextState missing")
        };

        Ok(())
    }

    #[tokio::test]
    async fn test_update_then_delete_and_reinsert_entities() {
        init();

        let mut mdib_access = base_local_mdib(false).await;

        // update metric state to make sure version is remembered correctly for reinsert
        {
            let current_mdib_version = mdib_access.mdib_version().await;
            let metric_entity = mdib_access.entity(handles::METRIC_0).await.unwrap();

            let mut modifications = MdibDescriptionModifications::default();
            modifications
                .add(MdibDescriptionModification::Update {
                    pair: metric_entity.entity.into(),
                })
                .expect("Could not add deletion");

            mdib_access
                .write_description(modifications)
                .await
                .expect("Could not write description");

            let metric_entity_updated = mdib_access.entity(handles::METRIC_0).await.unwrap();

            assert_eq!(
                1,
                metric_entity_updated
                    .entity
                    .get_abstract_descriptor()
                    .version()
            );
            assert_eq!(
                1,
                metric_entity_updated
                    .resolve_single_state()
                    .unwrap()
                    .descriptor_version()
            );
            assert_eq!(
                1,
                metric_entity_updated
                    .resolve_single_state()
                    .unwrap()
                    .version()
            );

            assert_eq!(
                current_mdib_version.version + 1,
                mdib_access.mdib_version().await.version
            );
        }

        let current_mdib_version = mdib_access.mdib_version().await;
        {
            let mut modifications = MdibDescriptionModifications::default();
            modifications
                .add(MdibDescriptionModification::Delete {
                    handle: handles::METRIC_0.to_string(),
                })
                .expect("Could not add deletion");

            modifications
                .add(MdibDescriptionModification::Delete {
                    handle: handles::CONTEXT_DESCRIPTOR_0.to_string(),
                })
                .expect("Could not add deletion");

            mdib_access
                .write_description(modifications)
                .await
                .expect("Could not write description");
        }

        let after_first_write = mdib_access.mdib_version().await;
        assert_eq!(current_mdib_version.version + 1, after_first_write.version);

        let mut subscription = mdib_access.subscribe().await;

        let mut modifications = MdibDescriptionModifications::default();
        modifications
            .add(MdibDescriptionModification::Insert {
                pair: Pair::from((
                    mdib_utils::create_minimal_numeric_metric_descriptor(handles::METRIC_0),
                    mdib_utils::create_minimal_numeric_metric_state(handles::METRIC_0),
                )),
                parent: Some(handles::CHANNEL_0.to_string()),
            })
            .unwrap_or_else(|_| panic!("Could not add metric {}", handles::METRIC_0));

        // patient context data
        let pat2 = {
            let stuff = mdib_utils::create_minimal_patient_context_state(
                handles::CONTEXT_DESCRIPTOR_0,
                handles::CONTEXT_1,
            );

            PatientContextState {
                abstract_context_state: AbstractContextState {
                    context_association_attr: Some(ContextAssociation {
                        enum_type: context_association_mod::EnumType::No,
                    }),
                    ..stuff.abstract_context_state
                },
                ..stuff
            }
        };

        modifications
            .add(MdibDescriptionModification::Insert {
                pair: Pair::from((
                    mdib_utils::create_minimal_patient_context_descriptor(
                        handles::CONTEXT_DESCRIPTOR_0,
                    ),
                    pat2,
                )),
                parent: Some(handles::SYSTEM_CONTEXT_0.to_string()),
            })
            .unwrap_or_else(|_| {
                panic!(
                    "Could not add patient context {}",
                    handles::CONTEXT_DESCRIPTOR_0
                )
            });

        let write_result = mdib_access
            .write_description(modifications)
            .await
            .expect("Could not write description");

        mdib_access.unsubscribe_all().await;

        let event0 = subscription.recv().await.expect("Event0 missing");

        let after_second_write = mdib_access.mdib_version().await;
        assert_eq!(after_first_write.version + 1, after_second_write.version);
        assert_eq!(write_result.mdib_version, after_second_write);
        assert_eq!(event0.mdib_version(), &after_second_write);

        let check_metric_entity = |entity: MdibEntity| {
            assert!(matches!(entity.entity, Entity::NumericMetric { .. }));
            if let Entity::NumericMetric(metric) = entity.entity {
                assert_eq!(2, metric.pair.descriptor.version());
                assert_eq!(2, metric.pair.state.descriptor_version());
                assert_eq!(2, metric.pair.state.version());
            } else {
                panic!()
            }
        };

        // check metric
        check_metric_entity(write_result.inserted.first().unwrap().clone());

        if let MdibAccessEvent::DescriptionModification {
            inserted,
            updated,
            deleted,
            ..
        } = event0.clone()
        {
            check_metric_entity(inserted.first().unwrap().clone());
            assert_eq!(2, inserted.len());
            assert_eq!(2, updated.len());
            assert_eq!(0, deleted.len());

            // check that correct parents were updated exactly once
            let updated_handles: Vec<String> = updated
                .iter()
                .map(|state| state.entity.get_abstract_descriptor().handle())
                .collect();
            assert!(updated_handles.contains(&handles::CHANNEL_0.to_string()));
            assert!(updated_handles.contains(&handles::SYSTEM_CONTEXT_0.to_string()))
        } else {
            panic!()
        }

        check_metric_entity(mdib_access.entity(handles::METRIC_0).await.unwrap());

        let check_context_entity = |entity: MdibEntity| {
            assert!(matches!(entity.entity, Entity::PatientContext { .. }));
            if let Entity::PatientContext(metric) = entity.entity {
                assert_eq!(1, metric.pair.descriptor.version());
                assert_eq!(1, metric.pair.states.len());
                assert_eq!(1, metric.pair.states.first().unwrap().descriptor_version());
                assert_eq!(1, metric.pair.states.first().unwrap().version());
            } else {
                panic!()
            }
        };

        // check context
        check_context_entity(write_result.inserted.get(1).unwrap().clone());

        if let MdibAccessEvent::DescriptionModification {
            inserted,
            updated,
            deleted,
            ..
        } = event0
        {
            check_context_entity(inserted.get(1).unwrap().clone());
            assert_eq!(2, inserted.len());
            assert_eq!(2, updated.len());
            assert_eq!(0, deleted.len());

            // check that correct parents were updated exactly once
            let updated_handles: Vec<String> = updated
                .iter()
                .map(|state| state.entity.get_abstract_descriptor().handle())
                .collect();
            assert!(updated_handles.contains(&handles::CHANNEL_0.to_string()));
            assert!(updated_handles.contains(&handles::SYSTEM_CONTEXT_0.to_string()))
        } else {
            panic!()
        }

        check_context_entity(
            mdib_access
                .entity(handles::CONTEXT_DESCRIPTOR_0)
                .await
                .unwrap(),
        );
    }

    #[tokio::test]
    async fn test_update_only_subset_of_context_entity() {
        init();

        let mut mdib_access = base_local_mdib(false).await;
        let mut receiver = mdib_access.subscribe().await;

        let write_result = mdib_access
            .write_states(MdibStateModifications::Context {
                context_states: vec![mdib_utils::create_minimal_patient_context_state(
                    handles::CONTEXT_DESCRIPTOR_0,
                    handles::CONTEXT_1,
                )
                .into_abstract_context_state_one_of()],
            })
            .await;

        assert!(write_result.is_ok());

        mdib_access.unsubscribe_all().await;

        let event = receiver.recv().await.expect("Subscription failed");
        assert!(matches!(
            event,
            MdibAccessEvent::ContextStateModification { .. }
        ));

        let mut expected_context_state = mdib_utils::create_minimal_patient_context_state(
            handles::CONTEXT_DESCRIPTOR_0,
            handles::CONTEXT_1,
        );
        expected_context_state
            .abstract_context_state
            .abstract_multi_state
            .abstract_state
            .state_version_attr = Some(VersionCounter { unsigned_long: 1 });

        expected_context_state
            .abstract_context_state
            .abstract_multi_state
            .abstract_state
            .descriptor_version_attr = Some(ReferencedVersion {
            version_counter: VersionCounter { unsigned_long: 0 },
        });

        let expected_state = expected_context_state.into_abstract_context_state_one_of();

        match event {
            MdibAccessEvent::ContextStateModification {
                updated_context_states,
                removed_context_states,
                ..
            } => {
                assert_eq!(0, removed_context_states.len());
                assert_eq!(
                    &expected_state,
                    updated_context_states
                        .get(handles::MDS_0)
                        .expect("no mds1")
                        .first()
                        .expect("No context state update")
                )
            }
            _ => panic!("Wrong kind of modification"),
        }
    }

    #[tokio::test]
    async fn test_deletion_and_reinsertion_of_context_state() {
        init();

        let mut mdib_access = base_local_mdib(true).await;
        let mut receiver = mdib_access.subscribe().await;

        let no_assoc = Some(ContextAssociation {
            enum_type: context_association_mod::EnumType::No,
        });
        let assoc = Some(ContextAssociation {
            enum_type: context_association_mod::EnumType::Assoc,
        });

        assert!(mdib_access
            .context_state(handles::CONTEXT_1)
            .await
            .is_none());

        // delete context
        let no_assoc_context =
            AbstractContextStateOneOf::PatientContextState(PatientContextState {
                abstract_context_state: AbstractContextState {
                    context_association_attr: no_assoc.clone(),
                    ..mdib_utils::create_minimal_patient_context_state(
                        handles::CONTEXT_DESCRIPTOR_0,
                        handles::CONTEXT_1,
                    )
                    .abstract_context_state
                },
                ..mdib_utils::create_minimal_patient_context_state(
                    handles::CONTEXT_DESCRIPTOR_0,
                    handles::CONTEXT_1,
                )
            });

        mdib_access
            .write_states(MdibStateModifications::Context {
                context_states: vec![no_assoc_context],
            })
            .await
            .expect("write failed");

        let event = receiver.recv().await.expect("Event broken");
        assert!(matches!(
            event,
            MdibAccessEvent::ContextStateModification { .. }
        ));

        assert!(mdib_access
            .context_state(handles::CONTEXT_1)
            .await
            .is_none());

        // insert context
        let new_context = AbstractContextStateOneOf::PatientContextState(PatientContextState {
            abstract_context_state: AbstractContextState {
                context_association_attr: assoc.clone(),
                ..mdib_utils::create_minimal_patient_context_state(
                    handles::CONTEXT_DESCRIPTOR_0,
                    handles::CONTEXT_1,
                )
                .abstract_context_state
            },
            ..mdib_utils::create_minimal_patient_context_state(
                handles::CONTEXT_DESCRIPTOR_0,
                handles::CONTEXT_1,
            )
        });

        mdib_access
            .write_states(MdibStateModifications::Context {
                context_states: vec![new_context],
            })
            .await
            .expect("write failed");

        let event = receiver.recv().await.expect("Event broken");
        assert!(matches!(
            event,
            MdibAccessEvent::ContextStateModification { .. }
        ));

        assert!(mdib_access
            .context_state(handles::CONTEXT_1)
            .await
            .is_some());
    }

    #[tokio::test]
    async fn test_fail_on_inserting_existing_handle() {
        init();

        let mut mdib_access = base_local_mdib(false).await;
        let mut receiver = mdib_access.subscribe().await;

        {
            let mut modifications = MdibDescriptionModifications::default();
            modifications
                .add(MdibDescriptionModification::Insert {
                    pair: Pair::from((
                        mdib_utils::create_minimal_vmd_descriptor(handles::VMD_0),
                        mdib_utils::create_minimal_vmd_state(handles::VMD_0),
                    )),
                    parent: Some(handles::MDS_0.to_string()),
                })
                .unwrap_or_else(|_| panic!("Could not add vmd {}", handles::VMD_0));

            let write_result = mdib_access.write_description(modifications).await;

            match write_result {
                Ok(_) => {
                    panic!("Not ok!")
                }
                Err(err) => {
                    assert_eq!(
                        WriteError::PreprocessingError(
                            PreprocessingError::DuplicateDescriptorHandle {
                                handles: vec![handles::VMD_0.to_string()]
                            }
                        ),
                        err
                    )
                }
            }
        }
        {
            let mut modifications = MdibDescriptionModifications::default();
            modifications
                .add(MdibDescriptionModification::Insert {
                    pair: Pair::from((
                        mdib_utils::create_minimal_patient_context_descriptor(
                            handles::CONTEXT_DESCRIPTOR_0,
                        ),
                        mdib_utils::create_minimal_patient_context_state(
                            handles::CONTEXT_DESCRIPTOR_0,
                            handles::CONTEXT_0,
                        ),
                    )),
                    parent: Some(handles::SYSTEM_CONTEXT_0.to_string()),
                })
                .unwrap_or_else(|_| panic!("Could not add vmd {}", handles::CONTEXT_DESCRIPTOR_0));

            let write_result = mdib_access.write_description(modifications).await;

            match write_result {
                Ok(_) => {
                    panic!("Not ok!")
                }
                Err(err) => {
                    assert_eq!(
                        WriteError::PreprocessingError(
                            PreprocessingError::DuplicateDescriptorHandle {
                                handles: vec![handles::CONTEXT_DESCRIPTOR_0.to_string()]
                            }
                        ),
                        err
                    )
                }
            }
        }
        mdib_access.unsubscribe_all().await;

        if receiver.recv().await.is_some() {
            panic!("Should not be Some")
        }
    }

    #[tokio::test]
    async fn test_fail_on_update_unknown_entity() {
        init();

        let mut mdib_access = base_local_mdib(false).await;
        let mut receiver = mdib_access.subscribe().await;

        let mut modifications = MdibDescriptionModifications::default();
        modifications
            .add(MdibDescriptionModification::Update {
                pair: Pair::from((
                    mdib_utils::create_minimal_numeric_metric_descriptor(handles::METRIC_3),
                    mdib_utils::create_minimal_numeric_metric_state(handles::METRIC_3),
                )),
            })
            .unwrap_or_else(|_| panic!("Could not add metric {}", handles::METRIC_3));

        let write_result = mdib_access.write_description(modifications).await;

        match write_result {
            Ok(_) => {
                panic!("Not ok!")
            }
            Err(err) => {
                assert_eq!(
                    WriteError::PreprocessingError(PreprocessingError::EntityUnknown {
                        handle: handles::METRIC_3.to_string()
                    }),
                    err
                )
            }
        }

        mdib_access.unsubscribe_all().await;

        if receiver.recv().await.is_some() {
            panic!("Should not be Some")
        }
    }

    #[tokio::test]
    async fn test_fail_on_wrong_insertion_order() {
        init();
        let mdib_version = MdibVersionBuilder::default().seq("wat:abc").create();

        let mut mdib_access = LocalMdibAccessImpl::<
            MdibStorageImpl,
            DefaultLocalDescriptionPreprocessors<_>,
            DefaultLocalStatePreprocessors<_>,
        >::new_with_context_option(mdib_version.clone(), false);
        let mut receiver = mdib_access.subscribe().await;

        let mut modifications = MdibDescriptionModifications::default();

        modifications
            .add(MdibDescriptionModification::Insert {
                pair: Pair::from((
                    mdib_utils::create_minimal_vmd_descriptor(handles::VMD_0),
                    mdib_utils::create_minimal_vmd_state(handles::VMD_0),
                )),
                parent: Some(handles::MDS_0.to_string()),
            })
            .unwrap_or_else(|_| panic!("Could not add vmd {}", handles::VMD_0));

        modifications
            .add(MdibDescriptionModification::Insert {
                pair: Pair::from((
                    mdib_utils::create_minimal_mds_descriptor(handles::MDS_0),
                    mdib_utils::create_minimal_mds_state(handles::MDS_0),
                )),
                parent: None,
            })
            .unwrap_or_else(|_| panic!("Could not add mds {}", handles::MDS_0));

        let write_result = mdib_access.write_description(modifications).await;

        match write_result {
            Ok(_) => {
                panic!("Not ok!")
            }
            Err(err) => {
                assert_eq!(
                    WriteError::PreprocessingError(PreprocessingError::ParentEntityMissing {
                        parent: handles::MDS_0.to_string(),
                        child: handles::VMD_0.to_string()
                    }),
                    err
                )
            }
        }

        mdib_access.unsubscribe_all().await;

        if receiver.recv().await.is_some() {
            panic!("Should not be Some")
        }
    }

    #[tokio::test]
    async fn test_descriptor_child_remover() {
        init();

        let mdib_version = MdibVersionBuilder::default().seq("wat:abc").create();

        let mut mdib_access = LocalMdibAccessImpl::<MdibStorageImpl, _, _>::new_with_context_option(
            mdib_version.clone(),
            false,
        );

        let mut modifications = MdibDescriptionModifications::default();
        let vmd_desc = mdib_utils::create_minimal_vmd_descriptor(handles::VMD_0);
        modifications
            .add(MdibDescriptionModification::Insert {
                pair: Pair::from((
                    MdsDescriptor {
                        vmd: vec![vmd_desc],
                        ..mdib_utils::create_minimal_mds_descriptor(handles::MDS_0)
                    },
                    mdib_utils::create_minimal_mds_state(handles::MDS_0),
                )),
                parent: None,
            })
            .unwrap_or_else(|_| panic!("Could not add mds {}", handles::MDS_0));

        mdib_access
            .write_description(modifications)
            .await
            .expect("Write failed for no reason");

        match mdib_access
            .entity(handles::MDS_0)
            .await
            .expect("Mds missing")
            .entity
        {
            Entity::Mds(mds) => {
                assert!(mds.pair.descriptor.vmd.is_empty())
            }
            _ => panic!("Wrong entity type"),
        }
    }

    #[tokio::test]
    async fn test_read_transaction_locks() {
        init();

        let mdib_access = base_local_mdib(false).await;
        let storage = mdib_access.mdib_access.clone();
        let storage2 = mdib_access.mdib_access.clone();

        let read_sleep = 200;

        {
            let read_transaction = mdib_access.read_transaction(|access| {
                async move {
                    info!("read_transaction started");
                    tokio::time::sleep(Duration::from_millis(read_sleep)).await;
                    let mdd = access.md_description_version().await;
                    info!("read_transaction finished");
                    mdd
                }
                .boxed()
            });

            // shorter than the lock above to make sure this times out
            let concurrent_write = timeout(Duration::from_millis(read_sleep / 2), async {
                info!("concurrent_write started");
                let x = {
                    // trace this so it doesn't get optimized away and is not unused
                    let x = storage.write().await;
                    error!("Acquired lock, this must not happen: {:?}", &x);
                    x
                };
                info!("concurrent_write finished");
                x
            });

            let (_read, write) = tokio::join!(read_transaction, concurrent_write);
            if write.is_ok() {
                panic!("Should've failed")
            };
        }

        if timeout(Duration::from_secs(read_sleep), storage2.write())
            .await
            .is_err()
        {
            panic!("Should've succeeded")
        };
    }

    async fn base_local_mdib(
        remove_not_associated_context_states: bool,
    ) -> LocalMdibAccessImpl<
        MdibStorageImpl,
        DefaultLocalDescriptionPreprocessors<MdibStorageImpl>,
        DefaultLocalStatePreprocessors<MdibStorageImpl>,
    > {
        info!("Creating base local mdib");
        let mdib_version = MdibVersionBuilder::default().seq("wat:abc").create();

        let mut mdib_access = LocalMdibAccessImpl::new_with_context_option(
            mdib_version,
            remove_not_associated_context_states,
        );

        let modifications = mdib_modification_presets();
        match mdib_access.write_description(modifications).await {
            Ok(acc) => acc,
            Err(err) => {
                error!("Something broke");
                panic!("base mdib broke {:?}", err)
            }
        };

        mdib_access
    }
}
