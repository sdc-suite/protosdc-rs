use itertools::Itertools;
use std::collections::HashMap;
use std::marker::PhantomData;

use protosdc_biceps::biceps::AbstractContextStateOneOf;

use crate::common::biceps_util::{MultiState, State};
use crate::common::mdib_state_modifications::MdibStateModifications;
use crate::common::preprocessing::{PreprocessingError, StatePreprocessor};
use crate::common::storage::mdib_storage::MdibStorage;

/// Preprocessing segment that returns an error if two context state have the same handle but
/// different descriptor handles.
///
/// This could mean that the context state handle is used multiple times or that the context state
/// was assigned to a new descriptor. Both operations are not allowed.
#[derive(Debug)]
pub struct DuplicateContextStateHandleHandler<T: MdibStorage> {
    all_context_states: HashMap<String, AbstractContextStateOneOf>,
    phantom: PhantomData<T>,
}

impl<T: MdibStorage> Default for DuplicateContextStateHandleHandler<T> {
    fn default() -> Self {
        DuplicateContextStateHandleHandler {
            all_context_states: HashMap::new(),
            phantom: Default::default(),
        }
    }
}

impl<T: MdibStorage> StatePreprocessor<T> for DuplicateContextStateHandleHandler<T> {
    fn before_first_modification(
        &mut self,
        mdib_storage: &T,
        modifications: MdibStateModifications,
    ) -> Result<MdibStateModifications, PreprocessingError>
    where
        T: MdibStorage,
    {
        self.all_context_states = mdib_storage
            .context_states()
            .into_iter()
            .map(|it| (it.state_handle(), it))
            .collect();

        Ok(modifications)
    }

    fn process(
        &mut self,
        _: &T,
        modifications: MdibStateModifications,
    ) -> Result<MdibStateModifications, PreprocessingError>
    where
        T: MdibStorage,
    {
        let errors = match &modifications {
            MdibStateModifications::Context { context_states } => context_states
                .iter()
                .map(|it| match it {
                    AbstractContextStateOneOf::OperatorContextState(it) => {
                        (it.descriptor_handle(), it.state_handle())
                    }
                    AbstractContextStateOneOf::EnsembleContextState(it) => {
                        (it.descriptor_handle(), it.state_handle())
                    }
                    AbstractContextStateOneOf::WorkflowContextState(it) => {
                        (it.descriptor_handle(), it.state_handle())
                    }
                    AbstractContextStateOneOf::PatientContextState(it) => {
                        (it.descriptor_handle(), it.state_handle())
                    }
                    AbstractContextStateOneOf::LocationContextState(it) => {
                        (it.descriptor_handle(), it.state_handle())
                    }
                    AbstractContextStateOneOf::MeansContextState(it) => {
                        (it.descriptor_handle(), it.state_handle())
                    }
                    AbstractContextStateOneOf::AbstractContextState(it) => {
                        (it.descriptor_handle(), it.state_handle())
                    }
                })
                .filter_map(|(descriptor_handle, state_handle)| {
                    match self.all_context_states.get(&state_handle) {
                        None => None,
                        Some(state) => {
                            if state.descriptor_handle() != descriptor_handle {
                                Some(state.state_handle())
                            } else {
                                None
                            }
                        }
                    }
                })
                .collect_vec(),
            _ => return Ok(modifications),
        };

        if !errors.is_empty() {
            return Err(PreprocessingError::DuplicateContextStateHandle { handles: errors });
        }
        Ok(modifications)
    }
}

#[cfg(test)]
mod test {
    use env_logger;

    use crate::common::biceps_util::ContextState;
    use crate::common::mdib_state_modifications::MdibStateModifications;
    use crate::common::preprocessing::{PreprocessingError, StatePreprocessor};
    use crate::provider::preprocessing::duplicate_context_state_handle_handler::DuplicateContextStateHandleHandler;
    use crate::test_util::mdib::base_storage;
    use crate::test_util::{handles, mdib_utils};

    fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[tokio::test]
    async fn test_valid_location() {
        init();
        let storage = base_storage();
        let mut checker = DuplicateContextStateHandleHandler::default();

        let modification = MdibStateModifications::Context {
            context_states: vec![mdib_utils::create_minimal_location_context_state(
                handles::CONTEXT_DESCRIPTOR_1,
                handles::CONTEXT_4,
            )
            .into_abstract_context_state_one_of()],
        };

        // should work
        let pre_result = checker
            .before_first_modification(&storage, modification)
            .expect("pre failed?");
        let result = checker.process(&storage, pre_result);

        assert!(result.is_ok())
    }

    #[tokio::test]
    async fn test_location_context_patient_collision() {
        init();
        let storage = base_storage();
        let mut checker = DuplicateContextStateHandleHandler::default();

        let modification = MdibStateModifications::Context {
            context_states: vec![mdib_utils::create_minimal_location_context_state(
                handles::CONTEXT_DESCRIPTOR_1,
                handles::CONTEXT_0,
            )
            .into_abstract_context_state_one_of()],
        };

        // should work
        let pre_result = checker
            .before_first_modification(&storage, modification)
            .expect("pre failed?");
        let result = checker.process(&storage, pre_result);

        assert!(result.is_err());
        assert_eq!(
            PreprocessingError::DuplicateContextStateHandle {
                handles: vec![handles::CONTEXT_0.to_string()]
            },
            result.err().unwrap()
        );
    }
}
