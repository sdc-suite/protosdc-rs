pub mod crypto;
pub mod service;

#[cfg(feature = "test-utilities")]
pub mod test_util;

pub use service::{Service, ServiceDelegate, ServiceError, ServiceState};

use log::warn;
use std::fmt::Debug;
use tokio::sync::{broadcast, mpsc, oneshot};

/// Attempts sending an event to the provided channel, logging a warning otherwise.
///
/// This function attempts to send an event of any type `T` that can be converted into `U`
/// through a given mpsc (multi-producer, single-consumer) channel. If an error occurs
/// during the send operation, it logs the error.
///
/// # Arguments
///
/// * `channel` - An mpsc Sender channel through which the event is sent.
/// * `event` - The event of type `T` to be sent through the channel.
///
/// # Type parameters
///
/// * `T` - The type of the event to be sent. Must implement Into<U>.
/// * `U` - The type the event is converted into before sending. Must implement Debug.
///
/// # Errors
///
/// This function will log a warning if the send operation fails.
///
/// # Example
///
/// ```rust
/// use common::send_log_error;
/// use tokio::sync::mpsc;
///
/// # tokio_test::block_on(async {
/// let (tx, _rx) = mpsc::channel::<i32>(1);
/// let event: i32 = 16;
/// send_log_error(&tx, event).await;
/// # })
/// ```
pub async fn send_log_error<T: Into<U>, U: Debug>(channel: &mpsc::Sender<U>, event: T) {
    if let Err(err) = channel.send(event.into()).await {
        warn!("Error sending mpsc event: {:?}", err)
    };
}

/// Attempts sending an event to the provided oneshot, logging a warning otherwise.
///
/// This function attempts to send an event of any type `T` that can be converted into `U`
/// through a given oneshot channel. If an error occurs
/// during the send operation, it logs the error.
///
/// # Arguments
///
/// * `channel` - A oneshot Sender channel through which the event is sent.
/// * `event` - The event of type `T` to be sent through the channel.
///
/// # Type parameters
///
/// * `T` - The type of the event to be sent. Must implement Into<U>.
/// * `U` - The type the event is converted into before sending. Must implement Debug.
///
/// # Errors
///
/// This function will log a warning if the send operation fails.
///
/// # Example
///
/// ```rust
/// use common::oneshot_log_error;
/// use tokio::sync::oneshot;
///
/// # tokio_test::block_on(async {
/// let (tx, _rx) = oneshot::channel::<i32>();
/// let event: i32 = 16;
/// oneshot_log_error(tx, event);
/// # })
/// ```
pub fn oneshot_log_error<T: Into<U>, U: Debug>(channel: oneshot::Sender<U>, event: T) {
    if let Err(err) = channel.send(event.into()) {
        warn!("Error sending oneshot event: {:?}", err)
    };
}

/// Attempts sending an event to the provided channel, logging a warning otherwise.
///
/// This function attempts to send an event of any type `T` that can be converted into `U`
/// through a given broadcast channel. If an error occurs
/// during the send operation, it logs the error.
///
/// # Arguments
///
/// * `channel` - A broadcast Sender channel through which the event is sent.
/// * `event` - The event of type `T` to be sent through the channel.
///
/// # Type parameters
///
/// * `T` - The type of the event to be sent. Must implement Into<U>.
/// * `U` - The type the event is converted into before sending. Must implement Debug.
///
/// # Errors
///
/// This function will log a warning if the send operation fails.
///
/// # Example
///
/// ```rust
/// use common::broadcast_event_log_error;
/// use tokio::sync::broadcast;
///
/// # tokio_test::block_on(async {
/// let (tx, _rx) = broadcast::channel::<i32>(1);
/// let event: i32 = 16;
/// broadcast_event_log_error(&tx, event);
/// # })
/// ```
pub fn broadcast_event_log_error<T: Into<U>, U: Debug>(channel: &broadcast::Sender<U>, event: T) {
    if let Err(err) = channel.send(event.into()) {
        warn!("Error sending broadcast event: {:?}", err)
    };
}
