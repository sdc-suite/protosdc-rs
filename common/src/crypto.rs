use std::convert::TryInto;
use std::sync::Arc;

use log::{debug, error};
use pem::Pem;
use pkcs8::der::pem::PemLabel;
use pkcs8::der::Decode;
use pkcs8::{LineEnding, PrivateKeyInfo};
use thiserror::Error;
use tokio;
use tokio::io;
use tokio_rustls::rustls::client::danger::{
    HandshakeSignatureValid, ServerCertVerified, ServerCertVerifier,
};
use tokio_rustls::rustls::client::verify_server_cert_signed_by_trust_anchor;
use tokio_rustls::rustls::crypto::{
    ring, verify_tls12_signature, verify_tls13_signature, CryptoProvider, WebPkiSupportedAlgorithms,
};
use tokio_rustls::rustls::pki_types::{CertificateDer, PrivateKeyDer, ServerName, UnixTime};
use tokio_rustls::rustls::server::{ParsedCertificate, WebPkiClientVerifier};
use tokio_rustls::rustls::{
    ClientConfig, DigitallySignedStruct, Error as TLSError, RootCertStore, ServerConfig,
    SignatureScheme,
};

use tonic::transport::{Identity, ServerTlsConfig};

#[derive(Debug, Error)]
pub enum CryptoError {
    #[error(transparent)]
    IOError(io::Error),
    #[error(transparent)]
    TLSError(TLSError),
    #[error("Could not load the private key supplied")]
    PrivateKeyLoading,
    #[error("OpenSSL error: {0}")]
    OpenSSLError(String),
    #[error("Crypto error occurred: {0}")]
    Other(String),
}

impl CryptoError {
    pub fn other(reason: impl Into<String>) -> Self {
        Self::Other(reason.into())
    }
}

#[cfg(feature = "tls_openssl")]
impl From<openssl::error::ErrorStack> for CryptoError {
    fn from(value: openssl::error::ErrorStack) -> Self {
        Self::OpenSSLError(format!("{value:?}"))
    }
}

/// Struct containing the certificate data to be used for connections using custom
/// certificate validators for
/// clients ([`CustomWebPkiVerifier`](CustomWebPkiVerifier))
/// which disables hostname validation.
#[derive(Debug, Clone)]
pub struct CryptoConfig {
    private_key: Vec<u8>,
    public_key: Vec<u8>,
    ca_cert: Vec<u8>,
    private_key_pass: Option<String>,
}

impl CryptoConfig {
    pub async fn new(
        private_key_path: &str,
        public_key_path: &str,
        ca_cert_path: &str,
        private_key_pass: Option<&str>,
    ) -> Result<Self, CryptoError> {
        let private_key = tokio::fs::read(private_key_path)
            .await
            .map_err(CryptoError::IOError)?;
        let public_key = tokio::fs::read(public_key_path)
            .await
            .map_err(CryptoError::IOError)?;
        let ca_cert = tokio::fs::read(ca_cert_path)
            .await
            .map_err(CryptoError::IOError)?;

        let pass = private_key_pass.map(|p| p.to_string());

        Ok(Self {
            private_key,
            public_key,
            ca_cert,
            private_key_pass: pass,
        })
    }

    pub fn new_from_memory(
        private_key: Vec<u8>,
        public_key: Vec<u8>,
        ca_cert: Vec<u8>,
        private_key_pass: Option<String>,
    ) -> Self {
        Self {
            private_key,
            public_key,
            ca_cert,
            private_key_pass,
        }
    }

    // TODO: I assume this can be optimized, we should not need pem_rfc7468, but from_pem is
    //  broken for EncryptedPrivateKeyInfo
    pub(crate) fn decrypt_private_key(
        key: &[u8],
        password: &String,
    ) -> Result<Vec<u8>, CryptoError> {
        // load pem
        let mut decoder = pem_rfc7468::Decoder::new(key).map_err(|err| {
            error!("{err}");
            CryptoError::PrivateKeyLoading
        })?;
        let mut buffer = vec![];

        let decoded = decoder.decode_to_end(&mut buffer).map_err(|err| {
            error!("{err}");
            CryptoError::PrivateKeyLoading
        })?;

        let info = pkcs8::EncryptedPrivateKeyInfo::from_der(decoded).map_err(|err| {
            error!("{err}");
            CryptoError::PrivateKeyLoading
        })?;

        let decrypted = info.decrypt(password).map_err(|err| {
            error!("{err}");
            CryptoError::PrivateKeyLoading
        })?;

        decrypted
            .to_pem(PrivateKeyInfo::PEM_LABEL, LineEnding::LF)
            .map_err(|err| {
                error!("{err}");
                CryptoError::PrivateKeyLoading
            })
            .map(|it| (*it).clone().into_bytes())
    }

    fn get_decrypted_private_key(&self) -> Result<Vec<u8>, CryptoError> {
        match &self.private_key_pass {
            Some(pass) => Self::decrypt_private_key(&self.private_key, pass),
            None => Ok(self.private_key.clone()),
        }
    }

    fn load_keys(
        &self,
    ) -> Result<
        (
            Vec<CertificateDer<'static>>,
            PrivateKeyDer<'static>,
            RootCertStore,
        ),
        CryptoError,
    > {
        let mut cert = std::io::Cursor::new(&self.public_key[..]);
        let public_key_loaded = rustls_pemfile::certs(&mut cert)
            .collect::<Result<Vec<_>, _>>()
            .map_err(CryptoError::IOError)?;

        let loaded_private_key = self.get_decrypted_private_key()?;

        let key = std::io::Cursor::new(&loaded_private_key[..]);
        let private_loaded = load_rustls_private_key(key)
            .map_err(CryptoError::IOError)?
            .clone_key();

        let mut root_certs_cursor = std::io::Cursor::new(&self.ca_cert[..]);
        let root_certs = rustls_pemfile::certs(&mut root_certs_cursor)
            .collect::<Result<Vec<_>, _>>()
            .map_err(CryptoError::IOError)?;
        let mut root_store = RootCertStore::empty();
        root_store.add_parsable_certificates(root_certs.clone());

        Ok((public_key_loaded, private_loaded, root_store))
    }
}

impl TryInto<ServerTlsConfig> for &CryptoConfig {
    type Error = CryptoError;

    fn try_into(self) -> Result<ServerTlsConfig, Self::Error> {
        // Ensure CryptoProvider was installed
        if let Err(_err) = CryptoProvider::install_default(ring::default_provider()) {
            debug!("TryInto<ServerTlsConfig>: Could not install crypto provider")
        }

        let mut cfg = ServerTlsConfig::new();
        let loaded_private_key = self.get_decrypted_private_key()?;

        cfg = cfg.identity(Identity::from_pem(&self.public_key, loaded_private_key));
        cfg = cfg.client_ca_root(tonic::transport::Certificate::from_pem(&self.ca_cert));
        Ok(cfg)
    }
}

impl TryInto<ServerConfig> for CryptoConfig {
    type Error = CryptoError;

    fn try_into(self) -> Result<ServerConfig, Self::Error> {
        let (public_key, private_key, ca_store) = self.load_keys()?;

        // Ensure CryptoProvider was installed
        if let Err(_err) = CryptoProvider::install_default(ring::default_provider()) {
            debug!("TryInto<ServerConfig>: Could not install crypto provider")
        }

        let verifier = WebPkiClientVerifier::builder(Arc::new(ca_store))
            .build()
            .map_err(|err| Self::Error::other(format!("{:?}", err)))?;

        ServerConfig::builder()
            .with_client_cert_verifier(verifier)
            .with_single_cert(public_key, private_key)
            .map_err(CryptoError::TLSError)
    }
}

impl TryInto<ClientConfig> for CryptoConfig {
    type Error = CryptoError;

    fn try_into(self) -> Result<ClientConfig, Self::Error> {
        let (public_key, private_key, root_store) = self.load_keys()?;

        // Ensure CryptoProvider was installed
        if let Err(_err) = CryptoProvider::install_default(ring::default_provider()) {
            debug!("TryInto<ClientConfig>: Could not install crypto provider")
        }
        let builder_intermediate = ClientConfig::builder();

        let verifier = CustomWebPkiVerifier::new(
            Arc::new(root_store),
            CryptoProvider::get_default()
                .ok_or(Self::Error::other("Could not get default crypto_provider"))?,
        );

        let mut client = builder_intermediate
            .dangerous()
            .with_custom_certificate_verifier(Arc::new(verifier))
            .with_client_auth_cert(public_key, private_key)
            .map_err(CryptoError::TLSError)?;
        client.enable_sni = false;
        Ok(client)
    }
}

#[cfg(feature = "tls_openssl")]
impl TryInto<openssl::ssl::SslAcceptorBuilder> for CryptoConfig {
    type Error = CryptoError;

    fn try_into(self) -> Result<openssl::ssl::SslAcceptorBuilder, Self::Error> {
        let mut acceptor =
            openssl::ssl::SslAcceptor::mozilla_modern_v5(openssl::ssl::SslMethod::tls())
                .map_err(|err| CryptoError::OpenSSLError(format!("{err:?}")))?;

        let ca_cert = openssl::x509::X509::from_pem(&self.ca_cert)?;

        let public_key = openssl::x509::X509::from_pem(&self.public_key)?;

        let loaded_private_key = self.get_decrypted_private_key()?;
        let private_key = openssl::pkey::PKey::private_key_from_pem(&loaded_private_key)?;

        acceptor.set_private_key(&private_key)?;
        acceptor.set_certificate(&public_key)?;
        acceptor.add_client_ca(&ca_cert)?;
        acceptor.cert_store_mut().add_cert(ca_cert)?;
        acceptor.set_verify(
            openssl::ssl::SslVerifyMode::PEER | openssl::ssl::SslVerifyMode::FAIL_IF_NO_PEER_CERT,
        );

        Ok(acceptor)
    }
}

/// Builds a dummy `ClientConfig` type for a function that communicates over
/// an untrusted network. The configuration has safe defaults, a custom certificate
/// verifier and no client authentication.
pub fn dummy_client_config() -> ClientConfig {
    // Ensure CryptoProvider was installed
    if let Err(_err) = CryptoProvider::install_default(ring::default_provider()) {
        debug!("dummy_client_config()>: Could not install crypto provider")
    }

    let builder_intermediate = ClientConfig::builder();

    let verifier = CustomWebPkiVerifier::new(
        Arc::new(RootCertStore::empty()),
        CryptoProvider::get_default().expect("Could not get default crypto_provider"),
    );

    let mut client = builder_intermediate
        .dangerous()
        .with_custom_certificate_verifier(Arc::new(verifier))
        .with_no_client_auth();
    client.enable_sni = false;
    client
}

/// Default rustls [ServerCertVerifier], see the trait impl for more information.
///
/// Modified from [`WebPkiServerVerifier`](tokio_rustls::rustls::client::WebPkiServerVerifier)
#[derive(Debug)]
struct CustomWebPkiVerifier {
    roots: Arc<RootCertStore>,
    supported: WebPkiSupportedAlgorithms,
}

impl CustomWebPkiVerifier {
    /// Constructs a new `WebPkiVerifier`.
    ///
    /// `roots` is the set of trust anchors to trust for issuing server certs.
    fn new(roots: Arc<RootCertStore>, crypto_provider: &CryptoProvider) -> Self {
        Self {
            roots,
            supported: crypto_provider.signature_verification_algorithms,
        }
    }
}

impl ServerCertVerifier for CustomWebPkiVerifier {
    /// Will verify the certificate is valid in the following ways:
    /// - Signed by a trusted `MyRootCertStore` CA
    /// - Not Expired
    fn verify_server_cert(
        &self,
        end_entity: &CertificateDer<'_>,
        intermediates: &[CertificateDer<'_>],
        _server_name: &ServerName<'_>,
        _ocsp_response: &[u8],
        now: UnixTime,
    ) -> Result<ServerCertVerified, TLSError> {
        let cert = ParsedCertificate::try_from(end_entity)?;

        verify_server_cert_signed_by_trust_anchor(
            &cert,
            &self.roots,
            intermediates,
            now,
            self.supported.all,
        )?;

        Ok(ServerCertVerified::assertion())
    }

    fn verify_tls12_signature(
        &self,
        message: &[u8],
        cert: &CertificateDer<'_>,
        dss: &DigitallySignedStruct,
    ) -> Result<HandshakeSignatureValid, TLSError> {
        verify_tls12_signature(message, cert, dss, &self.supported)
    }

    fn verify_tls13_signature(
        &self,
        message: &[u8],
        cert: &CertificateDer<'_>,
        dss: &DigitallySignedStruct,
    ) -> Result<HandshakeSignatureValid, TLSError> {
        verify_tls13_signature(message, cert, dss, &self.supported)
    }

    fn supported_verify_schemes(&self) -> Vec<SignatureScheme> {
        self.supported.supported_schemes()
    }
}

/// Loads a single private key from either a pkcs8-encoded file or from an RSA key.
///
/// # Arguments
///
/// * `cursor`: to load from
///
/// returns: Result<PrivateKey, Error>
fn load_rustls_private_key(mut cursor: std::io::Cursor<&[u8]>) -> Result<PrivateKeyDer, io::Error> {
    // First attempt to load the private key assuming it is PKCS8-encoded
    if let Some(Ok(key)) = rustls_pemfile::pkcs8_private_keys(&mut cursor).next() {
        return Ok(PrivateKeyDer::from(key));
    }

    // If it not, try loading the private key as an RSA key
    cursor.set_position(0);
    if let Some(Ok(key)) = rustls_pemfile::rsa_private_keys(&mut cursor).next() {
        return Ok(PrivateKeyDer::from(key));
    }

    // Otherwise we have a Private Key parsing problem
    error!("Could not parse private key");
    Err(io::Error::new(
        io::ErrorKind::InvalidData,
        "Could not load private key as pkcs8 or rsa",
    ))
}

const ENCODE_CONFIG: pem::EncodeConfig = match cfg!(target_family = "windows") {
    true => pem::EncodeConfig::new().set_line_ending(pem::LineEnding::CRLF),
    false => pem::EncodeConfig::new().set_line_ending(pem::LineEnding::LF),
};

pub fn der_to_pem(der: impl Into<Vec<u8>>) -> String {
    let p = Pem::new("CERTIFICATE", der);
    pem::encode_config(&p, ENCODE_CONFIG)
}
