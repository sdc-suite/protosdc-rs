use biceps::common::biceps_util::{Descriptor, State};
use biceps::common::mdib_description_modification::MdibDescriptionModification;
use biceps::common::mdib_description_modifications::MdibDescriptionModifications;
use biceps::common::mdib_pair::Pair;
use biceps::test_util::mdib::mdib_modification_presets;
use biceps::test_util::{handles, mdib_utils};

const PERFORMANCE_CHANNEL_NAME: &str = "performancechannel";
const PERFORMANCE_METRIC_PREFIX: &str = "performancemetric_";

pub fn performance_modification_presets(metrics: usize) -> MdibDescriptionModifications {
    let mut modifications = mdib_modification_presets();

    // add performance channel
    modifications
        .add(MdibDescriptionModification::Insert {
            pair: Pair::try_from((
                mdib_utils::create_minimal_channel_descriptor(PERFORMANCE_CHANNEL_NAME)
                    .into_abstract_descriptor_one_of(),
                mdib_utils::create_minimal_channel_state(PERFORMANCE_CHANNEL_NAME)
                    .into_abstract_state_one_of(),
            ))
            .expect("Valid pair"),
            parent: Some(handles::VMD_0.to_string()),
        })
        .unwrap_or_else(|_| panic!("Could not add channel {}", handles::CHANNEL_0));

    for i in 0..=metrics {
        let handle = format!("{}{}", PERFORMANCE_METRIC_PREFIX, i);
        modifications
            .add(MdibDescriptionModification::Insert {
                pair: Pair::try_from((
                    mdib_utils::create_minimal_numeric_metric_descriptor(&handle)
                        .into_abstract_descriptor_one_of(),
                    mdib_utils::create_minimal_numeric_metric_state(&handle)
                        .into_abstract_state_one_of(),
                ))
                .expect("Valid pair"),
                parent: Some(PERFORMANCE_CHANNEL_NAME.to_string()),
            })
            .unwrap_or_else(|_| panic!("Could not add metric {}", &handle));
    }

    modifications
}
