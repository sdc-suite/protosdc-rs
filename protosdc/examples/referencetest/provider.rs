use std::net::SocketAddr;
use std::str::FromStr;
use std::time::{Duration, SystemTime, UNIX_EPOCH};

use anyhow::Result;
use async_trait::async_trait;
use log::{error, info, warn};
use tokio::task;

use biceps::common::access::mdib_access::MdibAccess;
use biceps::common::biceps_util::{AlertState, MetricState, State};
use biceps::common::mdib_entity::{Entity, EntityBase};
use biceps::common::mdib_state_modifications::MdibStateModifications;
use biceps::common::storage::mdib_storage::WriteStateResult;
use biceps::entity_filter;
use biceps::provider::access::local_mdib_access::LocalMdibAccess;
use biceps::test_util::mdib::mdib_modification_presets;
use biceps::utilities::localized_text::LocalizedTextBuilder;
use common::crypto::CryptoConfig;
use common::Service;
use protosdc::provider::device::SdcDevice;
use protosdc::provider::device::ServerConfig;
use protosdc::provider::localization_storage::LocalizationStorageImpl;
use protosdc::provider::metadata::{DeviceMetadata, LocalizedString};
use protosdc::provider::plugins::ScopesPlugin;
use protosdc::provider::sco::{
    Context, InvocationCallError, InvocationResponse, OperationInvocationReceiver,
};
use protosdc::provider::sco_util::{
    FailedInvocationStateTypes, NotFailedInvocationStateTypes, ReportBuilder,
};
use protosdc_biceps::biceps::abstract_metric_value_mod::MetricQuality;
use protosdc_biceps::biceps::{
    generation_mode_mod, invocation_error_mod, measurement_validity_mod, AbstractMetricValue,
    Activate, AlertConditionState, EnumStringMetricState, GenerationMode, MeasurementValidity,
    NumericMetricState, NumericMetricValue, SetString, SetValue, StringMetricState,
    StringMetricValue, Timestamp,
};
use protosdc_biceps::types::ProtoDecimal;

use crate::performance_mode::performance_modification_presets;

#[derive(Clone)]
struct ExampleProviderOperationInvocationReceiver {}

impl ExampleProviderOperationInvocationReceiver {
    async fn set_string_write<MDIB>(
        request: SetString,
        old_state: StringMetricState,
        is_enum: bool,
        context: &mut Context<MDIB>,
    ) -> Option<WriteStateResult>
    where
        MDIB: 'static + LocalMdibAccess + Send + Sync + Clone,
    {
        info!(
            "Writing {} into {}",
            &request.requested_string_value,
            old_state.descriptor_handle()
        );

        let old_value = match old_state.metric_value {
            None => StringMetricValue {
                abstract_metric_value: AbstractMetricValue {
                    extension_element: None,
                    metric_quality: MetricQuality {
                        extension_element: None,
                        validity_attr: MeasurementValidity {
                            enum_type: measurement_validity_mod::EnumType::Vld,
                        },
                        mode_attr: None,
                        qi_attr: None,
                    },
                    annotation: vec![],
                    start_time_attr: None,
                    stop_time_attr: None,
                    determination_time_attr: None,
                },
                value_attr: None,
            },
            Some(old) => old,
        };

        let new_state = StringMetricState {
            metric_value: Some(StringMetricValue {
                value_attr: Some(request.requested_string_value),
                ..old_value
            }),
            ..old_state
        };

        let modification = MdibStateModifications::Metric {
            metric_states: vec![match is_enum {
                false => new_state.into_abstract_metric_state_one_of(),
                true => EnumStringMetricState {
                    string_metric_state: new_state,
                }
                .into_abstract_metric_state_one_of(),
            }],
        };

        context.mdib_access.write_states(modification).await.ok()
    }
}

#[async_trait]
impl<MDIB> OperationInvocationReceiver<MDIB> for ExampleProviderOperationInvocationReceiver
where
    MDIB: 'static + LocalMdibAccess + Send + Sync + Clone,
{
    async fn activate(
        &self,
        _operation_handle: String,
        mut context: Context<MDIB>,
        _request: Activate,
    ) -> std::result::Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        let version = context.mdib_access.mdib_version().await;
        let response = context
            .create_response(
                ReportBuilder::create()
                    .success(NotFailedInvocationStateTypes::Wait)
                    .mdib_version(version)
                    .build(),
            )
            .await;

        task::spawn(async move {
            info!("Async finishing task");
            tokio::time::sleep(Duration::from_secs(1)).await;
            let version = context.mdib_access.mdib_version().await;
            context
                .send_report(
                    ReportBuilder::create()
                        .success(NotFailedInvocationStateTypes::Fin)
                        .mdib_version(version)
                        .build(),
                )
                .await;

            info!("Async task finished");
        });
        Ok(response)
    }

    async fn set_string(
        &self,
        _operation_handle: String,
        mut context: Context<MDIB>,
        request: SetString,
    ) -> std::result::Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        let mdib_access = context.mdib_access.clone();
        let version = mdib_access.mdib_version().await;
        let response = context
            .create_response(
                ReportBuilder::create()
                    .success(NotFailedInvocationStateTypes::Wait)
                    .mdib_version(version)
                    .build(),
            )
            .await;

        task::spawn(async move {
            info!("Async finishing task");
            tokio::time::sleep(Duration::from_secs(1)).await;

            // find operation target
            let operation = context
                .mdib_access
                .entity(&request.abstract_set.operation_handle_ref.string)
                .await;
            let operation_target_handle = match operation {
                Some(op) => match op.entity {
                    Entity::SetStringOperation(entity) => Some(
                        entity
                            .pair
                            .descriptor
                            .abstract_operation_descriptor
                            .operation_target_attr
                            .string,
                    ),
                    _ => None,
                },
                None => None,
            };

            if operation_target_handle.is_none() {
                warn!("Could not find operation target");
                let version = context.mdib_access.mdib_version().await;
                context
                    .send_report(
                        ReportBuilder::create()
                            .mdib_version(version)
                            .failure(FailedInvocationStateTypes::Fail)
                            .error(invocation_error_mod::EnumType::Unkn)
                            .error_message(
                                LocalizedTextBuilder::create()
                                    .no_service("Unknown operation")
                                    .lang("en")
                                    .build(),
                            )
                            .build(),
                    )
                    .await;
                return;
            }

            // find target state
            let operation_target_entity =
                mdib_access.entity(&operation_target_handle.unwrap()).await;
            let write_result =
                match operation_target_entity {
                    None => None,
                    Some(entity) => {
                        // write updated state
                        match entity.entity {
                            Entity::StringMetric(ent) => {
                                Self::set_string_write(request, ent.pair.state, false, &mut context)
                                    .await
                            }
                            Entity::EnumStringMetric(ent) => {
                                // validate new value is permitted
                                let permitted =
                                    ent.pair.descriptor.allowed_value.iter().any(|allowed| {
                                        request.requested_string_value == allowed.value
                                    });
                                match permitted {
                                    false => {
                                        error!(
                                            "{} is not an allowed value for {}",
                                            request.requested_string_value,
                                            ent.handle()
                                        );
                                        None
                                    }
                                    true => {
                                        Self::set_string_write(
                                            request,
                                            ent.pair.state.string_metric_state,
                                            true,
                                            &mut context,
                                        )
                                        .await
                                    }
                                }
                            }
                            _ => None,
                        }
                    }
                };

            let version = mdib_access.mdib_version().await;
            match write_result {
                None => {
                    warn!("Operation failed");
                    context
                        .send_report(
                            ReportBuilder::create()
                                .mdib_version(version)
                                .failure(FailedInvocationStateTypes::Fail)
                                .error(invocation_error_mod::EnumType::Oth)
                                .build(),
                        )
                        .await
                }
                Some(_) => {
                    context
                        .send_report(
                            ReportBuilder::create()
                                .mdib_version(version)
                                .success(NotFailedInvocationStateTypes::Fin)
                                .build(),
                        )
                        .await
                }
            };

            info!("Async task finished");
        });
        Ok(response)
    }

    async fn set_value(
        &self,
        _operation_handle: String,
        mut context: Context<MDIB>,
        _request: SetValue,
    ) -> std::result::Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        let version = context.mdib_access.mdib_version().await;
        let response = context
            .create_response(
                ReportBuilder::create()
                    .mdib_version(version)
                    .success(NotFailedInvocationStateTypes::Wait)
                    .build(),
            )
            .await;

        task::spawn(async move {
            info!("Async finishing task");
            tokio::time::sleep(Duration::from_secs(1)).await;
            let version = context.mdib_access.mdib_version().await;
            context
                .send_report(
                    ReportBuilder::create()
                        .mdib_version(version)
                        .success(NotFailedInvocationStateTypes::Fin)
                        .build(),
                )
                .await;

            info!("Async task finished");
        });
        Ok(response)
    }
}

#[derive(Debug, PartialEq, Eq)]
pub enum ProviderMode {
    ReferenceProvider,
    PerformanceProvider,
}

pub async fn run_provider(
    adapter: String,
    port: u16,
    epr: &str,
    crypto_config: Option<CryptoConfig>,
    mode: ProviderMode,
    proxy: Option<String>,
    localization_storage: Option<LocalizationStorageImpl>,
) -> Result<()> {
    let addr = SocketAddr::from_str(&format!("{}:{}", adapter, port))?;

    let server_config = ServerConfig {
        crypto_config,
        server_address: addr,
    };
    let mdib_data = match mode {
        ProviderMode::ReferenceProvider => mdib_modification_presets(),
        ProviderMode::PerformanceProvider => performance_modification_presets(1000),
    };

    let sequence = "prefix:seq";

    let mut device = SdcDevice::new(
        sequence.to_string(),
        server_config,
        ExampleProviderOperationInvocationReceiver {},
        epr.to_string(),
        Some(
            DeviceMetadata::builder()
                .friendly_name(vec![LocalizedString::builder()
                    .value("protosdc-rs referencetest provider")
                    .locale("en-US")
                    .build()])
                .manufacturer_url("https://protosdc.org")
                .build(),
        ),
        proxy,
        localization_storage,
        ScopesPlugin::default(),
    )
    .await?;
    device.start().await?;
    device.local_mdib.write_description(mdib_data).await?;

    let mut task_mdib = device.local_mdib.clone();

    // start updating before the connection is done
    let update_task = tokio::spawn(async move {
        // collect all alert conditions
        let conditions: Vec<NumericMetricState> = task_mdib
            .entities_by_type(entity_filter!(NumericMetric))
            .await
            .iter()
            .map(|it| match &it.entity {
                Entity::NumericMetric(ent) => ent.pair.state.clone(),
                _ => panic!("Your filter is broken."),
            })
            .collect();

        // update METRIC_0 every second
        let mut i = 0;
        loop {
            if mode == ProviderMode::ReferenceProvider {
                tokio::time::sleep(Duration::from_secs(1)).await;
            }

            let updated_states = conditions
                .iter()
                .map(|orig_state| NumericMetricState {
                    metric_value: Some(NumericMetricValue {
                        abstract_metric_value: AbstractMetricValue {
                            extension_element: None,
                            metric_quality: MetricQuality {
                                extension_element: None,
                                validity_attr: MeasurementValidity {
                                    enum_type: measurement_validity_mod::EnumType::Vld,
                                },
                                mode_attr: Some(GenerationMode {
                                    enum_type: generation_mode_mod::EnumType::Demo,
                                }),
                                qi_attr: None,
                            },
                            annotation: vec![],
                            start_time_attr: None,
                            stop_time_attr: None,
                            determination_time_attr: None,
                        },
                        value_attr: Some(ProtoDecimal::from(i)),
                    }),
                    ..orig_state.clone()
                })
                .map(|it| it.into_abstract_metric_state_one_of())
                .collect();

            let modifications = MdibStateModifications::Metric {
                metric_states: updated_states,
            };
            task_mdib
                .write_states(modifications)
                .await
                .expect("Write failed");

            i += 1;
        }
    });

    let mut task_mdib = device.local_mdib.clone();
    let alarm_update_task = tokio::spawn(async move {
        // collect all alert conditions
        let conditions: Vec<AlertConditionState> = task_mdib
            .entities_by_type(entity_filter!(AlertCondition))
            .await
            .iter()
            .map(|it| match &it.entity {
                Entity::AlertCondition(ent) => ent.pair.state.clone(),
                _ => panic!("Your filter is broken."),
            })
            .collect();

        let mut presence = true;
        loop {
            tokio::time::sleep(Duration::from_secs(1)).await;

            let start = SystemTime::now();
            let since_the_epoch = start
                .duration_since(UNIX_EPOCH)
                .expect("Time went backwards");

            let updated_states = conditions
                .iter()
                .map(|orig_state| AlertConditionState {
                    presence_attr: Some(presence),
                    determination_time_attr: Some(Timestamp {
                        unsigned_long: since_the_epoch.as_millis() as u64,
                    }),
                    ..orig_state.clone()
                })
                .map(|it| it.into_abstract_alert_state_one_of())
                .collect();

            task_mdib
                .write_states(MdibStateModifications::Alert {
                    alert_states: updated_states,
                })
                .await
                .expect("Write failed?");

            presence = !presence;
        }
    });

    update_task.await?;
    alarm_update_task.await?;
    Ok(())
}
