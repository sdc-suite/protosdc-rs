use std::collections::HashMap;
use std::fmt::{Display, Formatter};
use std::net::IpAddr;
use std::str::FromStr;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::{Arc, Mutex};
use std::time::{Duration, Instant};

use anyhow::Result;
use futures::{future, stream, StreamExt};
use log::{error, info, trace};
use strum::IntoEnumIterator;
use strum_macros::EnumIter;
use tokio_stream::wrappers::ReceiverStream;

use biceps::common::access::mdib_access::{MdibAccess, MdibAccessEvent, ObservableMdib};
use biceps::common::biceps_util::{ContextState, State};
use biceps::common::mdib_entity::EntityBase;
use biceps::consumer::access::remote_mdib_access::RemoteMdibAccess;
use biceps::entity_filter;
use common::crypto::CryptoConfig;
use network::udp_binding::{create_udp_binding, PROTOSDC_MULTICAST_PORT};
use protosdc::consumer::grpc_consumer::{Consumer, ConsumerImpl};
use protosdc::consumer::remote_device::SdcRemoteDevice;
use protosdc::consumer::remote_device_connector;
use protosdc::consumer::sco::sco_controller::SetServiceAccess;
use protosdc::consumer::sco::sco_transaction::ScoTransaction;
use protosdc::discovery::consumer::discovery_consumer::{DiscoveryConsumer, DiscoveryConsumerImpl};
use protosdc_biceps::biceps::{
    context_association_mod, invocation_state_mod, AbstractSet, AbstractSetOneOf, Activate,
    ContextAssociation, HandleRef, InvocationState, SetString, SetValue,
};
use protosdc_biceps::types::ProtoDecimal;
use protosdc_proto::discovery::Endpoint;

const REPORT_TIMEOUT: Duration = Duration::from_secs(30);
const REPORT_TIMEOUT_TOLERANCE: Duration = Duration::from_secs(3);

const EXPECTED_METRIC_UPDATES: i32 = 6;
const EXPECTED_ALERT_UPDATES: i32 = 6;

const SET_STRING_VALUE: &str = "protosdc-rs was here";
const SET_VALUE_VALUE: i32 = 1337;

#[derive(Debug, PartialEq, Eq, Hash, EnumIter, Clone)]
enum ReferenceTestStep {
    // DEV-23, DEV-24:
    // Discovery of a provider with a specific endpoint reference address
    //
    // See that SearchRequest is answered
    //
    Test1,
    // DEV-25:
    // Connect to the provider with specific endpoint, i.e. establish TCP connection(s) and retrieve endpoint metadata
    //
    Test2,
    // DEV-26:
    // Read MDIB of the provider
    //
    Test3,
    // DEV-27:
    // Subscribe at least metrics, alerts, waveforms and operation invoked reports of the provider
    //
    Test4,
    // DEV-26, DEV-27:
    // Check that least one patient context exists
    //
    Test5,
    // DEV-26, DEV-27:
    // Check that at least one location context exists
    //
    Test6,
    // DEV-35, DEV-36, DEV-29:
    // Check that metric updates for one metric arrive at least 5 times in 30 seconds
    //
    Test7,
    // DEV-38, DEV-39:
    // Check that alert updates of one alert condition arrive at least 5 times in 30 seconds
    //
    Test8,
    // DEV-31, (DEV-44, DEV-45):
    // Execute external control operations (any operation that exists in the containment tree,
    // if none exist: skip test) by checking the ultimate transaction result is "finished"
    //
    // a. Any Activate
    // b. Any SetString
    // c. Any SetValue
    //
    Test9a,
    Test9b,
    Test9c,
    // DEV-34:
    // Shutdown connection (cancel subscription, close connection)
    //
    Test10,
}

impl Display for ReferenceTestStep {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let text = match self {
            ReferenceTestStep::Test1 => "Test 01 (Discovery)",
            ReferenceTestStep::Test2 => "Test 02 (Connect)",
            ReferenceTestStep::Test3 => "Test 03 (GetMdib)",
            ReferenceTestStep::Test4 => "Test 04 (Subscription)",
            ReferenceTestStep::Test5 => "Test 05 (Location)",
            ReferenceTestStep::Test6 => "Test 06 (Patient)",
            ReferenceTestStep::Test7 => "Test 07 (Metric Updates)",
            ReferenceTestStep::Test8 => "Test 08 (Alert Updates)",
            ReferenceTestStep::Test9a => "Test 09a (Activate)",
            ReferenceTestStep::Test9b => "Test 09b (SetString)",
            ReferenceTestStep::Test9c => "Test 09c (SetValue)",
            ReferenceTestStep::Test10 => "Test 10 (Disconnect)",
        };
        write!(f, "{}", text)
    }
}

#[derive(Debug, PartialEq, Eq, Hash)]
enum TestStatus {
    Initial,
    Passed,
    Failed,
    Skipped,
}

impl Display for TestStatus {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

async fn run_activate<V, W>(set_service: &mut V, handle: &str) -> bool
where
    V: SetServiceAccess<W>,
    W: ScoTransaction,
{
    let call_result = set_service
        .invoke(AbstractSetOneOf::Activate(Activate {
            abstract_set: AbstractSet {
                extension_element: None,
                operation_handle_ref: HandleRef {
                    string: handle.to_string(),
                },
            },
            argument: vec![],
        }))
        .await;

    let call = match call_result {
        Ok(it) => it,
        Err(err) => {
            error!("{:?}", err);
            return false;
        }
    };

    info!("Waiting for result for activate {}", handle);
    let reports = call.wait_for_final_report().await;
    info!("Number of reports for {}: {}", handle, reports.len());

    match reports.last() {
        None => false,
        Some(r) => {
            r.report_part.invocation_info.invocation_state
                == InvocationState {
                    enum_type: invocation_state_mod::EnumType::Fin,
                }
        }
    }
}

async fn run_set_string<V, W>(set_service: &mut V, handle: &str) -> bool
where
    V: SetServiceAccess<W>,
    W: ScoTransaction,
{
    let call_result = set_service
        .invoke(AbstractSetOneOf::SetString(SetString {
            abstract_set: AbstractSet {
                extension_element: None,
                operation_handle_ref: HandleRef {
                    string: handle.to_string(),
                },
            },
            requested_string_value: SET_STRING_VALUE.to_string(),
        }))
        .await;

    let call = match call_result {
        Ok(it) => it,
        Err(err) => {
            error!("{:?}", err);
            return false;
        }
    };

    let reports = call.wait_for_final_report().await;

    match reports.last() {
        None => false,
        Some(r) => {
            r.report_part.invocation_info.invocation_state
                == InvocationState {
                    enum_type: invocation_state_mod::EnumType::Fin,
                }
        }
    }
}

async fn run_set_value<V, W>(set_service: &mut V, handle: &str) -> bool
where
    V: SetServiceAccess<W>,
    W: ScoTransaction,
{
    let call_result = set_service
        .invoke(AbstractSetOneOf::SetValue(SetValue {
            abstract_set: AbstractSet {
                extension_element: None,
                operation_handle_ref: HandleRef {
                    string: handle.to_string(),
                },
            },
            requested_numeric_value: ProtoDecimal::from(SET_VALUE_VALUE),
        }))
        .await;

    let call = match call_result {
        Ok(it) => it,
        Err(err) => {
            error!("{:?}", err);
            return false;
        }
    };

    let reports = call.wait_for_final_report().await;

    match reports.last() {
        None => false,
        Some(r) => {
            r.report_part.invocation_info.invocation_state
                == InvocationState {
                    enum_type: invocation_state_mod::EnumType::Fin,
                }
        }
    }
}

async fn run_operation_tests<T, U, V, W>(
    remote_device: &SdcRemoteDevice<T, U, V, W>,
    result_map: &mut HashMap<ReferenceTestStep, TestStatus>,
) where
    T: Consumer,
    U: RemoteMdibAccess + Clone,
    V: SetServiceAccess<W> + Clone,
    W: ScoTransaction,
{
    info!("Running operation tests");
    let set_service = match remote_device.set_service().await {
        Some(service) => service,
        None => return,
    };

    //run activate
    let activate_ops = {
        remote_device
            .get_mdib_access()
            .await
            .entities_by_type(entity_filter!(ActivateOperation))
            .await
    };
    if activate_ops.is_empty() {
        result_map.insert(ReferenceTestStep::Test9a, TestStatus::Skipped);
    } else {
        let activate_result = stream::iter(activate_ops.into_iter())
            .any(|op| {
                info!("Invoking activate {}", &op.entity.handle());
                let mut set_inner = set_service.clone();
                async move { run_activate(&mut set_inner, &op.entity.handle()).await }
            })
            .await;

        result_map.insert(
            ReferenceTestStep::Test9a,
            match activate_result {
                true => TestStatus::Passed,
                false => TestStatus::Failed,
            },
        );
    }

    // run set string
    let set_string_ops = {
        remote_device
            .get_mdib_access()
            .await
            .entities_by_type(entity_filter!(SetStringOperation))
            .await
    };
    if set_string_ops.is_empty() {
        result_map.insert(ReferenceTestStep::Test9b, TestStatus::Skipped);
    } else {
        let set_string_result = stream::iter(set_string_ops.into_iter())
            .any(|op| {
                info!("Invoking set string {}", &op.entity.handle());
                let mut set_inner = set_service.clone();
                async move { run_set_string(&mut set_inner, &op.entity.handle()).await }
            })
            .await;

        result_map.insert(
            ReferenceTestStep::Test9b,
            match set_string_result {
                true => TestStatus::Passed,
                false => TestStatus::Failed,
            },
        );
    }

    // run set value
    let set_value_ops = {
        remote_device
            .get_mdib_access()
            .await
            .entities_by_type(entity_filter!(SetValueOperation))
            .await
    };
    if set_value_ops.is_empty() {
        result_map.insert(ReferenceTestStep::Test9c, TestStatus::Skipped);
    } else {
        let set_value_result = stream::iter(set_value_ops.into_iter())
            .any(|op| {
                info!("Invoking set value {}", &op.entity.handle());
                let mut set_inner = set_service.clone();
                async move { run_set_value(&mut set_inner, &op.entity.handle()).await }
            })
            .await;

        result_map.insert(
            ReferenceTestStep::Test9c,
            match set_value_result {
                true => TestStatus::Passed,
                false => TestStatus::Failed,
            },
        );
    }
}

pub async fn run_consumer(
    adapter: String,
    epr: &str,
    crypto_config: Option<CryptoConfig>,
    proxy: Option<String>,
) -> Result<()> {
    let mut result_map: HashMap<ReferenceTestStep, TestStatus> = HashMap::new();
    result_map.insert(ReferenceTestStep::Test1, TestStatus::Initial);
    result_map.insert(ReferenceTestStep::Test2, TestStatus::Initial);
    result_map.insert(ReferenceTestStep::Test3, TestStatus::Initial);
    result_map.insert(ReferenceTestStep::Test4, TestStatus::Initial);
    result_map.insert(ReferenceTestStep::Test5, TestStatus::Initial);
    result_map.insert(ReferenceTestStep::Test6, TestStatus::Initial);
    result_map.insert(ReferenceTestStep::Test7, TestStatus::Initial);
    result_map.insert(ReferenceTestStep::Test8, TestStatus::Initial);
    result_map.insert(ReferenceTestStep::Test9a, TestStatus::Initial);
    result_map.insert(ReferenceTestStep::Test9b, TestStatus::Initial);
    result_map.insert(ReferenceTestStep::Test9c, TestStatus::Initial);
    result_map.insert(ReferenceTestStep::Test10, TestStatus::Initial);

    let adapter_address = IpAddr::from_str(&adapter)?;

    let mut discovery_consumer = DiscoveryConsumerImpl::new(
        create_udp_binding(adapter_address, Some(PROTOSDC_MULTICAST_PORT))
            .await
            .map_err(anyhow::Error::new)?,
        proxy,
        crypto_config.clone(),
    )
    .await?;
    let result = discovery_consumer.search(vec![], Some(1), None).await?;

    let mut filtered_results: Vec<Endpoint> = result
        .into_iter()
        .filter(|it| it.endpoint_identifier.ends_with(&epr))
        .collect();

    let endpoint = filtered_results
        .pop()
        .unwrap_or_else(|| panic!("No matching providers found for epr {}", &epr));

    result_map.insert(ReferenceTestStep::Test1, TestStatus::Passed);

    let modified_endpoint = match &crypto_config.is_some() {
        true => ConsumerImpl::add_https_prefix_to_endpoint(endpoint),
        false => ConsumerImpl::add_http_prefix_to_endpoint(endpoint),
    };

    let mut consumer = ConsumerImpl::default();
    consumer
        .connect(modified_endpoint.clone(), crypto_config)
        .await?;
    result_map.insert(ReferenceTestStep::Test2, TestStatus::Passed);

    let mut remote_device = remote_device_connector::connect(consumer).await?;
    {
        let mdib_version = remote_device.mdib_version().await;
        info!("Remote Device Mdib Version: {}", mdib_version);
    }
    result_map.insert(ReferenceTestStep::Test3, TestStatus::Passed);

    let remote_device_sub = { remote_device.get_mdib_access().await.subscribe().await };

    let run_task = Arc::new(AtomicBool::new(true));
    let run_task_copy = run_task.clone();
    let alert_reports_by_handle: Arc<Mutex<HashMap<String, i32>>> =
        Arc::new(Mutex::new(HashMap::new()));
    let metric_reports_by_handle: Arc<Mutex<HashMap<String, i32>>> =
        Arc::new(Mutex::new(HashMap::new()));

    let alert_reports_by_handle_task = alert_reports_by_handle.clone();
    let metric_reports_by_handle_task = metric_reports_by_handle.clone();
    let sub_collection_task = tokio::spawn(async move {
        let start = Instant::now();
        let actual_messages = ReceiverStream::new(remote_device_sub)
            .take_while(|_| future::ready(run_task_copy.load(Ordering::Relaxed)))
            .map(|it| {
                match &it {
                    MdibAccessEvent::AlertStateModification { states, .. } => {
                        trace!("alert event! States: {:?} :)", states.len());
                        let mut handle_map = alert_reports_by_handle_task.lock().unwrap();
                        states
                            .iter()
                            .flat_map(|(_, mds_states)| {
                                mds_states.iter().map(|state| state.descriptor_handle())
                            })
                            .for_each(|handle| *handle_map.entry(handle).or_insert(0) += 1)
                    }
                    MdibAccessEvent::ComponentStateModification { states, .. } => {
                        trace!("component event! {:?} :)", states.len())
                    }
                    MdibAccessEvent::ContextStateModification {
                        removed_context_states,
                        updated_context_states,
                        ..
                    } => {
                        trace!(
                            "context event! removed:{:?} - updated:{:?} :)",
                            removed_context_states.len(),
                            updated_context_states.len()
                        )
                    }
                    MdibAccessEvent::MetricStateModification { states, .. } => {
                        trace!("metric event! States: {:?} :)", states.len());
                        let mut handle_map = metric_reports_by_handle_task.lock().unwrap();
                        states
                            .iter()
                            .flat_map(|(_, mds_states)| {
                                mds_states.iter().map(|state| state.descriptor_handle())
                            })
                            .for_each(|handle| *handle_map.entry(handle).or_insert(0) += 1)
                    }
                    MdibAccessEvent::OperationStateModification { states, .. } => {
                        trace!("operation event! {:?} :)", states.len())
                    }
                    MdibAccessEvent::WaveformModification { states, .. } => {
                        trace!("waveform event! {:?} :)", states.len())
                    }
                    MdibAccessEvent::DescriptionModification { .. } => {}
                };
                it
            })
            .fold(0, |x, _| async move { x + 1 })
            .await;
        let finish_time = start.elapsed().as_secs();
        info!("Received {} messages in sub_collection_task in {}s, which is equivalent to {:.2} msg/s", actual_messages, finish_time, actual_messages as f64/finish_time as f64)
    });
    result_map.insert(ReferenceTestStep::Test4, TestStatus::Passed);

    result_map.insert(
        ReferenceTestStep::Test5,
        match remote_device
            .get_mdib_access()
            .await
            .entities_by_type(entity_filter!(PatientContext))
            .await
            .iter()
            .any(|it| {
                it.resolve_context_states().iter().any(|state| {
                    state.context_association()
                        == Some(ContextAssociation {
                            enum_type: context_association_mod::EnumType::Assoc,
                        })
                })
            }) {
            true => TestStatus::Passed,
            false => TestStatus::Failed,
        },
    );

    result_map.insert(
        ReferenceTestStep::Test6,
        match remote_device
            .get_mdib_access()
            .await
            .entities_by_type(entity_filter!(LocationContext))
            .await
            .iter()
            .any(|it| {
                it.resolve_context_states().iter().any(|state| {
                    state.context_association()
                        == Some(ContextAssociation {
                            enum_type: context_association_mod::EnumType::Assoc,
                        })
                })
            }) {
            true => TestStatus::Passed,
            false => TestStatus::Failed,
        },
    );

    let start_instant = Instant::now();

    tokio::time::sleep(Duration::from_secs(2)).await;
    run_operation_tests(&remote_device, &mut result_map).await;

    // wait the intended timeout
    let elapsed = start_instant.elapsed();
    let to_wait = match elapsed < REPORT_TIMEOUT + REPORT_TIMEOUT_TOLERANCE {
        true => REPORT_TIMEOUT + REPORT_TIMEOUT_TOLERANCE - start_instant.elapsed(),
        false => Duration::from_nanos(1),
    };
    info!("Waiting the remaining {}s for data", &to_wait.as_secs());
    tokio::time::sleep(to_wait).await;

    info!("Ending subscription, waited enough");
    run_task.store(false, Ordering::Relaxed);

    sub_collection_task.await?;

    // check for metric with enough updates
    let metric_result = match metric_reports_by_handle
        .lock()
        .unwrap()
        .iter()
        .any(|(_, count)| count >= &EXPECTED_METRIC_UPDATES)
    {
        true => TestStatus::Passed,
        false => TestStatus::Failed,
    };
    result_map.insert(ReferenceTestStep::Test7, metric_result);
    let metric_states_received: i32 = metric_reports_by_handle
        .lock()
        .unwrap()
        .iter()
        .map(|(_, count)| count)
        .sum();
    info!("Received {} metric state updates", metric_states_received);

    // check for alerts with enough updates
    let alert_result = match alert_reports_by_handle
        .lock()
        .unwrap()
        .iter()
        .any(|(_, count)| count >= &EXPECTED_ALERT_UPDATES)
    {
        true => TestStatus::Passed,
        false => TestStatus::Failed,
    };
    result_map.insert(ReferenceTestStep::Test8, alert_result);
    let alert_states_received: i32 = alert_reports_by_handle
        .lock()
        .unwrap()
        .iter()
        .map(|(_, count)| count)
        .sum();
    info!("Received {} alert state updates", alert_states_received);

    result_map.insert(
        ReferenceTestStep::Test10,
        match remote_device.disconnect().await {
            Ok(_) => TestStatus::Passed,
            Err(err) => {
                error!("{}", err);
                TestStatus::Failed
            }
        },
    );

    tokio::time::sleep(Duration::from_secs(10)).await;

    ReferenceTestStep::iter()
        .map(|it| (it.clone(), result_map.get(&it)))
        .filter(|(_, result)| result.is_some())
        .for_each(|(test, result)| info!("{}: {}", test, result.unwrap()));

    Ok(())
}
