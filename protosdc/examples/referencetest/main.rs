extern crate core;

use std::path::PathBuf;
use std::str::FromStr;

use anyhow::Result;
use clap::{Parser, Subcommand};
use common::crypto::CryptoConfig;
use log::info;

use crate::provider::ProviderMode;

pub mod consumer;
pub mod performance_mode;
pub mod provider;

/// protosdc-rs reference test
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Path to the public key to use
    #[clap(long)]
    public_key_path: Option<String>,

    /// Path to the private key to use
    #[clap(long)]
    private_key_path: Option<String>,

    /// Password to decrypt the private key
    #[clap(long)]
    private_key_password: Option<String>,

    /// Path to the ca certificate
    #[clap(long)]
    ca_cert_path: Option<String>,

    /// IP of the adapter to use
    #[clap(short, long)]
    adapter_ip: String,

    /// Proxy Url
    #[clap(short, long)]
    proxy_url: Option<String>,

    #[clap(subcommand)]
    command: Commands,
}

#[derive(Subcommand, Debug)]
enum Commands {
    /// Run the protosdc-rs reference provider
    Provider {
        /// Port to use for the provider
        #[clap(short, long, default_value_t = 12313)]
        port: u16,

        /// EndpointReference to use for the provider
        #[clap(short, long, default_value = "protosdc-rs-provider")]
        epr: String,

        /// Enable performance test mode instead of reference test
        #[clap(long)]
        performance_mode: bool,
    },
    /// Run the protosdc-rs reference consumer
    Consumer {
        /// Characters the EndpointReference of the provider to connect to ends with
        #[clap(long)]
        epr: String,
    },
}

pub fn file_exists(path: &str) -> bool {
    PathBuf::from_str(path)
        .unwrap_or_else(|_| panic!("Path {} was not valid", &path))
        .exists()
}

async fn get_crypto_config(args: &Args) -> Option<CryptoConfig> {
    if args.public_key_path.is_none()
        && args.private_key_path.is_none()
        && args.ca_cert_path.is_none()
    {
        info!("No crypto parameters provided");
        return None;
    }

    info!(
        "Running with crypto parameters public {:?} private {:?} ca {:?} password {:?}",
        &args.public_key_path,
        &args.private_key_path,
        &args.ca_cert_path,
        &args.private_key_password
    );
    assert!(file_exists(
        args.public_key_path
            .as_ref()
            .expect("Path to public key file required")
    ));
    assert!(file_exists(
        args.private_key_path
            .as_ref()
            .expect("Path to private key file required")
    ));
    assert!(file_exists(
        args.ca_cert_path
            .as_ref()
            .expect("Path to ca certificate file required")
    ));

    Some(
        CryptoConfig::new(
            &args.private_key_path.as_ref().unwrap().clone(),
            &args.public_key_path.as_ref().unwrap().clone(),
            &args.ca_cert_path.as_ref().unwrap().clone(),
            match &args.private_key_password.as_ref() {
                None => None,
                Some(x) => Some(x),
            },
        )
        .await
        .expect("Could not load certs"),
    )
}

fn init() {
    env_logger::init();
}

#[tokio::main]
async fn main() -> Result<()> {
    init();

    let args = Args::parse();
    let crypto_config = get_crypto_config(&args).await;

    match &args.command {
        Commands::Provider {
            port,
            epr,
            performance_mode,
        } => {
            provider::run_provider(
                args.adapter_ip,
                *port,
                epr,
                crypto_config,
                match performance_mode {
                    true => ProviderMode::PerformanceProvider,
                    false => ProviderMode::ReferenceProvider,
                },
                args.proxy_url,
                // TODO: Load file with texts optionally.
                None,
            )
            .await?
        }
        Commands::Consumer { epr } => {
            consumer::run_consumer(args.adapter_ip, epr, crypto_config, args.proxy_url).await?
        }
    };

    info!("Done");
    Ok(())
}
