use anyhow::{Context, Result};
use tokio::time::timeout;
use tokio::time::Duration;

use network::mock_udp_binding::MockUdpBinding;
use protosdc::discovery::consumer::discovery_consumer::{
    DiscoveryConsumer, DiscoveryConsumerImpl, DiscoveryEvent,
};
use protosdc::discovery::provider::discovery_provider::{DiscoveryProvider, DiscoveryProviderImpl};
use protosdc_proto::common::Uri;
use protosdc_proto::discovery::{
    scope_matcher, search_filter, Endpoint, ScopeMatcher, SearchFilter,
};

#[cfg(test)]
pub mod test_util;

const MAX_WAIT: Duration = Duration::from_secs(2);
const ENDPOINT: &str = "TodoFunnyString";

fn string_to_uri(data: &[&str]) -> Vec<Uri> {
    data.iter()
        .map(|it| Uri {
            value: it.to_string(),
        })
        .collect()
}

async fn startup() -> Result<(
    DiscoveryConsumerImpl<MockUdpBinding>,
    DiscoveryProviderImpl<MockUdpBinding>,
)> {
    let mock_binding_consumer = MockUdpBinding::default();
    let mock_binding_provider =
        MockUdpBinding::new_with_channel(mock_binding_consumer.mock_sender.clone());

    let provider_endpoint = Endpoint {
        endpoint_identifier: ENDPOINT.to_string(),
        scope: vec![],
        physical_address: vec![],
    };

    let consumer = DiscoveryConsumerImpl::new(mock_binding_consumer, None, None).await?;
    let provider =
        DiscoveryProviderImpl::new(mock_binding_provider, None, provider_endpoint.into(), None)
            .await?;

    Ok((consumer, provider))
}

#[tokio::test]
async fn test_adhoc_hello() -> Result<()> {
    common::test_util::init_logging();
    let mock_binding_consumer = MockUdpBinding::default();
    let mock_binding_provider =
        MockUdpBinding::new_with_channel(mock_binding_consumer.mock_sender.clone());

    let provider_endpoint = Endpoint {
        endpoint_identifier: ENDPOINT.to_string(),
        scope: vec![],
        physical_address: vec![],
    };

    let consumer = DiscoveryConsumerImpl::new(mock_binding_consumer, None, None).await?;
    let mut provider =
        DiscoveryProviderImpl::new(mock_binding_provider, None, provider_endpoint.into(), None)
            .await?;

    let mut consumer_events = consumer.subscribe();
    // trigger a hello
    let _ = provider.hello().await;

    if let Ok(message) = timeout(MAX_WAIT, consumer_events.recv()).await {
        match message {
            Ok(DiscoveryEvent::EndpointEntered {
                endpoint,
                proxy,
                secure,
            }) => {
                assert_eq!(ENDPOINT, &endpoint.endpoint_identifier);
                assert!(!proxy);
                assert!(!secure);
            }
            _ => panic!("Wrong event type {:?}", message),
        }
    } else {
        panic!(".recv() timed out after {}s", MAX_WAIT.as_secs_f32())
    }

    let expected_scopes: Vec<Uri> = string_to_uri(&["https://host/path1", "https://host/path2"]);

    provider.update_scopes(expected_scopes.clone()).await;

    if let Ok(Ok(message)) = timeout(MAX_WAIT, consumer_events.recv()).await {
        match message {
            DiscoveryEvent::EndpointEntered {
                endpoint,
                proxy,
                secure,
            } => {
                assert_eq!(ENDPOINT, &endpoint.endpoint_identifier);
                assert_eq!(expected_scopes, endpoint.scope);
                assert!(!proxy);
                assert!(!secure);
            }
            _ => panic!("Wrong event type {:?}", message),
        }
    } else {
        panic!("Error in events")
    }

    let physical_address = "hey://aight.mate".to_string();

    provider
        .update_physical_address(string_to_uri(&[&physical_address]))
        .await;

    if let Ok(Ok(message)) = timeout(MAX_WAIT, consumer_events.recv()).await {
        match message {
            DiscoveryEvent::EndpointEntered {
                endpoint,
                proxy,
                secure,
            } => {
                assert_eq!(ENDPOINT, &endpoint.endpoint_identifier);
                assert_eq!(expected_scopes, endpoint.scope);
                assert_eq!(
                    physical_address,
                    endpoint.physical_address.first().unwrap().value
                );
                assert!(!proxy);
                assert!(!secure);
            }
            _ => panic!("Wrong event type {:?}", message),
        }
    } else {
        panic!("Error in events")
    }

    Ok(())
}

#[tokio::test]
async fn test_adhoc_scope_search() -> Result<()> {
    common::test_util::init_logging();
    let (mut consumer, mut provider) = startup().await?;

    // test string compare
    {
        let expected_scopes: Vec<Uri> =
            string_to_uri(&["https://host/path1", "https://host/path2"]);

        provider.update_scopes(expected_scopes.clone()).await;

        let search_filters: Vec<SearchFilter> = expected_scopes
            .iter()
            .map(|it| ScopeMatcher {
                algorithm: scope_matcher::Algorithm::StringCompare as i32,
                scope: Some(it.clone()),
            })
            .map(|it| SearchFilter {
                r#type: Some(search_filter::Type::ScopeMatcher(it)),
            })
            .collect();

        let result = timeout(MAX_WAIT, consumer.search(search_filters, Some(1), None))
            .await
            .context("Search failed")?
            .context("Search failed")?;
        assert_eq!(1, result.len());
        assert_eq!(&provider.endpoint().await, result.first().unwrap())
    }
    // test uri compare
    {
        let expected_scopes: Vec<Uri> =
            string_to_uri(&["https://host/path1", "https://host/path2"]);

        provider.update_scopes(expected_scopes.clone()).await;

        let search_filters: Vec<SearchFilter> = expected_scopes
            .iter()
            .map(|it| ScopeMatcher {
                algorithm: scope_matcher::Algorithm::Rfc3986 as i32,
                scope: Some(it.clone()),
            })
            .map(|it| SearchFilter {
                r#type: Some(search_filter::Type::ScopeMatcher(it)),
            })
            .collect();

        let result = timeout(MAX_WAIT, consumer.search(search_filters, Some(1), None))
            .await
            .context("Search failed")?
            .context("Search failed")?;
        assert_eq!(1, result.len());
        assert_eq!(&provider.endpoint().await, result.first().unwrap())
    }
    // test uri compare case-insensitivity
    {
        let expected_scopes: Vec<Uri> =
            string_to_uri(&["https://host/path1", "https://host/path2"]);
        let search_scopes: Vec<Uri> = string_to_uri(&["https://host/path1", "HTTPS://host/path2"]);

        provider.update_scopes(expected_scopes.clone()).await;

        let search_filters: Vec<SearchFilter> = search_scopes
            .iter()
            .map(|it| ScopeMatcher {
                algorithm: scope_matcher::Algorithm::Rfc3986 as i32,
                scope: Some(it.clone()),
            })
            .map(|it| SearchFilter {
                r#type: Some(search_filter::Type::ScopeMatcher(it)),
            })
            .collect();

        let result = timeout(MAX_WAIT, consumer.search(search_filters, Some(1), None))
            .await
            .context("Search failed")?
            .context("Search failed")?;
        assert_eq!(1, result.len());
        assert_eq!(&provider.endpoint().await, result.first().unwrap())
    }

    Ok(())
}

#[tokio::test]
async fn test_adhoc_no_filter_empty_filter() -> Result<()> {
    common::test_util::init_logging();
    let (mut consumer, provider) = startup().await?;

    // no filters
    {
        let result = timeout(MAX_WAIT, consumer.search(vec![], Some(1), None))
            .await
            .context("Search failed")?
            .context("Search failed")?;
        assert_eq!(1, result.len());
        assert_eq!(&provider.endpoint().await, result.first().unwrap())
    }
    // empty filters
    {
        let filter = SearchFilter::default();
        timeout(MAX_WAIT, consumer.search(vec![filter], Some(1), None))
            .await
            .expect_err("Should've timed out");
    }

    Ok(())
}

#[tokio::test]
async fn test_adhoc_search_endpoint() -> Result<()> {
    common::test_util::init_logging();
    let (mut consumer, provider) = startup().await?;

    // no filters
    {
        timeout(MAX_WAIT, consumer.search_endpoint("whatever".to_string()))
            .await
            .expect_err("Should've timed out");
    }
    // empty filters
    {
        let result = timeout(MAX_WAIT, consumer.search_endpoint(ENDPOINT.to_string()))
            .await
            .context("Search failed")?
            .context("Search failed")?;
        assert!(result.is_some());
        assert_eq!(provider.endpoint().await, result.unwrap())
    }

    Ok(())
}
