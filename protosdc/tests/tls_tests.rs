use std::net::IpAddr;

use anyhow::Result;
use log::info;

use biceps::common::access::mdib_access::MdibAccess;
use biceps::common::mdib_entity::EntityBase;
use common::Service;
use common_test::crypto::generate_certificates;
use network::platform::test_util::determine_adapter_address_for_multicast;
use network::udp_binding::IPV4_MULTICAST_ADDRESS;
use protosdc::consumer::grpc_consumer::{Consumer, ConsumerError, ConsumerImpl, ProviderService};

use crate::test_util::{connect_consumer, create_device};

#[cfg(test)]
pub mod test_util;

/// Tests whether basic communication works when using encrypted communication.
#[tokio::test]
async fn test_encrypted_connection() -> Result<()> {
    common::test_util::init_logging();

    // find address which does ipv4 multicast
    let multicast_address = IpAddr::V4(IPV4_MULTICAST_ADDRESS);
    let adapter_address = determine_adapter_address_for_multicast(multicast_address).await;

    let generated_certs = generate_certificates()?;

    let mut device = create_device(
        adapter_address,
        Some(generated_certs.provider_crypto_config()),
        None,
    )
    .await?;
    device.start().await?;

    let to_connect = device.endpoint().await;
    let remote_device =
        connect_consumer(to_connect, Some(generated_certs.consumer_crypto_config())).await?;

    {
        let mdib_version = remote_device.mdib_version().await;
        info!("Remote Device Mdib Version: {:?}", mdib_version);
    }

    // get all entities
    let all_entities = device.local_mdib.entities_by_type(|_| true).await;

    assert!(!all_entities.is_empty());

    for entity in &all_entities {
        assert!(remote_device
            .get_mdib_access()
            .await
            .entity(&entity.entity.handle())
            .await
            .is_some())
    }

    Ok(())
}

/// Tests whether connections between Provider and Consumer fail when the certificates are signed
/// by different CAs.
#[tokio::test]
async fn test_not_matching_certs() -> Result<()> {
    common::test_util::init_logging();

    // find address which does ipv4 multicast
    let multicast_address = IpAddr::V4(IPV4_MULTICAST_ADDRESS);
    let adapter_address = determine_adapter_address_for_multicast(multicast_address).await;

    let generated_certs_one = generate_certificates()?;
    let generated_certs_two = generate_certificates()?;

    let mut device_one = create_device(
        adapter_address,
        Some(generated_certs_one.provider_crypto_config()),
        None,
    )
    .await?;
    device_one.start().await?;
    let to_connect_one = device_one.endpoint().await;

    let mut device_two = create_device(
        adapter_address,
        Some(generated_certs_two.provider_crypto_config()),
        None,
    )
    .await?;
    device_two.start().await?;
    let to_connect_two = device_two.endpoint().await;

    // device one, certs one
    {
        let mut consumer = ConsumerImpl::default();
        let result = consumer
            .connect(
                to_connect_one.clone(),
                Some(generated_certs_one.consumer_crypto_config()),
            )
            .await;

        assert!(result.is_ok());
    }
    // device one, certs two
    {
        let mut consumer = ConsumerImpl::default();
        let result = consumer
            .connect(
                to_connect_one,
                Some(generated_certs_two.consumer_crypto_config()),
            )
            .await;

        let err = result.expect_err("Connect should've returned an error");
        assert!(matches!(err, ConsumerError::ServiceConnection { .. }));

        match err {
            ConsumerError::ServiceConnection { service, errors } => {
                assert_eq!(ProviderService::Metadata, service);
                assert_eq!(1, errors.len());
            }
            _ => panic!(),
        }
    }
    // device two, certs one
    {
        let mut consumer = ConsumerImpl::default();
        let result = consumer
            .connect(
                to_connect_two.clone(),
                Some(generated_certs_one.consumer_crypto_config()),
            )
            .await;

        let err = result.expect_err("Connect should've returned an error");
        assert!(matches!(err, ConsumerError::ServiceConnection { .. }));

        match err {
            ConsumerError::ServiceConnection { service, errors } => {
                assert_eq!(ProviderService::Metadata, service);
                assert_eq!(1, errors.len());
            }
            _ => panic!(),
        }
    }
    // device two, certs two
    {
        let mut consumer = ConsumerImpl::default();
        let result = consumer
            .connect(
                to_connect_two,
                Some(generated_certs_two.consumer_crypto_config()),
            )
            .await;

        assert!(result.is_ok());
    }

    Ok(())
}

/// Tests whether connections between Provider and Consumer fail when either side doesn't
/// provide tls.
#[tokio::test]
async fn test_no_unencrypted_connection() -> Result<()> {
    common::test_util::init_logging();

    // find address which does ipv4 multicast
    let multicast_address = IpAddr::V4(IPV4_MULTICAST_ADDRESS);
    let adapter_address = determine_adapter_address_for_multicast(multicast_address).await;

    let generated_certs_one = generate_certificates()?;

    let mut device_one = create_device(
        adapter_address,
        Some(generated_certs_one.provider_crypto_config()),
        None,
    )
    .await?;
    device_one.start().await?;
    let to_connect_one = device_one.endpoint().await;

    let mut device_two = create_device(adapter_address, None, None).await?;
    device_two.start().await?;
    let to_connect_two = device_two.endpoint().await;

    // device one, certs
    {
        let mut consumer = ConsumerImpl::default();
        let result = consumer
            .connect(
                to_connect_one.clone(),
                Some(generated_certs_one.consumer_crypto_config()),
            )
            .await;

        assert!(result.is_ok());
    }
    // device one, no certs
    {
        let mut consumer = ConsumerImpl::default();
        let result = consumer.connect(to_connect_one, None).await;

        let err = result.expect_err("Connect should've returned an error");
        assert!(matches!(err, ConsumerError::ServiceConnection { .. }));

        match err {
            ConsumerError::ServiceConnection { service, errors } => {
                assert_eq!(ProviderService::Metadata, service);
                assert_eq!(1, errors.len());
            }
            _ => panic!("err was {:?}", err),
        }
    }
    // device two, certs
    {
        let mut consumer = ConsumerImpl::default();
        let result = consumer
            .connect(
                to_connect_two.clone(),
                Some(generated_certs_one.consumer_crypto_config()),
            )
            .await;

        let err = result.expect_err("Connect should've returned an error");
        assert!(matches!(err, ConsumerError::ServiceConnection { .. }));

        match err {
            ConsumerError::ServiceConnection { service, errors } => {
                assert_eq!(ProviderService::Metadata, service);
                assert_eq!(1, errors.len());
            }
            _ => panic!(),
        }
    }
    // device two, no certs
    {
        let mut consumer = ConsumerImpl::default();
        let result = consumer.connect(to_connect_two, None).await;

        assert!(result.is_ok());
    }

    Ok(())
}
