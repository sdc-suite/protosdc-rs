use std::net::IpAddr;

use anyhow::Result;
use futures::{stream, StreamExt};
use log::info;
use protosdc_proto::biceps::{AbstractGetMsg, GetLocalizedTextMsg};
use protosdc_proto::discovery::{search_filter, SearchFilter};
use protosdc_proto::sdc::{GetLocalizedTextRequest, GetSupportedLanguagesRequest};

use biceps::common::access::mdib_access::MdibAccess;
use common::Service;
use network::platform::test_util::determine_adapter_address_for_multicast;
use network::udp_binding::{create_udp_binding, IPV4_MULTICAST_ADDRESS};
use protosdc::common::action;
use protosdc::consumer::grpc_consumer::Consumer;
use protosdc::discovery::consumer::discovery_consumer::{DiscoveryConsumer, DiscoveryConsumerImpl};

use crate::test_util::{connect_consumer, create_device};

#[cfg(test)]
pub mod test_util;

#[tokio::test]
async fn test_get_service() -> Result<()> {
    common::test_util::init_logging();
    // find address which does ipv4 multicast
    let multicast_address = IpAddr::V4(IPV4_MULTICAST_ADDRESS);
    let adapter_address = determine_adapter_address_for_multicast(multicast_address).await;

    let mut device = create_device(adapter_address, None, None).await?;
    device.start().await?;

    let to_connect = device.endpoint().await;

    let remote_device = connect_consumer(to_connect, None).await?;
    {
        let mdib_version = remote_device.mdib_version().await;
        info!("Remote Device Mdib Version: {:?}", mdib_version);
    }

    // get all entities
    let all_entities = device.local_mdib.entities_by_type(|_| true).await;

    let get_mdib_response = remote_device.get_mdib().await?;
    let get_md_description_response = remote_device.get_md_description(vec![]).await?;
    let get_md_state_response = remote_device.get_md_state(vec![]).await?;
    let get_context_states_response = remote_device.get_context_states(vec![]).await?;

    common_test::tests::get::get_tests::test_get_service(
        &all_entities,
        &remote_device.get_mdib_access().await,
        &get_mdib_response,
        &get_md_description_response,
        &get_md_state_response,
        &get_context_states_response,
    )
    .await
}

#[tokio::test]
async fn test_metric_change() -> Result<()> {
    common::test_util::init_logging();

    // find address which does ipv4 multicast
    let multicast_address = IpAddr::V4(IPV4_MULTICAST_ADDRESS);
    let adapter_address = determine_adapter_address_for_multicast(multicast_address).await;

    let mut device = create_device(adapter_address, None, None).await?;
    device.start().await?;

    let to_connect = device.endpoint().await;
    let remote_device = connect_consumer(to_connect, None).await?;

    {
        let mdib_version = remote_device.mdib_version().await;
        info!("Remote Device Mdib Version: {:?}", mdib_version);
    }

    common_test::tests::metric_change::metric_change_tests::test_metric_change(
        remote_device.get_mdib_access().await,
        device.local_mdib.clone(),
    )
    .await
}

#[tokio::test]
async fn test_discovery() -> Result<()> {
    common::test_util::init_logging();

    // find address which does ipv4 multicast
    let multicast_address = IpAddr::V4(IPV4_MULTICAST_ADDRESS);
    let adapter_address = determine_adapter_address_for_multicast(multicast_address).await;

    let mut device = create_device(adapter_address, None, None).await?;
    device.start().await?;
    let device_endpoint = device.endpoint().await;

    let mut discovery_consumer = DiscoveryConsumerImpl::new(
        create_udp_binding(adapter_address, None)
            .await
            .map_err(anyhow::Error::new)?,
        None,
        None,
    )
    .await?;

    let result = discovery_consumer
        .search(
            vec![SearchFilter {
                r#type: Some(search_filter::Type::EndpointIdentifier(
                    device_endpoint.endpoint_identifier,
                )),
            }],
            Some(1),
            None,
        )
        .await;

    let to_connect = result.unwrap().pop().unwrap();
    let remote_device = connect_consumer(to_connect, None).await?;

    {
        let mdib_version = remote_device.mdib_version().await;
        info!("Remote Device Mdib Version: {:?}", mdib_version);
    }

    // make sure there is something there.
    assert!(!remote_device
        .get_mdib_access()
        .await
        .root_entities()
        .await
        .is_empty());

    Ok(())
}

#[tokio::test]
async fn test_localization_service() -> Result<()> {
    common::test_util::init_logging();

    let localization_storage =
        common_test::tests::localization::localization_types::localization_data();

    // find address which does ipv4 multicast
    let multicast_address = IpAddr::V4(IPV4_MULTICAST_ADDRESS);
    let adapter_address = determine_adapter_address_for_multicast(multicast_address).await;

    let mut device = create_device(adapter_address, None, Some(localization_storage)).await?;
    device.start().await?;

    let to_connect = device.endpoint().await;
    let remote_device = connect_consumer(to_connect, None).await?;

    {
        let mdib_version = remote_device.mdib_version().await;
        info!("Remote Device Mdib Version: {:?}", mdib_version);
    }

    // start queries for texts
    let mut localization_client = remote_device
        .consumer()
        .get_localization_service()
        .await
        .expect("Localization client absent");

    // get supported languages
    let supported_languages = {
        let request = GetSupportedLanguagesRequest {
            addressing: Some(protosdc::addressing::util::create_for_action(
                action::GET_SUPPORTED_LANGUAGES_REQUEST,
            )),
            payload: Some(protosdc_proto::biceps::GetSupportedLanguagesMsg {
                abstract_get: Some(AbstractGetMsg {
                    extension_element: None,
                }),
            }),
        };
        let supported_languages_response = localization_client
            .get_supported_languages(request)
            .await
            .expect("Error response");

        supported_languages_response
            .into_inner()
            .payload
            .expect("Payload missing")
            .lang
    };

    // get all texts
    let texts = {
        let request = GetLocalizedTextRequest {
            addressing: Some(protosdc::addressing::util::create_for_action(
                action::GET_LOCALIZED_TEXT_REQUEST,
            )),
            payload: Some(GetLocalizedTextMsg {
                abstract_get: Some(AbstractGetMsg {
                    extension_element: None,
                }),
                r#ref: vec![],
                version: None,
                lang: vec![],
                text_width: vec![],
                number_of_lines: vec![],
            }),
        };

        let texts_response = localization_client
            .get_localized_text(request)
            .await
            .expect("Error response");

        texts_response
            .into_inner()
            .map(|it| it.expect("error response"))
            .flat_map(|it| stream::iter(it.payload.expect("payload missing").text))
            .map(|it| it.try_into().expect("Could not map text"))
            .collect::<Vec<_>>()
            .await
    };

    common_test::tests::localization::localization_tests::test_localization_service(
        &supported_languages,
        &texts,
    )
    .await
}
