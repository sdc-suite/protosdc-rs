use std::fmt::{Display, Formatter};
use std::ops::Sub;
use std::str::FromStr;

use async_trait::async_trait;
use log::{debug, error, trace};
use tokio::sync::broadcast::{
    channel as BroadcastChannel, Receiver as BroadcastReceiver, Sender as BroadcastSender,
};
use tokio::task;
use tokio::time::timeout;
use tokio::time::{Duration, Instant};
use tokio_stream::wrappers::BroadcastStream;
use tokio_stream::StreamExt;

use network::udp_binding::{InboundUdpMessage, UdpBinding};
use protosdc_proto::discovery::consumer_proxy_service_client::ConsumerProxyServiceClient;
use protosdc_proto::discovery::discovery_message::Type;
use protosdc_proto::discovery::{
    search_filter, DiscoveryMessage, Endpoint, ProxySearchRequest, SearchFilter, SearchRequest,
};
use tokio_util::sync::{CancellationToken, DropGuard};

use crate::addressing;
use crate::common::crypto::CryptoConfig;
use crate::common::hyper_client::{create_client, get_dummy_uri, HyperClient};
use crate::discovery::common::error::DiscoveryError;
use crate::discovery::common::util as discovery_util;

pub const DEFAULT_MAX_WAIT: Duration = Duration::from_secs(10);
const BUFFER_SIZE: usize = 100;

#[derive(Debug, Clone)]
pub enum DiscoveryEvent {
    EndpointEntered {
        endpoint: Endpoint,
        proxy: bool,
        secure: bool,
    },
    EndpointLeft {
        endpoint: Endpoint,
        proxy: bool,
        secure: bool,
    },
    SearchMatch {
        search_id: String,
        endpoints: Vec<Endpoint>,
        proxy: bool,
        secure: bool,
    },
    SearchTimeout {
        search_id: String,
    },
}

#[async_trait]
pub trait DiscoveryConsumer {
    /// Subscribe to discovery events occurring during a running discovery.
    fn subscribe(&self) -> BroadcastReceiver<DiscoveryEvent>;

    /// Search for Endpoints matching the filter criteria.
    ///
    /// # Arguments
    ///
    /// * `search_filter`: Criteria to filter by. If empty, all endpoints should respond.
    /// * `max_responses`: Maximum responses to wait for until early return.
    /// * `max_wait`: Maximum time to wait until return. Defaults to [DEFAULT_MAX_WAIT][`crate::discovery::consumer::DEFAULT_MAX_WAIT`]
    ///
    /// returns: Result<Vec<Endpoint>, DiscoveryError>
    async fn search(
        &mut self,
        search_filter: Vec<SearchFilter>,
        max_responses: Option<usize>,
        max_wait: Option<Duration>,
    ) -> Result<Vec<Endpoint>, DiscoveryError>;

    /// Searches for the first endpoint responding to the filter criteria.
    ///
    /// # Arguments
    ///
    /// * `search_filter`: Criteria to filter by. If empty, all endpoints should respond.
    /// * `max_wait`: Maximum time to wait until return. Defaults to [DEFAULT_MAX_WAIT][`crate::discovery::consumer::DEFAULT_MAX_WAIT`]
    ///
    /// returns: Result<Option<Endpoint>, DiscoveryError>
    async fn search1(
        &mut self,
        search_filter: Vec<SearchFilter>,
        max_wait: Option<Duration>,
    ) -> Result<Option<Endpoint>, DiscoveryError>;

    /// Searches for the endpoint with the given endpoint identifier.
    ///
    /// # Arguments
    ///
    /// * `identifier`: Endpoint identifier to search for.
    ///
    /// returns: Result<Option<Endpoint>, DiscoveryError>
    async fn search_endpoint(
        &mut self,
        identifier: String,
    ) -> Result<Option<Endpoint>, DiscoveryError>;
}

#[derive(Debug)]
pub struct DiscoveryConsumerImpl<T>
where
    T: UdpBinding + Send,
{
    udp_binding: T,
    proxy_address: Option<String>,
    proxy_client: Option<ConsumerProxyServiceClient<HyperClient>>,
    _udp_task: task::JoinHandle<()>,
    _drop_guard: DropGuard,
    discovery_event_sender: BroadcastSender<DiscoveryEvent>,
    discovery_message_sender: BroadcastSender<DiscoveryMessage>,
}

impl<T> Display for DiscoveryConsumerImpl<T>
where
    T: UdpBinding + Display + Send,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "DiscoveryConsumerImpl (udp_binding {}, discovery_proxy: {:?})",
            self.udp_binding, self.proxy_address,
        )
    }
}

async fn handle_message(
    message: DiscoveryMessage,
    event_sender: &BroadcastSender<DiscoveryEvent>,
    message_sender: &BroadcastSender<DiscoveryMessage>,
) {
    if let Some(ref payload) = message.r#type {
        match payload {
            Type::Hello(hello) => match &hello.endpoint {
                None => {}
                Some(endpoint) => {
                    event_sender
                        .send(DiscoveryEvent::EndpointEntered {
                            endpoint: endpoint.clone(),
                            proxy: false,
                            secure: false,
                        })
                        .map_err(|err| error!("Unexpected error in event distribution: {}", err))
                        .ok();
                }
            },
            Type::Bye(bye) => match &bye.endpoint {
                None => {}
                Some(endpoint) => {
                    event_sender
                        .send(DiscoveryEvent::EndpointLeft {
                            endpoint: endpoint.clone(),
                            proxy: false,
                            secure: false,
                        })
                        .map_err(|err| error!("Unexpected error in event distribution: {}", err))
                        .ok();
                }
            },
            Type::SearchRequest(_) => {
                // NOP
            }
            Type::SearchResponse(_) => {
                message_sender
                    .send(message.clone())
                    .map_err(|err| error!("Unexpected error in event distribution: {}", err))
                    .ok();
            }
        }
    }
}

async fn process_udp_messages(
    receiver: BroadcastReceiver<InboundUdpMessage>,
    event_sender: BroadcastSender<DiscoveryEvent>,
    message_sender: BroadcastSender<DiscoveryMessage>,
    cancellation_token: CancellationToken,
) {
    discovery_util::process_udp_messages(
        receiver,
        |_header, message| handle_message(message, &event_sender, &message_sender),
        cancellation_token,
    )
    .await
}

impl<T> DiscoveryConsumerImpl<T>
where
    T: UdpBinding + Send,
{
    pub async fn new(
        udp_binding: T,
        discovery_proxy_address: Option<String>,
        crypto_config: Option<CryptoConfig>,
    ) -> Result<Self, DiscoveryError> {
        let (discovery_event_sender, _) = BroadcastChannel(BUFFER_SIZE);
        let (discovery_message_sender, _) = BroadcastChannel(BUFFER_SIZE);
        let cancellation_token = CancellationToken::new();

        let receiver = udp_binding.subscribe();

        // kickstart processing udp messages
        let run = process_udp_messages(
            receiver,
            discovery_event_sender.clone(),
            discovery_message_sender.clone(),
            cancellation_token.clone(),
        );
        let udp_task = tokio::spawn(run);

        // connect discovery proxy if present
        let proxy_client = match &discovery_proxy_address {
            Some(addr) => Some(connect_proxy(addr, crypto_config).await?),
            None => None,
        };

        Ok(DiscoveryConsumerImpl {
            udp_binding,
            proxy_address: discovery_proxy_address,
            proxy_client,
            _udp_task: udp_task,
            discovery_event_sender,
            discovery_message_sender,
            _drop_guard: cancellation_token.drop_guard(),
        })
    }
}

async fn connect_proxy(
    proxy_address: &str,
    crypto_config: Option<CryptoConfig>,
) -> Result<ConsumerProxyServiceClient<HyperClient>, DiscoveryError> {
    let uri = hyper::Uri::from_str(proxy_address)?;
    let client = create_client(&uri, &crypto_config).await?;
    Ok(ConsumerProxyServiceClient::with_origin(
        client,
        get_dummy_uri(&uri),
    ))
}

async fn run_search(
    search_id: String,
    message_id: String,
    max_response: Option<usize>,
    max_wait: Duration,
    message_receiver: BroadcastReceiver<DiscoveryMessage>,
    event_sender: BroadcastSender<DiscoveryEvent>,
) -> Vec<Endpoint> {
    let mut message_stream = BroadcastStream::new(message_receiver);
    let mut data = vec![];
    let start = Instant::now();
    let mut response_count = 0;

    let mut remaining = max_wait.sub(start.elapsed());
    'outer: while let Ok(Some(item)) = timeout(remaining, message_stream.next()).await {
        remaining = max_wait.sub(start.elapsed());
        match item {
            Ok(message) => {
                let addressing_matches = match message.addressing {
                    None => false,
                    Some(addr) => match &addr.relates_id {
                        Some(a) => a == &message_id,
                        None => false,
                    },
                };
                if !addressing_matches {
                    continue;
                }
                if let Some(Type::SearchResponse(response)) = message.r#type {
                    match event_sender.send(DiscoveryEvent::SearchMatch {
                        search_id: search_id.clone(),
                        endpoints: response.endpoint.clone(),
                        proxy: false,
                        secure: false,
                    }) {
                        Ok(_) => {}
                        Err(err) => trace!("No receivers for event messages, dropping: {:?}", err),
                    };
                    data.extend(response.endpoint);
                    response_count += 1;
                    if let Some(max) = max_response {
                        if response_count >= max {
                            break 'outer;
                        }
                    }
                }
            }
            Err(err) => {
                error!("Error during discovery {:?}", err)
            }
        }
    }
    match event_sender.send(DiscoveryEvent::SearchTimeout {
        search_id: search_id.clone(),
    }) {
        Ok(_) => {}
        Err(err) => trace!("No receivers for event messages, dropping: {:?}", err),
    };
    data
}

#[async_trait]
impl<T> DiscoveryConsumer for DiscoveryConsumerImpl<T>
where
    T: UdpBinding + Sync + Send,
{
    fn subscribe(&self) -> BroadcastReceiver<DiscoveryEvent> {
        self.discovery_event_sender.subscribe()
    }

    async fn search(
        &mut self,
        search_filters: Vec<SearchFilter>,
        max_responses: Option<usize>,
        max_wait: Option<Duration>,
    ) -> Result<Vec<Endpoint>, DiscoveryError> {
        match &mut self.proxy_client {
            // udp it is
            None => {
                let addressing =
                    addressing::util::create_for_action(crate::common::action::SEARCH_REQUEST);
                let message_id = addressing.message_id.clone();
                let search = SearchRequest {
                    search_filter: search_filters,
                };
                let discovery_message = DiscoveryMessage {
                    addressing: Some(addressing),
                    r#type: Some(Type::SearchRequest(search)),
                };

                let search_id = format!("search_{}", message_id);
                let search_task = run_search(
                    search_id,
                    message_id,
                    max_responses,
                    max_wait.unwrap_or(DEFAULT_MAX_WAIT),
                    self.discovery_message_sender.subscribe(),
                    self.discovery_event_sender.clone(),
                );

                discovery_util::send_multicast(
                    self.udp_binding.get_message_channel_sender(),
                    discovery_message,
                )
                .await?;

                Ok(search_task.await)
            }
            // or is it?
            Some(ref mut client) => {
                let request = ProxySearchRequest {
                    addressing: Some(addressing::util::create_for_action(
                        crate::common::action::SEARCH_REQUEST,
                    )),
                    search_filter: search_filters,
                };
                let response = client
                    .search(request)
                    .await
                    .map_err(DiscoveryError::TonicStatusError)?;
                debug!("Proxy response: {:?}", &response);
                Ok(response.into_inner().endpoint)
            }
        }
    }

    async fn search1(
        &mut self,
        search_filter: Vec<SearchFilter>,
        max_wait: Option<Duration>,
    ) -> Result<Option<Endpoint>, DiscoveryError> {
        match self.search(search_filter, Some(1), max_wait).await {
            Ok(mut res) => Ok(res.pop()),
            Err(err) => return Err(err),
        }
    }

    async fn search_endpoint(
        &mut self,
        identifier: String,
    ) -> Result<Option<Endpoint>, DiscoveryError> {
        let filter = SearchFilter {
            r#type: Some(search_filter::Type::EndpointIdentifier(identifier)),
        };

        // TODO verify identifier correctness
        self.search1(vec![filter], None).await
    }
}
