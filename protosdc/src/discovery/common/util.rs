use futures::StreamExt;
use std::future::Future;
use std::net::{IpAddr, Ipv4Addr, SocketAddr};

use log::{debug, error, trace};
use prost::Message;
use protosdc_proto::discovery::DiscoveryMessage;
use tokio::sync::mpsc::Sender;
use tokio::sync::oneshot;
use tokio_stream::wrappers::BroadcastStream;
use tokio_util::sync::CancellationToken;

use network::udp_binding::{
    InboundUdpMessage, OutboundUdpMessage, UdpBindingError, UdpHeader, UdpMessage,
};

use crate::discovery::common::error::DiscoveryError;

use tokio::sync::broadcast::Receiver as BroadcastReceiver;

pub(crate) async fn process_udp_messages<F, Fut>(
    receiver: BroadcastReceiver<InboundUdpMessage>,
    mut handler: F,
    cancellation_token: CancellationToken,
) where
    F: FnMut(UdpHeader, DiscoveryMessage) -> Fut,
    Fut: Future<Output = ()>,
{
    let mut udp_stream = BroadcastStream::new(receiver);
    loop {
        tokio::select! {
            _ = cancellation_token.cancelled() => {
                debug!("process_udp_messages cancelled");
                break;
            }
            msg = udp_stream.next() => {
                match msg {
                    Some(Ok(item)) => {
                        // parse udp message
                        match DiscoveryMessage::decode(&*item.data) {
                            Err(err) => {
                                trace!("Could not parse inbound udp message from {:?}. {:?}", item.udp_header, err)
                            }
                            Ok(message) => {
                                handler(item.udp_header, message).await;
                            }
                        }
                    }
                    _ => {
                        debug!("process_udp_messages received empty event");
                        break;
                    }
                }
            }
        }
    }
}

pub(crate) async fn send_multicast<T>(
    udp_sender: Sender<UdpMessage>,
    message: T,
) -> Result<(), DiscoveryError>
where
    T: prost::Message,
{
    let (response_sender, response_receiver) = oneshot::channel();

    udp_sender
        .send(UdpMessage::OutboundMulticast {
            message: OutboundUdpMessage {
                data: message.encode_to_vec(),
                // address is ignored for multicast, set localhost
                address: SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 8080),
            },
            response: Some(response_sender),
        })
        .await
        .map_err(|err| error!("Unexpected error sending multicast message: {}", err))
        .ok();

    response_receiver
        .await
        .map_err(|err| DiscoveryError::UDPError(UdpBindingError::OneShotError(err)))?
        .map_err(DiscoveryError::UDPError)
}
