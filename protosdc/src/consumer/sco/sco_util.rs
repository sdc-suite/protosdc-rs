use protosdc_biceps::biceps::invocation_state_mod::EnumType;
use protosdc_biceps::biceps::{
    operation_invoked_report_mod, AbstractSetResponseOneOf, InvocationInfo, InvocationState,
    TransactionId,
};

pub(crate) fn invocation_info(response: &AbstractSetResponseOneOf) -> &InvocationInfo {
    match response {
        AbstractSetResponseOneOf::AbstractSetResponse(it) => &it.invocation_info,
        AbstractSetResponseOneOf::ActivateResponse(it) => &it.abstract_set_response.invocation_info,
        AbstractSetResponseOneOf::SetStringResponse(it) => {
            &it.abstract_set_response.invocation_info
        }
        AbstractSetResponseOneOf::SetValueResponse(it) => &it.abstract_set_response.invocation_info,
        AbstractSetResponseOneOf::SetComponentStateResponse(it) => {
            &it.abstract_set_response.invocation_info
        }
        AbstractSetResponseOneOf::SetMetricStateResponse(it) => {
            &it.abstract_set_response.invocation_info
        }
        AbstractSetResponseOneOf::SetContextStateResponse(it) => {
            &it.abstract_set_response.invocation_info
        }
        AbstractSetResponseOneOf::SetAlertStateResponse(it) => {
            &it.abstract_set_response.invocation_info
        }
    }
}

pub(crate) fn transaction_id(response: &AbstractSetResponseOneOf) -> &TransactionId {
    &invocation_info(response).transaction_id
}

pub(crate) fn invocation_info_op(
    report: &operation_invoked_report_mod::ReportPart,
) -> &InvocationInfo {
    &report.invocation_info
}

pub(crate) fn invocation_state_op(
    report: &operation_invoked_report_mod::ReportPart,
) -> &InvocationState {
    &invocation_info_op(report).invocation_state
}

/// Returns true if the invocation state of the report part is a final state, false otherwise.
pub fn is_final(report: &operation_invoked_report_mod::ReportPart) -> bool {
    match invocation_state_op(report).enum_type {
        EnumType::Wait | EnumType::Start => false,
        EnumType::Cnclld
        | EnumType::CnclldMan
        | EnumType::Fin
        | EnumType::FinMod
        | EnumType::Fail => true,
    }
}
