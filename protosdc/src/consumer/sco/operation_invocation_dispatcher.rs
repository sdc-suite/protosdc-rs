use std::collections::HashMap;
use std::sync::Arc;
use std::time::{Duration, Instant};

use arrayvec::ArrayVec;
use futures::stream::{self, StreamExt, TryStreamExt};
use log::{debug, error};
use thiserror::Error;
use tokio::sync::Mutex;

use crate::consumer::sco::sco_transaction::{
    ScoTransaction, ScoTransactionImpl, TransactionError, MAX_OPERATION_REPORTS,
};
use crate::consumer::sco::sco_util::is_final;
use biceps::common::mdib_version::MdibVersion;
use protosdc_biceps::biceps::{operation_invoked_report_mod, OperationInvokedReport};

pub type TransactionCounter = u32;
pub type PendingReportsMap =
    HashMap<TransactionCounter, ArrayVec<OperationReportPart, MAX_OPERATION_REPORTS>>;

#[derive(Clone, Debug)]
pub struct OperationReportPart {
    pub report_part: operation_invoked_report_mod::ReportPart,
    pub mdib_version: MdibVersion,
}

#[derive(Debug, Error)]
pub enum OperationError {
    #[error(transparent)]
    TransactionError(TransactionError),
    #[error("Transaction {transaction_id} received too many reports")]
    TooManyReports { transaction_id: u32 },
    #[error("Duplicate transaction id {transaction_id}")]
    DuplicateTransactionId { transaction_id: TransactionCounter },
    #[error("Unknown transaction id {transaction_id}")]
    UnknownTransactionId { transaction_id: TransactionCounter },
}

#[derive(Debug, Default)]
struct OperationInvocationDispatcherInner {
    pending_reports: PendingReportsMap,
    running_transactions: HashMap<TransactionCounter, Arc<ScoTransactionImpl>>,
    awaiting_transactions: HashMap<TransactionCounter, Instant>,
}

fn get_pending_reports(
    pending_reports: &mut PendingReportsMap,
    transaction_id: TransactionCounter,
) -> &mut ArrayVec<OperationReportPart, MAX_OPERATION_REPORTS> {
    pending_reports.entry(transaction_id).or_default();

    pending_reports
        .get_mut(&transaction_id)
        .expect("Just inserted, cannot be missing")
}

pub struct OperationInvocationDispatcher {
    transaction_timeout: Duration,
    inner: Mutex<OperationInvocationDispatcherInner>,
}

impl OperationInvocationDispatcher {
    pub fn new(transaction_timeout: Duration) -> Self {
        Self {
            transaction_timeout,
            inner: Mutex::new(OperationInvocationDispatcherInner::default()),
        }
    }

    pub async fn dispatch_report(
        &self,
        report: OperationInvokedReport,
    ) -> Result<(), OperationError> {
        self.cleanup_transactions().await;

        let report_mdib_version = MdibVersion::from(report.abstract_report.mdib_version_group_attr);

        let result: Result<Vec<()>, OperationError> = stream::iter(report.report_part)
            .then(|part: operation_invoked_report_mod::ReportPart| async {
                let part = OperationReportPart {
                    report_part: part,
                    mdib_version: report_mdib_version.clone(),
                };
                self.dispatch_report_part(part).await
            })
            .try_collect()
            .await;
        result?;
        Ok(())
    }

    async fn dispatch_report_part(&self, part: OperationReportPart) -> Result<(), OperationError> {
        let transaction_id = part.report_part.invocation_info.transaction_id.unsigned_int;

        debug!(
            "Received invocation state {:?} for transaction_id {}",
            part.report_part.invocation_info.invocation_state, transaction_id
        );

        let inner_data = &mut *self.inner.lock().await;
        inner_data
            .awaiting_transactions
            .insert(transaction_id, Instant::now());

        match is_final(&part.report_part) {
            true => {
                let transaction = &mut inner_data.running_transactions.remove(&transaction_id);

                let pending = get_pending_reports(&mut inner_data.pending_reports, transaction_id);
                pending
                    .try_push(part)
                    .map_err(|_err| OperationError::TooManyReports { transaction_id })?;
                match transaction {
                    None => Ok(()),
                    Some(t) => Self::add_reports_to_transaction(pending, t).await,
                }
            }
            false => {
                let transaction = &mut inner_data.running_transactions.get(&transaction_id);

                let pending = get_pending_reports(&mut inner_data.pending_reports, transaction_id);
                pending
                    .try_push(part)
                    .map_err(|_err| OperationError::TooManyReports { transaction_id })?;
                match transaction {
                    None => Ok(()),
                    Some(t) => Self::add_reports_to_transaction(pending, &t.clone()).await,
                }
            }
        }
    }

    pub async fn register_transaction(
        &self,
        transaction: Arc<ScoTransactionImpl>,
    ) -> Result<(), OperationError> {
        let inner_data = &mut *self.inner.lock().await;

        let awaiting_transactions = &mut inner_data.awaiting_transactions;
        let pending_reports = &mut inner_data.pending_reports;
        let running_transactions = &mut inner_data.running_transactions;

        let transaction_id = transaction.transaction_id();
        if running_transactions.get(&transaction_id).is_some() {
            error!("Received duplicate transaction id {}", &transaction_id);
            return Err(OperationError::DuplicateTransactionId { transaction_id });
        }

        debug!("Registering transaction {}", transaction_id);

        awaiting_transactions.remove(&transaction_id);
        running_transactions.insert(transaction_id, transaction);

        // TODO: This can probably be optimized somehow
        let transaction_ref = running_transactions
            .get(&transaction_id)
            .expect("Transaction was just inserted within a lock, this get can never fail");
        let pending = get_pending_reports(pending_reports, transaction_id);

        Self::add_reports_to_transaction(pending, transaction_ref).await
    }

    async fn add_reports_to_transaction(
        pending_report_parts: &mut ArrayVec<OperationReportPart, MAX_OPERATION_REPORTS>,
        transaction: &Arc<ScoTransactionImpl>,
    ) -> Result<(), OperationError> {
        let result: Result<Vec<()>, OperationError> = stream::iter(pending_report_parts.drain(..))
            .then(|part| async { transaction.add_report(part).await })
            .try_collect()
            .await
            .map_err(OperationError::TransactionError);
        result?;
        Ok(())
    }

    async fn cleanup_transactions(&self) {
        let mut inner_data = self.inner.lock().await;

        let elapsed: Vec<TransactionCounter> = inner_data
            .awaiting_transactions
            .iter()
            .filter(|(_transaction_id, start)| start.elapsed() >= self.transaction_timeout)
            .map(|it| *it.0)
            .collect();

        for transaction_id in elapsed {
            inner_data.awaiting_transactions.remove(&transaction_id);
            inner_data.pending_reports.remove(&transaction_id);
        }
    }
}
