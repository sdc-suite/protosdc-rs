use async_trait::async_trait;
use futures::{Stream, StreamExt};
use log::{debug, trace};
use std::pin::Pin;
use thiserror::Error;
use tokio::sync::broadcast;
use tokio::sync::Mutex;

use crate::consumer::sco::operation_invocation_dispatcher::OperationReportPart;
use crate::consumer::sco::sco_util;
use crate::consumer::sco::sco_util::is_final;
use common::broadcast_event_log_error;
use protosdc_biceps::biceps::AbstractSetResponseOneOf;
use tokio_stream::wrappers::BroadcastStream;
use tokio_util::sync::CancellationToken;

pub const MAX_OPERATION_REPORTS: usize = 3;

#[async_trait]
pub trait ScoTransaction {
    fn transaction_id(&self) -> u32;

    async fn reports(&self) -> Vec<OperationReportPart>;

    fn response(&self) -> &AbstractSetResponseOneOf;

    async fn wait_for_final_report(&self) -> Vec<OperationReportPart>;

    type StreamType: Stream<Item = OperationReportPart> + Send + 'static;

    /// Returns a stream of all report parts, including past and future updates, until the transaction has finished.
    async fn subscribe(&self) -> Self::StreamType;
}

#[derive(Debug, Error)]
pub enum TransactionError {
    #[error("Transaction {transaction_id} received too many reports")]
    TooManyReports { transaction_id: u32 },
    #[error("Could not distribute report")]
    ChannelDistributionError,
}

#[derive(Debug)]
struct ScoTransactionImplInner {
    reports: Vec<OperationReportPart>,
    report_receiver_channel: Option<broadcast::Sender<OperationReportPart>>,
}

#[derive(Debug)]
pub struct ScoTransactionImpl {
    response: AbstractSetResponseOneOf,
    report_wait: CancellationToken,
    inner: Mutex<ScoTransactionImplInner>,
}

impl ScoTransactionImpl {
    pub(crate) fn new(response: AbstractSetResponseOneOf) -> Self {
        let (tx, _rx) = broadcast::channel(MAX_OPERATION_REPORTS);

        Self {
            response,
            report_wait: CancellationToken::new(),
            inner: Mutex::new(ScoTransactionImplInner {
                reports: Default::default(),
                report_receiver_channel: Some(tx),
            }),
        }
    }

    pub(crate) async fn add_report(
        &self,
        report: OperationReportPart,
    ) -> Result<(), TransactionError> {
        let mut inner = self.inner.lock().await;

        match inner.report_receiver_channel.take() {
            None => Err(TransactionError::TooManyReports {
                transaction_id: sco_util::invocation_info_op(&report.report_part)
                    .transaction_id
                    .unsigned_int,
            }),
            Some(sender) => {
                let is_final_report = is_final(&report.report_part);
                inner.reports.push(report.clone());
                broadcast_event_log_error(&sender, report.clone());
                // notify if final
                if is_final_report {
                    self.report_wait.cancel();
                } else {
                    // add the sender back
                    inner.report_receiver_channel = Some(sender)
                }
                Ok(())
            }
        }
    }
}

#[async_trait]
impl ScoTransaction for ScoTransactionImpl {
    fn transaction_id(&self) -> u32 {
        sco_util::transaction_id(&self.response).unsigned_int
    }

    async fn reports(&self) -> Vec<OperationReportPart> {
        return self.inner.lock().await.reports.clone();
    }

    fn response(&self) -> &AbstractSetResponseOneOf {
        &self.response
    }

    async fn wait_for_final_report(&self) -> Vec<OperationReportPart> {
        trace!("wait_for_final_report called");
        {
            let rep = &self.inner.lock().await.reports;
            if rep.iter().map(|it| &it.report_part).any(is_final) {
                return rep.clone();
            }
        }

        self.report_wait.cancelled().await;
        debug!("wait_for_final_report received signal");
        return self.inner.lock().await.reports.clone();
    }

    type StreamType = Pin<Box<dyn Stream<Item = OperationReportPart> + Send + Sync + 'static>>;

    async fn subscribe(&self) -> Self::StreamType {
        let inner = self.inner.lock().await;
        let reports = inner.reports.clone();

        match &inner.report_receiver_channel {
            // transaction as already finished, return all reports
            None => Box::pin(futures::stream::iter(reports)) as Self::StreamType,
            // transaction is still running, chain present reports with all inbound reports
            Some(receiver) => {
                Box::pin(
                    futures::stream::iter(reports)
                        // chain future updates into our stream, after all already received updates
                        .chain(
                            BroadcastStream::new(receiver.subscribe())
                                .filter_map(|it| async move { it.ok() }),
                        ),
                ) as Self::StreamType
            }
        }
    }
}

#[cfg(test)]
mod test {
    use crate::consumer::sco::operation_invocation_dispatcher::OperationReportPart;
    use crate::consumer::sco::sco_transaction::{
        ScoTransaction, ScoTransactionImpl, TransactionError,
    };
    use biceps::common::mdib_version::MdibVersionBuilder;
    use futures::StreamExt;
    use protosdc_biceps::biceps::{
        invocation_state_mod, operation_invoked_report_mod, AbstractReportPart,
        AbstractSetResponse, AbstractSetResponseOneOf, ActivateResponse, HandleRef,
        InstanceIdentifier, InstanceIdentifierOneOf, InvocationInfo, InvocationState,
        TransactionId,
    };
    use std::time::Duration;
    use tokio::time::timeout;

    fn create_activate_response(
        invocation_state: invocation_state_mod::EnumType,
        transaction_id: u32,
    ) -> ActivateResponse {
        ActivateResponse {
            abstract_set_response: AbstractSetResponse {
                extension_element: None,
                invocation_info: InvocationInfo {
                    extension_element: None,
                    transaction_id: TransactionId {
                        unsigned_int: transaction_id,
                    },
                    invocation_state: InvocationState {
                        enum_type: invocation_state,
                    },
                    invocation_error: None,
                    invocation_error_message: vec![],
                },
                mdib_version_group_attr: MdibVersionBuilder::default().seq("xoxo").create().into(),
            },
        }
    }

    fn create_report_part(
        invocation_state: invocation_state_mod::EnumType,
        transaction_id: u32,
    ) -> OperationReportPart {
        let part = operation_invoked_report_mod::ReportPart {
            abstract_report_part: AbstractReportPart {
                extension_element: None,
                source_mds: None,
            },
            invocation_info: InvocationInfo {
                extension_element: None,
                transaction_id: TransactionId {
                    unsigned_int: transaction_id,
                },
                invocation_state: InvocationState {
                    enum_type: invocation_state,
                },
                invocation_error: None,
                invocation_error_message: vec![],
            },
            invocation_source: InstanceIdentifierOneOf::InstanceIdentifier(InstanceIdentifier {
                extension_element: None,
                r#type: None,
                identifier_name: vec![],
                root_attr: None,
                extension_attr: None,
            }),
            operation_handle_ref_attr: HandleRef {
                string: "who cares".to_string(),
            },
            operation_target_attr: None,
        };
        OperationReportPart {
            report_part: part,
            mdib_version: MdibVersionBuilder::default().seq("whatever").create(),
        }
    }

    #[tokio::test]
    async fn test_subscribe_stream_ends() -> anyhow::Result<()> {
        let transaction_id = 1;

        let transaction = ScoTransactionImpl::new(AbstractSetResponseOneOf::ActivateResponse(
            create_activate_response(invocation_state_mod::EnumType::Wait, transaction_id),
        ));

        let collect_future = transaction.subscribe().await.collect::<Vec<_>>();

        transaction
            .add_report(create_report_part(
                invocation_state_mod::EnumType::Wait,
                transaction_id,
            ))
            .await?;
        transaction
            .add_report(create_report_part(
                invocation_state_mod::EnumType::Start,
                transaction_id,
            ))
            .await?;
        transaction
            .add_report(create_report_part(
                invocation_state_mod::EnumType::Fin,
                transaction_id,
            ))
            .await?;

        let collected_parts = timeout(Duration::from_secs(1), collect_future).await?;

        assert_eq!(3, collected_parts.len(), "received {collected_parts:?}");

        // additional subscription completes immediately
        {
            let collect_future = transaction.subscribe().await.collect::<Vec<_>>();
            let collected_parts = timeout(Duration::from_secs(1), collect_future).await?;
            assert_eq!(3, collected_parts.len(), "received {collected_parts:?}");
        }
        let should_error = transaction
            .add_report(create_report_part(
                invocation_state_mod::EnumType::Fin,
                transaction_id,
            ))
            .await;
        assert!(matches!(
            should_error,
            Err(TransactionError::TooManyReports { .. })
        ));

        Ok(())
    }
}
