use std::collections::HashSet;

use async_trait::async_trait;
use futures::FutureExt;
use itertools::Itertools;
use log::{error, info};
use protosdc_biceps::biceps::{
    context_association_mod, AbstractContextStateOneOf, InstanceIdentifierOneOf,
};
use protosdc_proto::common::Uri;
use tokio::task::JoinHandle;

use crate::discovery::provider::discovery_provider::DiscoveryProvider;
use biceps::common::access::mdib_access::{MdibAccess, MdibAccessEvent};
use biceps::common::context::location_detail_query_mapper::{
    create_with_location_detail_query, LocationDetail as ContextLocationDetail,
};
use biceps::provider::access::local_mdib_access::{
    LocalMdibAccess, LocalMdibAccessReadTransaction,
};

pub trait DeviceMdibType:
    LocalMdibAccess + Sync + Send + Clone + LocalMdibAccessReadTransaction + 'static
{
}

impl<T> DeviceMdibType for T where
    T: LocalMdibAccess + Sync + Send + Clone + LocalMdibAccessReadTransaction + 'static
{
}

pub trait DeviceDiscoveryProvider: DiscoveryProvider + Send + Sync + 'static {}

impl<T> DeviceDiscoveryProvider for T where T: DiscoveryProvider + Send + Sync + 'static {}

/// Chains a variable amount of plugins into one [ProviderPluginChain].
#[macro_export]
macro_rules! proto_chain_plugins {
    // The pattern for a single `eval`
    ($first:expr, $second:expr, $($more:expr ),*) => {
        {
            $crate::provider::plugins::ProtoProviderPluginChain::create($first, proto_chain_plugins!($second, $($more ),*))
        }
    };

    ($first:expr, $second:expr) => {
        {
            $crate::provider::plugins::ProtoProviderPluginChain::create($first, $second)
        }
    };
}

pub struct ProtoSdcDeviceContext<V, T>
where
    V: DeviceMdibType,
    T: DeviceDiscoveryProvider + Sync + Send,
{
    pub mdib_access: V,
    pub discovery_access: T,
}

pub struct ProtoProviderPluginChain<C, N>
where
    C: ProtoSdcProviderPlugin,
    N: ProtoSdcProviderPlugin,
{
    current: C,
    next: N,
}

impl<C, N> ProtoProviderPluginChain<C, N>
where
    C: ProtoSdcProviderPlugin + Send + Sync,
    N: ProtoSdcProviderPlugin + Send + Sync,
{
    pub fn create(current: C, next: N) -> Self {
        Self { current, next }
    }
}

#[async_trait]
impl<C, N> ProtoSdcProviderPlugin for ProtoProviderPluginChain<C, N>
where
    C: ProtoSdcProviderPlugin + Send + Sync,
    N: ProtoSdcProviderPlugin + Send + Sync,
{
    async fn before_startup<M, D>(
        &mut self,
        context: &ProtoSdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryProvider + Clone,
    {
        self.current.before_startup(context).await?;
        self.next.before_startup(context).await
    }

    async fn after_startup<M, D>(
        &mut self,
        context: &ProtoSdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryProvider + Clone,
    {
        self.current.after_startup(context).await?;
        self.next.after_startup(context).await
    }

    async fn before_shutdown<M, D>(
        &mut self,
        context: &ProtoSdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryProvider + Clone,
    {
        self.current.before_shutdown(context).await?;
        self.next.before_shutdown(context).await
    }

    async fn after_shutdown<M, D>(
        &mut self,
        context: &ProtoSdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryProvider + Clone,
    {
        self.current.after_shutdown(context).await?;
        self.next.after_shutdown(context).await
    }
}

#[async_trait]
pub trait ProtoSdcProviderPlugin {
    async fn before_startup<M, D>(
        &mut self,
        _context: &ProtoSdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryProvider + Clone,
    {
        Ok(())
    }

    async fn after_startup<M, D>(
        &mut self,
        _context: &ProtoSdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryProvider + Clone,
    {
        Ok(())
    }

    async fn before_shutdown<M, D>(
        &mut self,
        _context: &ProtoSdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryProvider + Clone,
    {
        Ok(())
    }

    async fn after_shutdown<M, D>(
        &mut self,
        _context: &ProtoSdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryProvider + Clone,
    {
        Ok(())
    }
}

#[derive(Default)]
pub struct ScopesPlugin {
    pub processing_task: Option<JoinHandle<()>>,
    pub constant_types: Vec<String>,
}

impl ScopesPlugin {
    pub async fn update_from_mdib<M, P>(mdib: &M, processor: &mut P, additional_scopes: &[String])
    where
        M: LocalMdibAccess + LocalMdibAccessReadTransaction + 'static,
        P: DeviceDiscoveryProvider,
    {
        let location_scopes = mdib
            .read_transaction(|access| {
                async move {
                    access
                        .context_states()
                        .await
                        .into_iter()
                        .filter_map(|it| match it {
                            AbstractContextStateOneOf::LocationContextState(it) => Some(it),
                            _ => None,
                        })
                        .filter(|it| {
                            it.abstract_context_state.context_association_attr
                                == Some(protosdc_biceps::biceps::ContextAssociation {
                                    enum_type: context_association_mod::EnumType::Assoc,
                                })
                        })
                        .flat_map(|it| {
                            let location_detail: ContextLocationDetail = match it.location_detail {
                                None => ContextLocationDetail::default(),
                                Some(it) => it.into(),
                            };

                            it.abstract_context_state.identification.into_iter().map(
                                move |ident_oo| match ident_oo {
                                    InstanceIdentifierOneOf::InstanceIdentifier(ident) => {
                                        create_with_location_detail_query(&ident, &location_detail)
                                    }
                                    InstanceIdentifierOneOf::OperatingJurisdiction(juris) => {
                                        create_with_location_detail_query(
                                            &juris.instance_identifier,
                                            &location_detail,
                                        )
                                    }
                                },
                            )
                        })
                        .filter_map(|it| match it {
                            Ok(it) => Some(it),
                            Err(err) => {
                                error!("Error while processing update: {:?}", err);
                                None
                            }
                        })
                        .collect_vec()
                }
                .boxed()
            })
            .await;

        let mut scopes = HashSet::new();
        scopes.extend(location_scopes);
        scopes.extend(additional_scopes.iter().cloned().collect_vec());

        info!("Updating scopes to {:?}", scopes);
        let transformed_scopes = scopes
            .into_iter()
            .map(|it| Uri {
                value: it.to_string(),
            })
            .collect();

        processor.update_scopes(transformed_scopes).await;
    }
}

#[async_trait]
impl ProtoSdcProviderPlugin for ScopesPlugin {
    async fn before_startup<M, D>(
        &mut self,
        context: &ProtoSdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryProvider + Clone,
    {
        info!("Startup of automatic required scopes updating for device");

        let mut change_subscription = context.mdib_access.subscribe().await;
        let task_mdib = context.mdib_access.clone();
        let mut task_processor = context.discovery_access.clone();

        let constant_types = self.constant_types.clone();

        self.processing_task = Some(tokio::spawn(async move {
            info!("receiver_task started");
            loop {
                let message = change_subscription.recv().await;

                match message {
                    Some(msg) => match msg {
                        MdibAccessEvent::ContextStateModification { .. }
                        | MdibAccessEvent::DescriptionModification { .. } => {
                            ScopesPlugin::update_from_mdib(
                                &task_mdib,
                                &mut task_processor,
                                &constant_types,
                            )
                            .await
                        }
                        _ => {
                            // pass
                        }
                    },
                    None => {
                        error!("Channel was closed");
                        break;
                    }
                }
            }
            info!("receiver_task finished");
        }));

        Ok(())
    }

    async fn after_shutdown<M, D>(
        &mut self,
        _context: &ProtoSdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDiscoveryProvider,
    {
        info!("Automatic required types and scopes updating stopped");
        if let Some(it) = self.processing_task.take() {
            it.abort()
        }
        Ok(())
    }
}
