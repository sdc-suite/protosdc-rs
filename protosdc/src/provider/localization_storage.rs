use async_trait::async_trait;
use common::send_log_error;
use futures::{stream, StreamExt};
use protosdc_biceps::biceps::{
    localized_text_width_mod, LocalizedText, LocalizedTextContent, LocalizedTextRef,
    LocalizedTextWidth, ReferencedVersion, VersionCounter,
};
use protosdc_biceps::types::ProtoLanguage;
use protosdc_proto::biceps::LocalizedTextMsg;
use std::collections::{HashMap, HashSet};
use std::future;
use std::sync::Arc;
use thiserror::Error;
use tokio::sync::mpsc;

#[async_trait]
pub trait LocalizationStorage<T: Into<LocalizedText>> {
    /// Retrieves a set of languages available in storage.
    async fn get_languages(&self) -> HashSet<String>;

    /// Retrieves all localized texts for the provided language.
    ///
    /// # Arguments
    ///
    /// * `language` - A string slice that holds the language code.
    async fn get_texts_for_language(
        &self,
        language: &str,
    ) -> Result<Vec<T>, LocalizationStorageError>;

    /// Retrieves all localized texts for the provided text reference.
    ///
    /// # Arguments
    ///
    /// * `text_ref` - A string slice that holds a text reference.
    async fn get_texts_by_ref(&self, text_ref: &str) -> Result<Vec<T>, LocalizationStorageError>;

    /// Retrieves all localized texts for the provided text reference and language.
    ///
    /// # Arguments
    ///
    /// * `text_ref` - A string slice that holds a text reference.
    /// * `lang` -  A string slice that holds the language code.
    async fn get_texts_by_ref_and_lang(
        &self,
        text_ref: &str,
        lang: &str,
    ) -> Result<Vec<T>, LocalizationStorageError>;

    /// Retrieves all localized texts of the specific width.
    ///
    /// # Arguments
    ///
    /// * `width` - A TextWidth enum member representing the width attribute of the text.
    async fn get_texts_by_width(
        &self,
        width: TextWidth,
    ) -> Result<Vec<T>, LocalizationStorageError>;

    /// Execute a query against the localization storage.
    ///
    /// This function queries the localization storage with provided parameters and returns
    /// an asynchronous stream (mpsc::Receiver<T>) of results or an error related to
    /// localization storage (LocalizationStorageError). The Receiver is bounded to only ever contain
    /// a single chunk of response.
    ///
    /// # Arguments
    ///
    /// * `text_ref`: A reference to an array of `String`s designating text references to query.
    ///   None implies querying all text references.
    ///
    /// * `version`: The version of the text to query.
    ///   The version is specified as an integer using `u64`.
    ///   None implies no version filtering.
    ///
    /// * `lang`: A reference to an array of `String`s designating the language(s) to be queried.
    ///   None implies querying all available languages.
    ///
    /// * `width`: The width of the text to query.
    ///   Width is specified using the `TextWidth` enum.
    ///   None implies no width filtering.
    ///
    /// * `number_of_lines`: The number of lines of text to query.
    ///   The number of lines is specified as an integer using `u64`.
    ///   None implies no line filtering.
    ///
    /// # Returns
    ///
    /// * `Result<mpsc::Receiver<T>, LocalizationStorageError>`:
    ///   An asynchronous stream of queried contents if successful, otherwise an error related to
    ///   localization storage.
    ///
    /// # Errors
    ///
    /// This function will return an error if the query could not be performed for any reason,
    /// details of which are included in the `LocalizationStorageError`.
    async fn query(
        &self,
        text_ref: Option<Vec<String>>,
        version: Option<u64>,
        lang: Option<Vec<String>>,
        width: Option<TextWidth>,
        number_of_lines: Option<i64>,
    ) -> Result<mpsc::Receiver<Vec<T>>, LocalizationStorageError>;
}

#[derive(Clone, Debug, PartialOrd, PartialEq, Ord, Eq)]
pub enum TextWidth {
    XS,
    S,
    M,
    L,
    XL,
    XXL,
}

impl From<LocalizedTextWidth> for TextWidth {
    fn from(value: LocalizedTextWidth) -> Self {
        (&value).into()
    }
}

impl From<&LocalizedTextWidth> for TextWidth {
    fn from(value: &LocalizedTextWidth) -> Self {
        match value.enum_type {
            localized_text_width_mod::EnumType::xs => Self::XS,
            localized_text_width_mod::EnumType::s => Self::S,
            localized_text_width_mod::EnumType::m => Self::M,
            localized_text_width_mod::EnumType::l => Self::L,
            localized_text_width_mod::EnumType::xl => Self::XL,
            localized_text_width_mod::EnumType::xxl => Self::XXL,
        }
    }
}

impl From<TextWidth> for LocalizedTextWidth {
    fn from(value: TextWidth) -> Self {
        match value {
            TextWidth::XS => LocalizedTextWidth {
                enum_type: localized_text_width_mod::EnumType::xs,
            },
            TextWidth::S => LocalizedTextWidth {
                enum_type: localized_text_width_mod::EnumType::s,
            },
            TextWidth::M => LocalizedTextWidth {
                enum_type: localized_text_width_mod::EnumType::m,
            },
            TextWidth::L => LocalizedTextWidth {
                enum_type: localized_text_width_mod::EnumType::l,
            },
            TextWidth::XL => LocalizedTextWidth {
                enum_type: localized_text_width_mod::EnumType::xl,
            },
            TextWidth::XXL => LocalizedTextWidth {
                enum_type: localized_text_width_mod::EnumType::xxl,
            },
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct LocalizedStorageText {
    text_ref: String,
    lang: String,
    text: String,
    width: TextWidth,
    lines: u8,
    version: u8,
}

#[derive(Clone, Debug)]
pub struct InputText {
    pub text_ref: String,
    pub lang: String,
    pub text: String,
    pub width: TextWidth,
    pub lines: u8,
}

impl From<InputText> for LocalizedStorageText {
    fn from(value: InputText) -> Self {
        Self {
            text_ref: value.text_ref,
            lang: value.lang,
            text: value.text,
            width: value.width,
            lines: value.lines,
            version: 0,
        }
    }
}

impl From<LocalizedStorageText> for LocalizedText {
    fn from(value: LocalizedStorageText) -> Self {
        Self {
            localized_text_content: LocalizedTextContent { string: value.text },
            ref_attr: Some(LocalizedTextRef {
                string: value.text_ref,
            }),
            lang_attr: Some(ProtoLanguage {
                language: value.lang,
            }),
            version_attr: Some(ReferencedVersion {
                version_counter: VersionCounter {
                    unsigned_long: value.version as u64,
                },
            }),
            text_width_attr: Some(value.width.into()),
        }
    }
}

impl From<LocalizedStorageText> for LocalizedTextMsg {
    fn from(value: LocalizedStorageText) -> Self {
        let intermediate: LocalizedText = value.into();
        intermediate.into()
    }
}

// map which is ordered by text length
pub type LanguageSpecificMap = HashMap<String, Vec<LocalizedStorageText>>;

#[derive(Debug, Error)]
pub enum LocalizationStorageError {
    #[error("The language {0} is unsupported")]
    UnknownLanguage(String),

    #[error("The query was not supported: {0}")]
    UnsupportedQuery(String),
}

#[derive(Debug)]
pub struct LocalizationStorageImpl {
    storage: Arc<HashMap<String, LanguageSpecificMap>>,
}

impl LocalizationStorageImpl {
    pub fn new<T: Into<LocalizedStorageText>>(texts: Vec<T>) -> Self {
        let mut storage: HashMap<String, LanguageSpecificMap> = HashMap::new();

        texts.into_iter().for_each(|input_text| {
            let text = input_text.into();
            storage
                .entry(text.lang.clone())
                .or_default()
                .entry(text.text_ref.clone())
                .or_default()
                .push(text)
        });

        Self {
            storage: Arc::new(storage),
        }
    }

    async fn get_texts_by_ref_internal(
        storage: &HashMap<String, LanguageSpecificMap>,
        text_ref: &str,
    ) -> Result<Vec<LocalizedStorageText>, LocalizationStorageError> {
        Ok(storage
            .values()
            .filter_map(|it| it.get(text_ref))
            .flat_map(|it| it.iter())
            .cloned()
            .collect())
    }

    async fn get_texts_by_ref_and_lang_internal(
        storage: &HashMap<String, LanguageSpecificMap>,
        text_ref: &str,
        lang: &str,
    ) -> Result<Vec<LocalizedStorageText>, LocalizationStorageError> {
        Ok(
            storage
                .get(lang)
                .ok_or(LocalizationStorageError::UnknownLanguage(lang.to_string()))?
                .get(text_ref)
                .cloned()
                .unwrap_or_else(|| Vec::with_capacity(0)), // don't allocate
        )
    }
}

#[async_trait]
impl LocalizationStorage<LocalizedStorageText> for LocalizationStorageImpl {
    async fn get_languages(&self) -> HashSet<String> {
        self.storage.keys().cloned().collect()
    }

    async fn get_texts_for_language(
        &self,
        language: &str,
    ) -> Result<Vec<LocalizedStorageText>, LocalizationStorageError> {
        Ok(self
            .storage
            .get(language)
            .ok_or(LocalizationStorageError::UnknownLanguage(
                language.to_string(),
            ))?
            .values()
            .flat_map(|it| it.iter())
            .cloned()
            .collect())
    }

    async fn get_texts_by_ref(
        &self,
        text_ref: &str,
    ) -> Result<Vec<LocalizedStorageText>, LocalizationStorageError> {
        Self::get_texts_by_ref_internal(&self.storage, text_ref).await
    }

    async fn get_texts_by_ref_and_lang(
        &self,
        text_ref: &str,
        lang: &str,
    ) -> Result<Vec<LocalizedStorageText>, LocalizationStorageError> {
        Self::get_texts_by_ref_and_lang_internal(&self.storage, text_ref, lang).await
    }

    async fn get_texts_by_width(
        &self,
        width: TextWidth,
    ) -> Result<Vec<LocalizedStorageText>, LocalizationStorageError> {
        Ok(self
            .storage
            .values()
            .flat_map(|it| it.values().flat_map(|it2| it2.iter()))
            .filter(|it| it.width <= width)
            .cloned()
            .collect())
    }

    async fn query(
        &self,
        text_ref: Option<Vec<String>>,
        version: Option<u64>,
        lang: Option<Vec<String>>,
        width: Option<TextWidth>,
        _number_of_lines: Option<i64>,
    ) -> Result<mpsc::Receiver<Vec<LocalizedStorageText>>, LocalizationStorageError> {
        // TODO: currently ignores number of lines argument

        let (tx, rx) = mpsc::channel(1);

        let iter_sender = tx.clone();
        let task_storage = self.storage.clone();

        match (text_ref, version, lang, width) {
            (None, None, None, None) => {
                // all texts
                tokio::spawn(async move {
                    let iter_sender = iter_sender;
                    stream::iter(task_storage.iter())
                        .for_each(move |(_key, value)| {
                            let iter_sender = iter_sender.clone();
                            async move {
                                send_log_error(
                                    &iter_sender,
                                    value.values().flat_map(|it| it.iter()).cloned().collect::<Vec<_>>()
                                ).await;
                            }
                        }).await;
                });
            },
            (Some(text_refs), None, None, None) => {
                tokio::spawn(async move {
                    let iter_sender = iter_sender;
                    stream::iter(text_refs.iter())
                        .for_each(|text_ref| {
                            let iter_sender = iter_sender.clone();
                            let task_storage = &task_storage;
                            async move {
                                send_log_error(
                                    &iter_sender,
                                    Self::get_texts_by_ref_internal(task_storage, text_ref).await.expect("doesn't fail")
                                ).await;
                            }
                        }).await;
                });

            },
            (None, None, Some(languages), None) => {
                tokio::spawn(async move {
                    let iter_sender = iter_sender;
                    stream::iter(task_storage.iter())
                        .filter(|(key, _value)| future::ready(languages.contains(key)))
                        .for_each(|(_key, value)| async {
                            send_log_error(
                                &iter_sender,
                                value.values().flat_map(|it| it.iter()).cloned().collect::<Vec<_>>()
                            ).await;
                        }).await;
                });
            }
            (Some(text_refs), None, Some(languages), None) => {
                tokio::spawn(async move {
                    let iter_sender = iter_sender;
                    stream::iter(languages.into_iter())
                        .for_each(|language| {
                            let iter_sender = iter_sender.clone();
                            let task_storage = &task_storage;
                            let text_refs = &text_refs;
                            async move {
                                let texts = stream::iter(text_refs.iter())
                                    .filter_map(|text_ref| async { Self::get_texts_by_ref_and_lang_internal(task_storage, text_ref, language.as_str()).await.ok() })
                                    .flat_map(|texts| stream::iter(texts.into_iter()))
                                    .collect::<Vec<_>>()
                                    .await;
                                send_log_error(&iter_sender,texts).await;
                            }
                        }).await;
                });
            }
            _ => {
                return Err(LocalizationStorageError::UnsupportedQuery("Only queries for all texts, texts by language, texts by ref and combinations of both are supported".to_string()))
            }
        }

        Ok(rx)
    }
}

#[cfg(test)]
mod test {
    use crate::provider::localization_storage::{
        InputText, LocalizationStorage, LocalizationStorageImpl, TextWidth,
    };
    use futures::{stream, StreamExt};
    use std::collections::HashSet;
    use tokio_stream::wrappers::ReceiverStream;

    fn create_text_lang_only(lang: &str) -> InputText {
        create_text_lang_ref(lang, &uuid::Uuid::new_v4().to_string())
    }

    fn create_text_lang_ref(lang: &str, text_ref: &str) -> InputText {
        create_text(lang, text_ref, "abc", TextWidth::XS)
    }

    fn create_text(lang: &str, text_ref: &str, text: &str, width: TextWidth) -> InputText {
        InputText {
            text_ref: text_ref.to_string(),
            lang: lang.to_string(),
            text: text.to_string(),
            width,
            lines: 0,
        }
    }

    #[tokio::test]
    async fn test_supported_languages() -> anyhow::Result<()> {
        let languages = ["en", "de", "en", "fr_FR"];

        let my_texts = languages
            .iter()
            .map(|it| create_text_lang_only(it))
            .collect();

        let storage = LocalizationStorageImpl::new(my_texts);

        let languages_string: HashSet<String> = languages.iter().map(|it| it.to_string()).collect();

        assert_eq!(languages_string, storage.get_languages().await);

        Ok(())
    }

    #[tokio::test]
    async fn test_refs_and_lang() -> anyhow::Result<()> {
        let lang_ref = [
            ("en", "ref1"),
            ("de", "ref1"),
            ("en", "ref2"),
            ("fr_FR", "ref1"),
        ];

        let my_texts: Vec<InputText> = lang_ref
            .iter()
            .map(|(lang, text_ref)| create_text_lang_ref(lang, text_ref))
            .collect();

        let storage = LocalizationStorageImpl::new(my_texts.clone());

        assert!(!my_texts.is_empty());

        assert_eq!(3, storage.get_texts_by_ref("ref1").await?.len());
        assert_eq!(2, storage.get_texts_for_language("en").await?.len());

        let ref_only_query = ReceiverStream::new(
            storage
                .query(Some(vec!["ref1".to_string()]), None, None, None, None)
                .await?,
        )
        .flat_map(|it| stream::iter(it.into_iter()))
        .collect::<Vec<_>>()
        .await;
        assert_eq!(3, ref_only_query.len());

        let lang_only_query = ReceiverStream::new(
            storage
                .query(None, None, Some(vec!["en".to_string()]), None, None)
                .await?,
        )
        .flat_map(|it| stream::iter(it.into_iter()))
        .collect::<Vec<_>>()
        .await;
        assert_eq!(2, lang_only_query.len());

        let ref_and_land_query = ReceiverStream::new(
            storage
                .query(
                    Some(vec!["ref1".to_string()]),
                    None,
                    Some(vec!["en".to_string()]),
                    None,
                    None,
                )
                .await?,
        )
        .flat_map(|it| stream::iter(it.into_iter()))
        .collect::<Vec<_>>()
        .await;
        assert_eq!(1, ref_and_land_query.len());

        Ok(())
    }

    #[tokio::test]
    async fn test_large_store() -> anyhow::Result<()> {
        let texts_per_language = 10000;
        let languages = ["en_US", "de_DE", "fr_FR"];

        let my_texts: Vec<InputText> = languages
            .iter()
            .flat_map(|lang| {
                (0..texts_per_language).map(|n| create_text_lang_ref(lang, &format!("ref_{}", n)))
            })
            .collect();

        let storage = LocalizationStorageImpl::new(my_texts.clone());

        let en_texts = storage.get_texts_for_language("en_US").await?;
        assert_eq!(texts_per_language, en_texts.len());

        let texts_by_ref = storage.get_texts_by_ref("ref_5").await?;
        assert_eq!(languages.len(), texts_by_ref.len());

        let all_texts_receiver = storage.query(None, None, None, None, None).await?;
        let all_texts = ReceiverStream::new(all_texts_receiver)
            .collect::<Vec<_>>()
            .await;

        assert_eq!(3, all_texts.len());

        Ok(())
    }
}
