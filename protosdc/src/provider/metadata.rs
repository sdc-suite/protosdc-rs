use protosdc_proto::metadata::EndpointMetadata;
use typed_builder::TypedBuilder;

#[derive(Clone, Debug, Default, TypedBuilder)]
pub struct LocalizedString {
    #[builder(setter(into))]
    pub value: String,
    ///
    /// The language identified by a language code conforming to RFC3066 (<https://www.ietf.org/rfc/rfc3066.txt>).
    ///
    #[builder(setter(into))]
    pub locale: String,
}

impl From<protosdc_proto::common::LocalizedString> for LocalizedString {
    fn from(data: protosdc_proto::common::LocalizedString) -> Self {
        Self {
            value: data.value,
            locale: data.locale,
        }
    }
}

impl From<LocalizedString> for protosdc_proto::common::LocalizedString {
    fn from(value: LocalizedString) -> Self {
        Self {
            value: value.value,
            locale: value.locale,
        }
    }
}

#[derive(Clone, Debug, TypedBuilder)]
pub struct DeviceMetadata {
    #[builder(default)]
    pub friendly_name: Vec<LocalizedString>,
    #[builder(default, setter(strip_option, into))]
    pub firmware_version: Option<String>,
    #[builder(default, setter(strip_option, into))]
    pub hardware_version: Option<String>,
    #[builder(default, setter(strip_option, into))]
    pub software_version: Option<String>,
    #[builder(default, setter(strip_option, into))]
    pub serial_number: Option<String>,
    #[builder(default)]
    pub manufacturer: Vec<LocalizedString>,
    #[builder(default, setter(strip_option, into))]
    pub manufacturer_url: Option<String>,
    #[builder(default)]
    pub model_name: Vec<LocalizedString>,
    #[builder(default, setter(strip_option, into))]
    pub model_url: Option<String>,
}

impl From<EndpointMetadata> for DeviceMetadata {
    fn from(data: EndpointMetadata) -> Self {
        Self {
            friendly_name: data.friendly_name.into_iter().map(|it| it.into()).collect(),
            firmware_version: data.firmware_version,
            hardware_version: data.hardware_version,
            software_version: data.software_version,
            serial_number: data.serial_number,
            manufacturer: data.manufacturer.into_iter().map(|it| it.into()).collect(),
            manufacturer_url: data.manufacturer_url,
            model_name: data.model_name.into_iter().map(|it| it.into()).collect(),
            model_url: data.model_url,
        }
    }
}

impl From<DeviceMetadata> for EndpointMetadata {
    fn from(value: DeviceMetadata) -> Self {
        Self {
            friendly_name: value
                .friendly_name
                .into_iter()
                .map(|it| it.into())
                .collect(),
            firmware_version: value.firmware_version,
            hardware_version: value.hardware_version,
            software_version: value.software_version,
            serial_number: value.serial_number,
            manufacturer: value.manufacturer.into_iter().map(|it| it.into()).collect(),
            manufacturer_url: value.manufacturer_url,
            model_name: value.model_name.into_iter().map(|it| it.into()).collect(),
            model_url: value.model_url,
        }
    }
}

impl Default for DeviceMetadata {
    fn default() -> Self {
        Self::builder()
            .friendly_name(vec![LocalizedString {
                value: "und".to_string(), // undetermined (ISO 639-2)
                locale: "protosdc-rs-provider".to_string(),
            }])
            .build()
    }
}
