use std::future::Future;
use std::sync::atomic::{AtomicU32, Ordering};
use std::sync::Arc;

use async_trait::async_trait;
use log::{debug, error};
use thiserror::Error;
use tokio::sync::{mpsc, Mutex};

use crate::provider::sco_util::{FailedInvocationStateTypes, ReportBuilder, ReportData};
use biceps::common::channel_publisher::ChannelPublisher;
use biceps::common::mdib_entity::EntityBase;
use biceps::common::mdib_version::MdibVersion;
use biceps::provider::access::local_mdib_access::LocalMdibAccess;
use biceps::utilities::localized_text::LocalizedTextBuilder;
use protosdc_biceps::biceps::{
    invocation_error_mod, invocation_state_mod, operation_invoked_report_mod, AbstractReport,
    AbstractReportPart, AbstractSetResponse, Activate, HandleRef, InstanceIdentifier,
    InstanceIdentifierOneOf, InvocationError, InvocationInfo, InvocationState, LocalizedText,
    LocalizedTextContent, OperationInvokedReport, SetAlertState, SetComponentState,
    SetContextState, SetMetricState, SetString, SetValue, TransactionId,
};

#[derive(Debug, Error)]
pub enum InvocationCallError {
    #[error("No receiver was known")]
    UnknownReceiver,

    #[error("An error occurred: {error}")]
    Other { error: String },
}

pub struct InvocationResponse {
    mdib_version: MdibVersion,
    transaction_id: TransactionId,
    invocation_state: InvocationState,
    invocation_error: Option<InvocationError>,
    invocation_error_message: Vec<LocalizedText>,
}

impl From<InvocationResponse> for AbstractSetResponse {
    fn from(value: InvocationResponse) -> Self {
        Self {
            extension_element: None,
            invocation_info: InvocationInfo {
                extension_element: None,
                transaction_id: value.transaction_id,
                invocation_state: value.invocation_state,
                invocation_error: value.invocation_error,
                invocation_error_message: value.invocation_error_message,
            },
            mdib_version_group_attr: value.mdib_version.into(),
        }
    }
}

pub struct Context<MDIB>
where
    MDIB: LocalMdibAccess + Send + Sync + Clone,
{
    transaction_id: TransactionId,
    operation_handle: String,
    invocation_source: InstanceIdentifier,
    operation_invoked_report_senders: Arc<Mutex<ChannelPublisher<OperationInvokedReport>>>,
    pub mdib_access: MDIB,

    current_invocation_state: Option<InvocationState>,
    source_mds: String,
}

impl<MDIB> Context<MDIB>
where
    MDIB: LocalMdibAccess + Send + Sync + Clone,
{
    async fn new(
        transaction_id: TransactionId,
        operation_handle: String,
        invocation_source: InstanceIdentifier,
        operation_invoked_report_senders: Arc<Mutex<ChannelPublisher<OperationInvokedReport>>>,
        mdib_access: MDIB,
    ) -> Result<Self, InvocationResponse> {
        // determine mds for operation handle
        let mds_handle = match mdib_access.entity(&operation_handle).await {
            Some(ent) => Ok(ent.entity.mds()),
            None => Err(InvocationResponse {
                mdib_version: mdib_access.mdib_version().await,
                transaction_id: transaction_id.clone(),
                invocation_state: InvocationState {
                    enum_type: invocation_state_mod::EnumType::Fail,
                },
                invocation_error: Some(InvocationError {
                    enum_type: invocation_error_mod::EnumType::Inv,
                }),
                invocation_error_message: vec![LocalizedText {
                    localized_text_content: LocalizedTextContent {
                        string: "Unknown operation target".to_string(),
                    },
                    ref_attr: None,
                    lang_attr: None,
                    version_attr: None,
                    text_width_attr: None,
                }],
            }),
        }?;

        Ok(Self {
            transaction_id,
            operation_handle,
            invocation_source,
            operation_invoked_report_senders,
            mdib_access,
            current_invocation_state: None,
            source_mds: mds_handle,
        })
    }

    pub async fn create_response(&mut self, report_data: ReportData) -> InvocationResponse {
        if Some(&report_data.invocation_state) != self.current_invocation_state.as_ref() {
            debug!(
                "No matching OperationInvokedReport was sent before creating response. \
                    Sending response as OperationInvokedReport as well. \
                    Operation: {} - State: {:?} - Next: {:?}",
                self.operation_handle, self.current_invocation_state, &report_data.invocation_state
            );

            self.send_report(report_data.clone()).await;
        }

        InvocationResponse {
            mdib_version: report_data.mdib_version,
            transaction_id: self.transaction_id.clone(),
            invocation_state: report_data.invocation_state,
            invocation_error: report_data.invocation_error,
            invocation_error_message: report_data.invocation_error_message,
        }
    }

    pub async fn send_report(&mut self, report_data: ReportData) {
        self.current_invocation_state = Some(report_data.invocation_state.clone());

        let invocation_info = InvocationInfo {
            extension_element: None,
            transaction_id: self.transaction_id.clone(),
            invocation_state: report_data.invocation_state,
            invocation_error: report_data.invocation_error,
            invocation_error_message: report_data.invocation_error_message,
        };

        let report_part = operation_invoked_report_mod::ReportPart {
            abstract_report_part: AbstractReportPart {
                extension_element: None,
                source_mds: Some(HandleRef {
                    string: self.source_mds.clone(),
                }),
            },
            invocation_info,
            invocation_source: InstanceIdentifierOneOf::InstanceIdentifier(
                self.invocation_source.clone(),
            ),
            operation_handle_ref_attr: HandleRef {
                string: self.operation_handle.clone(),
            },
            operation_target_attr: report_data
                .operation_target
                .map(|target| HandleRef { string: target }),
        };

        let report = OperationInvokedReport {
            abstract_report: AbstractReport {
                extension_element: None,
                mdib_version_group_attr: report_data.mdib_version.into(),
            },
            report_part: vec![report_part],
        };

        self.operation_invoked_report_senders
            .lock()
            .await
            .send(report)
            .await
    }
}

#[derive(Clone, Debug)]
pub struct ScoController {
    operation_invoked_report_senders: Arc<Mutex<ChannelPublisher<OperationInvokedReport>>>,
    transaction_counter: Arc<AtomicU32>,
}

impl Default for ScoController {
    fn default() -> Self {
        Self {
            operation_invoked_report_senders: Arc::new(Mutex::new(ChannelPublisher::new(100))),
            transaction_counter: Arc::new(AtomicU32::new(0)),
        }
    }
}

impl ScoController {
    pub async fn subscribe(&self) -> mpsc::Receiver<OperationInvokedReport> {
        self.operation_invoked_report_senders
            .lock()
            .await
            .subscribe()
            .await
    }

    pub async fn process_incoming_set_operation<Fut, MDIB>(
        &self,
        handle: String,
        source: InstanceIdentifier,
        mdib_access: MDIB,
        process: impl FnOnce(Context<MDIB>) -> Fut,
    ) -> InvocationResponse
    where
        Fut: Future<Output = Result<InvocationResponse, (InvocationCallError, Context<MDIB>)>>,
        MDIB: LocalMdibAccess + Send + Sync + Clone,
    {
        let transaction_id = TransactionId {
            unsigned_int: self.transaction_counter.fetch_add(1, Ordering::Relaxed),
        };

        let context = match Context::new(
            transaction_id.clone(),
            handle.clone(),
            source,
            self.operation_invoked_report_senders.clone(),
            mdib_access.clone(),
        )
        .await
        {
            Ok(ctx) => ctx,
            Err(err) => return err,
        };

        match process(context).await {
            Ok(resp) => resp,
            Err((err, mut context)) => {
                error!(
                    "An error occurred during operation invocation, sending default error response"
                );
                context.create_response(
                    ReportBuilder::create()
                        .mdib_version(mdib_access.mdib_version().await)
                        .failure(FailedInvocationStateTypes::Fail)
                        .error(invocation_error_mod::EnumType::Unspec)
                        .error_message(LocalizedTextBuilder::create()
                            .no_service(format!("An error occurred during operation invocation for handle {handle}: {:?}", err))
                            .lang("en")
                            .build()
                        )
                        .build()
                ).await
            }
        }
    }
}

#[allow(unused_variables)]
#[async_trait]
pub trait OperationInvocationReceiver<MDIB>
where
    MDIB: 'static + LocalMdibAccess + Send + Sync + Clone,
{
    async fn activate(
        &self,
        operation_handle: String,
        context: Context<MDIB>,
        request: Activate,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        Err((InvocationCallError::UnknownReceiver, context))
    }

    async fn set_alert_state(
        &self,
        operation_handle: String,
        context: Context<MDIB>,
        request: SetAlertState,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        Err((InvocationCallError::UnknownReceiver, context))
    }

    async fn set_component_state(
        &self,
        operation_handle: String,
        context: Context<MDIB>,
        request: SetComponentState,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        Err((InvocationCallError::UnknownReceiver, context))
    }

    async fn set_context_state(
        &self,
        operation_handle: String,
        context: Context<MDIB>,
        request: SetContextState,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        Err((InvocationCallError::UnknownReceiver, context))
    }

    async fn set_metric_state(
        &self,
        operation_handle: String,
        context: Context<MDIB>,
        request: SetMetricState,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        Err((InvocationCallError::UnknownReceiver, context))
    }

    async fn set_string(
        &self,
        operation_handle: String,
        context: Context<MDIB>,
        request: SetString,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        Err((InvocationCallError::UnknownReceiver, context))
    }

    async fn set_value(
        &self,
        operation_handle: String,
        context: Context<MDIB>,
        request: SetValue,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        Err((InvocationCallError::UnknownReceiver, context))
    }
}

/// `DummyOperationInvocationReceiver` is a public struct that acts as a placeholder.
///
/// This struct doesn't hold any data and does not implement any specific behavior.
#[derive(Clone, Debug)]
pub struct DummyOperationInvocationReceiver {}

impl<MDIB> OperationInvocationReceiver<MDIB> for DummyOperationInvocationReceiver where
    MDIB: 'static + LocalMdibAccess + Send + Sync + Clone
{
}
