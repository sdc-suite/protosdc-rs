use protosdc_proto::common::Uri;
use protosdc_proto::discovery::Endpoint;
use std::net::SocketAddr;
use std::sync::Arc;
use tokio::sync::Mutex;
use typed_builder::TypedBuilder;

pub fn socket_address_to_string_http(socket_address: &SocketAddr) -> String {
    format!("http://{}:{}", socket_address.ip(), socket_address.port())
}

pub fn socket_address_to_string_https(socket_address: &SocketAddr) -> String {
    format!("https://{}:{}", socket_address.ip(), socket_address.port())
}

pub fn socket_address_to_uri_http(socket_address: &SocketAddr) -> Uri {
    Uri {
        value: socket_address_to_string_http(socket_address),
    }
}

pub fn socket_address_to_uri_https(socket_address: &SocketAddr) -> Uri {
    Uri {
        value: socket_address_to_string_https(socket_address),
    }
}

#[derive(Clone, Debug, TypedBuilder)]
pub struct EndpointData {
    pub endpoint: Arc<Mutex<Endpoint>>,
}

impl From<Endpoint> for EndpointData {
    fn from(value: Endpoint) -> Self {
        Self {
            endpoint: Arc::new(Mutex::new(value)),
        }
    }
}

//noinspection HttpUrlsUsage
/// If used as root of an instance identifier, this string specifies an extension to include a PEM string to
/// represent a certificate.
///
/// This is a workaround as SDC Glue lacks an appropriate definition and solves
/// https://sourceforge.net/p/opensdc/ieee11073-20701/7/.
///
pub const ROOT_PEM_STRING_SDC: &str =
    "http://standards.ieee.org/downloads/11073/11073-20701-2018/X509Certificate/PEM";

//noinspection HttpUrlsUsage
pub const ROOT_SDC: &str = "http://standards.ieee.org/downloads/11073/11073-20701-2018";
pub const ANONYMOUS_EXTENSION: &str = "AnonymousSdcParticipant";
