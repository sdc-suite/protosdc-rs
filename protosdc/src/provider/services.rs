use std::convert::TryInto;
use std::fmt::Debug;
use std::ops::{Deref, DerefMut};
use std::pin::Pin;
use std::sync::Arc;
use std::time::Duration;

use async_trait::async_trait;
use futures::FutureExt;
use futures::StreamExt;
use itertools::Itertools;
use log::{debug, error, info};
use tokio::net::TcpListener;
use tokio::sync::{oneshot, Mutex};
use tokio::task;
use tokio_stream::wrappers::{ReceiverStream, TcpListenerStream};
use tokio_stream::Stream;
use tonic::transport::Server;
use tonic::{Request, Response, Status};

use biceps::common::access::mdib_access::MdibAccess;
use biceps::common::biceps_util::State;
use biceps::provider::access::local_mdib_access::{
    LocalMdibAccess, LocalMdibAccessReadTransaction,
};
use common::{Service as CommonService, ServiceDelegate, ServiceError, ServiceState};
use protosdc_biceps::biceps::instance_identifier_mod::{ExtensionAttr, RootAttr};
use protosdc_biceps::biceps::{
    AbstractSetResponse, Activate, GetContextStates, GetLocalizedText, GetMdDescription,
    GetMdState, GetMdib, GetSupportedLanguages, InstanceIdentifier, MdibVersionGroup,
    SetAlertState, SetComponentState, SetContextState, SetMetricState, SetString, SetValue,
};

use common::crypto::der_to_pem;
use protosdc_proto::biceps::{
    AbstractContextStateOneOfMsg, AbstractGetResponseMsg, ActivateResponseMsg,
    GetContextStatesResponseMsg, GetLocalizedTextResponseMsg, GetMdDescriptionResponseMsg,
    GetMdStateResponseMsg, GetMdibResponseMsg, GetSupportedLanguagesResponseMsg, LocalizedTextMsg,
    SetAlertStateResponseMsg, SetComponentStateResponseMsg, SetContextStateResponseMsg,
    SetMetricStateResponseMsg, SetStringResponseMsg, SetValueResponseMsg,
};
use protosdc_proto::discovery::Endpoint;
use protosdc_proto::metadata::metadata_service_server::{MetadataService, MetadataServiceServer};
use protosdc_proto::metadata::{
    EndpointMetadata, GetMetadataRequest, GetMetadataResponse, Service,
};
use protosdc_proto::sdc::filter::Type;
use protosdc_proto::sdc::get_service_server::{GetService, GetServiceServer};
use protosdc_proto::sdc::localization_service_server::{
    LocalizationService, LocalizationServiceServer,
};
use protosdc_proto::sdc::mdib_reporting_service_server::{
    MdibReportingService, MdibReportingServiceServer,
};
use protosdc_proto::sdc::operation_invoked_report_stream::Message;
use protosdc_proto::sdc::set_service_server::{SetService, SetServiceServer};
use protosdc_proto::sdc::{
    episodic_report_stream, ActivateRequest, ActivateResponse, EpisodicReportRequest,
    EpisodicReportStream, GetContextStatesRequest, GetContextStatesResponse,
    GetLocalizedTextRequest, GetLocalizedTextStream, GetMdDescriptionRequest,
    GetMdDescriptionResponse, GetMdStateRequest, GetMdStateResponse, GetMdibRequest,
    GetMdibResponse, GetSupportedLanguagesRequest, GetSupportedLanguagesResponse,
    OperationInvokedReportRequest, OperationInvokedReportStream, PeriodicReportRequest,
    PeriodicReportStream, SetAlertStateRequest, SetAlertStateResponse, SetComponentStateRequest,
    SetComponentStateResponse, SetContextStateRequest, SetContextStateResponse,
    SetMetricStateRequest, SetMetricStateResponse, SetStringRequest, SetStringResponse,
    SetValueRequest, SetValueResponse, SubscriptionConfirmation,
};
use tokio_rustls::rustls::pki_types::CertificateDer;
use typed_builder::TypedBuilder;

use crate::addressing;
use crate::common::{action, service};
use crate::provider::common::{
    socket_address_to_uri_http, socket_address_to_uri_https, EndpointData, ANONYMOUS_EXTENSION,
    ROOT_PEM_STRING_SDC, ROOT_SDC,
};
use crate::provider::device::ServerConfig;
use crate::provider::localization_storage::{
    LocalizationStorage, LocalizationStorageImpl, LocalizedStorageText, TextWidth,
};
use crate::provider::mapper::MappingError;
use crate::provider::sco::{OperationInvocationReceiver, ScoController};
use crate::provider::{mapper, report_generator};

struct AvailableServices<T, MDIB, LOC>
where
    T: OperationInvocationReceiver<MDIB> + Sync + Send,
    MDIB: 'static + LocalMdibAccess + Send + Sync + Clone,
    LOC: 'static + LocalizationStorage<LocalizedStorageText>,
{
    metadata_service: Option<MetadataServiceImpl>,
    get_service: Option<GetServiceImpl<MDIB>>,
    set_service: Option<SetServiceImpl<T, MDIB>>,
    reporting_service: Option<MdibReportingServiceImpl>,
    localization_service: Option<LocalizationServiceImpl<LOC, MDIB>>,
}

pub struct PrimaryProviderServices<T, MDIB, LOC>
where
    T: OperationInvocationReceiver<MDIB> + Sync + Send,
    MDIB: 'static + LocalMdibAccess + Sync + Send + Clone,
    LOC: 'static + LocalizationStorage<LocalizedStorageText>,
{
    service_delegate: ServiceDelegate,
    available_services: Option<AvailableServices<T, MDIB, LOC>>,
    server_config: Option<ServerConfig>,
    service_task: Option<task::JoinHandle<Result<(), tonic::transport::Error>>>,
    service_shutdown: Option<oneshot::Sender<()>>,
}

#[derive(TypedBuilder)]
pub struct PrimaryProviderServicesConfig<T, MDIB, LOC = LocalizationStorageImpl>
where
    T: OperationInvocationReceiver<MDIB> + Sync + Send,
    MDIB: 'static + LocalMdibAccess + Send + Sync + Clone,
    LOC: LocalizationStorage<LocalizedStorageText>,
{
    mdib_access: MDIB,
    endpoint: EndpointData,
    endpoint_metadata: EndpointMetadata,
    services: Vec<Service>,
    server_config: ServerConfig,
    sco_controller: ScoController,
    operation_invocation_receiver: T,
    localization_storage: Option<LOC>,
}

impl<T, MDIB, LOC> PrimaryProviderServices<T, MDIB, LOC>
where
    T: OperationInvocationReceiver<MDIB> + Sync + Send,
    MDIB: 'static + LocalMdibAccess + Send + Sync + Clone,
    LOC: LocalizationStorage<LocalizedStorageText>,
{
    pub async fn new(config: PrimaryProviderServicesConfig<T, MDIB, LOC>) -> Self {
        let mut available_service_types = vec![
            service::METADATA_SERVICE.to_string(),
            service::GET_SERVICE.to_string(),
            service::SET_SERVICE.to_string(),
            service::MDIB_REPORTING_SERVICE.to_string(),
        ];

        if config.localization_storage.is_some() {
            available_service_types.push(service::LOCALIZATION_SERVICE.to_string())
        }

        let primary_services = Service {
            physical_address: vec![match config.server_config.crypto_config.is_some() {
                true => socket_address_to_uri_https(&config.server_config.server_address),
                false => socket_address_to_uri_http(&config.server_config.server_address),
            }],
            r#type: available_service_types,
            service_identifier: "protosdc-rs-primary-provider-services".to_string(),
        };

        let mut all_services = vec![primary_services];
        all_services.extend(config.services);

        let available_services = AvailableServices {
            metadata_service: Some(MetadataServiceImpl {
                endpoint: config.endpoint.endpoint.clone(),
                endpoint_metadata: config.endpoint_metadata,
                services: all_services,
            }),
            get_service: Some(GetServiceImpl {
                mdib_access: config.mdib_access.clone(),
            }),
            set_service: Some(SetServiceImpl::new(
                config.mdib_access.clone(),
                config.sco_controller,
                config.operation_invocation_receiver,
            )),
            reporting_service: Some(MdibReportingServiceImpl {
                report_generator: Arc::new(Mutex::new(report_generator::ReportGenerator::new(
                    config.mdib_access.subscribe().await,
                ))),
            }),
            localization_service: config
                .localization_storage
                .map(|str| LocalizationServiceImpl {
                    localized_text_storage: str,
                    mdib_access: config.mdib_access.clone(),
                }),
        };

        Self {
            service_delegate: ServiceDelegate::default(),
            available_services: Some(available_services),
            server_config: Some(config.server_config),
            service_task: None,
            service_shutdown: None,
            // actual_addresses: vec![]
        }
    }

    // pub fn actual_addresses(&self) -> &[String] {
    //     &self.actual_addresses
    // }
}

struct MetadataServiceImpl {
    endpoint: Arc<Mutex<Endpoint>>,
    endpoint_metadata: EndpointMetadata,
    services: Vec<Service>,
}

struct GetServiceImpl<MDIB>
where
    MDIB: LocalMdibAccess + Send + Sync,
{
    mdib_access: MDIB,
}

struct SetServiceImpl<T, MDIB>
where
    T: OperationInvocationReceiver<MDIB> + Sync + Send,
    MDIB: 'static + LocalMdibAccess + Send + Sync + Clone,
{
    mdib_access: MDIB,
    sco_controller: ScoController,
    operation_invocation_receiver: T,
}

struct LocalizationServiceImpl<S, MDIB>
where
    S: LocalizationStorage<LocalizedStorageText> + 'static,
    MDIB: 'static + LocalMdibAccess + Send + Sync + Clone,
{
    mdib_access: MDIB,
    localized_text_storage: S,
}

#[tonic::async_trait]
impl<S, MDIB> LocalizationService for LocalizationServiceImpl<S, MDIB>
where
    S: LocalizationStorage<LocalizedStorageText> + Send + Sync + 'static,
    MDIB: 'static + LocalMdibAccess + Send + Sync + Clone,
{
    async fn get_supported_languages(
        &self,
        request: Request<GetSupportedLanguagesRequest>,
    ) -> Result<Response<GetSupportedLanguagesResponse>, Status> {
        debug!("get_supported_languages");

        let inner = request.into_inner();

        let request_addr = match &inner.addressing {
            None => return Err(Status::invalid_argument("addressing element missing")),
            Some(it) => it,
        };

        // map the request to validate the request
        map_to::<_, GetSupportedLanguages, _>(inner.payload)?;

        let supported_languages = self.localized_text_storage.get_languages().await;
        let mdib_version: MdibVersionGroup = self.mdib_access.mdib_version().await.into();

        let response = GetSupportedLanguagesResponse {
            addressing: Some(addressing::util::create_reply_to(
                request_addr,
                action::GET_SUPPORTED_LANGUAGES_RESPONSE,
            )),
            payload: Some(GetSupportedLanguagesResponseMsg {
                abstract_get_response: Some(AbstractGetResponseMsg {
                    extension_element: None,
                    mdib_version_group_attr: Some(mdib_version.into()),
                }),
                lang: Vec::from_iter(supported_languages),
            }),
        };

        Ok(Response::new(response))
    }

    type GetLocalizedTextStream =
        Pin<Box<dyn Stream<Item = Result<GetLocalizedTextStream, Status>> + Send + Sync + 'static>>;

    async fn get_localized_text(
        &self,
        request: Request<GetLocalizedTextRequest>,
    ) -> Result<Response<Self::GetLocalizedTextStream>, Status> {
        debug!("get_localized_text");
        let inner = request.into_inner();
        let req_addressing = inner.addressing.as_ref().ok_or_else(|| {
            Status::invalid_argument("get_localized_text: Addressing element missing")
        })?;
        let res_addressing =
            addressing::util::create_reply_to(req_addressing, action::GET_LOCALIZED_TEXT_STREAM);

        // map the request to validate the request
        let mapped = map_to::<_, GetLocalizedText, _>(inner.payload)?;

        let refs: Vec<String> = mapped.r#ref.into_iter().map(|it| it.string).collect();

        let requested_text_refs = match refs.is_empty() {
            true => None,
            false => Some(refs),
        };
        let requested_version = mapped.version.map(|it| it.version_counter.unsigned_long);
        let requested_langs = match mapped.lang.is_empty() {
            true => None,
            false => Some(mapped.lang.into_iter().map(|it| it.language).collect()),
        };
        let requested_max_width: Option<TextWidth> = mapped
            .text_width
            .into_iter()
            // this filtering also drops any element that doesn't map. Is that okay?
            .map(TextWidth::from)
            .max();
        let requested_max_number_of_lines = mapped.number_of_lines.into_iter().max();

        debug!(
            "Inbound request parameters: \
        text_refs {requested_text_refs:?}, \
        requested_version {requested_version:?}, \
        requested_langs {requested_langs:?}, \
        requested_max_width {requested_max_width:?}, \
        requested_max_number_of_lines {requested_max_number_of_lines:?}"
        );

        let response_receiver = self
            .localized_text_storage
            .query(
                requested_text_refs,
                requested_version,
                requested_langs,
                requested_max_width,
                requested_max_number_of_lines,
            )
            .await
            .map_err(|err| Status::invalid_argument(format!("{err:?}")))?;

        let mdib_version: MdibVersionGroup = self.mdib_access.mdib_version().await.into();

        let response_stream = ReceiverStream::new(response_receiver)
            .map(|texts| {
                texts
                    .into_iter()
                    .map(|txt| {
                        let x: LocalizedTextMsg = txt.into();
                        x
                    })
                    .collect()
            })
            .map(move |texts| {
                let addr = res_addressing.clone();
                let version = mdib_version.clone();
                Ok(GetLocalizedTextStream {
                    addressing: Some(addr),
                    payload: Some(GetLocalizedTextResponseMsg {
                        abstract_get_response: Some(AbstractGetResponseMsg {
                            extension_element: None,
                            mdib_version_group_attr: Some(version.into()),
                        }),
                        text: texts,
                    }),
                })
            });

        Ok(Response::new(
            Box::pin(response_stream) as Self::GetLocalizedTextStream
        ))
    }
}

impl<T, MDIB> SetServiceImpl<T, MDIB>
where
    T: OperationInvocationReceiver<MDIB> + Sync + Send,
    MDIB: 'static + LocalMdibAccess + Send + Sync + Clone,
{
    pub fn new(
        mdib_access: MDIB,
        sco_controller: ScoController,
        operation_invocation_receiver: T,
    ) -> Self {
        Self {
            mdib_access,
            sco_controller,
            operation_invocation_receiver,
        }
    }
}

struct MdibReportingServiceImpl {
    report_generator: Arc<Mutex<report_generator::ReportGenerator>>,
}

#[tonic::async_trait]
impl MetadataService for MetadataServiceImpl {
    async fn get_metadata(
        &self,
        request: Request<GetMetadataRequest>,
    ) -> Result<Response<GetMetadataResponse>, Status> {
        let request_addr = match &request.get_ref().addressing {
            None => return Err(Status::invalid_argument("addressing element missing")),
            Some(it) => it,
        };

        let endpoint = {
            let ep = self.endpoint.lock().await;
            ep.deref().clone()
        };

        let response = GetMetadataResponse {
            addressing: Some(addressing::util::create_reply_to(
                request_addr,
                action::GET_METADATA_RESPONSE,
            )),
            endpoint: Some(endpoint),
            metadata: Some(self.endpoint_metadata.clone()),
            service: self.services.clone(),
        };

        Ok(Response::new(response))
    }
}

async fn build_get_mdib_response<T>(access: T) -> Result<GetMdibResponseMsg, MappingError>
where
    T: MdibAccess + Sync + Send,
{
    let mdib = mapper::map_mdib(&access).await?;
    // map to proto and create response
    let proto_mdib = mdib.into();
    let mdib_version: MdibVersionGroup = access.mdib_version().await.into();
    Ok(GetMdibResponseMsg {
        abstract_get_response: Some(AbstractGetResponseMsg {
            extension_element: None,
            mdib_version_group_attr: Some(mdib_version.into()),
        }),
        mdib: Some(proto_mdib),
    })
}

async fn build_get_description_response<T>(
    access: T,
    filter: &[String],
) -> Result<GetMdDescriptionResponseMsg, MappingError>
where
    T: MdibAccess + Sync + Send,
{
    let md_description = mapper::map_md_description(&access, filter).await?;
    // map to proto and create response
    let proto_description = md_description.into();
    let mdib_version: MdibVersionGroup = access.mdib_version().await.into();
    Ok(GetMdDescriptionResponseMsg {
        abstract_get_response: Some(AbstractGetResponseMsg {
            extension_element: None,
            mdib_version_group_attr: Some(mdib_version.into()),
        }),
        md_description: Some(proto_description),
    })
}

async fn build_get_state_response<T>(
    access: T,
    filter: &[String],
) -> Result<GetMdStateResponseMsg, MappingError>
where
    T: MdibAccess + Sync + Send,
{
    let md_description = mapper::map_md_state(&access, filter).await?;
    // map to proto and create response
    let proto_states = md_description.into();
    let mdib_version: MdibVersionGroup = access.mdib_version().await.into();
    Ok(GetMdStateResponseMsg {
        abstract_get_response: Some(AbstractGetResponseMsg {
            extension_element: None,
            mdib_version_group_attr: Some(mdib_version.into()),
        }),
        md_state: Some(proto_states),
    })
}

async fn build_get_context_states_response<T>(
    access: &T,
    filter: &[String],
) -> Result<GetContextStatesResponseMsg, MappingError>
where
    T: MdibAccess + Sync + Send,
{
    let context_states = access.context_states().await;

    let filtered: Vec<AbstractContextStateOneOfMsg> = match filter.is_empty() {
        true => context_states,
        false => context_states
            .into_iter()
            .filter(|it| filter.contains(&it.descriptor_handle()))
            .collect(),
    }
    .into_iter()
    .map(|it| it.into())
    .collect();

    // map to proto and create response
    let mdib_version: MdibVersionGroup = access.mdib_version().await.into();
    Ok(GetContextStatesResponseMsg {
        abstract_get_response: Some(AbstractGetResponseMsg {
            extension_element: None,
            mdib_version_group_attr: Some(mdib_version.into()),
        }),
        context_state: filtered,
    })
}

/// Maps a request to the designated type, or returns an error if the
/// request is empty or the mapping fails.
fn map_to<S, T, E>(request: Option<S>) -> Result<T, Status>
where
    S: TryInto<T, Error = E> + Clone,
    E: Debug,
{
    request
        .ok_or(Status::invalid_argument("Empty payload"))?
        .try_into()
        .map_err(|err| {
            error!("Mapping request failed: {:?}", err);
            Status::internal("Mapping request failed")
        })
}

#[tonic::async_trait]
impl<MDIB> GetService for GetServiceImpl<MDIB>
where
    MDIB: 'static + LocalMdibAccess + Send + Sync + Clone + LocalMdibAccessReadTransaction,
{
    async fn get_mdib(
        &self,
        request: Request<GetMdibRequest>,
    ) -> Result<Response<GetMdibResponse>, Status> {
        let req_addressing = request
            .get_ref()
            .addressing
            .as_ref()
            .ok_or_else(|| Status::invalid_argument("get_mdib: Addressing element missing"))?;
        let res_addressing =
            addressing::util::create_reply_to(req_addressing, action::GET_MDIB_RESPONSE);

        // map the (useless) request to validate the request
        map_to::<_, GetMdib, _>(request.into_inner().payload)?;

        let get_mdib_response_msg = self
            .mdib_access
            .read_transaction(|access| async move { build_get_mdib_response(access).await }.boxed())
            .await
            .map_err(|err| {
                error!("Mapping mdib response failed: {:?}", err);
                Status::invalid_argument(format!("Mapping mdib response failed: {:?}", err))
            })?;

        Ok(Response::new(GetMdibResponse {
            addressing: Some(res_addressing),
            payload: Some(get_mdib_response_msg),
        }))
    }

    async fn get_md_description(
        &self,
        request: Request<GetMdDescriptionRequest>,
    ) -> Result<Response<GetMdDescriptionResponse>, Status> {
        let req_addressing = request.get_ref().addressing.as_ref().ok_or_else(|| {
            Status::invalid_argument("get_md_description: Addressing element missing")
        })?;
        let res_addressing =
            addressing::util::create_reply_to(req_addressing, action::GET_MD_DESCRIPTION_RESPONSE);

        // map the request to validate the request
        let mapped = map_to::<_, GetMdDescription, _>(request.into_inner().payload)?;

        let req_filter = mapped
            .handle_ref
            .into_iter()
            .map(|it| it.string)
            .collect_vec();

        let get_md_description_response = self
            .mdib_access
            .read_transaction(|access| {
                async move { build_get_description_response(access, &req_filter).await }.boxed()
            })
            .await
            .map_err(|err| {
                error!("Mapping md description response failed: {:?}", err);
                Status::internal("Mapping md description response failed")
            })?;

        Ok(Response::new(GetMdDescriptionResponse {
            addressing: Some(res_addressing),
            payload: Some(get_md_description_response),
        }))
    }

    async fn get_md_state(
        &self,
        request: Request<GetMdStateRequest>,
    ) -> Result<Response<GetMdStateResponse>, Status> {
        let req_addressing =
            request.get_ref().addressing.as_ref().ok_or_else(|| {
                Status::invalid_argument("get_md_state: Addressing element missing")
            })?;
        let res_addressing =
            addressing::util::create_reply_to(req_addressing, action::GET_MD_STATE_RESPONSE);

        // map the request to validate the request
        let mapped = map_to::<_, GetMdState, _>(request.into_inner().payload)?;

        let req_filter = mapped
            .handle_ref
            .into_iter()
            .map(|it| it.string)
            .collect_vec();

        let get_md_state_response = self
            .mdib_access
            .read_transaction(|access| {
                async move { build_get_state_response(access, &req_filter).await }.boxed()
            })
            .await
            .map_err(|err| {
                error!("Mapping md state response failed: {:?}", err);
                Status::internal("Mapping md state response failed")
            })?;

        Ok(Response::new(GetMdStateResponse {
            addressing: Some(res_addressing),
            payload: Some(get_md_state_response),
        }))
    }

    async fn get_context_states(
        &self,
        request: Request<GetContextStatesRequest>,
    ) -> Result<Response<GetContextStatesResponse>, Status> {
        let req_addressing = request.get_ref().addressing.as_ref().ok_or_else(|| {
            Status::invalid_argument("get_context_states: Addressing element missing")
        })?;
        let res_addressing =
            addressing::util::create_reply_to(req_addressing, action::GET_CONTEXT_STATES_RESPONSE);

        // map the request to validate the request
        let mapped = map_to::<_, GetContextStates, _>(request.into_inner().payload)?;

        let req_filter = mapped
            .handle_ref
            .into_iter()
            .map(|it| it.string)
            .collect_vec();

        let get_context_states_response = self
            .mdib_access
            .read_transaction(|access| {
                async move { build_get_context_states_response(&access, &req_filter).await }.boxed()
            })
            .await
            .map_err(|err| {
                error!("Mapping md state response failed: {:?}", err);
                Status::internal("Mapping md state response failed")
            })?;

        Ok(Response::new(GetContextStatesResponse {
            addressing: Some(res_addressing),
            payload: Some(get_context_states_response),
        }))
    }
}

async fn get_source(certs: &Option<Arc<Vec<CertificateDer<'_>>>>) -> InstanceIdentifier {
    let cert_identifier = match certs {
        None => None,
        Some(certs) => certs.as_ref().first().map(|cert| der_to_pem(cert.as_ref())),
    };

    match cert_identifier {
        None => InstanceIdentifier {
            extension_element: None,
            r#type: None,
            identifier_name: vec![],
            root_attr: Some(RootAttr {
                any_u_r_i: (*ROOT_SDC).to_string().try_into().unwrap(),
            }),
            extension_attr: Some(ExtensionAttr {
                string: ANONYMOUS_EXTENSION.to_string(),
            }),
        },
        Some(subject) => InstanceIdentifier {
            extension_element: None,
            r#type: None,
            identifier_name: vec![],
            root_attr: Some(RootAttr {
                any_u_r_i: (*ROOT_PEM_STRING_SDC).to_string().try_into().unwrap(),
            }),
            extension_attr: Some(ExtensionAttr { string: subject }),
        },
    }
}

macro_rules! operation_handler {
    ($self:ident, $request:ident, $operation_invocation_receiver:ident, $request_type:ident, $response_type:ident, $base_response_msg_type:ident, $response_msg_type:ident, $request_name:expr, $response_action:ident, $oir_call:ident) => {{
        let req_addressing =
            $request.get_ref().addressing.as_ref().ok_or_else(|| {
                Status::invalid_argument("$request_name: Addressing element missing")
            })?;
        let res_addressing =
            addressing::util::create_reply_to(req_addressing, action::$response_action);

        let source = get_source(&$request.peer_certs()).await;

        // map the request to validate the request
        let mapped_request = map_to::<_, $request_type, _>($request.into_inner().payload)?;

        let operation_handle = mapped_request
            .abstract_set
            .operation_handle_ref
            .string
            .clone();
        let receiver = $self.$operation_invocation_receiver.clone();

        let call_result = $self
            .sco_controller
            .process_incoming_set_operation(
                operation_handle.clone(),
                source,
                $self.mdib_access.clone(),
                |context| async {
                    receiver
                        .$oir_call(operation_handle, context, mapped_request)
                        .await
                },
            )
            .await;

        let abstract_set_response: $base_response_msg_type = call_result.into();

        let response = $response_type {
            addressing: Some(res_addressing),
            payload: Some($response_msg_type {
                abstract_set_response: Some(abstract_set_response.into()),
            }),
        };

        Ok(Response::new(response))
    }};
}

#[tonic::async_trait]
impl<T, MDIB> SetService for SetServiceImpl<T, MDIB>
where
    T: 'static + OperationInvocationReceiver<MDIB> + Send + Sync + Clone,
    MDIB: 'static + LocalMdibAccess + Send + Sync + Clone,
{
    async fn activate(
        &self,
        request: Request<ActivateRequest>,
    ) -> Result<Response<ActivateResponse>, Status> {
        operation_handler!(
            self,
            request,
            operation_invocation_receiver,
            Activate,
            ActivateResponse,
            AbstractSetResponse,
            ActivateResponseMsg,
            "activate",
            ACTIVATE_RESPONSE,
            activate
        )
    }

    async fn set_metric_state(
        &self,
        request: Request<SetMetricStateRequest>,
    ) -> Result<Response<SetMetricStateResponse>, Status> {
        operation_handler!(
            self,
            request,
            operation_invocation_receiver,
            SetMetricState,
            SetMetricStateResponse,
            AbstractSetResponse,
            SetMetricStateResponseMsg,
            "set_metric_state",
            SET_METRIC_STATE_RESPONSE,
            set_metric_state
        )
    }

    async fn set_component_state(
        &self,
        request: Request<SetComponentStateRequest>,
    ) -> Result<Response<SetComponentStateResponse>, Status> {
        operation_handler!(
            self,
            request,
            operation_invocation_receiver,
            SetComponentState,
            SetComponentStateResponse,
            AbstractSetResponse,
            SetComponentStateResponseMsg,
            "set_component_state",
            SET_COMPONENT_STATE_RESPONSE,
            set_component_state
        )
    }

    async fn set_context_state(
        &self,
        request: Request<SetContextStateRequest>,
    ) -> Result<Response<SetContextStateResponse>, Status> {
        operation_handler!(
            self,
            request,
            operation_invocation_receiver,
            SetContextState,
            SetContextStateResponse,
            AbstractSetResponse,
            SetContextStateResponseMsg,
            "set_context_state",
            SET_CONTEXT_STATE_RESPONSE,
            set_context_state
        )
    }

    async fn set_alert_state(
        &self,
        request: Request<SetAlertStateRequest>,
    ) -> Result<Response<SetAlertStateResponse>, Status> {
        operation_handler!(
            self,
            request,
            operation_invocation_receiver,
            SetAlertState,
            SetAlertStateResponse,
            AbstractSetResponse,
            SetAlertStateResponseMsg,
            "set_alert_state",
            SET_ALERT_STATE_RESPONSE,
            set_alert_state
        )
    }

    async fn set_string(
        &self,
        request: Request<SetStringRequest>,
    ) -> Result<Response<SetStringResponse>, Status> {
        operation_handler!(
            self,
            request,
            operation_invocation_receiver,
            SetString,
            SetStringResponse,
            AbstractSetResponse,
            SetStringResponseMsg,
            "set_string",
            SET_STRING_RESPONSE,
            set_string
        )
    }

    async fn set_value(
        &self,
        request: Request<SetValueRequest>,
    ) -> Result<Response<SetValueResponse>, Status> {
        operation_handler!(
            self,
            request,
            operation_invocation_receiver,
            SetValue,
            SetValueResponse,
            AbstractSetResponse,
            SetValueResponseMsg,
            "set_value",
            SET_VALUE_RESPONSE,
            set_value
        )
    }

    type OperationInvokedReportStream = Pin<
        Box<
            dyn Stream<Item = Result<OperationInvokedReportStream, Status>> + Send + Sync + 'static,
        >,
    >;

    async fn operation_invoked_report(
        &self,
        request: Request<OperationInvokedReportRequest>,
    ) -> Result<Response<Self::OperationInvokedReportStream>, Status> {
        let req_addressing = request.get_ref().addressing.as_ref().ok_or_else(|| {
            Status::invalid_argument("operation_invoked_report: Addressing element missing")
        })?;
        let res_addressing = addressing::util::create_reply_to(
            req_addressing,
            action::OPERATION_INVOKED_REPORT_STREAM,
        );

        info!("Received operation invoked report subscription");
        let initial_confirmation = futures::stream::once(async {
            Message::SubscriptionConfirmation(SubscriptionConfirmation {})
        });

        let subscription = self.sco_controller.subscribe().await;
        let second_stream =
            ReceiverStream::new(subscription).map(|r| Message::OperationInvoked(r.into()));
        let response_stream = initial_confirmation
            .chain(second_stream)
            .map(move |result| {
                let mut response_addressing = res_addressing.clone();
                addressing::util::set_random_message_id(&mut response_addressing);

                info!("Sending operation invoked report {:?}", &result);

                Ok(OperationInvokedReportStream {
                    addressing: Some(response_addressing),
                    message: Some(result),
                })
            });

        Ok(Response::new(
            Box::pin(response_stream) as Self::OperationInvokedReportStream
        ))
    }
}

#[tonic::async_trait]
impl MdibReportingService for MdibReportingServiceImpl {
    type EpisodicReportStream =
        Pin<Box<dyn Stream<Item = Result<EpisodicReportStream, Status>> + Send + Sync + 'static>>;

    async fn episodic_report(
        &self,
        request: Request<EpisodicReportRequest>,
    ) -> Result<Response<Self::EpisodicReportStream>, Status> {
        let req_addressing = request.get_ref().addressing.as_ref().ok_or_else(|| {
            Status::invalid_argument("episodic_report: Addressing element missing")
        })?;
        let res_addressing =
            addressing::util::create_reply_to(req_addressing, action::EPISODIC_REPORT_STREAM);

        let _action_filter = match request.get_ref().filter.as_ref() {
            None => None,
            Some(filter) => match filter
                .r#type
                .as_ref()
                .ok_or_else(|| Status::invalid_argument("Empty filter element"))?
            {
                Type::OtherFilter(_) => return Err(Status::invalid_argument("Filter unsupported")),
                Type::HandleFilter(_) => {
                    return Err(Status::invalid_argument("Filter unsupported"))
                }
                Type::ActionFilter(actions) => Some(&actions.action),
            },
        };

        // TODO: for now I don't care about action filters either, sorry

        let initial_confirmation = futures::stream::once(async {
            episodic_report_stream::Message::SubscriptionConfirmation(SubscriptionConfirmation {})
        });

        let gen = self.report_generator.clone();
        let subscription = gen.lock().await.subscribe().await;
        let report_stream = ReceiverStream::new(subscription)
            .map(|r| episodic_report_stream::Message::Report(r.into()));
        let response_stream = initial_confirmation
            .chain(report_stream)
            .map(move |result| {
                let mut response_addressing = res_addressing.clone();
                addressing::util::set_random_message_id(&mut response_addressing);

                Ok(EpisodicReportStream {
                    addressing: Some(response_addressing),
                    message: Some(result),
                })
            });

        Ok(Response::new(
            Box::pin(response_stream) as Self::EpisodicReportStream
        ))
    }

    type PeriodicReportStream =
        Pin<Box<dyn Stream<Item = Result<PeriodicReportStream, Status>> + Send + Sync + 'static>>;

    async fn periodic_report(
        &self,
        _request: Request<PeriodicReportRequest>,
    ) -> Result<Response<Self::PeriodicReportStream>, Status> {
        Err(Status::aborted("Unsupported"))
    }
}

#[async_trait]
impl<T, MDIB, LOC> CommonService for PrimaryProviderServices<T, MDIB, LOC>
where
    T: 'static + OperationInvocationReceiver<MDIB> + Send + Sync + Clone,
    MDIB: 'static + LocalMdibAccess + Send + Sync + LocalMdibAccessReadTransaction + Clone,
    LOC: LocalizationStorage<LocalizedStorageText> + Send + Sync,
{
    async fn start(&mut self) -> Result<(), ServiceError> {
        self.service_delegate.start().await?;
        info!("Starting PrimaryProviderServices");

        let mut services = self.available_services.take().unwrap();
        let config = self.server_config.take().unwrap();
        let (tx, rx) = oneshot::channel();

        let server_crypto = match &config.crypto_config {
            None => None,
            Some(crypto) => Some(
                crypto
                    .try_into()
                    .map_err(|err| ServiceError::Other(anyhow::Error::new(err)))?,
            ),
        };

        self.service_shutdown = Some(tx);

        // reserve address early, so we can update things before losing them to tasks
        info!("Binding to {}", &config.server_address);
        let listener = TcpListener::bind(config.server_address).await.unwrap();
        let addr = listener.local_addr().unwrap();

        match services.metadata_service {
            None => {}
            Some(ref mut ms) => {
                let mut ep = ms.endpoint.lock().await;
                ep.deref_mut().physical_address = vec![match server_crypto.is_some() {
                    true => socket_address_to_uri_https(&addr),
                    false => socket_address_to_uri_http(&addr),
                }];
                let _ = &ms.services.iter_mut().for_each(|service| {
                    service.physical_address = vec![match server_crypto.is_some() {
                        true => socket_address_to_uri_https(&addr),
                        false => socket_address_to_uri_http(&addr),
                    }]
                });
            }
        }

        let mut builder = Server::builder();
        builder = match server_crypto {
            None => builder,
            Some(crypto) => builder
                .tls_config(crypto)
                .map_err(|err| {
                    error!("Error creating tls config: {:?}", err);
                    err
                })
                .expect("An unexpected error occurred while initializing tls"),
        };
        let router = builder
            .add_service(MetadataServiceServer::new(
                services.metadata_service.take().unwrap(),
            ))
            .add_service(GetServiceServer::new(services.get_service.take().unwrap()))
            .add_optional_service(services.set_service.take().map(SetServiceServer::new))
            .add_optional_service(
                services
                    .reporting_service
                    .take()
                    .map(MdibReportingServiceServer::new),
            )
            .add_optional_service(
                services
                    .localization_service
                    .take()
                    .map(LocalizationServiceServer::new),
            );

        self.service_task = Some(tokio::spawn(async move {
            let local_address = listener.local_addr();
            info!("Starting gRPC Server task on {:?}", &local_address);
            router
                .serve_with_incoming_shutdown(TcpListenerStream::new(listener), async {
                    drop(rx.await);
                })
                .await?;
            info!("gRPC Server task stopped on {:?}", &local_address);
            Ok(())
        }));

        info!("Started PrimaryProviderServices");
        self.service_delegate.started().await
    }

    async fn stop(&mut self) -> Result<(), ServiceError> {
        self.service_delegate.stop().await?;
        info!("Stopping PrimaryProviderServices");

        // unwind server
        self.service_shutdown
            .take()
            .unwrap()
            .send(())
            .map_err(|_| ServiceError::StopFailed {
                source: anyhow::Error::msg("gRPC service shutdown sender failed"),
            })?;

        // give it some time to shut down
        tokio::time::sleep(Duration::from_millis(100)).await;

        // drop server
        if self.service_task.take().is_none() {
            error!("service_task was unexpectedly not present")
        };

        info!("Stopped PrimaryProviderServices");
        self.service_delegate.stopped().await
    }

    fn is_running(&self) -> bool {
        self.service_delegate.is_running()
    }

    fn state(&self) -> &ServiceState {
        self.service_delegate.state()
    }
}
