use crate::common::crypto::{dummy_client_config, CryptoConfig, CryptoError};
use crate::common::middleware::uri_changer::UriChanger;
use hyper::http;
use hyper::http::uri::Scheme;
use hyper_rustls::HttpsConnector;
use hyper_util::client::legacy::connect::HttpConnector;
use hyper_util::rt::{TokioExecutor, TokioTimer};
use tonic::body::BoxBody;

pub(crate) type Request = http::Request<BoxBody>;
pub type HyperClient =
    hyper_util::client::legacy::Client<HttpsConnector<UriChanger<HttpConnector>>, BoxBody>;

pub(crate) const DUMMY_URI_HTTPS: &str = "https://example.com";
//noinspection HttpUrlsUsage
pub(crate) const DUMMY_URI_HTTP: &str = "http://example.com";

pub(crate) async fn create_client(
    address: &http::Uri,
    crypto: &Option<CryptoConfig>,
) -> Result<HyperClient, CryptoError> {
    let mut http = HttpConnector::new();
    http.enforce_http(false);

    let tls = match crypto {
        Some(cfg) => cfg.clone().try_into()?,
        None => dummy_client_config(),
    };

    let require_https = crypto.is_some() || address.scheme() == Some(&Scheme::HTTPS);

    // We have to do some wrapping here to map the request type from
    // `https://example.com` -> `https://[::1]:50051` because `rustls`
    // doesn't accept ip's as `ServerName`.
    let connector = tower::ServiceBuilder::new()
        .layer_fn(|s| {
            let tls = tls.clone();

            match require_https {
                true => hyper_rustls::HttpsConnectorBuilder::new()
                    .with_tls_config(tls)
                    .https_only()
                    .enable_http2()
                    .wrap_connector(s),
                false => hyper_rustls::HttpsConnectorBuilder::new()
                    .with_tls_config(tls)
                    .https_or_http()
                    .enable_http2()
                    .wrap_connector(s),
            }
        })
        // Since our cert is signed with `example.com` but we actually want to connect
        // to a local server we will override the Uri passed from the `HttpsConnector`
        // and map it to the correct `Uri` that will connect us directly to the local server.
        .layer_fn(|s| UriChanger::new(s, address.clone()))
        .service(http);

    let http_client = hyper_util::client::legacy::Client::builder(TokioExecutor::new())
        // disable connection pool, we want exactly one connection per host
        .pool_max_idle_per_host(0)
        // keep the connection alive
        .http2_keep_alive_while_idle(true)
        .http2_keep_alive_interval(std::time::Duration::from_secs(10))
        .http2_only(true)
        .timer(TokioTimer::new())
        .build(connector);

    // some checks for the type we expect to receive, compiler removes them
    check_sync(&http_client);
    check_service_request(&http_client);
    Ok(http_client)
}

fn check_sync<T>(_it: &T)
where
    T: Sync,
{
}

fn check_service_request<T>(_it: &T)
where
    T: tower_service::Service<Request>,
{
}

pub(crate) fn get_dummy_uri(uri: &hyper::Uri) -> hyper::Uri {
    match uri.scheme() {
        Some(scheme) => match scheme.as_str() {
            "https" => hyper::Uri::from_static(DUMMY_URI_HTTPS),
            _ => hyper::Uri::from_static(DUMMY_URI_HTTP),
        },
        None => hyper::Uri::from_static(DUMMY_URI_HTTP),
    }
}
