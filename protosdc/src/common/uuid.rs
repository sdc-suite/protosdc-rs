pub fn generate_provider_epr_uuid() -> String {
    format!("urn:uuid:{}", uuid::Uuid::new_v4())
}
