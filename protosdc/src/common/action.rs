use const_format::concatcp;

const DISCOVERY_ACTION_PREFIX: &str = "org.somda.protosdc.discovery.action.";
const GET_ACTION_PREFIX: &str = "org.somda.protosdc.get.action.";
const METADATA_ACTION_PREFIX: &str = "org.somda.protosdc.metadata.action.";
const SET_ACTION_PREFIX: &str = "org.somda.protosdc.set.action.";
const MDIB_REPORTING_ACTION_PREFIX: &str = "org.somda.protosdc.mdib_reporting.action.";
const LOCALIZATION_ACTION_PREFIX: &str = "org.somda.protosdc.localization.action.";

// discovery
pub const SEARCH_REQUEST: &str = concatcp!(DISCOVERY_ACTION_PREFIX, "SearchRequest");
pub const SEARCH_RESPONSE: &str = concatcp!(DISCOVERY_ACTION_PREFIX, "SearchResponse");
pub const HELLO: &str = concatcp!(DISCOVERY_ACTION_PREFIX, "Hello");
pub const BYE: &str = concatcp!(DISCOVERY_ACTION_PREFIX, "Bye");

// metadata service
pub const GET_METADATA_REQUEST: &str = concatcp!(METADATA_ACTION_PREFIX, "GetMetadataRequest");
pub const GET_METADATA_RESPONSE: &str = concatcp!(METADATA_ACTION_PREFIX, "GetMetadataResponse");

// get service
pub const GET_MDIB_REQUEST: &str = concatcp!(GET_ACTION_PREFIX, "GetMdibRequest");
pub const GET_MDIB_RESPONSE: &str = concatcp!(GET_ACTION_PREFIX, "GetMdibResponse");

pub const GET_MD_DESCRIPTION_REQUEST: &str =
    concatcp!(GET_ACTION_PREFIX, "GetMdDescriptionRequest");
pub const GET_MD_DESCRIPTION_RESPONSE: &str =
    concatcp!(GET_ACTION_PREFIX, "GetMdDescriptionResponse");

pub const GET_MD_STATE_REQUEST: &str = concatcp!(GET_ACTION_PREFIX, "GetMdStateRequest");
pub const GET_MD_STATE_RESPONSE: &str = concatcp!(GET_ACTION_PREFIX, "GetMdStateResponse");

pub const GET_CONTEXT_STATES_REQUEST: &str =
    concatcp!(GET_ACTION_PREFIX, "GetContextStatesRequest");
pub const GET_CONTEXT_STATES_RESPONSE: &str =
    concatcp!(GET_ACTION_PREFIX, "GetContextStatesResponse");

// mdib reporting service
pub const EPISODIC_REPORT_REQUEST: &str =
    concatcp!(MDIB_REPORTING_ACTION_PREFIX, "EpisodicReportRequest");
pub const EPISODIC_REPORT_STREAM: &str =
    concatcp!(MDIB_REPORTING_ACTION_PREFIX, "EpisodicReportStream");

// set service
pub const OPERATION_INVOKED_REPORT_REQUEST: &str =
    concatcp!(SET_ACTION_PREFIX, "OperationInvokedReportRequest");
pub const OPERATION_INVOKED_REPORT_STREAM: &str =
    concatcp!(SET_ACTION_PREFIX, "OperationInvokedReportStream");

pub const ACTIVATE_REQUEST: &str = concatcp!(SET_ACTION_PREFIX, "ActivateRequest");
pub const ACTIVATE_RESPONSE: &str = concatcp!(SET_ACTION_PREFIX, "ActivateResponse");

pub const SET_ALERT_STATE_REQUEST: &str = concatcp!(SET_ACTION_PREFIX, "SetAlertStateRequest");
pub const SET_ALERT_STATE_RESPONSE: &str = concatcp!(SET_ACTION_PREFIX, "SetAlertStateResponse");

pub const SET_COMPONENT_STATE_REQUEST: &str =
    concatcp!(SET_ACTION_PREFIX, "SetComponentStateRequest");
pub const SET_COMPONENT_STATE_RESPONSE: &str =
    concatcp!(SET_ACTION_PREFIX, "SetComponentStateResponse");

pub const SET_CONTEXT_STATE_REQUEST: &str = concatcp!(SET_ACTION_PREFIX, "SetContextStateRequest");
pub const SET_CONTEXT_STATE_RESPONSE: &str =
    concatcp!(SET_ACTION_PREFIX, "SetContextStateResponse");

pub const SET_METRIC_STATE_REQUEST: &str = concatcp!(SET_ACTION_PREFIX, "SetMetricStateRequest");
pub const SET_METRIC_STATE_RESPONSE: &str = concatcp!(SET_ACTION_PREFIX, "SetMetricStateResponse");

pub const SET_STRING_REQUEST: &str = concatcp!(SET_ACTION_PREFIX, "SetStringRequest");
pub const SET_STRING_RESPONSE: &str = concatcp!(SET_ACTION_PREFIX, "SetStringResponse");

pub const SET_VALUE_REQUEST: &str = concatcp!(SET_ACTION_PREFIX, "SetValueRequest");
pub const SET_VALUE_RESPONSE: &str = concatcp!(SET_ACTION_PREFIX, "SetValueResponse");

// localization service
pub const GET_SUPPORTED_LANGUAGES_REQUEST: &str =
    concatcp!(LOCALIZATION_ACTION_PREFIX, "GetSupportedLanguagesRequest");
pub const GET_SUPPORTED_LANGUAGES_RESPONSE: &str =
    concatcp!(LOCALIZATION_ACTION_PREFIX, "GetSupportedLanguagesResponse");
pub const GET_LOCALIZED_TEXT_REQUEST: &str =
    concatcp!(LOCALIZATION_ACTION_PREFIX, "GetLocalizedTextRequest");
pub const GET_LOCALIZED_TEXT_STREAM: &str =
    concatcp!(LOCALIZATION_ACTION_PREFIX, "GetLocalizedTextStream");
