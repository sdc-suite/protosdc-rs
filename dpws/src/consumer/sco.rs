use async_trait::async_trait;
use protosdc_biceps::biceps::{
    AbstractSetOneOf, AbstractSetResponseOneOf, ActivateResponse, SetAlertStateResponse,
    SetComponentStateResponse, SetContextStateResponse, SetMetricStateResponse, SetStringResponse,
    SetValueResponse,
};

use biceps::common::biceps_util::SetResponse;
use protosdc::consumer::sco::sco_controller::{SetServiceAccessError, SetServiceHandler};

use crate::soap::soap_client::SoapClient;
use crate::xml::common::SoapHeaderBuilder;
use crate::xml::messages::biceps_ext::{
    BICEPS_ACTION_ACTIVATE, BICEPS_ACTION_SET_ALERT_STATE, BICEPS_ACTION_SET_COMPONENT_STATE,
    BICEPS_ACTION_SET_CONTEXT_STATE, BICEPS_ACTION_SET_METRIC_STATE, BICEPS_ACTION_SET_STRING,
    BICEPS_ACTION_SET_VALUE,
};

#[derive(Clone)]
pub struct DpwsSetServiceHandler<S>
where
    S: SoapClient + Send + Sync,
{
    set_soap_client: S,
    context_soap_client: Option<S>,
}

impl<S> DpwsSetServiceHandler<S>
where
    S: SoapClient + Send + Sync,
{
    pub(crate) fn new(set_soap_client: S, context_soap_client: Option<S>) -> Self {
        Self {
            set_soap_client,
            context_soap_client,
        }
    }
}

#[async_trait]
impl<S> SetServiceHandler for DpwsSetServiceHandler<S>
where
    S: SoapClient + Send + Sync,
{
    async fn invoke(
        &mut self,
        request: AbstractSetOneOf,
    ) -> Result<AbstractSetResponseOneOf, SetServiceAccessError> {
        let response: AbstractSetResponseOneOf = match request {
            AbstractSetOneOf::Activate(msg) => self
                .set_soap_client
                .request_response::<_, ActivateResponse>(
                    SoapHeaderBuilder::new().action(BICEPS_ACTION_ACTIVATE),
                    msg,
                )
                .await
                .map_err(|err| SetServiceAccessError::TransportError {
                    message: format!("{:?}", err),
                })?
                .1
                .into_abstract_set_response_one_of(),
            AbstractSetOneOf::SetAlertState(msg) => self
                .set_soap_client
                .request_response::<_, SetAlertStateResponse>(
                    SoapHeaderBuilder::new().action(BICEPS_ACTION_SET_ALERT_STATE),
                    msg,
                )
                .await
                .map_err(|err| SetServiceAccessError::TransportError {
                    message: format!("{:?}", err),
                })?
                .1
                .into_abstract_set_response_one_of(),
            AbstractSetOneOf::SetComponentState(msg) => self
                .set_soap_client
                .request_response::<_, SetComponentStateResponse>(
                    SoapHeaderBuilder::new().action(BICEPS_ACTION_SET_COMPONENT_STATE),
                    msg,
                )
                .await
                .map_err(|err| SetServiceAccessError::TransportError {
                    message: format!("{:?}", err),
                })?
                .1
                .into_abstract_set_response_one_of(),
            AbstractSetOneOf::SetContextState(msg) => self
                .context_soap_client
                .as_ref()
                .ok_or(SetServiceAccessError::NoContextService)?
                .request_response::<_, SetContextStateResponse>(
                    SoapHeaderBuilder::new().action(BICEPS_ACTION_SET_CONTEXT_STATE),
                    msg,
                )
                .await
                .map_err(|err| SetServiceAccessError::TransportError {
                    message: format!("{:?}", err),
                })?
                .1
                .into_abstract_set_response_one_of(),
            AbstractSetOneOf::SetMetricState(msg) => self
                .set_soap_client
                .request_response::<_, SetMetricStateResponse>(
                    SoapHeaderBuilder::new().action(BICEPS_ACTION_SET_METRIC_STATE),
                    msg,
                )
                .await
                .map_err(|err| SetServiceAccessError::TransportError {
                    message: format!("{:?}", err),
                })?
                .1
                .into_abstract_set_response_one_of(),
            AbstractSetOneOf::SetString(msg) => self
                .set_soap_client
                .request_response::<_, SetStringResponse>(
                    SoapHeaderBuilder::new().action(BICEPS_ACTION_SET_STRING),
                    msg,
                )
                .await
                .map_err(|err| SetServiceAccessError::TransportError {
                    message: format!("{:?}", err),
                })?
                .1
                .into_abstract_set_response_one_of(),
            AbstractSetOneOf::SetValue(msg) => self
                .set_soap_client
                .request_response::<_, SetValueResponse>(
                    SoapHeaderBuilder::new().action(BICEPS_ACTION_SET_VALUE),
                    msg,
                )
                .await
                .map_err(|err| SetServiceAccessError::TransportError {
                    message: format!("{:?}", err),
                })?
                .1
                .into_abstract_set_response_one_of(),
            AbstractSetOneOf::AbstractSet(_) => return Err(SetServiceAccessError::AbstractRequest),
        };
        Ok(response)
    }
}
