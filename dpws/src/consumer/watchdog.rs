use crate::soap::dpws::client::event_sink::{EventSinkError, SubscribeResult};
use crate::soap::dpws::client::hosted_service::HostedService;
use crate::soap::dpws::client::hosting_service::HostingService;
use crate::soap::soap_client::SoapClient;
use async_trait::async_trait;
use common::{Service, ServiceDelegate, ServiceError, ServiceState};
use itertools::Itertools;
use log::{debug, error, info, warn};
use std::collections::HashMap;
use std::marker::PhantomData;
use std::sync::Arc;
use std::time::Duration;
use thiserror::Error;
use tokio::sync::broadcast::{channel, Receiver, Sender};
use tokio::sync::Mutex;
use tokio::task::JoinHandle;
use tokio::time::Instant;

#[derive(Clone, Debug, Error)]
pub enum WatchdogMessage {
    #[error("An error occurred: {0:?}")]
    Any(Arc<anyhow::Error>),
    #[error("Could not find expected hosted service with id {service_id}")]
    ServiceIdUnknown { service_id: String },
    #[error("Granted expires is too small, was {actual:?}, expected at least {expected:?}")]
    GrantedExpiresTooSmall {
        expected: Duration,
        actual: Duration,
    },
    #[error(transparent)]
    EventSinkError(Arc<EventSinkError>),
}

pub struct RemoteDeviceWatchdog<HOSTING, HOSTED, S, HS>
where
    HOSTING: HostingService<HOSTED, S, HS>,
    HOSTED: HostedService<HS>,
    S: SoapClient,
    HS: SoapClient + Send + Sync + Clone,
{
    service_delegate: ServiceDelegate,
    hosting: Arc<Mutex<HOSTING>>,
    subscribe_results: Option<HashMap<String, SubscribeResult>>,
    watchdog_period: Duration,
    requested_expires: Duration,
    watchdog_task: Option<JoinHandle<()>>,
    watchdog_sender: Sender<WatchdogMessage>,
    phantom_hosted: PhantomData<HOSTED>,
    phantom_s: PhantomData<S>,
    phantom_hs: PhantomData<HS>,
}

// TODO: watchdog for devices without subscriptions? Probably not (:
impl<HOSTING, HOSTED, S, HS> RemoteDeviceWatchdog<HOSTING, HOSTED, S, HS>
where
    HOSTING: HostingService<HOSTED, S, HS>,
    HOSTED: HostedService<HS>,
    S: SoapClient,
    HS: SoapClient + Send + Sync + Clone,
{
    pub(crate) fn new(
        hosting: Arc<Mutex<HOSTING>>,
        subscribe_results: HashMap<String, SubscribeResult>,
        watchdog_period: Duration,
        requested_expires: Duration,
    ) -> Self {
        let (tx, _) = channel(10); // TODO: constant for size
        Self {
            service_delegate: Default::default(),
            hosting,
            subscribe_results: Some(subscribe_results),
            watchdog_period,
            requested_expires,
            watchdog_task: None,
            watchdog_sender: tx,
            phantom_hosted: Default::default(),
            phantom_s: Default::default(),
            phantom_hs: Default::default(),
        }
    }

    pub async fn subscribe(&self) -> Receiver<WatchdogMessage> {
        self.watchdog_sender.subscribe()
    }
}

#[async_trait]
impl<HOSTING, HOSTED, S, HS> Service for RemoteDeviceWatchdog<HOSTING, HOSTED, S, HS>
where
    HOSTING: HostingService<HOSTED, S, HS> + Send + Sync + 'static,
    HOSTED: HostedService<HS> + Send + Sync,
    S: SoapClient + Send + Sync,
    HS: SoapClient + Send + Sync + Clone,
{
    async fn start(&mut self) -> Result<(), ServiceError> {
        self.service_delegate.start().await?;
        let subscribe_results_inner = self.subscribe_results.take().unwrap();
        let hosting_inner = self.hosting.clone();
        let watchdog_period = self.watchdog_period;
        let requested_expires = self.requested_expires;
        let sender = self.watchdog_sender.clone();

        info!(
            "Starting watchdog for subscriptions: {}",
            subscribe_results_inner
                .iter()
                .map(|it| it.1.subscription_id.clone())
                .join(", ")
        );

        self.watchdog_task = Some(tokio::spawn(async move {
            let mut interval = tokio::time::interval_at(Instant::now(), watchdog_period);
            'outer: loop {
                let now = interval.tick().await;

                for (service_id, result) in &subscribe_results_inner {
                    // unlock and renew
                    {
                        let hosting = hosting_inner.lock().await;

                        let hosted = match hosting.hosted_services().get(service_id.as_str()) {
                            None => {
                                let message = WatchdogMessage::ServiceIdUnknown {
                                    service_id: service_id.clone(),
                                };
                                error!("{:?}", message);
                                sender.send(message).ok(); // don't care
                                break 'outer;
                            }
                            Some(hosted) => hosted,
                        };

                        debug!("Renewing {} on {}", result.subscription_id, service_id);
                        let result = match hosted
                            .renew(result.subscription_id.as_str(), requested_expires)
                            .await
                        {
                            Ok(it) => it,
                            Err(err) => {
                                let message = WatchdogMessage::EventSinkError(Arc::new(err));
                                error!("{:?}", message);
                                sender.send(message).ok(); // don't care
                                break 'outer;
                            }
                        };
                        if result < watchdog_period {
                            let message = WatchdogMessage::GrantedExpiresTooSmall {
                                actual: result,
                                expected: watchdog_period,
                            };
                            error!("{:?}", message);
                            sender.send(message).ok(); // don't care
                            break 'outer;
                        }
                    }
                }

                let after_run = Instant::now();
                // measure time since this loop tick happened
                let elapsed = after_run.duration_since(now);
                if after_run.duration_since(now).gt(&watchdog_period) {
                    warn!(
                        "Watchdog interval took {}s, period is set at {}s, bursting to catch up",
                        elapsed.as_secs_f32(),
                        watchdog_period.as_secs_f32()
                    )
                }
            }
        }));

        self.service_delegate.started().await
    }

    async fn stop(&mut self) -> Result<(), ServiceError> {
        self.service_delegate.stop().await?;
        if let Some(task) = self.watchdog_task.take() {
            task.abort();
        }

        self.service_delegate.stopped().await
    }

    fn is_running(&self) -> bool {
        self.service_delegate.is_running()
    }

    fn state(&self) -> &ServiceState {
        self.service_delegate.state()
    }
}
