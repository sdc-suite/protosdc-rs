use crate::soap::common::{SoapError, MEDIA_TYPE_SOAP_HEADER, MEDIA_TYPE_SOAP_HEADER_UTF_8};
use crate::xml::common::SoapHeaderBuilder;
use crate::xml::messages::common::SoapEventMessage;
use crate::xml::messages::soap::envelope::{SoapHeader, SoapMessage};
use async_trait::async_trait;
use bytes::Bytes;
use http::{header, Response};
use protosdc_xml::{ComplexXmlTypeWrite, WriterError, XmlWriter};
use std::net::SocketAddr;
use tokio_rustls::rustls::pki_types::CertificateDer;

pub struct TransportInfo {
    // pub scheme: String,
    pub local: SocketAddr,
    pub remote: SocketAddr,
    pub peer_certificates: Option<Vec<CertificateDer<'static>>>,
}

#[async_trait]
pub trait RequestResponseHandlerTyped<M>
where
    M: SoapEventMessage,
{
    async fn request_response(
        &self,
        transport_info: TransportInfo,
        soap_header: SoapHeader,
        body: M,
    ) -> Result<Response<Bytes>, SoapError>;
}

/// Constructs a SOAP response given the SOAP header, the SOAP action, and the body of the response.
///
/// # Arguments
///
/// * `soap_header` - A reference to `SoapHeader` object which contains the header of the SOAP message.
/// * `action` - A static string slice representing the SOAP action.
/// * `body` - A generic parameter `B` which implements the `ComplexXmlTypeWrite` trait,
///            indicating the body of the SOAP message.
///
/// # Returns
///
/// This function returns a `Result` which is `Ok` if the SOAP response could be created successfully.
/// The `Ok` variant wraps a `Bytes` object containing the SOAP response.
/// If there is an error during processing, it returns `Err` wrapping the occurred `WriterError`.
pub(crate) fn create_soap_response<B>(
    soap_header: &SoapHeader,
    action: &'static str,
    body: B,
) -> Result<Bytes, WriterError>
where
    B: ComplexXmlTypeWrite,
{
    let header_builder = SoapHeaderBuilder::new()
        .action(action)
        .relates_to_optional(soap_header.message_id.as_ref().map(|it| it.value.clone()));

    let msg = SoapMessage {
        header: header_builder.build(),
    };

    let mut writer = XmlWriter::new(Vec::new());

    msg.to_xml_complex(&mut writer, body)?;

    let b = bytes::Bytes::from(writer.into_inner());

    Ok(b)
}

/// Create a SOAP HTTP response with specific headers.
///
/// This function constructs a new HTTP response object that includes
/// specific headers required for a SOAP response. It attaches the "Accept"
/// and "Content-Type" headers and sets their values to the media type
/// corresponding to SOAP formatted as UTF-8.
///
/// # Parameters
/// * `body`: A byte sequence that represents the body content of the HTTP response.
///            It's expected that the body content adheres to the SOAP message format.
///
/// # Returns
/// A `Response` object with the body content and the appropriate headers set.
pub(crate) fn create_soap_http_response_with_headers(body: Bytes) -> Response<Bytes> {
    let mut response = Response::new(body);

    response
        .headers_mut()
        .insert(header::ACCEPT, MEDIA_TYPE_SOAP_HEADER.clone());
    response
        .headers_mut()
        .insert(header::CONTENT_TYPE, MEDIA_TYPE_SOAP_HEADER_UTF_8.clone());

    response
}
