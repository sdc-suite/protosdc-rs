use crate::common::extension_handling::ExtensionHandlerFactory;
use crate::soap::common::SoapError;
use crate::soap::dpws::client::event_sink::{EventSink, EventSinkError, SubscribeResult};
use crate::soap::dpws::client::notification_sink::NotificationSink;
use crate::soap::soap_client::SoapClient;
use crate::xml::common::{Others, SoapHeaderBuilder};
use crate::xml::messages::addressing::WsaEndpointReference;
use crate::xml::messages::common::SoapElement;
use crate::xml::messages::dpws::host::Hosted;
use crate::xml::messages::soap::envelope::SoapMessage;
use crate::xml::messages::ws_eventing::common::Filter;
use async_trait::async_trait;
use bytes::Bytes;
use log::{error, warn};
use protosdc_xml::ComplexXmlTypeWrite;
use std::time::Duration;

#[async_trait]
pub trait HostedService<S>: SoapClient + EventSink
where
    S: SoapClient + Send + Sync + Clone,
{
    fn service_type(&self) -> &Hosted;

    fn soap_client(&self) -> &S;

    fn epr(&self) -> &WsaEndpointReference;

    async fn disconnect(self);
}

pub struct HostedServiceImpl<S, E, EHF>
where
    S: SoapClient,
    E: EventSink,
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    pub(crate) soap_client: S,
    pub(crate) hosted: Hosted,
    pub(crate) event_sink: E,
    pub(crate) epr: WsaEndpointReference,
    pub(crate) _extension_handler_factory: EHF,
}

impl<S, E, EHF> HostedServiceImpl<S, E, EHF>
where
    S: SoapClient,
    E: EventSink,
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    pub(crate) fn new(
        soap_client: S,
        epr: WsaEndpointReference,
        hosted: Hosted,
        event_sink: E,
        extension_handler_factory: EHF,
    ) -> Self {
        Self {
            soap_client,
            hosted,
            event_sink,
            epr,
            _extension_handler_factory: extension_handler_factory,
        }
    }
}

#[async_trait]
impl<S, E, EHF> EventSink for HostedServiceImpl<S, E, EHF>
where
    E: EventSink + Sync + Send,
    S: SoapClient + Sync + Send,
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    async fn subscribe<NS>(
        &self,
        filter: Option<Filter>,
        duration: Duration,
        sink: NS,
    ) -> Result<SubscribeResult, EventSinkError>
    where
        NS: NotificationSink + Sync + Send + 'static,
    {
        self.event_sink.subscribe(filter, duration, sink).await
    }

    async fn renew(
        &self,
        subscription_id: &str,
        duration: Duration,
    ) -> Result<Duration, EventSinkError> {
        self.event_sink.renew(subscription_id, duration).await
    }

    async fn get_status(&self, subscription_id: &str) -> Result<Duration, EventSinkError> {
        self.event_sink.get_status(subscription_id).await
    }

    async fn unsubscribe(&self, subscription_id: &str) -> Result<(), EventSinkError> {
        self.event_sink.unsubscribe(subscription_id).await
    }

    async fn unsubscribe_all(&self) -> Result<(), EventSinkError> {
        self.event_sink.unsubscribe_all().await
    }
}

// TODO: Why does EventSink need to be safe, it doesn't even matter...
#[async_trait]
impl<S, E, EHF> SoapClient for HostedServiceImpl<S, E, EHF>
where
    E: EventSink + Sync + Send,
    S: SoapClient + Sync + Send,
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    async fn request_response<REQ, RESP>(
        &self,
        header_builder: SoapHeaderBuilder<Others>,
        body: REQ,
    ) -> Result<(SoapMessage, RESP), SoapError>
    where
        REQ: ComplexXmlTypeWrite + Send + Sync,
        RESP: SoapElement + Send + Sync,
    {
        self.soap_client
            .request_response(header_builder, body)
            .await
    }

    async fn request_response_opt_body<REQ, RESP>(
        &self,
        header_builder: SoapHeaderBuilder<Others>,
        body: REQ,
    ) -> Result<(SoapMessage, Option<RESP>), SoapError>
    where
        REQ: ComplexXmlTypeWrite + Send + Sync,
        RESP: SoapElement + Send + Sync,
    {
        self.soap_client
            .request_response_opt_body(header_builder, body)
            .await
    }

    async fn notify_serialized(&self, _body: Bytes) -> Result<(), SoapError> {
        // no notifications for hosted service
        error!("No notifications for hosting service, use subscription manager");
        Err(SoapError::TransportError)
    }

    async fn request_response_opt_body_serialized<RESP>(
        &self,
        body: Bytes,
    ) -> Result<(SoapMessage, Option<RESP>), SoapError>
    where
        RESP: SoapElement + Send + Sync,
    {
        self.soap_client
            .request_response_opt_body_serialized(body)
            .await
    }

    fn target_url(&self) -> &str {
        self.soap_client.target_url()
    }
}

#[async_trait]
impl<S, E, EHF> HostedService<S> for HostedServiceImpl<S, E, EHF>
where
    S: SoapClient + Sync + Send + Clone,
    E: EventSink + Send + Sync,
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    fn service_type(&self) -> &Hosted {
        &self.hosted
    }

    fn soap_client(&self) -> &S {
        &self.soap_client
    }

    fn epr(&self) -> &WsaEndpointReference {
        &self.epr
    }

    async fn disconnect(self) {
        if let Err(err) = self.unsubscribe_all().await {
            warn!("Could not unsubscribe from device: {}", err)
        }
    }
}
