use crate::http::server::common::ContextMessage;
use crate::soap::common::SoapError;
use crate::soap::eventing::ComplexXmlTypeReadInner;
use crate::xml::messages::common::SoapEventMessage;
use async_trait::async_trait;
use protosdc_xml::ParserError;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum NotificationSinkError {
    #[error(transparent)]
    SoapError(#[from] SoapError),
    #[error("Subscription ending: {status}")]
    SubscriptionEnd { status: String },
}

impl From<ParserError> for NotificationSinkError {
    fn from(value: ParserError) -> Self {
        let sr: SoapError = value.into();
        sr.into()
    }
}

#[async_trait]
pub trait NotificationSink {
    async fn receive_notification(
        &self,
        notification: ContextMessage,
    ) -> Result<(), NotificationSinkError>;
}

#[async_trait]
pub trait NotificationSinkTyped<M>
where
    M: ComplexXmlTypeReadInner + SoapEventMessage,
{
    async fn receive_notification(&self, notification: M) -> Result<(), NotificationSinkError>;
}
