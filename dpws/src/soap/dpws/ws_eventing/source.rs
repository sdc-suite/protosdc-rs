use crate::common::extension_handling::ExtensionHandlerFactory;
use crate::http::client::common::HttpClient;
use crate::http::client::reqwest::ReqwestClientAccess;
use crate::http::server::axum::{AxumRegistryAccess, AxumServerAccess};
use crate::http::server::common::{
    BoxBody, ContextMessage, ContextMessageResponse, RegistryError, Server, ServerError,
    ServerRegistry,
};
use crate::soap::common::{
    create_soap_http_message, simple_sender_fault, AcceptableContentType, RequestResponseHandler,
    SoapError, SOAP_FAULT_ACTION_STR, WSE_ACTION_GET_STATUS_RESPONSE_STR,
    WSE_ACTION_RENEW_RESPONSE_STR, WSE_ACTION_SUBSCRIBE_RESPONSE_STR,
    WSE_ACTION_UNSUBSCRIBE_RESPONSE_STR,
};
use crate::soap::dpws::device::hosted_service::{hosted_service_handle, HostedServiceInfo};
use crate::soap::dpws::subscription_manager::{
    SourceSubscriptionManager, SourceSubscriptionManagerAccess,
    SourceSubscriptionManagerAccessImpl, SourceSubscriptionManagerImpl, SubscriptionManager,
    WsEventingStatus,
};
use crate::soap::eventing::{ComplexXmlTypeReadInner, WsEventingRequest};
use crate::soap::soap_server::{RequestResponseHandlerTyped, TransportInfo};
use crate::xml::common::SoapHeaderBuilder;
use crate::xml::messages::addressing::WsaEndpointReference;
use crate::xml::messages::common::{
    SoapElement, SoapEventMessage, DPWS_ACTION_DIALECT, SOAP_ENVELOPE_TAG, WSE_DELIVERY_MODE_PUSH,
};
use crate::xml::messages::duration::XsdDuration;
use crate::xml::messages::soap::envelope::{SoapHeader, SoapMessage};
use crate::xml::messages::soap::fault::Fault;
use crate::xml::messages::ws_eventing::common::FilterContent;
use crate::xml::messages::ws_eventing::renew::{
    GetStatusResponse, Renew, RenewResponse, UnsubscribeResponse,
};
use crate::xml::messages::ws_eventing::subscribe::Subscribe;
use crate::xml::messages::ws_eventing::subscribe_response::SubscribeResponse;
use async_trait::async_trait;
use bytes::{Buf, Bytes};
use futures::{stream, StreamExt};
use http::{header, Request, Response, StatusCode};
use http_body_util::BodyExt;
use log::{debug, error, info, warn};
use protosdc_xml::{
    find_start_element_reader, ComplexXmlTypeWrite, GenericXmlReaderComplexTypeRead, ParserError,
    XmlReader, XmlWriter,
};
use quick_xml::events::Event;
use std::collections::HashMap;
use std::fmt::Debug;
use std::marker::PhantomData;
use std::sync::Arc;
use std::time::Duration;
use tokio::sync::{oneshot, RwLock};
use uuid::Uuid;

pub const MAX_EXPIRES: Duration = Duration::from_secs(15);

pub struct WsEventingSource<H, M, E, I, EHF>
where
    H: RequestResponseHandlerTyped<M>,
    M: SoapEventMessage + ComplexXmlTypeReadInner,
    E: EventSource + RequestResponseHandlerTyped<Subscribe>,
    I: HostedServiceInfo + 'static,
    EHF: ExtensionHandlerFactory + Send + Sync,
{
    pub(crate) phantom: PhantomData<M>,
    pub(crate) event_handler: H,
    pub(crate) event_source: E,
    pub(crate) host_metadata: I,
    pub(crate) extension_handler: EHF,
}

pub struct WsEventingSourceHandler {}

impl<H, M, E, I, EHF> WsEventingSource<H, M, E, I, EHF>
where
    H: RequestResponseHandlerTyped<M>,
    M: SoapEventMessage + ComplexXmlTypeReadInner,
    E: EventSource + RequestResponseHandlerTyped<Subscribe>,
    I: HostedServiceInfo + 'static,
    EHF: ExtensionHandlerFactory + Send + Sync,
{
    pub fn new(
        event_handler: H,
        event_source: E,
        host_metadata: I,
        extension_handler: EHF,
    ) -> Self {
        Self {
            phantom: Default::default(),
            event_handler,
            event_source,
            host_metadata,
            extension_handler,
        }
    }
}

#[async_trait]
impl<H, M, E, I, EHF> RequestResponseHandler for WsEventingSource<H, M, E, I, EHF>
where
    H: RequestResponseHandlerTyped<M> + Send + Sync,
    M: SoapEventMessage + ComplexXmlTypeReadInner + Sync + Send,
    E: EventSource + RequestResponseHandlerTyped<Subscribe> + Sync + Send,
    I: HostedServiceInfo + 'static + Send + Sync,
    EHF: ExtensionHandlerFactory + Send + Sync,
{
    async fn process_message(&self, context: ContextMessage) -> Result<(), SoapError> {
        hosted_service_handle(
            &self.event_handler,
            &self.event_source,
            context,
            &self.host_metadata,
            self.extension_handler.clone(),
        )
        .await
    }
}

#[derive(Debug)]
pub enum NotificationSourceStatus {
    TimedOut,
    DeliveryFailed,
}

#[derive(Debug)]
struct SubscriptionRegistryInner<SOURCE>
where
    SOURCE: SourceSubscriptionManager,
{
    subscription_managers: HashMap<String, SOURCE>,
    action_map: HashMap<String, Vec<String>>,
}

impl<SOURCE> Default for SubscriptionRegistryInner<SOURCE>
where
    SOURCE: SourceSubscriptionManager,
{
    fn default() -> Self {
        Self {
            subscription_managers: HashMap::default(),
            action_map: HashMap::default(),
        }
    }
}

#[derive(Clone, Debug)]
pub struct ActionBasedSubscriptionRegistry<SOURCE>
where
    SOURCE: SourceSubscriptionManager,
{
    inner: Arc<RwLock<SubscriptionRegistryInner<SOURCE>>>,
}

impl<SOURCE> ActionBasedSubscriptionRegistry<SOURCE>
where
    SOURCE: SourceSubscriptionManager,
{
    fn new() -> Self {
        Self {
            inner: Arc::new(RwLock::new(Default::default())),
        }
    }
}

impl<SOURCE> ActionBasedSubscriptionRegistry<SOURCE>
where
    SOURCE: SourceSubscriptionManager,
{
    async fn add_subscription(&self, subscription_manager: SOURCE, actions: Vec<String>) {
        let mut inner = self.inner.write().await;

        let sub_id = subscription_manager.subscription_id().await.to_string();
        inner
            .subscription_managers
            .insert(sub_id.clone(), subscription_manager);

        for action in actions {
            let entry = inner.action_map.entry(action).or_default();
            entry.push(sub_id.clone());
        }
    }

    async fn get_subscription_ids_for_action(&self, action: &str) -> Vec<String> {
        self.inner
            .read()
            .await
            .action_map
            .get(action)
            .cloned()
            .unwrap_or_default()
    }

    async fn get_subscription(
        &self,
        subscription_id: &str,
    ) -> Option<SourceSubscriptionManagerAccessImpl> {
        self.inner
            .read()
            .await
            .subscription_managers
            .get(subscription_id)
            .map(|it| it.access())
    }

    async fn remove_subscription(&self, subscription_id: &str) -> Option<SOURCE> {
        let mut inner = self.inner.write().await;

        inner.action_map.iter_mut().for_each(|it| {
            it.1.retain(|elem| elem != subscription_id);
        });

        inner.subscription_managers.remove(subscription_id)
    }

    async fn get_subscriptions(&self) -> HashMap<String, SourceSubscriptionManagerAccessImpl> {
        self.inner
            .read()
            .await
            .subscription_managers
            .iter()
            .map(|it| (it.0.clone(), it.1.access()))
            .collect()
    }
}

#[async_trait]
pub trait EventSource {
    async fn send_notification<M>(&self, action: &'static str, payload: M) -> Result<(), SoapError>
    where
        M: SoapElement + Send + Sync;

    async fn subscription_end_all(&self, status: WsEventingStatus);

    async fn get_active_subscriptions(&self) -> Vec<SourceSubscriptionManagerAccessImpl>;
}

pub type DefaultEventSourceImpl<EHF> = ActionBasedEventSourceImpl<
    SourceSubscriptionManagerImpl,
    AxumRegistryAccess,
    AxumServerAccess,
    ReqwestClientAccess,
    EHF,
>;

pub struct ActionBasedEventSourceImpl<SOURCE, R, S, H, EHF>
where
    SOURCE: SourceSubscriptionManager,
    R: ServerRegistry<S>,
    S: Server,
    H: HttpClient,
    EHF: ExtensionHandlerFactory + Send + Sync,
{
    phantom_s: PhantomData<S>,
    subscription_registry: ActionBasedSubscriptionRegistry<SOURCE>,
    registry_access: R,
    server_handler: S,
    context_path_map: Arc<RwLock<HashMap<String, String>>>,
    http_client: H,
    extension_handler_factory: EHF,
}

impl<SOURCE, R, S, H, EHF> ActionBasedEventSourceImpl<SOURCE, R, S, H, EHF>
where
    SOURCE: SourceSubscriptionManager + Send + Sync,
    R: ServerRegistry<S> + Send + Sync,
    S: Server + Send + Sync,
    H: HttpClient,
    EHF: ExtensionHandlerFactory + Send + Sync,
{
    pub async fn new(
        registry: R,
        // the handler used for this event source
        server_handler: S,
        http_client: H,
        extension_handler_factory: EHF,
    ) -> Result<Self, ServerError> {
        Ok(Self {
            phantom_s: Default::default(),
            subscription_registry: ActionBasedSubscriptionRegistry::new(),
            // TODO: not really needed, right?
            registry_access: registry,
            server_handler,
            context_path_map: Arc::new(RwLock::new(Default::default())),
            http_client,
            extension_handler_factory,
        })
    }
}

impl<SOURCE, R, S, H, EHF> ActionBasedEventSourceImpl<SOURCE, R, S, H, EHF>
where
    SOURCE: SourceSubscriptionManager + Send + Sync,
    R: ServerRegistry<S> + Send + Sync,
    S: Server + Send + Sync,
    H: HttpClient,
    EHF: ExtensionHandlerFactory + Send + Sync,
{
    async fn unregister_http_handler(&self, subscription_id: &str) {
        if let Some(context_path) = self.context_path_map.write().await.remove(subscription_id) {
            self.server_handler
                .unregister_context_path(&context_path)
                .await
                .map_err(|err| {
                    error!("Error unregistering context path {}", err);
                    err
                })
                .ok(); // don't care
        }
    }

    async fn remove_stale_subscriptions(&self) {
        for (subscription_id, sub_man) in self.subscription_registry.get_subscriptions().await {
            if let Err(err) = sub_man.manager_status().await {
                info!("Removing subscription manager {}: {}", subscription_id, err);
                self.subscription_registry
                    .remove_subscription(&subscription_id)
                    .await;
                self.unregister_http_handler(&subscription_id).await;
            }
        }
    }
}

#[async_trait]
impl<SOURCE, R, S, H, EHF> EventSource for ActionBasedEventSourceImpl<SOURCE, R, S, H, EHF>
where
    SOURCE: SourceSubscriptionManager + Send + Sync,
    R: ServerRegistry<S> + Send + Sync,
    S: Server + Send + Sync,
    H: HttpClient + Send + Sync,
    EHF: ExtensionHandlerFactory + Send + Sync,
{
    async fn send_notification<M>(&self, action: &'static str, payload: M) -> Result<(), SoapError>
    where
        M: SoapElement + Send + Sync,
    {
        self.remove_stale_subscriptions().await;

        let affected_subscriptions = self
            .subscription_registry
            .get_subscription_ids_for_action(action)
            .await;

        if affected_subscriptions.is_empty() {
            debug!("No subscribers for {}, bailing", action);
            return Ok(());
        }

        // serialize body
        let mut writer = XmlWriter::new_no_declaration(vec![]);
        payload.to_xml_complex(Some(M::tag_str()), &mut writer, false)?;

        let content = writer.into_inner();
        let bytes_data = bytes::Bytes::from(content);

        // parallel send all the messages
        stream::iter(affected_subscriptions)
            .filter_map(|it| async move { self.subscription_registry.get_subscription(&it).await })
            .for_each(|it| {
                // actually doesn't clone the data, only the view on it
                let b = bytes_data.clone();
                async move { it.send_notification(action, b).await }
            })
            .await;

        Ok(())
    }

    async fn subscription_end_all(&self, status: WsEventingStatus) {
        self.remove_stale_subscriptions().await;

        stream::iter(self.subscription_registry.get_subscriptions().await)
            .for_each(|it| {
                let s = status.clone();
                async move { it.1.send_end_to(s).await }
            })
            .await;
    }

    async fn get_active_subscriptions(&self) -> Vec<SourceSubscriptionManagerAccessImpl> {
        self.subscription_registry
            .get_subscriptions()
            .await
            .into_iter()
            .map(|it| it.1)
            .collect()
    }
}

fn get_max_expires(expires: &Option<XsdDuration>) -> Duration {
    match expires {
        None => MAX_EXPIRES,
        Some(it) => {
            let converted: Duration = it.clone().into();
            if converted > MAX_EXPIRES {
                MAX_EXPIRES
            } else {
                converted
            }
        }
    }
}

#[async_trait]
impl<SOURCE, R, S, H, EHF> RequestResponseHandlerTyped<Subscribe>
    for ActionBasedEventSourceImpl<SOURCE, R, S, H, EHF>
where
    SOURCE: SourceSubscriptionManager + Send + Sync,
    R: ServerRegistry<S> + Send + Sync,
    S: Server + Send + Sync,
    H: HttpClient + Clone + Send + Sync + 'static,
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    async fn request_response(
        &self,
        transport_info: TransportInfo,
        soap_header: SoapHeader,
        mut body: Subscribe,
    ) -> Result<Response<Bytes>, SoapError> {
        // validate delivery mode
        if body.delivery.mode.as_deref() != Some(WSE_DELIVERY_MODE_PUSH)
            && body.delivery.mode.as_deref().is_some()
        {
            error!("Unsupported delivery mode {:?}", body.delivery.mode);
            return Err(SoapError::SoapFault(simple_sender_fault(format!(
                "Delivery mode {:?} unsupported",
                body.delivery.mode
            ))));
        }

        // validate delivery endpoint
        if body.delivery.notify_to.len() != 1 {
            error!(
                "Unsupported number of notify to elements: {:#?}",
                body.delivery.notify_to
            );
            return Err(SoapError::SoapFault(simple_sender_fault(format!(
                "Unsupported number of notify to elements: {:#?}",
                body.delivery.notify_to
            ))));
        }

        let notify_to = body
            .delivery
            .notify_to
            .pop()
            .expect("Can't be missing")
            .endpoint_reference;
        let notify_to_uri =
            http::Uri::try_from(notify_to.address.value.clone()).map_err(|err| {
                SoapError::SoapFault(simple_sender_fault(format!(
                    "Could not convert notify to address to uri: {:?}",
                    err
                )))
            })?;

        let expires = get_max_expires(&body.expires);

        // validate filter
        let filter_actions = match body.filter.as_ref() {
            None => {
                return Err(SoapError::SoapFault(simple_sender_fault(
                    "Filter required".to_string(),
                )))
            }
            Some(f) => match f.dialect.as_deref() {
                Some(DPWS_ACTION_DIALECT) => match f.filters.as_ref() {
                    None => vec!["".to_string()],
                    Some(f) => match f {
                        FilterContent::Element(_) => {
                            return Err(SoapError::SoapFault(simple_sender_fault(
                                "Filter dialect does not support element content",
                            )))
                        }
                        FilterContent::Text(text) => {
                            text.split(' ').map(|it| it.to_string()).collect()
                        }
                    },
                },
                it => {
                    return Err(SoapError::SoapFault(simple_sender_fault(format!(
                        "Filter dialect unsupported: {:?}",
                        it
                    ))))
                }
            },
        };

        // create subscription manager epr and register handler
        let (mut subscription_channel, subscription_manager_epr) = {
            let modified_addr = transport_info.local;
            let server = self.registry_access.init_server(modified_addr).await?;

            let context_path = format!("subscription_manager/{}", Uuid::new_v4());
            let registration_result = server
                .register_context_path(&context_path)
                .await
                .map_err(RegistryError::ServerError)?;

            let epr = WsaEndpointReference::builder()
                .address(registration_result.address.address.to_string())
                .build();

            (registration_result.channel, epr)
        };

        // create subscription manager
        let subscription_manager = SOURCE::new(
            subscription_manager_epr.clone(),
            expires,
            notify_to,
            body.end_to,
            body.filter,
            self.http_client.clone(),
            self.extension_handler_factory.clone(),
        )
        .await;

        info!(
            "Created new subscription manager {} for filter {:?}, notify to address is {}, expires {}s",
            subscription_manager.subscription_id().await,
            subscription_manager.filter().await,
            notify_to_uri,
            expires.as_secs_f32()
        );

        let task_access = subscription_manager.access();

        let epr = subscription_manager.manager_epr().await;
        let expires = subscription_manager.expires().await;

        self.subscription_registry
            .add_subscription(subscription_manager, filter_actions)
            .await;

        let task_ehf = self.extension_handler_factory.clone();
        // spawn task for processing subscription manager messages
        tokio::spawn(async move {
            while let Some(context_msg) = subscription_channel.recv().await {
                let (soap_message, ws_eventing_msg) =
                    match handle_parse_subscription_manager_message(
                        context_msg.request,
                        task_ehf.clone(),
                    )
                    .await
                    {
                        Ok(it) => it,
                        Err(err) => {
                            error!("Could not parse inbound message: {:}", err);
                            let fault: Fault = err.into();
                            send_oneshot_response(context_msg.response_sender, fault.into());
                            continue;
                        }
                    };

                let response = match ws_eventing_msg {
                    WsEventingRequest::Subscribe(_) => {
                        let err = simple_sender_fault(
                            "Subscribe unsupported at subscription manager".to_string(),
                        );
                        create_response(soap_message.header, err, SOAP_FAULT_ACTION_STR)
                    }
                    WsEventingRequest::GetStatus(_msg) => {
                        let expires = task_access.expires().await;

                        let body = GetStatusResponse {
                            get_status_response: Renew {
                                attributes: Default::default(),
                                expires: Some(expires.into()),
                                children: vec![],
                            },
                        };
                        create_response(
                            soap_message.header,
                            body,
                            WSE_ACTION_GET_STATUS_RESPONSE_STR,
                        )
                    }
                    WsEventingRequest::Renew(msg) => {
                        let new_duration = get_max_expires(&msg.expires);
                        task_access.process_renew(new_duration).await;

                        let body = RenewResponse {
                            renew_response: Renew {
                                attributes: Default::default(),
                                expires: Some(new_duration.into()),
                                children: vec![],
                            },
                        };
                        create_response(soap_message.header, body, WSE_ACTION_RENEW_RESPONSE_STR)
                    }
                    WsEventingRequest::Unsubscribe(_msg) => {
                        task_access
                            .send_end_to(WsEventingStatus::StatusSourceShuttingDown)
                            .await;

                        let body = UnsubscribeResponse {};
                        create_response(
                            soap_message.header,
                            body,
                            WSE_ACTION_UNSUBSCRIBE_RESPONSE_STR,
                        )
                    }
                };

                let status_code = match response {
                    Ok(_) => StatusCode::OK,
                    Err(_) => StatusCode::BAD_REQUEST,
                };

                match response {
                    Ok(it) => {
                        let payload = create_soap_http_message(it);
                        send_oneshot_response(
                            context_msg.response_sender,
                            ContextMessageResponse {
                                status_code,
                                payload,
                            },
                        );
                    }
                    Err(it) => {
                        let fault: Fault = it.into();
                        send_oneshot_response(context_msg.response_sender, fault.into());
                    }
                };
            }
        });

        info!("Spawned processing task");

        let subscribe_response = SubscribeResponse {
            attributes: Default::default(),
            subscription_manager: epr,
            expires: expires.into(),
            children: vec![],
        };

        let response_bytes = create_response(
            soap_header,
            subscribe_response,
            WSE_ACTION_SUBSCRIBE_RESPONSE_STR,
        )?;

        info!("Sending SubscribeResponse");

        Ok(create_soap_http_message(response_bytes))
    }
}

fn create_response<M: ComplexXmlTypeWrite>(
    request: SoapHeader,
    response_body: M,
    action: &str,
) -> Result<Bytes, SoapError> {
    let header_builder = SoapHeaderBuilder::new()
        .action(action)
        .relates_to_optional(request.message_id.map(|it| it.value));

    let response = SoapMessage {
        header: header_builder.build(),
    };

    let mut writer = XmlWriter::new(vec![]);
    response.to_xml_complex(&mut writer, response_body)?;

    Ok(bytes::Bytes::from(writer.into_inner()))
}

async fn handle_parse_subscription_manager_message<EHF: ExtensionHandlerFactory + Send + Sync>(
    request: Request<BoxBody>,
    extension_handler_factory: EHF,
) -> Result<(SoapMessage, WsEventingRequest), SoapError> {
    let (parts, body) = request.into_parts();

    // validate content type
    let _content_type: AcceptableContentType = parts
        .headers
        .get(header::CONTENT_TYPE)
        .ok_or(SoapError::InvalidContentType)?
        .try_into()?;

    // start by parsing soap envelope and header
    let body_vec = body
        .collect()
        .await
        .map_err(|_| SoapError::ResponseBodyMissing)?
        .to_bytes();
    let mut buf = Vec::with_capacity(body_vec.len());
    let mut reader =
        XmlReader::create_custom(body_vec.reader(), extension_handler_factory.create());

    let soap_message: SoapMessage =
        find_start_element_reader!(SoapMessage, SOAP_ENVELOPE_TAG, reader, buf)?;

    // dispatch based on body
    loop {
        match reader.read_next(&mut buf) {
            Ok((ref ns, Event::Start(ref e))) => {
                let m_ns = WsEventingRequest::ns_tag();
                break match (ns, e.local_name()) {
                    (x, _) if x == &m_ns => {
                        let msg = WsEventingRequest::from_xml_complex(m_ns, e, &mut reader)?;

                        return Ok((soap_message, msg));
                    }
                    (_, _) => Err(ParserError::UnexpectedParserStartState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: "EventSinkHandler".to_string(),
                    })?,
                };
            }
            Ok((ref _ns, Event::DocType(ref _e))) => {}
            Ok((ref _ns, Event::Decl(ref _e))) => {}
            Ok((ref _ns, Event::Comment(ref _e))) => {}
            other => Err(SoapError::ParserError(ParserError::UnexpectedParserEvent {
                event: format!("{:?}", other),
            }))?,
        }
    }

    Err(SoapError::ResponseBodyMissing) // TODO: Better error
}

fn send_oneshot_response<T: Debug>(sender: oneshot::Sender<T>, t: T) {
    let send_result = sender.send(t);
    if let Err(err) = send_result {
        warn!("Could not send message: {:?}", err);
    }
}
