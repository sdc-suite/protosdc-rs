use crate::common::extension_handling::ExtensionHandlerFactory;
use crate::http::client::common::HttpClient;
use crate::soap::common::{
    SoapError, WSE_ACTION_SUBSCRIPTION_END_STR, WSE_STATUS_DELIVERY_FAILURE,
    WSE_STATUS_SOURCE_CANCELLING, WSE_STATUS_SOURCE_SHUTTING_DOWN,
};
use crate::soap::soap_client::{SoapClient, SoapClientImpl};
use crate::xml::common::create_header_for_action_ref_param;
use crate::xml::messages::addressing::{WsaAttributedURIType, WsaEndpointReference};
use crate::xml::messages::soap::envelope::SoapMessage;
use crate::xml::messages::ws_eventing::common::Filter;
use crate::xml::messages::ws_eventing::subscription_end::SubscriptionEnd;
use async_trait::async_trait;
use bytes::Bytes;
use common::{oneshot_log_error, send_log_error};
use log::{debug, error, info, warn};
use protosdc_xml::XmlWriter;
use std::sync::Arc;
use std::time::{Duration, Instant};
use thiserror::Error;
use tokio::sync::mpsc::{channel, Sender};
use tokio::sync::{oneshot, RwLock};
use tokio::task::JoinHandle;
use uuid::Uuid;

pub const DEFAULT_CHANNEL_SIZE: usize = 10;

#[derive(Clone, Debug)]
pub enum WsEventingStatus {
    // failed to deliver message
    StatusSourceDeliveryFailure,
    // controlled shutdown
    StatusSourceShuttingDown,
    // terminated by source but not because expired
    StatusSourceCancelling,
}

impl WsEventingStatus {
    fn to_status(&self) -> String {
        match self {
            WsEventingStatus::StatusSourceDeliveryFailure => {
                WSE_STATUS_DELIVERY_FAILURE.to_string()
            }
            WsEventingStatus::StatusSourceShuttingDown => {
                WSE_STATUS_SOURCE_SHUTTING_DOWN.to_string()
            }
            WsEventingStatus::StatusSourceCancelling => WSE_STATUS_SOURCE_CANCELLING.to_string(),
        }
    }
}

#[derive(Clone, Debug, Error)]
pub enum SourceSubscriptionManagerStatus {
    #[error("The subscription was not renewed in time and timed out")]
    TimedOut,
    #[error("Delivery of a notification failed")]
    DeliveryFailed,
    #[error("The subscription was ended")]
    Ended,
}

/// Represents a subscription manager.
#[async_trait]
pub trait SubscriptionManager {
    /// Returns the subscription ID associated with this subscription manager.
    async fn subscription_id(&self) -> String;

    /// Returns the last granted expires.
    ///
    /// Note that this is **not** the remaining time, only the time granted in either
    /// the `SubscribeResponse` or the last `RenewResponse`
    async fn expires(&self) -> Duration;

    /// Returns the remaining time on the subscription. If the subscription has expired,
    /// it will return `Duration::ZERO`.
    async fn remaining(&self) -> Duration;

    /// Returns the Web Services Addressing (WS-Addressing) endpoint reference to which
    /// notifications should be sent for this subscription.
    async fn notify_to(&self) -> WsaEndpointReference;

    /// Returns the Web Services Addressing (WS-Addressing) endpoint reference to which
    /// EndTo messages should be sent for this subscription. If no EndTo is present, use `notify_to`.
    async fn end_to(&self) -> Option<WsaEndpointReference>;

    /// Returns the filter that applies to the managed subscription.
    async fn filter(&self) -> Option<Filter>;

    /// Returns the Web Services Addressing (WS-Addressing) endpoint reference that identifies the
    /// subscription manager endpoint reference for this subscription.
    async fn manager_epr(&self) -> WsaEndpointReference;
}

#[async_trait]
pub trait SinkSubscriptionManager: SubscriptionManager {
    fn new(
        subscription_manager: WsaEndpointReference,
        expires: Duration,
        notify_to: WsaEndpointReference,
        end_to: Option<WsaEndpointReference>,
        filter: Option<Filter>,
        subscription_id: String,
    ) -> Self;

    /// Renews a subscription by `duration` time.
    async fn process_renew(&self, duration: Duration);
}

#[async_trait]
pub trait SourceSubscriptionManagerAccess {
    /// Renews a subscription by `duration` time.
    async fn process_renew(&self, duration: Duration);

    /// Sends out the notification.
    ///
    /// Depending on implementation, this might add the notification to a delivery queue and not
    /// immediately send it.
    ///
    /// # Arguments
    ///
    /// * `action`: of the notification to send
    /// * `body`: the content of the body to send out, **not** a serialized SoapBody
    ///
    /// returns: ()
    async fn send_notification(&self, action: &'static str, soap_body: Bytes);

    /// Returns the status of the source subscription manager.
    ///
    /// The result will be Ok if the manager is healthy and an Err if the subscription
    /// is stale.
    ///
    /// returns: Result<(), SourceSubscriptionManagerStatus>
    async fn manager_status(&self) -> Result<(), SourceSubscriptionManagerStatus>;

    /// Sends an end to to the subscriber and ends marks the subscription manager as stale.
    ///
    /// # Arguments
    ///
    /// * `status`: to use for the subscription end
    ///
    /// returns: ()
    async fn send_end_to(&self, status: WsEventingStatus);
}

#[async_trait]
pub trait SourceSubscriptionManager: SourceSubscriptionManagerAccess + SubscriptionManager {
    async fn new<H, EHF>(
        subscription_manager: WsaEndpointReference,
        expires: Duration,
        notify_to: WsaEndpointReference,
        end_to: Option<WsaEndpointReference>,
        filter: Option<Filter>,
        http_client: H,
        extension_handler_factory: EHF,
    ) -> Self
    where
        H: HttpClient + Clone + Send + Sync + 'static,
        EHF: ExtensionHandlerFactory + Send + Sync + 'static;

    /// Returns an access to the source subscription manager.
    fn access(&self) -> SourceSubscriptionManagerAccessImpl;
}

struct ExpiresData {
    last_duration_update: Instant,
    expires: Duration,
}

pub struct SubscriptionManagerBase {
    subscription_id: String,
    subscription_manager: WsaEndpointReference,
    notify_to: WsaEndpointReference,
    end_to: Option<WsaEndpointReference>,
    expires: Arc<RwLock<ExpiresData>>,
    filter: Option<Filter>,
}

impl SubscriptionManagerBase {
    async fn renew(&self, expires: Duration) {
        debug!("Renewing {} by {:?}", self.subscription_id, &expires);

        let mut inner = self.expires.write().await;
        inner.expires = expires;
        inner.last_duration_update = Instant::now();
    }
}

#[async_trait]
impl SubscriptionManager for SubscriptionManagerBase {
    async fn subscription_id(&self) -> String {
        self.subscription_id.clone()
    }

    async fn expires(&self) -> Duration {
        let inner = self.expires.read().await;
        inner.expires
    }

    async fn remaining(&self) -> Duration {
        let inner = self.expires.read().await;
        inner
            .expires
            .saturating_sub(Instant::now().saturating_duration_since(inner.last_duration_update))
    }

    async fn notify_to(&self) -> WsaEndpointReference {
        self.notify_to.clone()
    }

    async fn end_to(&self) -> Option<WsaEndpointReference> {
        self.end_to.clone()
    }

    async fn filter(&self) -> Option<Filter> {
        self.filter.clone()
    }

    async fn manager_epr(&self) -> WsaEndpointReference {
        self.subscription_manager.clone()
    }
}

pub struct SinkSubscriptionManagerImpl {
    base: SubscriptionManagerBase,
}

pub struct SourceSubscriptionManagerImpl {
    _processing_task: JoinHandle<()>,
    sender: Sender<SourceSubscriptionManagerAccessMessage>,
}

pub struct SourceSubscriptionManagerAccessImpl {
    sender: Sender<SourceSubscriptionManagerAccessMessage>,
}

#[derive(Debug)]
pub enum SourceSubscriptionManagerAccessMessage {
    SubscriptionId {
        response: oneshot::Sender<String>,
    },
    Expires {
        response: oneshot::Sender<Duration>,
    },
    Remaining {
        response: oneshot::Sender<Duration>,
    },
    NotifyTo {
        response: oneshot::Sender<WsaEndpointReference>,
    },
    EndTo {
        response: oneshot::Sender<Option<WsaEndpointReference>>,
    },
    Filter {
        response: oneshot::Sender<Option<Filter>>,
    },
    ManagerEpr {
        response: oneshot::Sender<WsaEndpointReference>,
    },

    Renew {
        duration: Duration,
        response: oneshot::Sender<()>,
    },
    SendNotification {
        action: &'static str,
        soap_body: Bytes,
        response: oneshot::Sender<()>,
    },
    ManagerStatus {
        response: oneshot::Sender<Result<(), SourceSubscriptionManagerStatus>>,
    },
    SendEndTo {
        status: WsEventingStatus,
        response: oneshot::Sender<()>,
    },
}

#[async_trait]
impl SubscriptionManager for SinkSubscriptionManagerImpl {
    async fn subscription_id(&self) -> String {
        self.base.subscription_id().await
    }

    async fn expires(&self) -> Duration {
        self.base.expires().await
    }

    async fn remaining(&self) -> Duration {
        self.base.remaining().await
    }

    async fn notify_to(&self) -> WsaEndpointReference {
        self.base.notify_to().await
    }

    async fn end_to(&self) -> Option<WsaEndpointReference> {
        self.base.end_to().await
    }

    async fn filter(&self) -> Option<Filter> {
        self.base.filter().await
    }

    async fn manager_epr(&self) -> WsaEndpointReference {
        self.base.manager_epr().await
    }
}

#[async_trait]
impl SinkSubscriptionManager for SinkSubscriptionManagerImpl {
    fn new(
        subscription_manager: WsaEndpointReference,
        expires: Duration,
        notify_to: WsaEndpointReference,
        end_to: Option<WsaEndpointReference>,
        filter: Option<Filter>,
        subscription_id: String,
    ) -> Self {
        Self {
            base: SubscriptionManagerBase {
                subscription_id,
                subscription_manager,
                notify_to,
                end_to,
                expires: Arc::new(RwLock::new(ExpiresData {
                    last_duration_update: Instant::now(),
                    expires,
                })),
                filter,
            },
        }
    }
    async fn process_renew(&self, duration: Duration) {
        self.base.renew(duration).await;
    }
}

#[async_trait]
impl SubscriptionManager for SourceSubscriptionManagerImpl {
    async fn subscription_id(&self) -> String {
        self.access().subscription_id().await
    }

    async fn expires(&self) -> Duration {
        self.access().expires().await
    }

    async fn remaining(&self) -> Duration {
        self.access().remaining().await
    }

    async fn notify_to(&self) -> WsaEndpointReference {
        self.access().notify_to().await
    }

    async fn end_to(&self) -> Option<WsaEndpointReference> {
        self.access().end_to().await
    }

    async fn filter(&self) -> Option<Filter> {
        self.access().filter().await
    }

    async fn manager_epr(&self) -> WsaEndpointReference {
        self.access().manager_epr().await
    }
}

#[async_trait]
impl SubscriptionManager for SourceSubscriptionManagerAccessImpl {
    async fn subscription_id(&self) -> String {
        let (tx, rx) = oneshot::channel();
        send_log_error(
            &self.sender,
            SourceSubscriptionManagerAccessMessage::SubscriptionId { response: tx },
        )
        .await;

        match rx.await {
            Ok(resp) => resp,
            Err(err) => {
                error!("{:?}", err);
                "ERROR".to_string()
            }
        }
    }

    async fn expires(&self) -> Duration {
        let (tx, rx) = oneshot::channel();
        send_log_error(
            &self.sender,
            SourceSubscriptionManagerAccessMessage::Expires { response: tx },
        )
        .await;

        match rx.await {
            Ok(resp) => resp,
            Err(err) => {
                error!("{:?}", err);
                Duration::default()
            }
        }
    }

    async fn remaining(&self) -> Duration {
        let (tx, rx) = oneshot::channel();
        send_log_error(
            &self.sender,
            SourceSubscriptionManagerAccessMessage::Remaining { response: tx },
        )
        .await;

        match rx.await {
            Ok(resp) => resp,
            Err(err) => {
                error!("{:?}", err);
                Duration::default()
            }
        }
    }

    async fn notify_to(&self) -> WsaEndpointReference {
        let (tx, rx) = oneshot::channel();
        send_log_error(
            &self.sender,
            SourceSubscriptionManagerAccessMessage::NotifyTo { response: tx },
        )
        .await;

        match rx.await {
            Ok(resp) => resp,
            Err(err) => {
                error!("{:?}", err);
                // TODO: all of these are fatal, provide results, not defaults
                WsaEndpointReference {
                    attributes: Default::default(),
                    address: WsaAttributedURIType {
                        attributes: Default::default(),
                        value: "".to_string(),
                    },
                    reference_parameters: None,
                    metadata: None,
                    children: vec![],
                }
            }
        }
    }

    async fn end_to(&self) -> Option<WsaEndpointReference> {
        let (tx, rx) = oneshot::channel();
        send_log_error(
            &self.sender,
            SourceSubscriptionManagerAccessMessage::EndTo { response: tx },
        )
        .await;

        match rx.await {
            Ok(resp) => resp,
            Err(err) => {
                error!("{:?}", err);
                // TODO: all of these are fatal, provide results, not defaults
                None
            }
        }
    }

    async fn filter(&self) -> Option<Filter> {
        let (tx, rx) = oneshot::channel();
        send_log_error(
            &self.sender,
            SourceSubscriptionManagerAccessMessage::Filter { response: tx },
        )
        .await;

        match rx.await {
            Ok(resp) => resp,
            Err(err) => {
                error!("{:?}", err);
                None
            }
        }
    }

    async fn manager_epr(&self) -> WsaEndpointReference {
        let (tx, rx) = oneshot::channel();
        send_log_error(
            &self.sender,
            SourceSubscriptionManagerAccessMessage::ManagerEpr { response: tx },
        )
        .await;

        match rx.await {
            Ok(resp) => resp,
            Err(err) => {
                error!("{:?}", err);
                // TODO: all of these are fatal, provide results, not defaults
                WsaEndpointReference {
                    attributes: Default::default(),
                    address: WsaAttributedURIType {
                        attributes: Default::default(),
                        value: "".to_string(),
                    },
                    reference_parameters: None,
                    metadata: None,
                    children: vec![],
                }
            }
        }
    }
}

#[async_trait]
impl SourceSubscriptionManagerAccess for SourceSubscriptionManagerAccessImpl {
    async fn process_renew(&self, duration: Duration) {
        let (tx, rx) = oneshot::channel();
        send_log_error(
            &self.sender,
            SourceSubscriptionManagerAccessMessage::Renew {
                duration,
                response: tx,
            },
        )
        .await;

        match rx.await {
            Ok(resp) => resp,
            Err(err) => {
                error!("{:?}", err);
            }
        }
    }

    async fn send_notification(&self, action: &'static str, soap_body: Bytes) {
        let (tx, rx) = oneshot::channel();
        send_log_error(
            &self.sender,
            SourceSubscriptionManagerAccessMessage::SendNotification {
                action,
                soap_body,
                response: tx,
            },
        )
        .await;

        match rx.await {
            Ok(resp) => resp,
            Err(err) => {
                error!("{:?}", err);
            }
        }
    }

    async fn manager_status(&self) -> Result<(), SourceSubscriptionManagerStatus> {
        let (tx, rx) = oneshot::channel();
        send_log_error(
            &self.sender,
            SourceSubscriptionManagerAccessMessage::ManagerStatus { response: tx },
        )
        .await;

        match rx.await {
            Ok(resp) => resp,
            Err(err) => {
                error!("{:?}", err);
                Err(SourceSubscriptionManagerStatus::TimedOut)
            }
        }
    }

    async fn send_end_to(&self, status: WsEventingStatus) {
        let (tx, rx) = oneshot::channel();
        send_log_error(
            &self.sender,
            SourceSubscriptionManagerAccessMessage::SendEndTo {
                status,
                response: tx,
            },
        )
        .await;

        match rx.await {
            Ok(resp) => resp,
            Err(err) => {
                error!("{:?}", err);
            }
        }
    }
}

#[async_trait]
impl SourceSubscriptionManagerAccess for SourceSubscriptionManagerImpl {
    async fn process_renew(&self, duration: Duration) {
        self.access().process_renew(duration).await
    }

    async fn send_notification(&self, action: &'static str, soap_body: Bytes) {
        self.access().send_notification(action, soap_body).await
    }

    async fn manager_status(&self) -> Result<(), SourceSubscriptionManagerStatus> {
        self.access().manager_status().await
    }

    async fn send_end_to(&self, status: WsEventingStatus) {
        self.access().send_end_to(status).await
    }
}

#[async_trait]
impl SourceSubscriptionManager for SourceSubscriptionManagerImpl {
    async fn new<H, EHF>(
        subscription_manager: WsaEndpointReference,
        expires: Duration,
        notify_to: WsaEndpointReference,
        end_to: Option<WsaEndpointReference>,
        filter: Option<Filter>,
        http_client: H,
        extension_handler_factory: EHF,
    ) -> Self
    where
        H: HttpClient + Clone + Send + Sync + 'static,
        EHF: ExtensionHandlerFactory + Send + Sync + 'static,
    {
        let subscription_id = Uuid::new_v4().to_string();
        let (tx, mut rx) = channel(DEFAULT_CHANNEL_SIZE);

        let base = SubscriptionManagerBase {
            subscription_id,
            subscription_manager,
            notify_to,
            end_to,
            expires: Arc::new(RwLock::new(ExpiresData {
                last_duration_update: Instant::now(),
                expires,
            })),
            filter,
        };

        let notify_to_client = SoapClientImpl::new(
            http_client.clone(),
            base.notify_to.clone(),
            extension_handler_factory.clone(),
        );

        let end_to_client = base.end_to().await.map(|it| {
            SoapClientImpl::new(http_client, it.clone(), extension_handler_factory.clone())
        });

        let task = tokio::spawn(async move {
            let mut manager_status: Result<(), SourceSubscriptionManagerStatus> = Ok(());

            while let Some(msg) = rx.recv().await {
                // info!("SourceSubscriptionManager: msg {:?}", &msg);
                match msg {
                    SourceSubscriptionManagerAccessMessage::SubscriptionId { response } => {
                        oneshot_log_error(response, base.subscription_id().await);
                    }
                    SourceSubscriptionManagerAccessMessage::Expires { response } => {
                        oneshot_log_error(response, base.expires().await);
                    }
                    SourceSubscriptionManagerAccessMessage::Remaining { response } => {
                        oneshot_log_error(response, base.remaining().await);
                    }
                    SourceSubscriptionManagerAccessMessage::NotifyTo { response } => {
                        oneshot_log_error(response, base.notify_to().await);
                    }
                    SourceSubscriptionManagerAccessMessage::EndTo { response } => {
                        oneshot_log_error(response, base.end_to().await);
                    }
                    SourceSubscriptionManagerAccessMessage::Filter { response } => {
                        oneshot_log_error(response, base.filter().await);
                    }
                    SourceSubscriptionManagerAccessMessage::ManagerEpr { response } => {
                        oneshot_log_error(response, base.manager_epr().await);
                    }
                    SourceSubscriptionManagerAccessMessage::Renew { duration, response } => {
                        base.renew(duration).await;
                        oneshot_log_error(response, ());
                    }
                    SourceSubscriptionManagerAccessMessage::SendNotification {
                        action,
                        soap_body,
                        response,
                    } => {
                        // check if source failed, skip otherwise
                        if manager_status.is_err() {
                            warn!("Source ended or failed, not sending action {}", action);
                            continue;
                        }

                        // also check if we have time remaining before sending
                        if base.remaining().await == Duration::ZERO {
                            warn!("Source ended, not sending action {}", action);
                            manager_status = Err(SourceSubscriptionManagerStatus::TimedOut)
                        }

                        let result = send_notification(
                            action,
                            soap_body,
                            &notify_to_client,
                            base.notify_to().await,
                        )
                        .await;
                        if let Err(_err) = result {
                            manager_status = Err(SourceSubscriptionManagerStatus::DeliveryFailed);
                        }

                        oneshot_log_error(response, ());
                    }
                    SourceSubscriptionManagerAccessMessage::ManagerStatus { response } => {
                        oneshot_log_error(response, manager_status.clone());
                    }
                    SourceSubscriptionManagerAccessMessage::SendEndTo { status, response } => {
                        // check if source already ended
                        match manager_status {
                            Err(SourceSubscriptionManagerStatus::Ended)
                            | Err(SourceSubscriptionManagerStatus::TimedOut) => {
                                warn!("Source already ended or timed out, not sending end to");
                                continue;
                            }
                            _ => {}
                        }

                        manager_status = Err(match status {
                            WsEventingStatus::StatusSourceDeliveryFailure => {
                                SourceSubscriptionManagerStatus::DeliveryFailed
                            }
                            WsEventingStatus::StatusSourceShuttingDown => {
                                SourceSubscriptionManagerStatus::Ended
                            }
                            WsEventingStatus::StatusSourceCancelling => {
                                SourceSubscriptionManagerStatus::Ended
                            }
                        });

                        if let (Some(client), Some(end_to)) = (&end_to_client, base.end_to().await)
                        {
                            if let Err(err) = send_end_to(
                                status,
                                client,
                                end_to,
                                base.subscription_manager.clone(),
                            )
                            .await
                            {
                                // unlucky, but doesn't really matter
                                warn!("Error sending end to: {:?}", err);
                            }
                        };
                        oneshot_log_error(response, ());
                    }
                }
            }

            info!("SourceSubscriptionManager task finished");
        });

        Self {
            _processing_task: task,
            sender: tx,
        }
    }

    fn access(&self) -> SourceSubscriptionManagerAccessImpl {
        SourceSubscriptionManagerAccessImpl {
            sender: self.sender.clone(),
        }
    }
}

async fn send_end_to<H, EHF>(
    status: WsEventingStatus,
    soap_client: &SoapClientImpl<H, EHF>,
    end_to: WsaEndpointReference,
    subscription_manager: WsaEndpointReference,
) -> Result<(), SoapError>
where
    H: HttpClient + Clone + Send + Sync,
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    let action = WSE_ACTION_SUBSCRIPTION_END_STR;

    // construct body
    let body = SubscriptionEnd {
        attributes: Default::default(),
        subscription_manager,
        status: status.to_status(),
        reason: None,
        children: vec![],
    };

    // construct header
    let header = create_header_for_action_ref_param(
        action,
        end_to.address.value.as_str(),
        &end_to.reference_parameters,
    );
    let msg = SoapMessage { header };

    let mut writer = XmlWriter::new(Vec::new());
    msg.to_xml_complex(&mut writer, body)?;
    let payload: Bytes = writer.into_inner().into();

    soap_client.notify_serialized(payload).await
}

async fn send_notification<H, EHF>(
    action: &str,
    soap_body: Bytes,
    soap_client: &SoapClientImpl<H, EHF>,
    notify_to: WsaEndpointReference,
) -> Result<(), SoapError>
where
    H: HttpClient + Clone + Send + Sync,
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    // construct header
    let header = create_header_for_action_ref_param(
        action,
        notify_to.address.value.as_str(),
        &notify_to.reference_parameters,
    );

    let msg = SoapMessage { header };

    let mut writer = XmlWriter::new(Vec::new());
    msg.to_xml_complex_bytes(&mut writer, soap_body)?;
    let payload: Bytes = writer.into_inner().into();

    soap_client.notify_serialized(payload).await
}
