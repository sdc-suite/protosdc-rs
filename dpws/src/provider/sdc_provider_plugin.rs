use std::collections::HashSet;

use async_trait::async_trait;
use futures::FutureExt;
use itertools::Itertools;
use log::{debug, error, info};
use protosdc_biceps::biceps::{
    context_association_mod, AbstractContextStateOneOf, InstanceIdentifierOneOf,
};
use tokio::task::JoinHandle;

use biceps::common::access::mdib_access::{MdibAccess, MdibAccessEvent};
use biceps::provider::access::local_mdib_access::{
    LocalMdibAccess, LocalMdibAccessReadTransaction,
};

use crate::provider::sdc_provider::{
    DeviceDpwsDiscoveryProvider, DeviceMdibType, SdcDeviceContext,
};
use crate::xml::messages::common::GLUE_SCOPE;
use biceps::common::context::location_detail_query_mapper::{
    create_with_location_detail_query, LocationDetail,
};

/// Chains a variable amount of plugins into one [ProviderPluginChain].
#[macro_export]
macro_rules! chain_plugins {
    // The pattern for a single `eval`
    ($first:expr, $second:expr, $($more:expr ),*) => {
        {
            $crate::provider::sdc_provider_plugin::ProviderPluginChain::create($first, chain_plugins!($second, $($more ),*))
        }
    };

    ($first:expr, $second:expr) => {
        {
            $crate::provider::sdc_provider_plugin::ProviderPluginChain::create($first, $second)
        }
    };
}

/// A provider plugin chain that behaves as a plugin but delegates the implementation
/// to two different provider plugins.
///
/// # Generic Parameters
/// - `C` - The type of the current provider plugin.
/// - `N` - The type of the next provider plugin.
///
/// # Requirements
/// Both `C` and `N` must implement the `SdcProviderPlugin` trait.
///
/// # Fields
/// - `current` - The current provider plugin.
/// - `next` - The next provider plugin.
pub struct ProviderPluginChain<C, N>
where
    C: SdcProviderPlugin,
    N: SdcProviderPlugin,
{
    current: C,
    next: N,
}

impl<C, N> ProviderPluginChain<C, N>
where
    C: SdcProviderPlugin + Send + Sync,
    N: SdcProviderPlugin + Send + Sync,
{
    pub fn create(current: C, next: N) -> Self {
        Self { current, next }
    }
}

#[async_trait]
impl<C, N> SdcProviderPlugin for ProviderPluginChain<C, N>
where
    C: SdcProviderPlugin + Send + Sync,
    N: SdcProviderPlugin + Send + Sync,
{
    async fn before_startup<M, D>(&mut self, context: &SdcDeviceContext<M, D>) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDpwsDiscoveryProvider + Clone,
    {
        self.current.before_startup(context).await?;
        self.next.before_startup(context).await
    }

    async fn after_startup<M, D>(&mut self, context: &SdcDeviceContext<M, D>) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDpwsDiscoveryProvider + Clone,
    {
        self.current.after_startup(context).await?;
        self.next.after_startup(context).await
    }

    async fn before_shutdown<M, D>(
        &mut self,
        context: &SdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDpwsDiscoveryProvider + Clone,
    {
        self.current.before_shutdown(context).await?;
        self.next.before_shutdown(context).await
    }

    async fn after_shutdown<M, D>(&mut self, context: &SdcDeviceContext<M, D>) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDpwsDiscoveryProvider + Clone,
    {
        self.current.after_shutdown(context).await?;
        self.next.after_shutdown(context).await
    }
}

/// Represents a plugin which extends the provider and has access to the device and discovery.
#[async_trait]
pub trait SdcProviderPlugin {
    async fn before_startup<M, D>(
        &mut self,
        _context: &SdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDpwsDiscoveryProvider + Clone,
    {
        Ok(())
    }

    async fn after_startup<M, D>(&mut self, _context: &SdcDeviceContext<M, D>) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDpwsDiscoveryProvider + Clone,
    {
        Ok(())
    }

    async fn before_shutdown<M, D>(
        &mut self,
        _context: &SdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDpwsDiscoveryProvider + Clone,
    {
        Ok(())
    }

    async fn after_shutdown<M, D>(
        &mut self,
        _context: &SdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDpwsDiscoveryProvider + Clone,
    {
        Ok(())
    }
}

/// [SdcProviderPlugin] which updates the devices types and scopes according to the data available
/// in the Mdib.
#[derive(Default)]
pub struct TypesScopesPlugin {
    processing_task: Option<JoinHandle<()>>,
    constant_types: Vec<String>,
}

impl TypesScopesPlugin {
    pub(crate) async fn update_from_mdib<M, P>(
        mdib: &M,
        processor: &mut P,
        additional_scopes: &[String],
    ) where
        M: LocalMdibAccess + LocalMdibAccessReadTransaction + 'static,
        P: DeviceDpwsDiscoveryProvider,
    {
        let location_scopes = mdib
            .read_transaction(|access| {
                async move {
                    access
                        .context_states()
                        .await
                        .into_iter()
                        .filter_map(|it| match it {
                            AbstractContextStateOneOf::LocationContextState(it) => Some(it),
                            _ => None,
                        })
                        .filter(|it| {
                            it.abstract_context_state.context_association_attr
                                == Some(protosdc_biceps::biceps::ContextAssociation {
                                    enum_type: context_association_mod::EnumType::Assoc,
                                })
                        })
                        .flat_map(|it| {
                            let location_detail: LocationDetail = match it.location_detail {
                                None => LocationDetail::default(),
                                Some(it) => it.into(),
                            };

                            it.abstract_context_state.identification.into_iter().map(
                                move |ident_oo| match ident_oo {
                                    InstanceIdentifierOneOf::InstanceIdentifier(ident) => {
                                        create_with_location_detail_query(&ident, &location_detail)
                                    }
                                    InstanceIdentifierOneOf::OperatingJurisdiction(juris) => {
                                        create_with_location_detail_query(
                                            &juris.instance_identifier,
                                            &location_detail,
                                        )
                                    }
                                },
                            )
                        })
                        .filter_map(|it| match it {
                            Ok(it) => Some(it),
                            Err(err) => {
                                error!("Error while processing update: {:?}", err);
                                None
                            }
                        })
                        .collect_vec()
                }
                .boxed()
            })
            .await;

        let mut scopes = HashSet::new();
        scopes.extend(location_scopes);
        scopes.extend(additional_scopes.iter().cloned().collect_vec());
        scopes.insert(GLUE_SCOPE.to_string());

        debug!("Updating scopes to {:?}", scopes);
        processor.update_scopes(scopes.into_iter().collect()).await;
    }
}

#[async_trait]
impl SdcProviderPlugin for TypesScopesPlugin {
    async fn before_startup<M, D>(&mut self, context: &SdcDeviceContext<M, D>) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDpwsDiscoveryProvider + Clone,
    {
        info!("Startup of automatic required types and scopes updating for device");

        let mut change_subscription = context.mdib_access.subscribe().await;
        let task_mdib = context.mdib_access.clone();
        let mut task_processor = context.discovery_provider.clone();

        let constant_types = self.constant_types.clone();

        self.processing_task = Some(tokio::spawn(async move {
            info!("receiver_task started");
            loop {
                let message = change_subscription.recv().await;

                match message {
                    Some(msg) => match msg {
                        MdibAccessEvent::ContextStateModification { .. }
                        | MdibAccessEvent::DescriptionModification { .. } => {
                            TypesScopesPlugin::update_from_mdib(
                                &task_mdib,
                                &mut task_processor,
                                &constant_types,
                            )
                            .await
                        }
                        _ => {
                            // pass
                        }
                    },
                    None => {
                        error!("Channel was closed");
                        break;
                    }
                }
            }
            info!("receiver_task finished");
        }));

        Ok(())
    }

    async fn after_shutdown<M, D>(
        &mut self,
        _context: &SdcDeviceContext<M, D>,
    ) -> anyhow::Result<()>
    where
        M: DeviceMdibType,
        D: DeviceDpwsDiscoveryProvider + Clone,
    {
        info!("Automatic required types and scopes updating stopped");
        if let Some(it) = self.processing_task.take() {
            it.abort()
        }
        Ok(())
    }
}
