use crate::xml::messages::addressing::{
    WsaAttributedURIType, WsaEndpointReference, WsaReferenceParameters, XmlElement,
};
use crate::xml::messages::common::WSA_IS_REFERENCE_PARAMETER_TAG_STR;
use crate::xml::messages::discovery::AppSequence;
use crate::xml::messages::soap::envelope::SoapHeader;
use protosdc_xml::ExpandedName;
use std::collections::HashMap;
use std::marker::PhantomData;
use uuid::Uuid;

pub trait SoapHeaderBuilderState {}

pub enum ActionNeeded {}
pub enum Others {}

impl SoapHeaderBuilderState for ActionNeeded {}
impl SoapHeaderBuilderState for Others {}

pub struct SoapHeaderBuilder<S: SoapHeaderBuilderState> {
    to: Option<String>,
    message_id: Option<String>,
    from: Option<WsaEndpointReference>,
    action: Option<String>,
    relates_to: Option<String>,
    app_sequence: Option<AppSequence>,
    children: Option<Vec<XmlElement>>,
    state: PhantomData<S>,
}

impl Default for SoapHeaderBuilder<ActionNeeded> {
    fn default() -> Self {
        SoapHeaderBuilder {
            to: None,
            message_id: None,
            from: None,
            action: None,
            relates_to: None,
            app_sequence: None,
            children: None,
            state: Default::default(),
        }
    }
}

impl SoapHeaderBuilder<ActionNeeded> {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn action(self, action: impl AsRef<str>) -> SoapHeaderBuilder<Others> {
        SoapHeaderBuilder {
            to: None,
            message_id: None,
            from: None,
            action: Some(action.as_ref().to_string()),
            relates_to: None,
            app_sequence: None,
            children: None,
            state: Default::default(),
        }
    }
}

impl SoapHeaderBuilder<Others> {
    pub fn message_id(mut self, message_id: impl AsRef<str>) -> Self {
        self.message_id = Some(message_id.as_ref().to_string());
        self
    }
    pub fn from(mut self, from: WsaEndpointReference) -> Self {
        self.from = Some(from);
        self
    }
    pub fn relates_to(mut self, relates_to: impl AsRef<str>) -> Self {
        self.relates_to = Some(relates_to.as_ref().to_string());
        self
    }
    pub fn relates_to_optional(mut self, relates_to: Option<impl AsRef<str>>) -> Self {
        if let Some(rel_to) = relates_to {
            self.relates_to = Some(rel_to.as_ref().to_string())
        }
        self
    }
    pub fn to(mut self, to: impl AsRef<str>) -> Self {
        self.to = Some(to.as_ref().to_string());
        self
    }

    pub fn reference_parameters_response(mut self, reference_parameters: &[XmlElement]) -> Self {
        if reference_parameters.is_empty() {
            // short circuit if empty
            return self;
        }
        let mut transformed = reference_parameters
            .iter()
            .map(|param| add_reference_parameter_attribute(param.clone()))
            .collect();
        match self.children.as_mut() {
            None => self.children = Some(transformed),
            Some(children) => children.append(&mut transformed),
        }
        self
    }
    pub fn reference_parameter_response(mut self, reference_parameter: XmlElement) -> Self {
        match self.children.as_mut() {
            None => {
                self.children = Some(vec![add_reference_parameter_attribute(reference_parameter)])
            }
            Some(children) => children.push(add_reference_parameter_attribute(reference_parameter)),
        }
        self
    }
    pub fn build(self) -> SoapHeader {
        SoapHeader {
            attributes: HashMap::with_capacity(0),
            to: self.to.map(|it| it.into()),
            message_id: Some(WsaAttributedURIType {
                attributes: Default::default(),
                value: self
                    .message_id
                    .unwrap_or_else(|| format!("urn:uuid:{}", Uuid::new_v4().urn())),
            }),
            from: self.from,
            action: Some(self.action.expect("can't be").into()),
            relates_to: self.relates_to.map(|it| it.into()),
            app_sequence: self.app_sequence,
            children: self.children.unwrap_or(Vec::with_capacity(0)),
        }
    }
}

pub fn create_builder_for_action(action: &str, to: &str) -> SoapHeaderBuilder<Others> {
    SoapHeaderBuilder::new().action(action).to(to)
}

pub fn create_header_for_action(action: &str, to: &str) -> SoapHeader {
    SoapHeader {
        attributes: HashMap::with_capacity(0),
        to: Some(to.to_string().into()),
        message_id: Some(format!("urn:uuid:{}", Uuid::new_v4().urn()).as_str().into()),
        from: None,
        action: Some(action.into()),
        relates_to: None,
        app_sequence: None,
        children: Vec::with_capacity(0),
    }
}

pub fn create_header_for_action_ref_param(
    action: &str,
    to: &str,
    reference_parameters: &Option<WsaReferenceParameters>,
) -> SoapHeader {
    SoapHeader {
        attributes: HashMap::with_capacity(0),
        to: Some(to.to_string().into()),
        message_id: Some(format!("urn:uuid:{}", Uuid::new_v4().urn()).as_str().into()),
        from: None,
        action: Some(action.into()),
        relates_to: None,
        app_sequence: None,
        children: match reference_parameters {
            None => Vec::with_capacity(0),
            Some(ref_param) => {
                ref_param
                    .parameters
                    .iter()
                    // attach
                    .map(|param| add_reference_parameter_attribute(param.clone()))
                    .collect()
            }
        },
    }
}

// add is reference parameter attribute
fn add_reference_parameter_attribute(mut elem: XmlElement) -> XmlElement {
    elem.attributes.insert(
        ExpandedName {
            namespace: WSA_IS_REFERENCE_PARAMETER_TAG_STR
                .0
                .map(|it| it.to_string()),
            local_name: WSA_IS_REFERENCE_PARAMETER_TAG_STR.1.to_string(),
        },
        "true".to_string(),
    );

    elem
}

pub fn create_header_reply_for_action(action: &str, to: &str, relates_to: &str) -> SoapHeader {
    SoapHeader {
        attributes: HashMap::with_capacity(0),
        to: Some(to.to_string().into()),
        message_id: Some(format!("urn:uuid:{}", Uuid::new_v4().urn()).as_str().into()),
        from: None,
        action: Some(action.to_string().into()),
        relates_to: Some(relates_to.to_string().into()),
        app_sequence: None,
        children: Vec::with_capacity(0),
    }
}
