use crate::xml::messages::common::{
    write_attributes, ADDRESS, ADDRESS_BYTES, ADDRESS_TAG, ADDRESS_TAG_STR, ATTRIBUTED_URI_TYPE,
    DO_NOT_CARE_QNAME, ENDPOINT_REFERENCE, METADATA_BYTES, METADATA_TAG_STR, REFERENCE_PARAMETERS,
    REFERENCE_PARAMETERS_BYTES, REFERENCE_PARAMETERS_TAG, REFERENCE_PARAMETERS_TAG_STR,
    WS_ADDRESSING_NAMESPACE, WS_ADDRESSING_NAMESPACE_BYTES,
};
use protosdc_xml::xml_reader::collect_attributes_reader;
use protosdc_xml::{ComplexXmlTypeWrite, WriterError, XmlWriter};
use protosdc_xml::{ExpandedName, GenericXmlReaderComplexTypeRead, ParserError};
use quick_xml::events::{BytesStart, Event};
use quick_xml::name::{Namespace, ResolveResult};
use std::collections::HashMap;
use std::io::BufRead;
use typed_builder::TypedBuilder;

#[derive(Clone, Debug, PartialEq, TypedBuilder)]
pub struct WsaEndpointReference {
    #[builder(default)]
    pub attributes: HashMap<ExpandedName, String>,
    #[builder(setter(into))]
    pub address: WsaAttributedURIType,
    #[builder(default, setter(strip_option))]
    pub reference_parameters: Option<WsaReferenceParameters>,
    #[builder(default, setter(strip_option))]
    pub metadata: Option<XmlElement>,
    #[builder(default)]
    pub children: Vec<XmlElement>,
}

#[derive(Clone, Debug, PartialEq, TypedBuilder)]
pub struct WsaAttributedURIType {
    #[builder(default)]
    pub attributes: HashMap<ExpandedName, String>,
    #[builder(default, setter(into))]
    pub value: String,
}

impl From<&str> for WsaAttributedURIType {
    fn from(value: &str) -> Self {
        Self {
            attributes: HashMap::with_capacity(0),
            value: value.to_string(),
        }
    }
}

impl From<String> for WsaAttributedURIType {
    fn from(value: String) -> Self {
        Self {
            attributes: HashMap::with_capacity(0),
            value,
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct WsaReferenceParameters {
    pub attributes: HashMap<ExpandedName, String>,
    pub parameters: Vec<XmlElement>,
}

/// Fast forwarding container that does not parse an element
pub struct Skipped;

/// Generic XML Element Container
#[derive(Clone, Debug, PartialEq)]
// TODO: Limit parser depth for this element, it is a potential overflow attack
pub struct XmlElement {
    pub tag: ExpandedName,
    pub attributes: HashMap<ExpandedName, String>,
    pub children: Vec<XmlElement>,
    pub text: Option<String>,
}

//
// parsers
//

impl GenericXmlReaderComplexTypeRead for WsaEndpointReference {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;

    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
        }
        let mut state = State::Root;

        // collect attributes
        let attributes: HashMap<ExpandedName, String> = collect_attributes_reader(event, reader)?;

        // read value
        state = State::Start;

        let mut address: Option<WsaAttributedURIType> = None;
        let mut reference_parameters: Option<WsaReferenceParameters> = None;
        let mut metadata: Option<XmlElement> = None;
        let mut children: Vec<XmlElement> = Vec::with_capacity(0);

        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => match (state, ns, e.local_name().into_inner())
                {
                    (
                        _,
                        ResolveResult::Bound(Namespace(WS_ADDRESSING_NAMESPACE_BYTES)),
                        ADDRESS_BYTES,
                    ) => {
                        address = Some(WsaAttributedURIType::from_xml_complex(
                            ADDRESS_TAG,
                            e,
                            reader,
                        )?);
                    }
                    (
                        _,
                        ResolveResult::Bound(Namespace(WS_ADDRESSING_NAMESPACE_BYTES)),
                        REFERENCE_PARAMETERS_BYTES,
                    ) => {
                        reference_parameters = Some(WsaReferenceParameters::from_xml_complex(
                            REFERENCE_PARAMETERS_TAG,
                            e,
                            reader,
                        )?);
                    }
                    (
                        _,
                        ResolveResult::Bound(Namespace(WS_ADDRESSING_NAMESPACE_BYTES)),
                        METADATA_BYTES,
                    ) => {
                        metadata =
                            Some(XmlElement::from_xml_complex(DO_NOT_CARE_QNAME, e, reader)?);
                    }
                    (_, _, _) => {
                        children.push(XmlElement::from_xml_complex(DO_NOT_CARE_QNAME, e, reader)?);
                    }
                },

                Ok((ref ns, Event::End(ref e))) => match (state, ns, e.local_name().into_inner()) {
                    (_, ns, local_name) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            attributes,
                            address: address.ok_or(ParserError::MandatoryElementMissing {
                                element_name: ADDRESS.to_string(),
                            })?,
                            reference_parameters,
                            metadata,
                            children,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref _e))) => {
                    Err(ParserError::UnexpectedParserTextEventState {
                        parser_state: format!("{:?}", state),
                    })?;
                }
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl GenericXmlReaderComplexTypeRead for WsaReferenceParameters {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
        }
        let mut state = State::Root;

        // collect attributes
        let attributes: HashMap<ExpandedName, String> = collect_attributes_reader(event, reader)?;

        // read value
        state = State::Start;

        let mut children = vec![];

        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref _ns, Event::Start(ref e))) => {
                    children.push(XmlElement::from_xml_complex(DO_NOT_CARE_QNAME, e, reader)?);
                }

                Ok((ref ns, Event::End(ref e))) => match (state, ns, e.local_name().into_inner()) {
                    (_, ns, local_name) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            attributes,
                            parameters: children,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref _e))) => {
                    Err(ParserError::UnexpectedParserTextEventState {
                        parser_state: format!("{:?}", state),
                    })?;
                }
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl GenericXmlReaderComplexTypeRead for XmlElement {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        _tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
        }
        let mut state = State::Root;

        let resolved_tag = reader.resolve_element(event.name());

        let qname: ExpandedName = resolved_tag.try_into()?;

        // collect attributes
        let attributes: HashMap<ExpandedName, String> = collect_attributes_reader(event, reader)?;

        // read value
        state = State::Start;
        let mut children = vec![];
        let mut text = None;
        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref _ns, Event::Start(ref e))) => {
                    // add more generic children we don't know
                    children.push(XmlElement::from_xml_complex(DO_NOT_CARE_QNAME, e, reader)?)
                }

                Ok((ref ns, Event::End(ref e))) => match (state, ns, e.local_name().into_inner()) {
                    (_, ns, local_name)
                        if &qname.resolve_result() == ns
                            && qname.local_name.as_bytes() == local_name =>
                    {
                        state = State::End;
                        return Ok(Self {
                            tag: qname,
                            attributes,
                            children,
                            text,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref e))) => match state {
                    State::Start => {
                        text = Some(
                            e.unescape()
                                .map_err(ParserError::QuickXMLError)?
                                .to_string(),
                        );
                    }
                    _ => Err(ParserError::UnexpectedParserTextEventState {
                        parser_state: format!("{:?}", state),
                    })?,
                },
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl GenericXmlReaderComplexTypeRead for WsaAttributedURIType {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
        }
        let mut state = State::Root;

        // collect attributes
        let attributes: HashMap<ExpandedName, String> = collect_attributes_reader(event, reader)?;

        // read value
        state = State::Start;
        let mut uri = None;

        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref _ns, Event::Start(ref e))) => {
                    Err(ParserError::UnexpectedParserStartState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?;
                }

                Ok((ref ns, Event::End(ref e))) => match (state, ns, e.local_name().into_inner()) {
                    (_, ns, local_name) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            attributes,
                            value: uri.unwrap_or_default(),
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref e))) => match state {
                    State::Start => {
                        uri = Some(
                            e.unescape()
                                .map_err(ParserError::QuickXMLError)?
                                .to_string(),
                        );
                    }
                    _ => Err(ParserError::UnexpectedParserTextEventState {
                        parser_state: format!("{:?}", state),
                    })?,
                },
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

//
// writers
//

impl ComplexXmlTypeWrite for WsaAttributedURIType {
    fn to_xml_complex(
        &self,
        tag_name: Option<(Option<&'static str>, &'static str)>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_ADDRESSING_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| ATTRIBUTED_URI_TYPE, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;

        writer.write_text(self.value.as_str())?;

        writer.write_end(element_namespace, element_tag)?;

        Ok(())
    }
}

impl ComplexXmlTypeWrite for WsaEndpointReference {
    fn to_xml_complex(
        &self,
        tag_name: Option<(Option<&'static str>, &'static str)>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_ADDRESSING_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| ENDPOINT_REFERENCE, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;

        self.address
            .to_xml_complex(Some(ADDRESS_TAG_STR), writer, false)?;

        if let Some(reference_parameters) = &self.reference_parameters {
            reference_parameters.to_xml_complex(
                Some(REFERENCE_PARAMETERS_TAG_STR),
                writer,
                false,
            )?;
        }

        if let Some(metadata) = &self.metadata {
            metadata.to_xml_complex(Some(METADATA_TAG_STR), writer, false)?;
        }

        for child in &self.children {
            child.to_xml_complex(None, writer, false)?;
        }

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

impl ComplexXmlTypeWrite for XmlElement {
    fn to_xml_complex(
        &self,
        tag_name: Option<(Option<&'static str>, &'static str)>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => self.tag.namespace.as_deref(),
        };
        let element_tag = tag_name.map_or_else(|| self.tag.local_name.as_str(), |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;

        if let Some(text) = &self.text {
            writer.write_text(text)?;
        }

        for child in &self.children {
            child.to_xml_complex(None, writer, false)?;
        }

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

impl ComplexXmlTypeWrite for WsaReferenceParameters {
    fn to_xml_complex(
        &self,
        tag_name: Option<(Option<&'static str>, &'static str)>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_ADDRESSING_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| REFERENCE_PARAMETERS, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;

        for child in &self.parameters {
            child.to_xml_complex(None, writer, false)?;
        }

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::xml::messages::addressing::{WsaAttributedURIType, XmlElement, DO_NOT_CARE_QNAME};
    use crate::xml::messages::common::{MESSAGE_ID_TAG, MESSAGE_ID_TAG_STR};
    use itertools::Itertools;
    use protosdc_xml::{
        find_start_element_reader, ExpandedName, GenericXmlReaderComplexTypeRead, XmlReader,
    };
    use protosdc_xml::{ComplexXmlTypeWrite, XmlWriter};

    fn test_message_id(message_id: &WsaAttributedURIType) {
        assert_eq!(2, message_id.attributes.len());
        // iterate with reproducible order
        let mut attributes_iter = message_id
            .attributes
            .iter()
            .sorted_by(|a, b| Ord::cmp(&b.1, &a.1));

        assert_eq!(
            (
                &ExpandedName {
                    namespace: Some("https://example.org".to_string()),
                    local_name: "key".to_string(),
                },
                &"value".to_string()
            ),
            attributes_iter.next().unwrap()
        );
        assert_eq!(
            (
                &ExpandedName {
                    namespace: None,
                    local_name: "cool_attr".to_string(),
                },
                &"so_cool".to_string()
            ),
            attributes_iter.next().unwrap()
        );
        assert!(attributes_iter.next().is_none());

        assert_eq!(
            "https://example.com/6B29FC40-CA47-1067-B31D-00DD010662DA".to_string(),
            message_id.value
        );
    }

    #[test]
    fn test_parse_attributed_uri() -> anyhow::Result<()> {
        let message_id = {
            let input = "<wsa:MessageID xmlns:wsa=\"http://www.w3.org/2005/08/addressing\" xmlns:test=\"https://example.org\" cool_attr=\"so_cool\" test:key=\"value\">\
        https://example.com/6B29FC40-CA47-1067-B31D-00DD010662DA\
        </wsa:MessageID>";

            let mut reader = XmlReader::create_typed(input.as_bytes());
            let mut buf = vec![];

            let message_id: WsaAttributedURIType =
                find_start_element_reader!(WsaAttributedURIType, MESSAGE_ID_TAG, reader, buf)?;
            test_message_id(&message_id);
            message_id
        };

        // back to xml, parse it again and test it
        let mut writer = XmlWriter::new(vec![]);
        message_id.to_xml_complex(Some(MESSAGE_ID_TAG_STR), &mut writer, false)?;
        let writer_buf = writer.inner();

        let mut reader = XmlReader::create_typed(writer_buf.as_ref());
        let mut buf = vec![];

        let message_id: WsaAttributedURIType =
            find_start_element_reader!(WsaAttributedURIType, MESSAGE_ID_TAG, reader, buf)?;
        test_message_id(&message_id);

        Ok(())
    }

    #[test]
    fn test_parse_empty_attributed_uri() -> anyhow::Result<()> {
        let message_id = {
            let input = "<wsa:MessageID xmlns:wsa=\"http://www.w3.org/2005/08/addressing\"/>";

            let mut reader = XmlReader::create_typed(input.as_bytes());
            let mut buf = vec![];

            let message_id: WsaAttributedURIType =
                find_start_element_reader!(WsaAttributedURIType, MESSAGE_ID_TAG, reader, buf)?;
            assert!(message_id.value.is_empty());
            message_id
        };

        // back to xml, parse it again and test it
        let mut writer = XmlWriter::new(vec![]);
        message_id.to_xml_complex(Some(MESSAGE_ID_TAG_STR), &mut writer, false)?;
        let writer_buf = writer.inner();

        let mut reader = XmlReader::create_typed(writer_buf.as_ref());
        let mut buf = vec![];

        let message_id: WsaAttributedURIType =
            find_start_element_reader!(WsaAttributedURIType, MESSAGE_ID_TAG, reader, buf)?;
        assert!(message_id.value.is_empty());

        Ok(())
    }

    fn test_xml_element(message_id: &XmlElement) {
        assert_eq!(
            ExpandedName {
                namespace: MESSAGE_ID_TAG_STR.0.map(|it| it.to_string()),
                local_name: MESSAGE_ID_TAG_STR.1.to_string()
            },
            message_id.tag
        );

        // iterate with reproducible order
        let mut attributes_iter = message_id
            .attributes
            .iter()
            .sorted_by(|a, b| Ord::cmp(&b.1, &a.1));

        assert_eq!(
            (
                &ExpandedName {
                    namespace: Some("https://example.org".to_string()),
                    local_name: "key".to_string(),
                },
                &"value".to_string()
            ),
            attributes_iter.next().unwrap()
        );
        assert_eq!(
            (
                &ExpandedName {
                    namespace: None,
                    local_name: "cool_attr".to_string(),
                },
                &"so_cool".to_string()
            ),
            attributes_iter.next().unwrap()
        );
        assert!(attributes_iter.next().is_none());

        assert_eq!(
            Some("https://example.com/6B29FC40-CA47-1067-B31D-00DD010662DA".to_string()),
            message_id.text
        );
    }

    #[test]
    fn parse_xml_element() -> anyhow::Result<()> {
        let message_id = {
            let input = "<wsa:MessageID xmlns:wsa=\"http://www.w3.org/2005/08/addressing\" xmlns:test=\"https://example.org\" cool_attr=\"so_cool\" test:key=\"value\">\
            https://example.com/6B29FC40-CA47-1067-B31D-00DD010662DA\
            </wsa:MessageID>";

            let mut reader = XmlReader::create_typed(input.as_bytes());
            let mut buf = vec![];

            let message_id: XmlElement =
                find_start_element_reader!(XmlElement, DO_NOT_CARE_QNAME, reader, buf)?;
            test_xml_element(&message_id);
            message_id
        };

        // back to xml, parse it again and test it
        let mut writer = XmlWriter::new(vec![]);
        message_id.to_xml_complex(None, &mut writer, false)?;
        let writer_buf = writer.inner();

        let mut reader = XmlReader::create_typed(writer_buf.as_ref());
        let mut buf = vec![];

        let message_id: XmlElement =
            find_start_element_reader!(XmlElement, DO_NOT_CARE_QNAME, reader, buf)?;
        test_xml_element(&message_id);

        Ok(())
    }
}
