use crate::xml::messages::common::{
    soap_element_impl, write_attributes, MEX_DIALECT, MEX_DIALECT_TAG_REF, MEX_GET_METADATA,
    MEX_IDENTIFIER, MEX_IDENTIFIER_TAG_REF, WS_MEX_NAMESPACE,
};
use protosdc_xml::xml_reader::collect_attributes_reader;
use protosdc_xml::{ComplexXmlTypeWrite, WriterError, XmlWriter};
use protosdc_xml::{ExpandedName, GenericXmlReaderComplexTypeRead, ParserError};
use quick_xml::events::{BytesStart, Event};
use std::collections::HashMap;
use std::io::BufRead;

#[derive(Clone, Debug, PartialEq)]
pub struct GetMetadata {
    pub attributes: HashMap<ExpandedName, String>,
    pub dialect: Option<String>,
    pub identifier: Option<String>,
}

soap_element_impl!(GetMetadata, MEX_GET_METADATA);

impl GenericXmlReaderComplexTypeRead for GetMetadata {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
            DialectStart,
            DialectEnd,
            IdentifierStart,
            IdentifierEnd,
        }
        let mut state = State::Root;

        // collect attributes
        let attributes: HashMap<ExpandedName, String> = collect_attributes_reader(event, reader)?;

        // read value
        state = State::Start;

        let mut dialect: Option<String> = None;
        let mut identifier: Option<String> = None;

        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => {
                    match (state, (ns, e.local_name().into_inner())) {
                        (State::Start, MEX_DIALECT_TAG_REF) => {
                            state = State::DialectStart;
                        }
                        (State::Start, MEX_IDENTIFIER_TAG_REF)
                        | (State::DialectEnd, MEX_IDENTIFIER_TAG_REF) => {
                            state = State::IdentifierStart;
                        }
                        (_, _) => Err(ParserError::UnexpectedParserStartState {
                            element_name: match String::from_utf8(
                                e.local_name().into_inner().to_vec(),
                            ) {
                                Ok(it) => it,
                                Err(_) => "FromUtf8Error".to_string(),
                            },
                            parser_state: format!("{:?}", state),
                        })?,
                    }
                }

                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (State::DialectStart, MEX_DIALECT_TAG_REF) => {
                        state = State::DialectEnd;
                    }
                    (State::IdentifierStart, MEX_DIALECT_TAG_REF) => {
                        state = State::IdentifierEnd;
                    }
                    (_, (ns, local_name)) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            attributes,
                            dialect,
                            identifier,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref e))) => match state {
                    State::DialectStart => {
                        dialect = Some(
                            e.unescape()
                                .map_err(ParserError::QuickXMLError)?
                                .to_string(),
                        );
                    }
                    State::IdentifierStart => {
                        identifier = Some(
                            e.unescape()
                                .map_err(ParserError::QuickXMLError)?
                                .to_string(),
                        );
                    }
                    _ => Err(ParserError::UnexpectedParserTextEventState {
                        parser_state: format!("{:?}", state),
                    })?,
                },
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl ComplexXmlTypeWrite for GetMetadata {
    fn to_xml_complex(
        &self,
        tag_name: Option<(Option<&'static str>, &'static str)>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_MEX_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| MEX_GET_METADATA, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;

        if let Some(dialect) = &self.dialect {
            writer.write_start(Some(WS_MEX_NAMESPACE), MEX_DIALECT)?;
            writer.write_text(dialect.as_str())?;
            writer.write_end(Some(WS_MEX_NAMESPACE), MEX_DIALECT)?;
        }

        if let Some(identifier) = &self.identifier {
            writer.write_start(Some(WS_MEX_NAMESPACE), MEX_IDENTIFIER)?;
            writer.write_text(identifier.as_str())?;
            writer.write_end(Some(WS_MEX_NAMESPACE), MEX_IDENTIFIER)?;
        }

        writer.write_end(element_namespace, element_tag)?;

        Ok(())
    }
}

#[cfg(test)]
mod test {
    use crate::xml::messages::common::MEX_GET_METADATA_TAG;
    use crate::xml::messages::ws_metadata_exchange::get_metadata::GetMetadata;
    use protosdc_xml::{find_start_element_reader, GenericXmlReaderComplexTypeRead, XmlReader};
    use protosdc_xml::{ComplexXmlTypeWrite, XmlWriter};

    fn test_get_metadata(get_metadata: &GetMetadata) {
        assert!(get_metadata.identifier.is_none());
        assert!(get_metadata.dialect.is_none());
        assert!(get_metadata.attributes.is_empty());
    }

    #[test]
    fn test_roundtrip() -> anyhow::Result<()> {
        let get_metadata = {
            let input =
                "<wsm:GetMetadata xmlns:wsm=\"http://schemas.xmlsoap.org/ws/2004/09/mex\"/>";

            let mut reader = XmlReader::create_typed(input.as_bytes());
            let mut buf = vec![];

            let get_metadata: GetMetadata =
                find_start_element_reader!(GetMetadata, MEX_GET_METADATA_TAG, reader, buf)?;
            test_get_metadata(&get_metadata);
            get_metadata
        };

        // back to xml, parse it again and test it
        let mut writer = XmlWriter::new(vec![]);
        get_metadata.to_xml_complex(None, &mut writer, false)?;
        let writer_buf = writer.inner();

        let mut reader = XmlReader::create_typed(writer_buf.as_ref());
        let mut buf = vec![];

        let get_metadata_again: GetMetadata =
            find_start_element_reader!(GetMetadata, MEX_GET_METADATA_TAG, reader, buf)?;
        test_get_metadata(&get_metadata_again);

        Ok(())
    }
}
