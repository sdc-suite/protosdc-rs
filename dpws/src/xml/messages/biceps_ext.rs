use crate::xml::messages::common::{create_tag_constants, ConstQualifiedName};
use const_format::concatcp;
use protosdc_biceps::biceps::{
    ActivateResponse, DescriptionModificationReport, EpisodicAlertReport, EpisodicComponentReport,
    EpisodicContextReport, EpisodicMetricReport, EpisodicOperationalStateReport,
    GetContextStatesResponse, GetLocalizedTextResponse, GetMdDescriptionResponse,
    GetMdStateResponse, GetMdibResponse, GetSupportedLanguagesResponse, OperationInvokedReport,
    SetAlertStateResponse, SetComponentStateResponse, SetContextStateResponse,
    SetMetricStateResponse, SetStringResponse, SetValueResponse, WaveformStream,
};

pub const BICEPS_PM_NAMESPACE: &str =
    "http://standards.ieee.org/downloads/11073/11073-10207-2017/participant";
pub const BICEPS_MSG_NAMESPACE: &str =
    "http://standards.ieee.org/downloads/11073/11073-10207-2017/message";
pub const SDC_NAMESPACE: &str = "http://standards.ieee.org/downloads/11073/11073-20701-2018";

pub const BICEPS_MSG_NAMESPACE_BYTES: &[u8] = BICEPS_MSG_NAMESPACE.as_bytes();

pub const GET_MDIB: &str = "GetMdib";
create_tag_constants!(BICEPS_MSG_NAMESPACE, GET_MDIB);
pub const GET_MDIB_RESPONSE: &str = "GetMdibResponse";
create_tag_constants!(BICEPS_MSG_NAMESPACE, GET_MDIB_RESPONSE);
pub const GET_MD_DESCRIPTION: &str = "GetMdDescription";
create_tag_constants!(BICEPS_MSG_NAMESPACE, GET_MD_DESCRIPTION);
pub const GET_MD_DESCRIPTION_RESPONSE: &str = "GetMdDescriptionResponse";
create_tag_constants!(BICEPS_MSG_NAMESPACE, GET_MD_DESCRIPTION_RESPONSE);
pub const GET_MD_STATE: &str = "GetMdState";
create_tag_constants!(BICEPS_MSG_NAMESPACE, GET_MD_STATE);
pub const GET_MD_STATE_RESPONSE: &str = "GetMdStateResponse";
create_tag_constants!(BICEPS_MSG_NAMESPACE, GET_MD_STATE_RESPONSE);

pub const GET_CONTEXT_STATES: &str = "GetContextStates";
create_tag_constants!(BICEPS_MSG_NAMESPACE, GET_CONTEXT_STATES);
pub const GET_CONTEXT_STATES_RESPONSE: &str = "GetContextStatesResponse";
create_tag_constants!(BICEPS_MSG_NAMESPACE, GET_CONTEXT_STATES_RESPONSE);
pub const GET_CONTEXT_STATES_BY_IDENTIFICATION: &str = "GetContextStatesByIdentification";
create_tag_constants!(BICEPS_MSG_NAMESPACE, GET_CONTEXT_STATES_BY_IDENTIFICATION);
pub const GET_CONTEXT_STATES_BY_IDENTIFICATION_RESPONSE: &str =
    "GetContextStatesByIdentificationResponse";
create_tag_constants!(
    BICEPS_MSG_NAMESPACE,
    GET_CONTEXT_STATES_BY_IDENTIFICATION_RESPONSE
);
pub const GET_CONTEXT_STATES_BY_FILTER: &str = "GetContextStatesByFilter";
create_tag_constants!(BICEPS_MSG_NAMESPACE, GET_CONTEXT_STATES_BY_FILTER);
pub const GET_CONTEXT_STATES_BY_FILTER_RESPONSE: &str = "GetContextStatesByFilterResponse";
create_tag_constants!(BICEPS_MSG_NAMESPACE, GET_CONTEXT_STATES_BY_FILTER_RESPONSE);

pub const ACTIVATE: &str = "Activate";
create_tag_constants!(BICEPS_MSG_NAMESPACE, ACTIVATE);
pub const ACTIVATE_RESPONSE: &str = "ActivateResponse";
create_tag_constants!(BICEPS_MSG_NAMESPACE, ACTIVATE_RESPONSE);
pub const SET_ALERT_STATE: &str = "SetAlertState";
create_tag_constants!(BICEPS_MSG_NAMESPACE, SET_ALERT_STATE);
pub const SET_ALERT_STATE_RESPONSE: &str = "SetAlertStateResponse";
create_tag_constants!(BICEPS_MSG_NAMESPACE, SET_ALERT_STATE_RESPONSE);
pub const SET_COMPONENT_STATE: &str = "SetComponentState";
create_tag_constants!(BICEPS_MSG_NAMESPACE, SET_COMPONENT_STATE);
pub const SET_COMPONENT_STATE_RESPONSE: &str = "SetComponentStateResponse";
create_tag_constants!(BICEPS_MSG_NAMESPACE, SET_COMPONENT_STATE_RESPONSE);
pub const SET_CONTEXT_STATE: &str = "SetContextState";
create_tag_constants!(BICEPS_MSG_NAMESPACE, SET_CONTEXT_STATE);
pub const SET_CONTEXT_STATE_RESPONSE: &str = "SetContextStateResponse";
create_tag_constants!(BICEPS_MSG_NAMESPACE, SET_CONTEXT_STATE_RESPONSE);
pub const SET_METRIC_STATE: &str = "SetMetricState";
create_tag_constants!(BICEPS_MSG_NAMESPACE, SET_METRIC_STATE);
pub const SET_METRIC_STATE_RESPONSE: &str = "SetMetricStateResponse";
create_tag_constants!(BICEPS_MSG_NAMESPACE, SET_METRIC_STATE_RESPONSE);
pub const SET_STRING: &str = "SetString";
create_tag_constants!(BICEPS_MSG_NAMESPACE, SET_STRING);
pub const SET_STRING_RESPONSE: &str = "SetStringResponse";
create_tag_constants!(BICEPS_MSG_NAMESPACE, SET_STRING_RESPONSE);
pub const SET_VALUE: &str = "SetValue";
create_tag_constants!(BICEPS_MSG_NAMESPACE, SET_VALUE);
pub const SET_VALUE_RESPONSE: &str = "SetValueResponse";
create_tag_constants!(BICEPS_MSG_NAMESPACE, SET_VALUE_RESPONSE);

pub const EPISODIC_ALERT_REPORT: &str = "EpisodicAlertReport";
create_tag_constants!(BICEPS_MSG_NAMESPACE, EPISODIC_ALERT_REPORT);
pub const EPISODIC_COMPONENT_REPORT: &str = "EpisodicComponentReport";
create_tag_constants!(BICEPS_MSG_NAMESPACE, EPISODIC_COMPONENT_REPORT);
pub const EPISODIC_CONTEXT_REPORT: &str = "EpisodicContextReport";
create_tag_constants!(BICEPS_MSG_NAMESPACE, EPISODIC_CONTEXT_REPORT);
pub const EPISODIC_METRIC_REPORT: &str = "EpisodicMetricReport";
create_tag_constants!(BICEPS_MSG_NAMESPACE, EPISODIC_METRIC_REPORT);
pub const EPISODIC_OPERATIONAL_STATE_REPORT: &str = "EpisodicOperationalStateReport";
create_tag_constants!(BICEPS_MSG_NAMESPACE, EPISODIC_OPERATIONAL_STATE_REPORT);
pub const WAVEFORM_STREAM: &str = "WaveformStream";
create_tag_constants!(BICEPS_MSG_NAMESPACE, WAVEFORM_STREAM);
pub const DESCRIPTION_MODIFICATION_REPORT: &str = "DescriptionModificationReport";
create_tag_constants!(BICEPS_MSG_NAMESPACE, DESCRIPTION_MODIFICATION_REPORT);
pub const OPERATION_INVOKED_REPORT: &str = "OperationInvokedReport";
create_tag_constants!(BICEPS_MSG_NAMESPACE, OPERATION_INVOKED_REPORT);
pub const SYSTEM_ERROR_REPORT: &str = "SystemErrorReport";
create_tag_constants!(BICEPS_MSG_NAMESPACE, SYSTEM_ERROR_REPORT);
pub const PERIODIC_ALERT_REPORT: &str = "PeriodicAlertReport";
create_tag_constants!(BICEPS_MSG_NAMESPACE, PERIODIC_ALERT_REPORT);
pub const PERIODIC_COMPONENT_REPORT: &str = "PeriodicComponentReport";
create_tag_constants!(BICEPS_MSG_NAMESPACE, PERIODIC_COMPONENT_REPORT);
pub const PERIODIC_CONTEXT_REPORT: &str = "PerioSetAlertStateResponsedicContextReport";
create_tag_constants!(BICEPS_MSG_NAMESPACE, PERIODIC_CONTEXT_REPORT);
pub const PERIODIC_METRIC_REPORT: &str = "PeriodicMetricReport";
create_tag_constants!(BICEPS_MSG_NAMESPACE, PERIODIC_METRIC_REPORT);
pub const PERIODIC_OPERATIONAL_STATE_REPORT: &str = "PeriodicOperationalStateReport";
create_tag_constants!(BICEPS_MSG_NAMESPACE, PERIODIC_OPERATIONAL_STATE_REPORT);

pub const GET_SUPPORTED_LANGUAGES: &str = "GetSupportedLanguages";
create_tag_constants!(BICEPS_MSG_NAMESPACE, GET_SUPPORTED_LANGUAGES);
pub const GET_SUPPORTED_LANGUAGES_RESPONSE: &str = "GetSupportedLanguagesResponse";
create_tag_constants!(BICEPS_MSG_NAMESPACE, GET_SUPPORTED_LANGUAGES_RESPONSE);
pub const GET_LOCALIZED_TEXT: &str = "GetLocalizedText";
create_tag_constants!(BICEPS_MSG_NAMESPACE, GET_LOCALIZED_TEXT);
pub const GET_LOCALIZED_TEXT_RESPONSE: &str = "GetLocalizedTextResponse";
create_tag_constants!(BICEPS_MSG_NAMESPACE, GET_LOCALIZED_TEXT_RESPONSE);

const BICEPS_CONTEXT_SERVICE: &str = "ContextService";
const BICEPS_DESCRIPTION_EVENT_SERVICE: &str = "DescriptionEventService";
const BICEPS_GET_SERVICE: &str = "GetService";
const BICEPS_SET_SERVICE: &str = "SetService";
const BICEPS_STATE_EVENT_SERVICE: &str = "StateEventService";
const BICEPS_WAVEFORM_SERVICE: &str = "WaveformService";
const BICEPS_CONTAINMENT_TREE_SERVICE: &str = "ContainmentTreeService";
const BICEPS_LOCALIZATION_SERVICE: &str = "LocalizationService";

pub const BICEPS_PORT_TYPE_GET: ConstQualifiedName<'static> = ConstQualifiedName {
    namespace: Some(SDC_NAMESPACE),
    local_name: BICEPS_GET_SERVICE,
};
pub const BICEPS_PORT_TYPE_SET: ConstQualifiedName<'static> = ConstQualifiedName {
    namespace: Some(SDC_NAMESPACE),
    local_name: BICEPS_SET_SERVICE,
};
pub const BICEPS_PORT_TYPE_STATE_EVENT: ConstQualifiedName<'static> = ConstQualifiedName {
    namespace: Some(SDC_NAMESPACE),
    local_name: BICEPS_STATE_EVENT_SERVICE,
};
pub const BICEPS_PORT_TYPE_CONTEXT: ConstQualifiedName<'static> = ConstQualifiedName {
    namespace: Some(SDC_NAMESPACE),
    local_name: BICEPS_CONTEXT_SERVICE,
};
pub const BICEPS_PORT_TYPE_DESCRIPTION_EVENT: ConstQualifiedName<'static> = ConstQualifiedName {
    namespace: Some(SDC_NAMESPACE),
    local_name: BICEPS_DESCRIPTION_EVENT_SERVICE,
};
pub const BICEPS_PORT_TYPE_WAVEFORM: ConstQualifiedName<'static> = ConstQualifiedName {
    namespace: Some(SDC_NAMESPACE),
    local_name: BICEPS_WAVEFORM_SERVICE,
};
pub const BICEPS_PORT_TYPE_CONTAINMENT_TREE: ConstQualifiedName<'static> = ConstQualifiedName {
    namespace: Some(SDC_NAMESPACE),
    local_name: BICEPS_CONTAINMENT_TREE_SERVICE,
};
pub const BICEPS_PORT_TYPE_LOCALIZATION: ConstQualifiedName<'static> = ConstQualifiedName {
    namespace: Some(SDC_NAMESPACE),
    local_name: BICEPS_LOCALIZATION_SERVICE,
};

const BICEPS_ACTION_PREFIX: &str = concatcp!(SDC_NAMESPACE, "/");

pub const BICEPS_ACTION_GET_MDIB: &str =
    concatcp!(BICEPS_ACTION_PREFIX, BICEPS_GET_SERVICE, "/GetMdib");
pub const BICEPS_ACTION_GET_MDIB_RESPONSE: &str =
    concatcp!(BICEPS_ACTION_PREFIX, BICEPS_GET_SERVICE, "/GetMdibResponse");

pub const BICEPS_ACTION_GET_MD_DESCRIPTION: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_GET_SERVICE,
    "/GetMdDescription"
);
pub const BICEPS_ACTION_GET_MD_DESCRIPTION_RESPONSE: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_GET_SERVICE,
    "/GetMdDescriptionResponse"
);

pub const BICEPS_ACTION_GET_MD_STATE: &str =
    concatcp!(BICEPS_ACTION_PREFIX, BICEPS_GET_SERVICE, "/GetMdState");
pub const BICEPS_ACTION_GET_MD_STATE_RESPONSE: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_GET_SERVICE,
    "/GetMdStateResponse"
);

pub const BICEPS_ACTION_GET_CONTEXT_STATES: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_CONTEXT_SERVICE,
    "/GetContextStates"
);
pub const BICEPS_ACTION_GET_CONTEXT_STATES_RESPONSE: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_CONTEXT_SERVICE,
    "/GetContextStatesResponse"
);

pub const BICEPS_ACTION_EPISODIC_ALERT_REPORT: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_STATE_EVENT_SERVICE,
    "/EpisodicAlertReport"
);
pub const BICEPS_ACTION_EPISODIC_COMPONENT_REPORT: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_STATE_EVENT_SERVICE,
    "/EpisodicComponentReport"
);
pub const BICEPS_ACTION_EPISODIC_CONTEXT_REPORT: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_CONTEXT_SERVICE,
    "/EpisodicContextReport"
);
pub const BICEPS_ACTION_EPISODIC_METRIC_REPORT: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_STATE_EVENT_SERVICE,
    "/EpisodicMetricReport"
);
pub const BICEPS_ACTION_EPISODIC_OPERATIONAL_STATE_REPORT: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_STATE_EVENT_SERVICE,
    "/EpisodicOperationalStateReport"
);
pub const BICEPS_ACTION_DESCRIPTION_MODIFICATION_REPORT: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_DESCRIPTION_EVENT_SERVICE,
    "/DescriptionModificationReport"
);
pub const BICEPS_ACTION_OPERATION_INVOKED_REPORT: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_SET_SERVICE,
    "/OperationInvokedReport"
);
pub const BICEPS_ACTION_SYSTEM_ERROR_REPORT: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_STATE_EVENT_SERVICE,
    "/SystemErrorReport"
);
pub const BICEPS_ACTION_WAVEFORM_STREAM: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_WAVEFORM_SERVICE,
    "/WaveformStream"
);

pub const BICEPS_ACTION_ACTIVATE: &str =
    concatcp!(BICEPS_ACTION_PREFIX, BICEPS_SET_SERVICE, "/Activate");
pub const BICEPS_ACTION_ACTIVATE_RESPONSE: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_SET_SERVICE,
    "/ActivateResponse"
);
pub const BICEPS_ACTION_SET_ALERT_STATE: &str =
    concatcp!(BICEPS_ACTION_PREFIX, BICEPS_SET_SERVICE, "/SetAlertState");
pub const BICEPS_ACTION_SET_ALERT_STATE_RESPONSE: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_SET_SERVICE,
    "/SetAlertStateResponse"
);
pub const BICEPS_ACTION_SET_COMPONENT_STATE: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_SET_SERVICE,
    "/SetComponentState"
);
pub const BICEPS_ACTION_SET_COMPONENT_STATE_RESPONSE: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_SET_SERVICE,
    "/SetComponentStateResponse"
);
pub const BICEPS_ACTION_SET_CONTEXT_STATE: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_CONTEXT_SERVICE,
    "/SetContextState"
);
pub const BICEPS_ACTION_SET_CONTEXT_STATE_RESPONSE: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_CONTEXT_SERVICE,
    "/SetContextStateResponse"
);
pub const BICEPS_ACTION_SET_METRIC_STATE: &str =
    concatcp!(BICEPS_ACTION_PREFIX, BICEPS_SET_SERVICE, "/SetMetricState");
pub const BICEPS_ACTION_SET_METRIC_STATE_RESPONSE: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_SET_SERVICE,
    "/SetMetricStateResponse"
);
pub const BICEPS_ACTION_SET_STRING: &str =
    concatcp!(BICEPS_ACTION_PREFIX, BICEPS_SET_SERVICE, "/SetString");
pub const BICEPS_ACTION_SET_STRING_RESPONSE: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_SET_SERVICE,
    "/SetStringResponse"
);
pub const BICEPS_ACTION_SET_VALUE: &str =
    concatcp!(BICEPS_ACTION_PREFIX, BICEPS_SET_SERVICE, "/SetValue");
pub const BICEPS_ACTION_SET_VALUE_RESPONSE: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_SET_SERVICE,
    "/SetValueResponse"
);

pub const BICEPS_ACTION_GET_SUPPORTED_LANGUAGES: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_LOCALIZATION_SERVICE,
    "/GetSupportedLanguages"
);
pub const BICEPS_ACTION_GET_SUPPORTED_LANGUAGES_RESPONSE: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_LOCALIZATION_SERVICE,
    "/GetSupportedLanguagesResponse"
);
pub const BICEPS_ACTION_GET_LOCALIZED_TEXT: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_LOCALIZATION_SERVICE,
    "/GetLocalizedText"
);
pub const BICEPS_ACTION_GET_LOCALIZED_TEXT_RESPONSE: &str = concatcp!(
    BICEPS_ACTION_PREFIX,
    BICEPS_LOCALIZATION_SERVICE,
    "/GetLocalizedTextResponse"
);

macro_rules! soap_element_impl_biceps {
    ($struct_name:ident, $constant_name:ident) => {
        impl crate::xml::messages::common::SoapElement for $struct_name {
            fn tag() -> protosdc_xml::QNameSlice {
                paste::paste! {
                    crate::xml::messages::biceps_ext::[<$constant_name _TAG>]
                }
            }
            fn tag_str() -> (Option<&'static str>, &'static str) {
                paste::paste! {
                    crate::xml::messages::biceps_ext::[<$constant_name _TAG_STR>]
                }
            }
            fn ns_tag() -> quick_xml::name::ResolveResult<'static> {
                paste::paste! {
                    crate::xml::messages::biceps_ext::[<$constant_name _TAG>].0
                }
            }
            fn ns_tag_str() -> Option<&'static str> {
                paste::paste! {
                    crate::xml::messages::biceps_ext::[<$constant_name _TAG_STR>].0
                }
            }
        }
    };
}

soap_element_impl_biceps!(GetMdibResponse, GET_MDIB_RESPONSE);
soap_element_impl_biceps!(GetMdDescriptionResponse, GET_MD_DESCRIPTION_RESPONSE);
soap_element_impl_biceps!(GetMdStateResponse, GET_MD_STATE_RESPONSE);
soap_element_impl_biceps!(GetContextStatesResponse, GET_CONTEXT_STATES_RESPONSE);

soap_element_impl_biceps!(
    GetSupportedLanguagesResponse,
    GET_SUPPORTED_LANGUAGES_RESPONSE
);
soap_element_impl_biceps!(GetLocalizedTextResponse, GET_LOCALIZED_TEXT_RESPONSE);

soap_element_impl_biceps!(ActivateResponse, ACTIVATE_RESPONSE);
soap_element_impl_biceps!(SetAlertStateResponse, SET_ALERT_STATE_RESPONSE);
soap_element_impl_biceps!(SetComponentStateResponse, SET_COMPONENT_STATE_RESPONSE);
soap_element_impl_biceps!(SetContextStateResponse, SET_CONTEXT_STATE_RESPONSE);
soap_element_impl_biceps!(SetMetricStateResponse, SET_METRIC_STATE_RESPONSE);
soap_element_impl_biceps!(SetStringResponse, SET_STRING_RESPONSE);
soap_element_impl_biceps!(SetValueResponse, SET_VALUE_RESPONSE);

soap_element_impl_biceps!(EpisodicAlertReport, EPISODIC_ALERT_REPORT);
soap_element_impl_biceps!(EpisodicComponentReport, EPISODIC_COMPONENT_REPORT);
soap_element_impl_biceps!(EpisodicContextReport, EPISODIC_CONTEXT_REPORT);
soap_element_impl_biceps!(EpisodicMetricReport, EPISODIC_METRIC_REPORT);
soap_element_impl_biceps!(
    EpisodicOperationalStateReport,
    EPISODIC_OPERATIONAL_STATE_REPORT
);
soap_element_impl_biceps!(WaveformStream, WAVEFORM_STREAM);
soap_element_impl_biceps!(
    DescriptionModificationReport,
    DESCRIPTION_MODIFICATION_REPORT
);
soap_element_impl_biceps!(OperationInvokedReport, OPERATION_INVOKED_REPORT);
