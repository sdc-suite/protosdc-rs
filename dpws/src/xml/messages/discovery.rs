use std::collections::HashMap;

use crate::xml::messages::addressing::{WsaEndpointReference, XmlElement};
use crate::xml::messages::common::{
    soap_element_impl, write_attributes, HttpUri, APP_SEQUENCE, BYE, DO_NOT_CARE_QNAME,
    ENDPOINT_REFERENCE, ENDPOINT_REFERENCE_TAG, ENDPOINT_REFERENCE_TAG_REF,
    ENDPOINT_REFERENCE_TAG_STR, HELLO, MATCH_BY, METADATA_VERSION_TAG_REF,
    METADATA_VERSION_TAG_STR, PROBE, PROBE_MATCH, PROBE_MATCHES, PROBE_MATCH_TAG,
    PROBE_MATCH_TAG_REF, PROBE_MATCH_TAG_STR, RESOLVE, RESOLVE_MATCH, RESOLVE_MATCHES,
    RESOLVE_MATCH_TAG, RESOLVE_MATCH_TAG_REF, RESOLVE_MATCH_TAG_STR, SCOPES, SCOPES_TAG,
    SCOPES_TAG_REF, SCOPES_TAG_STR, TYPES, TYPES_TAG, TYPES_TAG_REF, TYPES_TAG_STR,
    WSDD_INSTANCE_ID, WSDD_MESSAGE_NUMBER, WSDD_SEQUENCE_ID, WS_DISCOVERY_NAMESPACE,
    X_ADDRS_TAG_REF, X_ADDRS_TAG_STR,
};
use protosdc_xml::xml_reader::collect_attributes_reader;
use protosdc_xml::{ComplexXmlTypeWrite, WriterError, XmlWriter};
use protosdc_xml::{
    ExpandedName, GenericXmlReaderComplexTypeRead, GenericXmlReaderSimpleTypeRead, ParserError,
    XmlReaderSimpleXmlTypeRead,
};
use quick_xml::events::{BytesStart, Event};
use std::io::BufRead;
use std::ops::Deref;

#[derive(Clone, Debug, PartialEq)]
pub struct ProbeMatches {
    pub attributes: HashMap<ExpandedName, String>,
    pub probe_match: Vec<ProbeMatch>,
    pub children: Vec<XmlElement>,
}
soap_element_impl!(ProbeMatches, PROBE);

#[derive(Clone, Debug, PartialEq)]
pub struct Hello {
    pub hello: HelloProbeMatchResolveMatch,
}
soap_element_impl!(Hello, HELLO);

#[derive(Clone, Debug, PartialEq)]
pub struct ProbeMatch {
    pub probe_match: HelloProbeMatchResolveMatch,
}

#[derive(Clone, Debug, PartialEq)]
pub struct ResolveMatch {
    pub resolve_match: HelloProbeMatchResolveMatch,
}

#[derive(Clone, Debug, PartialEq)]
pub struct HelloProbeMatchResolveMatch {
    pub attributes: HashMap<ExpandedName, String>,

    pub endpoint_reference: WsaEndpointReference,
    pub types: Option<Types>,
    pub scopes: Option<Scopes>,
    pub x_addrs: Option<HttpUriList>,
    pub metadata_version: u32,
    pub children: Vec<XmlElement>,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Bye {
    pub attributes: HashMap<ExpandedName, String>,

    pub endpoint_reference: WsaEndpointReference,
    pub types: Option<Types>,
    pub scopes: Option<Scopes>,
    pub x_addrs: Option<HttpUriList>,
    pub metadata_version: Option<u32>,
    pub children: Vec<XmlElement>,
}
soap_element_impl!(Bye, BYE);

#[derive(Clone, Debug, PartialEq)]
struct HelloByeProbeMatchCommon {
    pub attributes: HashMap<ExpandedName, String>,

    pub endpoint_reference: WsaEndpointReference,
    pub types: Option<Types>,
    pub scopes: Option<Scopes>,
    pub x_addrs: Option<HttpUriList>,
    pub metadata_version: Option<u32>,
    pub children: Vec<XmlElement>,
}

#[derive(Clone, Debug, PartialEq)]
pub struct AppSequence {
    pub attributes: HashMap<ExpandedName, String>,

    pub instance_id: u64,
    pub sequence_id: Option<String>,
    pub message_number: u32,
}
soap_element_impl!(AppSequence, APP_SEQUENCE);

impl From<HelloProbeMatchResolveMatch> for HelloByeProbeMatchCommon {
    fn from(value: HelloProbeMatchResolveMatch) -> Self {
        Self {
            attributes: value.attributes,
            endpoint_reference: value.endpoint_reference,
            types: value.types,
            scopes: value.scopes,
            x_addrs: value.x_addrs,
            metadata_version: Some(value.metadata_version),
            children: value.children,
        }
    }
}

impl From<Bye> for HelloByeProbeMatchCommon {
    fn from(value: Bye) -> Self {
        Self {
            attributes: value.attributes,
            endpoint_reference: value.endpoint_reference,
            types: value.types,
            scopes: value.scopes,
            x_addrs: value.x_addrs,
            metadata_version: value.metadata_version,
            children: value.children,
        }
    }
}

impl From<HelloByeProbeMatchCommon> for Bye {
    fn from(value: HelloByeProbeMatchCommon) -> Self {
        Self {
            attributes: value.attributes,
            endpoint_reference: value.endpoint_reference,
            types: value.types,
            scopes: value.scopes,
            x_addrs: value.x_addrs,
            metadata_version: value.metadata_version,
            children: value.children,
        }
    }
}

impl From<HelloProbeMatchResolveMatch> for Bye {
    fn from(value: HelloProbeMatchResolveMatch) -> Self {
        let common: HelloByeProbeMatchCommon = value.into();
        common.into()
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct Probe {
    pub attributes: HashMap<ExpandedName, String>,
    pub types: Option<Types>,
    pub scopes: Option<Scopes>,
    pub children: Vec<XmlElement>,
}
soap_element_impl!(Probe, PROBE);

#[derive(Clone, Debug, PartialEq)]
pub struct Resolve {
    pub attributes: HashMap<ExpandedName, String>,
    pub endpoint_references: Vec<WsaEndpointReference>,
    pub children: Vec<XmlElement>,
}
soap_element_impl!(Resolve, RESOLVE);

#[derive(Clone, Debug, PartialEq)]
pub struct ResolveMatches {
    pub attributes: HashMap<ExpandedName, String>,
    pub resolve_match: Option<HelloProbeMatchResolveMatch>,
    pub children: Vec<XmlElement>,
}
soap_element_impl!(ResolveMatches, RESOLVE_MATCHES);

#[derive(Clone, Debug, PartialEq)]
pub struct Types {
    pub types: ExpandedNameList,
}

impl From<ExpandedNameList> for Types {
    fn from(value: ExpandedNameList) -> Self {
        Self { types: value }
    }
}

impl From<&[ExpandedName]> for Types {
    fn from(value: &[ExpandedName]) -> Self {
        Self {
            types: value.into(),
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct Scopes {
    pub match_by: Option<String>,
    pub attributes: HashMap<ExpandedName, String>,

    pub scopes: UriList,
}

impl From<&[String]> for Scopes {
    fn from(value: &[String]) -> Self {
        Self {
            match_by: None,
            attributes: HashMap::with_capacity(0),
            scopes: value.into(),
        }
    }
}

#[derive(Clone, Debug, PartialEq, Default)]
pub struct ExpandedNameList {
    pub list: Vec<ExpandedName>,
}

impl From<&[ExpandedName]> for ExpandedNameList {
    fn from(value: &[ExpandedName]) -> Self {
        Self {
            list: value.to_vec(),
        }
    }
}

#[derive(Clone, Debug, PartialEq, Default)]
pub struct HttpUriList {
    pub list: Vec<HttpUri>,
}

// impl From<&[http::Uri]> for AnyUriList {
//     fn from(value: &[http::Uri]) -> Self {
//         Self {
//             list: value.to_vec(),
//         }
//     }
// }

impl From<&[HttpUri]> for HttpUriList {
    fn from(value: &[HttpUri]) -> Self {
        Self {
            list: value.to_vec(),
        }
    }
}

#[derive(Clone, Debug, PartialEq, Default)]
pub struct UriList {
    pub list: Vec<String>,
}

impl From<&[String]> for UriList {
    fn from(value: &[String]) -> Self {
        Self {
            list: value.to_vec(),
        }
    }
}

//
// parsers
//

impl GenericXmlReaderComplexTypeRead for Resolve {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
            EndpointReferenceStart,
            EndpointReferenceEnd,
            AnyStart,
            AnyEnd,
        }
        let mut state = State::Root;

        let attributes = collect_attributes_reader(event, reader)?;

        let mut endpoint_references: Vec<WsaEndpointReference> = Vec::with_capacity(1);
        let mut children: Vec<XmlElement> = Vec::with_capacity(0);

        state = State::Start;
        let mut buf = vec![];

        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => {
                    match (state, (ns, e.local_name().into_inner())) {
                        (State::Start, ENDPOINT_REFERENCE_TAG_REF)
                        | (State::EndpointReferenceEnd, ENDPOINT_REFERENCE_TAG_REF) => {
                            state = State::EndpointReferenceStart;
                            endpoint_references.push(WsaEndpointReference::from_xml_complex(
                                ENDPOINT_REFERENCE_TAG,
                                e,
                                reader,
                            )?);
                            state = State::EndpointReferenceEnd;
                        }
                        (State::Start, _)
                        | (State::EndpointReferenceEnd, _)
                        | (State::AnyEnd, _) => {
                            state = State::AnyStart;
                            children.push(XmlElement::from_xml_complex(
                                DO_NOT_CARE_QNAME,
                                e,
                                reader,
                            )?);
                            state = State::AnyEnd;
                        }

                        _ => Err(ParserError::UnexpectedParserStartState {
                            element_name: match String::from_utf8(
                                e.local_name().into_inner().to_vec(),
                            ) {
                                Ok(it) => it,
                                Err(_) => "FromUtf8Error".to_string(),
                            },
                            parser_state: format!("{:?}", state),
                        })?,
                    }
                }

                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (_, (ns, local_name)) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            attributes,
                            endpoint_references,
                            children,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },
                Ok((_, Event::Text(ref _e))) => Err(ParserError::UnexpectedParserTextEventState {
                    parser_state: format!("{:?}", state),
                })?,
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl GenericXmlReaderComplexTypeRead for ResolveMatches {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
            ResolveMatchStart,
            ResolveMatchEnd,
            AnyStart,
            AnyEnd,
        }
        let mut state = State::Root;

        let attributes = collect_attributes_reader(event, reader)?;

        let mut resolve_match: Option<HelloProbeMatchResolveMatch> = None;
        let mut children: Vec<XmlElement> = Vec::with_capacity(0);

        state = State::Start;
        let mut buf = vec![];

        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => {
                    match (state, (ns, e.local_name().into_inner())) {
                        (State::Start, RESOLVE_MATCH_TAG_REF) => {
                            state = State::ResolveMatchStart;
                            resolve_match = Some(HelloProbeMatchResolveMatch::from_xml_complex(
                                RESOLVE_MATCH_TAG,
                                e,
                                reader,
                            )?);
                            state = State::ResolveMatchEnd;
                        }

                        (State::Start, _) | (State::ResolveMatchEnd, _) | (State::AnyEnd, _) => {
                            state = State::AnyStart;
                            children.push(XmlElement::from_xml_complex(
                                DO_NOT_CARE_QNAME,
                                e,
                                reader,
                            )?);
                            state = State::AnyEnd;
                        }

                        _ => Err(ParserError::UnexpectedParserStartState {
                            element_name: match String::from_utf8(
                                e.local_name().into_inner().to_vec(),
                            ) {
                                Ok(it) => it,
                                Err(_) => "FromUtf8Error".to_string(),
                            },
                            parser_state: format!("{:?}", state),
                        })?,
                    }
                }

                Ok((ref ns, Event::End(ref e))) => match (state, ns, e.local_name().into_inner()) {
                    (_, ns, local_name) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            attributes,
                            resolve_match,
                            children,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref _e))) => Err(ParserError::UnexpectedParserTextEventState {
                    parser_state: format!("{:?}", state),
                })?,
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl GenericXmlReaderComplexTypeRead for ProbeMatches {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
            ProbeMatchStart,
            ProbeMatchEnd,
            AnyStart,
            AnyEnd,
        }
        let mut state = State::Root;

        let attributes = collect_attributes_reader(event, reader)?;

        let mut probe_match: Vec<ProbeMatch> = Vec::with_capacity(1); // one is the likeliest number of matches
        let mut children: Vec<XmlElement> = Vec::with_capacity(0);

        state = State::Start;
        let mut buf = vec![];

        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => {
                    match (state, (ns, e.local_name().into_inner())) {
                        (State::Start, PROBE_MATCH_TAG_REF)
                        | (State::ProbeMatchEnd, PROBE_MATCH_TAG_REF) => {
                            state = State::ProbeMatchStart;

                            probe_match.push(ProbeMatch {
                                probe_match: HelloProbeMatchResolveMatch::from_xml_complex(
                                    PROBE_MATCH_TAG,
                                    e,
                                    reader,
                                )?,
                            });
                            state = State::ProbeMatchEnd;
                        }

                        (State::Start, _) | (State::ProbeMatchEnd, _) | (State::AnyEnd, _) => {
                            state = State::AnyStart;
                            children.push(XmlElement::from_xml_complex(
                                DO_NOT_CARE_QNAME,
                                e,
                                reader,
                            )?);
                            state = State::AnyEnd;
                        }

                        _ => Err(ParserError::UnexpectedParserStartState {
                            element_name: match String::from_utf8(
                                e.local_name().into_inner().to_vec(),
                            ) {
                                Ok(it) => it,
                                Err(_) => "FromUtf8Error".to_string(),
                            },
                            parser_state: format!("{:?}", state),
                        })?,
                    }
                }

                Ok((ref ns, Event::End(ref e))) => match (state, ns, e.local_name().into_inner()) {
                    (_, ns, local_name) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            attributes,
                            probe_match,
                            children,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref _e))) => Err(ParserError::UnexpectedParserTextEventState {
                    parser_state: format!("{:?}", state),
                })?,
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl GenericXmlReaderComplexTypeRead for Hello {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        let common = HelloProbeMatchResolveMatch::from_xml_complex(tag_name, event, reader)?;
        Ok(Hello { hello: common })
    }
}

impl GenericXmlReaderComplexTypeRead for Probe {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
            TypesStart,
            TypesEnd,
            ScopesStart,
            ScopesEnd,
            AnyStart,
            AnyEnd,
        }
        let mut state = State::Root;

        let attributes = collect_attributes_reader(event, reader)?;

        let mut types: Option<Types> = None;
        let mut scopes: Option<Scopes> = None;
        let mut children: Vec<XmlElement> = Vec::with_capacity(0);

        state = State::Start;
        let mut buf = vec![];

        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => {
                    match (state, (ns, e.local_name().into_inner())) {
                        (State::Start, TYPES_TAG_REF) => {
                            state = State::TypesStart;
                            types = Some(Types::from_xml_complex(TYPES_TAG, e, reader)?);
                            state = State::TypesEnd;
                        }
                        (State::Start, SCOPES_TAG_REF) | (State::TypesEnd, SCOPES_TAG_REF) => {
                            state = State::ScopesStart;
                            scopes = Some(Scopes::from_xml_complex(SCOPES_TAG, e, reader)?);
                            state = State::ScopesEnd;
                        }

                        (State::Start, _)
                        | (State::TypesEnd, _)
                        | (State::ScopesEnd, _)
                        | (State::AnyEnd, _) => {
                            state = State::AnyStart;
                            children.push(XmlElement::from_xml_complex(
                                DO_NOT_CARE_QNAME,
                                e,
                                reader,
                            )?);
                            state = State::AnyEnd;
                        }

                        _ => Err(ParserError::UnexpectedParserStartState {
                            element_name: match String::from_utf8(
                                e.local_name().into_inner().to_vec(),
                            ) {
                                Ok(it) => it,
                                Err(_) => "FromUtf8Error".to_string(),
                            },
                            parser_state: format!("{:?}", state),
                        })?,
                    }
                }

                Ok((ref ns, Event::End(ref e))) => match (state, ns, e.local_name().into_inner()) {
                    (_, ns, local_name) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            attributes,
                            types,
                            scopes,
                            children,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref _e))) => Err(ParserError::UnexpectedParserTextEventState {
                    parser_state: format!("{:?}", state),
                })?,
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl GenericXmlReaderComplexTypeRead for Scopes {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
        }
        let mut state = State::Root;

        let mut attributes = collect_attributes_reader(event, reader)?;
        let match_by = attributes.remove(&ExpandedName {
            namespace: None,
            local_name: "MatchBy".to_string(),
        });

        let mut scopes: Option<UriList> = None;

        state = State::Start;
        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref _ns, Event::Start(ref e))) => {
                    Err(ParserError::UnexpectedParserStartState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?
                }

                Ok((ref ns, Event::End(ref e))) => match (state, ns, e.local_name().into_inner()) {
                    (_, ns, local_name) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            attributes,
                            match_by,
                            scopes: scopes.unwrap_or_default(), // no scopes is ok
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref e))) => match state {
                    State::Start => {
                        scopes = Some(UriList::from_xml_simple(
                            e.unescape().map_err(ParserError::QuickXMLError)?.as_bytes(),
                            reader,
                        )?);
                    }
                    _ => Err(ParserError::UnexpectedParserTextEventState {
                        parser_state: format!("{:?}", state),
                    })?,
                },
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl GenericXmlReaderComplexTypeRead for Types {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        _event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
        }
        let mut state = State::Root;

        let mut types: Option<ExpandedNameList> = None;

        state = State::Start;

        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref _ns, Event::Start(ref e))) => {
                    Err(ParserError::UnexpectedParserStartState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?
                }

                Ok((ref ns, Event::End(ref e))) => match (state, ns, e.local_name().into_inner()) {
                    (_, ns, local_name) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            types: types.unwrap_or_default(),
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref e))) => match state {
                    State::Start => {
                        types = Some(ExpandedNameList::from_xml_simple(
                            e.unescape().map_err(ParserError::QuickXMLError)?.as_bytes(),
                            reader,
                        )?);
                    }
                    _ => Err(ParserError::UnexpectedParserTextEventState {
                        parser_state: format!("{:?}", state),
                    })?,
                },
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl GenericXmlReaderSimpleTypeRead for ExpandedNameList {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // IntelliJ inference here is just wrong
    //noinspection RsTypeCheck
    fn from_xml_simple<B: BufRead>(
        data: &[u8],
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        let elements = data.split(|it| it == &32u8); // split on space
        let parsed = elements
            .map(|it| ExpandedName::from_xml_simple(it, reader))
            .collect::<Result<Vec<_>, ParserError>>()?;
        Ok(Self { list: parsed })
    }
}

impl ExpandedNameList {
    pub(crate) fn to_xml_simple(
        &self,
        writer: &mut XmlWriter<Vec<u8>>,
    ) -> Result<String, WriterError> {
        Ok(self
            .list
            .iter()
            .map(|it| writer.qualify_tag_add_ns(it.namespace.as_deref(), it.local_name.as_ref()))
            .collect::<Result<Vec<String>, WriterError>>()?
            .join(" "))
    }
}

impl GenericXmlReaderSimpleTypeRead for HttpUriList {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // IntelliJ inference here is just wrong
    //noinspection RsUnresolvedReference
    //noinspection RsTypeCheck
    fn from_xml_simple<B: BufRead>(
        data: &[u8],
        _reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        let elements = data.split(|it| it == &32u8); // split on space
        let parsed = elements
            .map(std::str::from_utf8)
            .flat_map(|it| {
                it.map(|s| HttpUri::try_from(s.to_string()).map_err(ParserError::MappingError))
            })
            .collect::<Result<Vec<_>, ParserError>>()?;
        Ok(Self { list: parsed })
    }
}

impl GenericXmlReaderSimpleTypeRead for UriList {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // IntelliJ inference here is just wrong
    //noinspection RsUnresolvedReference
    //noinspection RsTypeCheck
    fn from_xml_simple<B: BufRead>(
        data: &[u8],
        _reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        let elements = data.split(|it| it == &32u8); // split on space
        let parsed = elements
            .map(std::str::from_utf8)
            .flat_map(|it| it.map(|s| s.to_string()))
            .collect::<Vec<String>>();
        Ok(Self { list: parsed })
    }
}

impl HttpUriList {
    fn to_xml_simple(&self, _writer: &mut XmlWriter<Vec<u8>>) -> Result<String, WriterError> {
        Ok(self
            .list
            .iter()
            .map(|it| it.to_string())
            .collect::<Vec<_>>()
            .join(" "))
    }
}

impl UriList {
    fn to_xml_simple(&self, _writer: &mut XmlWriter<Vec<u8>>) -> Result<String, WriterError> {
        Ok(self
            .list
            .iter()
            .map(|it| it.as_str())
            .collect::<Vec<_>>()
            .join(" "))
    }
}

impl GenericXmlReaderComplexTypeRead for HelloProbeMatchResolveMatch {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        let common = HelloByeProbeMatchCommon::from_xml_complex(tag_name, event, reader)?;
        Ok(Self {
            attributes: common.attributes,
            endpoint_reference: common.endpoint_reference,
            types: common.types,
            scopes: common.scopes,
            x_addrs: common.x_addrs,
            metadata_version: common.metadata_version.ok_or(
                ParserError::MandatoryElementMissing {
                    element_name: "MetadataVersion".to_string(),
                },
            )?,
            children: common.children,
        })
    }
}

impl GenericXmlReaderComplexTypeRead for Bye {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        let common = HelloByeProbeMatchCommon::from_xml_complex(tag_name, event, reader)?;
        Ok(Self {
            attributes: common.attributes,
            endpoint_reference: common.endpoint_reference,
            types: common.types,
            scopes: common.scopes,
            x_addrs: common.x_addrs,
            metadata_version: common.metadata_version,
            children: common.children,
        })
    }
}

impl GenericXmlReaderComplexTypeRead for HelloByeProbeMatchCommon {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
            EndpointReferenceStart,
            EndpointReferenceEnd,
            TypesStart,
            TypesEnd,
            ScopesStart,
            ScopesEnd,
            XAddrsStart,
            XAddrsEnd,
            MetadataVersionStart,
            MetadataVersionEnd,
            AnyStart,
            AnyEnd,
        }
        let mut state = State::Root;

        // collect attributes
        let attributes: HashMap<ExpandedName, String> = collect_attributes_reader(event, reader)?;

        // read value
        state = State::Start;
        let mut endpoint_reference = None;
        let mut types: Option<Types> = None;
        let mut scopes: Option<Scopes> = None;
        let mut x_addrs: Option<HttpUriList> = None;
        let mut metadata_version: Option<u32> = None;
        let mut children: Vec<XmlElement> = Vec::with_capacity(0);

        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => {
                    match (state, (ns, e.local_name().into_inner())) {
                        (State::Start, ENDPOINT_REFERENCE_TAG_REF) => {
                            state = State::EndpointReferenceStart;
                            endpoint_reference = Some(WsaEndpointReference::from_xml_complex(
                                ENDPOINT_REFERENCE_TAG,
                                e,
                                reader,
                            )?);
                            state = State::EndpointReferenceEnd;
                        }
                        (State::EndpointReferenceEnd, TYPES_TAG_REF) => {
                            state = State::TypesStart;
                            types = Some(Types::from_xml_complex(TYPES_TAG, e, reader)?);
                            state = State::TypesEnd;
                        }

                        (State::EndpointReferenceEnd, SCOPES_TAG_REF)
                        | (State::TypesEnd, SCOPES_TAG_REF) => {
                            state = State::ScopesStart;
                            scopes = Some(Scopes::from_xml_complex(SCOPES_TAG, e, reader)?);
                            state = State::ScopesEnd;
                        }

                        (State::EndpointReferenceEnd, X_ADDRS_TAG_REF)
                        | (State::TypesEnd, X_ADDRS_TAG_REF)
                        | (State::ScopesEnd, X_ADDRS_TAG_REF) => {
                            state = State::XAddrsStart;
                        }

                        (State::EndpointReferenceEnd, METADATA_VERSION_TAG_REF)
                        | (State::TypesEnd, METADATA_VERSION_TAG_REF)
                        | (State::ScopesEnd, METADATA_VERSION_TAG_REF)
                        | (State::XAddrsEnd, METADATA_VERSION_TAG_REF) => {
                            state = State::MetadataVersionStart;
                        }

                        (State::EndpointReferenceEnd, _)
                        | (State::TypesEnd, _)
                        | (State::ScopesEnd, _)
                        | (State::XAddrsEnd, _)
                        | (State::MetadataVersionEnd, _)
                        | (State::AnyEnd, _) => {
                            state = State::AnyStart;
                            children.push(XmlElement::from_xml_complex(
                                DO_NOT_CARE_QNAME,
                                e,
                                reader,
                            )?);
                            state = State::AnyEnd
                        }
                        _ => Err(ParserError::UnexpectedParserStartState {
                            element_name: match String::from_utf8(
                                e.local_name().into_inner().to_vec(),
                            ) {
                                Ok(it) => it,
                                Err(_) => "FromUtf8Error".to_string(),
                            },
                            parser_state: format!("{:?}", state),
                        })?,
                    }
                }

                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (State::EndpointReferenceEnd, (ns, local_name))
                    | (State::TypesEnd, (ns, local_name))
                    | (State::ScopesEnd, (ns, local_name))
                    | (State::XAddrsEnd, (ns, local_name))
                    | (State::MetadataVersionEnd, (ns, local_name))
                    | (State::AnyEnd, (ns, local_name))
                        if &tag_name.0 == ns && tag_name.1 == local_name =>
                    {
                        state = State::End;
                        return Ok(Self {
                            attributes,
                            endpoint_reference: endpoint_reference.ok_or(
                                ParserError::MandatoryElementMissing {
                                    element_name: ENDPOINT_REFERENCE.to_string(),
                                },
                            )?,
                            types,
                            scopes,
                            x_addrs,
                            metadata_version,
                            children,
                        });
                    }
                    (State::TypesStart, TYPES_TAG_REF) => {
                        state = State::TypesEnd;
                    }
                    (State::ScopesStart, SCOPES_TAG_REF) => {
                        state = State::ScopesEnd;
                    }
                    (State::XAddrsStart, X_ADDRS_TAG_REF) => {
                        state = State::XAddrsEnd;
                    }
                    (State::MetadataVersionStart, METADATA_VERSION_TAG_REF) => {
                        state = State::MetadataVersionEnd;
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref e))) => match state {
                    State::MetadataVersionStart => {
                        let decoded = &e.unescape().map_err(ParserError::QuickXMLError)?;
                        metadata_version = Some(
                            decoded
                                .deref()
                                .parse::<u32>()
                                .map_err(ParserError::ParseIntError)?,
                        )
                    }
                    State::XAddrsStart => {
                        x_addrs = Some(HttpUriList::from_xml_simple(
                            e.unescape().map_err(ParserError::QuickXMLError)?.as_bytes(),
                            reader,
                        )?);
                    }
                    _ => Err(ParserError::UnexpectedParserTextEventState {
                        parser_state: format!("{:?}", state),
                    })?,
                },
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl GenericXmlReaderComplexTypeRead for AppSequence {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
        }
        let mut state = State::Root;

        // collect attributes
        let mut attributes: HashMap<ExpandedName, String> =
            collect_attributes_reader(event, reader)?;

        let instance_id_opt: Option<String> = attributes.remove(&ExpandedName {
            namespace: None,
            local_name: WSDD_INSTANCE_ID.to_string(),
        });
        let instance_id: Option<u64> = match instance_id_opt {
            Some(it) => Some(
                it.deref()
                    .parse::<u64>()
                    .map_err(ParserError::ParseIntError)?,
            ),
            None => None,
        };

        let sequence_id: Option<String> = attributes.remove(&ExpandedName {
            namespace: None,
            local_name: WSDD_SEQUENCE_ID.to_string(),
        });

        let message_number_opt: Option<String> = attributes.remove(&ExpandedName {
            namespace: None,
            local_name: WSDD_MESSAGE_NUMBER.to_string(),
        });

        let message_number: Option<u32> = match message_number_opt {
            Some(it) => Some(
                it.deref()
                    .parse::<u32>()
                    .map_err(ParserError::ParseIntError)?,
            ),
            None => None,
        };

        // read value
        state = State::Start;

        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref _ns, Event::Start(ref e))) => {
                    Err(ParserError::UnexpectedParserStartState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?
                }

                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (State::Start, (ns, local_name))
                        if &tag_name.0 == ns && tag_name.1 == local_name =>
                    {
                        state = State::End;
                        return Ok(Self {
                            attributes,
                            instance_id: instance_id.ok_or(
                                ParserError::MandatoryAttributeMissing {
                                    attribute_name: WSDD_INSTANCE_ID.to_string(),
                                },
                            )?,
                            sequence_id,
                            message_number: message_number.ok_or(
                                ParserError::MandatoryAttributeMissing {
                                    attribute_name: WSDD_MESSAGE_NUMBER.to_string(),
                                },
                            )?,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref _e))) => Err(ParserError::UnexpectedParserTextEventState {
                    parser_state: format!("{:?}", state),
                })?,
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

//
// writers
//

impl ComplexXmlTypeWrite for Resolve {
    fn to_xml_complex(
        &self,
        tag_name: Option<(Option<&'static str>, &'static str)>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_DISCOVERY_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| RESOLVE, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;

        for endpoint_reference in &self.endpoint_references {
            endpoint_reference.to_xml_complex(Some(ENDPOINT_REFERENCE_TAG_STR), writer, false)?;
        }

        for child in &self.children {
            child.to_xml_complex(None, writer, false)?;
        }

        writer.write_end(element_namespace, element_tag)?;

        Ok(())
    }
}

impl ComplexXmlTypeWrite for ResolveMatches {
    fn to_xml_complex(
        &self,
        tag_name: Option<(Option<&'static str>, &'static str)>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_DISCOVERY_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| RESOLVE_MATCHES, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;

        if let Some(endpoint_reference) = &self.resolve_match {
            endpoint_reference.to_xml_complex(Some(RESOLVE_MATCH_TAG_STR), writer, false)?;
        }

        for child in &self.children {
            child.to_xml_complex(None, writer, false)?;
        }

        writer.write_end(element_namespace, element_tag)?;

        Ok(())
    }
}

impl ComplexXmlTypeWrite for Probe {
    fn to_xml_complex(
        &self,
        tag_name: Option<(Option<&'static str>, &'static str)>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_DISCOVERY_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| PROBE, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;

        if let Some(types) = &self.types {
            types.to_xml_complex(Some(TYPES_TAG_STR), writer, false)?;
        }

        if let Some(scopes) = &self.scopes {
            scopes.to_xml_complex(Some(SCOPES_TAG_STR), writer, false)?;
        }

        for child in &self.children {
            child.to_xml_complex(None, writer, false)?;
        }

        writer.write_end(element_namespace, element_tag)?;

        Ok(())
    }
}

impl ComplexXmlTypeWrite for Types {
    fn to_xml_complex(
        &self,
        tag_name: Option<(Option<&'static str>, &'static str)>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_DISCOVERY_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| TYPES, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;

        let mapped = self.types.to_xml_simple(writer)?;

        writer.write_text(mapped.as_str())?;

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

impl ComplexXmlTypeWrite for Scopes {
    fn to_xml_complex(
        &self,
        tag_name: Option<(Option<&'static str>, &'static str)>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_DISCOVERY_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| SCOPES, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;

        if let Some(match_by) = &self.match_by {
            writer.add_attribute((MATCH_BY, match_by.as_str()))?;
        }

        let mapped = self.scopes.to_xml_simple(writer)?;

        writer.write_text(mapped.as_str())?;

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

impl ComplexXmlTypeWrite for ProbeMatches {
    fn to_xml_complex(
        &self,
        tag_name: Option<(Option<&'static str>, &'static str)>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_DISCOVERY_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| PROBE_MATCHES, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;

        for probe_match in &self.probe_match {
            probe_match.to_xml_complex(Some(PROBE_MATCH_TAG_STR), writer, false)?;
        }

        for child in &self.children {
            child.to_xml_complex(None, writer, false)?;
        }

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

impl ComplexXmlTypeWrite for HelloByeProbeMatchCommon {
    fn to_xml_complex(
        &self,
        tag_name: Option<(Option<&'static str>, &'static str)>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_DISCOVERY_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| PROBE_MATCH, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;

        self.endpoint_reference
            .to_xml_complex(Some(ENDPOINT_REFERENCE_TAG_STR), writer, false)?;

        if let Some(types) = &self.types {
            types.to_xml_complex(Some(TYPES_TAG_STR), writer, false)?;
        }

        if let Some(scopes) = &self.scopes {
            scopes.to_xml_complex(Some(SCOPES_TAG_STR), writer, false)?;
        }

        if let Some(x_addrs) = &self.x_addrs {
            writer.write_start(X_ADDRS_TAG_STR.0, X_ADDRS_TAG_STR.1)?;
            let text = x_addrs.to_xml_simple(writer)?;
            writer.write_text(text.as_str())?;
            writer.write_end(X_ADDRS_TAG_STR.0, X_ADDRS_TAG_STR.1)?;
        }

        if let Some(metadata_version) = &self.metadata_version {
            writer.write_start(METADATA_VERSION_TAG_STR.0, METADATA_VERSION_TAG_STR.1)?;
            writer.write_text(metadata_version.to_string().as_str())?;
            writer.write_end(METADATA_VERSION_TAG_STR.0, METADATA_VERSION_TAG_STR.1)?;
        }

        for child in &self.children {
            child.to_xml_complex(None, writer, false)?;
        }

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

impl ComplexXmlTypeWrite for HelloProbeMatchResolveMatch {
    fn to_xml_complex(
        &self,
        tag_name: Option<(Option<&'static str>, &'static str)>,
        writer: &mut XmlWriter<Vec<u8>>,
        xsi_type: bool,
    ) -> Result<(), WriterError> {
        let common: HelloByeProbeMatchCommon = self.clone().into();
        common.to_xml_complex(tag_name, writer, xsi_type)
    }
}

impl ComplexXmlTypeWrite for Hello {
    fn to_xml_complex(
        &self,
        tag_name: Option<(Option<&'static str>, &'static str)>,
        writer: &mut XmlWriter<Vec<u8>>,
        xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_DISCOVERY_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| HELLO, |(_, it)| it);

        let common: HelloByeProbeMatchCommon = self.hello.clone().into();
        common.to_xml_complex(Some((element_namespace, element_tag)), writer, xsi_type)
    }
}

impl ComplexXmlTypeWrite for ProbeMatch {
    fn to_xml_complex(
        &self,
        tag_name: Option<(Option<&'static str>, &'static str)>,
        writer: &mut XmlWriter<Vec<u8>>,
        xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_DISCOVERY_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| PROBE_MATCH, |(_, it)| it);

        let common: HelloByeProbeMatchCommon = self.probe_match.clone().into();
        common.to_xml_complex(Some((element_namespace, element_tag)), writer, xsi_type)
    }
}

impl ComplexXmlTypeWrite for ResolveMatch {
    fn to_xml_complex(
        &self,
        tag_name: Option<(Option<&'static str>, &'static str)>,
        writer: &mut XmlWriter<Vec<u8>>,
        xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_DISCOVERY_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| RESOLVE_MATCH, |(_, it)| it);

        let common: HelloByeProbeMatchCommon = self.resolve_match.clone().into();
        common.to_xml_complex(Some((element_namespace, element_tag)), writer, xsi_type)
    }
}

impl ComplexXmlTypeWrite for Bye {
    fn to_xml_complex(
        &self,
        tag_name: Option<(Option<&'static str>, &'static str)>,
        writer: &mut XmlWriter<Vec<u8>>,
        xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_DISCOVERY_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| BYE, |(_, it)| it);

        let common: HelloByeProbeMatchCommon = self.clone().into();
        common.to_xml_complex(Some((element_namespace, element_tag)), writer, xsi_type)
    }
}

impl ComplexXmlTypeWrite for AppSequence {
    fn to_xml_complex(
        &self,
        tag_name: Option<(Option<&'static str>, &'static str)>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_DISCOVERY_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| APP_SEQUENCE, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;

        writer.add_attribute((WSDD_INSTANCE_ID, self.instance_id.to_string().as_str()))?;

        if let Some(sequence_id) = &self.sequence_id {
            writer.add_attribute((WSDD_SEQUENCE_ID, sequence_id.to_string().as_str()))?;
        }
        writer.add_attribute((
            WSDD_MESSAGE_NUMBER,
            self.message_number.to_string().as_str(),
        ))?;

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::discovery::constants::{
        BYE_TAG, HELLO_TAG, PROBE_MATCHES_TAG, PROBE_TAG, RESOLVE_MATCHES_TAG, RESOLVE_TAG,
    };
    use crate::xml::messages::common::{
        APP_SEQUENCE_TAG, APP_SEQUENCE_TAG_STR, DPWS_NAMESPACE, HELLO_TAG_STR, MDPWS_NAMESPACE,
        RESOLVE_MATCHES_TAG_STR, RESOLVE_TAG_STR,
    };
    use crate::xml::messages::discovery::{
        AppSequence, Bye, Hello, HelloProbeMatchResolveMatch, Probe, ProbeMatches, Resolve,
        ResolveMatches,
    };
    use lazy_static::lazy_static;
    use protosdc_xml::{
        find_start_element_reader, ExpandedName, GenericXmlReaderComplexTypeRead, XmlReader,
    };

    use protosdc_xml::{ComplexXmlTypeWrite, XmlWriter};

    lazy_static! {
        static ref DPWS_TYPE: ExpandedName = ExpandedName {
            namespace: Some(DPWS_NAMESPACE.to_string()),
            local_name: "Device".to_string(),
        };
        static ref MDPWS_TYPE: ExpandedName = ExpandedName {
            namespace: Some(MDPWS_NAMESPACE.to_string()),
            local_name: "MedicalDevice".to_string(),
        };
    }

    fn test_probe(probe: &Probe) {
        assert!(probe
            .types
            .as_ref()
            .unwrap()
            .types
            .list
            .contains(&*MDPWS_TYPE));

        assert_eq!(
            "http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01/rfc3986",
            probe
                .scopes
                .as_ref()
                .unwrap()
                .match_by
                .as_ref()
                .unwrap()
                .to_string()
                .as_str()
        );

        let scope_list = &probe.scopes.as_ref().unwrap().scopes.list;

        assert_eq!(1, scope_list.len());

        assert!(scope_list
            .iter()
            .any(|it| "sdc.mds.pkp:1.2.840.10004.20701.1.1" == it.to_string().as_str()));
    }

    #[test]
    fn parse_probe() -> anyhow::Result<()> {
        let probe = {
            let input = "        <wsd:Probe xmlns:wsd=\"http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01\">
            <wsd:Types xmlns=\"http://standards.ieee.org/downloads/11073/11073-20702-2016\">MedicalDevice</wsd:Types>
            <wsd:Scopes MatchBy=\"http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01/rfc3986\">sdc.mds.pkp:1.2.840.10004.20701.1.1</wsd:Scopes>
            </wsd:Probe>";

            let mut reader = XmlReader::create_typed(input.as_bytes());
            let mut buf = vec![];

            let probe: Probe = find_start_element_reader!(Probe, PROBE_TAG, reader, buf)?;
            test_probe(&probe);
            probe
        };

        // back to xml, parse it again and test it
        let mut writer = XmlWriter::new(vec![]);
        probe.to_xml_complex(None, &mut writer, false)?;
        let writer_buf = writer.inner();

        let mut reader = XmlReader::create_typed(writer_buf.as_ref());
        let mut buf = vec![];

        let probe_again: Probe = find_start_element_reader!(Probe, PROBE_TAG, reader, buf)?;
        test_probe(&probe_again);

        Ok(())
    }

    fn test_probe_matches(probe_matches: &ProbeMatches) {
        assert_eq!(1, probe_matches.probe_match.len());
        let probe_match = probe_matches.probe_match.first().unwrap();

        assert_eq!(
            "urn:uuid:7a09f8a4-42a0-4aaa-852c-932e7e8401fc",
            probe_match
                .probe_match
                .endpoint_reference
                .address
                .value
                .to_string()
                .as_str()
        );

        assert!(probe_match
            .probe_match
            .types
            .as_ref()
            .unwrap()
            .types
            .list
            .contains(&DPWS_TYPE));
        assert!(probe_match
            .probe_match
            .types
            .as_ref()
            .unwrap()
            .types
            .list
            .contains(&MDPWS_TYPE));

        let scope_list = &probe_match.probe_match.scopes.as_ref().unwrap().scopes.list;

        assert_eq!(3, scope_list.len());

        assert!(scope_list.iter().any(|it| "sdc.ctxt.loc:/sdc.ctxt.loc.detail/r_fac%2F%2F%2Fr_poc%2F%2Fr_bed?fac=r_fac&poc=r_poc&bed=r_bed" == it.to_string().as_str()));
        assert!(scope_list
            .iter()
            .any(|it| "sdc.cdc.type:///130535" == it.to_string().as_str()));
        assert!(scope_list
            .iter()
            .any(|it| "sdc.mds.pkp:1.2.840.10004.20701.1.1" == it.to_string().as_str()));

        let x_addr_list = &probe_match.probe_match.x_addrs.as_ref().unwrap();

        assert!(x_addr_list.list.iter().any(|it| {
            "https://192.168.0.105:58173/7a09f8a4-42a0-4aaa-852c-932e7e8401fc"
                == it.to_string().as_str()
        }));

        assert_eq!(1671659256, probe_match.probe_match.metadata_version);

        assert!(probe_match.probe_match.children.is_empty());
    }

    #[test]
    fn parse_probe_matches() -> anyhow::Result<()> {
        let probe_matches = {
            let input = "        <wsd:ProbeMatches xmlns:wsa=\"http://www.w3.org/2005/08/addressing\" xmlns:wsd=\"http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01\">
            <wsd:ProbeMatch xmlns:dpws=\"http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01\">
                <wsa:EndpointReference>
                    <wsa:Address>urn:uuid:7a09f8a4-42a0-4aaa-852c-932e7e8401fc</wsa:Address>
                </wsa:EndpointReference>
                <wsd:Types xmlns=\"http://standards.ieee.org/downloads/11073/11073-20702-2016\">dpws:Device MedicalDevice</wsd:Types>
                <wsd:Scopes>sdc.ctxt.loc:/sdc.ctxt.loc.detail/r_fac%2F%2F%2Fr_poc%2F%2Fr_bed?fac=r_fac&amp;poc=r_poc&amp;bed=r_bed sdc.cdc.type:///130535 sdc.mds.pkp:1.2.840.10004.20701.1.1</wsd:Scopes>
                <wsd:XAddrs>https://192.168.0.105:58173/7a09f8a4-42a0-4aaa-852c-932e7e8401fc</wsd:XAddrs>
                 <wsd:MetadataVersion>1671659256</wsd:MetadataVersion>
            </wsd:ProbeMatch>
        </wsd:ProbeMatches>";

            let mut reader = XmlReader::create_typed(input.as_bytes());
            let mut buf = vec![];

            let probe_matches: ProbeMatches =
                find_start_element_reader!(ProbeMatches, PROBE_MATCHES_TAG, reader, buf)?;
            test_probe_matches(&probe_matches);
            probe_matches
        };

        // back to xml, parse it again and test it
        let mut writer = XmlWriter::new(vec![]);
        probe_matches.to_xml_complex(None, &mut writer, false)?;
        let writer_buf = writer.inner();

        let mut reader = XmlReader::create_typed(writer_buf.as_ref());
        let mut buf = vec![];

        let probe_again: ProbeMatches =
            find_start_element_reader!(ProbeMatches, PROBE_MATCHES_TAG, reader, buf)?;
        test_probe_matches(&probe_again);

        Ok(())
    }

    #[test]
    fn parse_bye() -> anyhow::Result<()> {
        let bye = {
            let input = "        <wsd:Bye xmlns:wsa=\"http://www.w3.org/2005/08/addressing\" xmlns:wsd=\"http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01\">
            <wsa:EndpointReference>
                <wsa:Address>urn:uuid:7a09f8a4-42a0-4aaa-852c-932e7e8401fc</wsa:Address>
            </wsa:EndpointReference>
            <wsd:Types xmlns:dpws=\"http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01\" xmlns=\"http://standards.ieee.org/downloads/11073/11073-20702-2016\">dpws:Device MedicalDevice</wsd:Types>
            <wsd:Scopes MatchBy=\"http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01/rfc3986\">sdc.ctxt.loc:/sdc.ctxt.loc.detail/r_fac%2F%2F%2Fr_poc%2F%2Fr_bed?fac=r_fac&amp;poc=r_poc&amp;bed=r_bed sdc.cdc.type:///130535 sdc.mds.pkp:1.2.840.10004.20701.1.1</wsd:Scopes>
            <wsd:XAddrs>https://192.168.0.105:54415/7a09f8a4-42a0-4aaa-852c-932e7e8401fc</wsd:XAddrs>
        </wsd:Bye>";

            let mut reader = XmlReader::create_typed(input.as_bytes());
            let mut buf = vec![];

            let bye: Bye = find_start_element_reader!(Bye, BYE_TAG, reader, buf)?;
            assert!(bye.metadata_version.is_none());
            bye
        };

        // back to xml, parse it again and test it
        let mut writer = XmlWriter::new(vec![]);
        bye.to_xml_complex(None, &mut writer, false)?;
        let writer_buf = writer.inner();

        let mut reader = XmlReader::create_typed(writer_buf.as_ref());
        let mut buf = vec![];

        let bye_again: Bye = find_start_element_reader!(Bye, BYE_TAG, reader, buf)?;
        assert!(bye_again.metadata_version.is_none());

        Ok(())
    }

    fn test_hello(hello: &HelloProbeMatchResolveMatch) {
        assert_eq!(
            "urn:uuid:7a09f8a4-42a0-4aaa-852c-932e7e8401fc",
            hello.endpoint_reference.address.value.to_string().as_str()
        );

        assert!(hello
            .types
            .as_ref()
            .unwrap()
            .types
            .list
            .contains(&DPWS_TYPE));
        assert!(hello
            .types
            .as_ref()
            .unwrap()
            .types
            .list
            .contains(&MDPWS_TYPE));

        assert_eq!(
            "http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01/rfc3986",
            hello
                .scopes
                .as_ref()
                .unwrap()
                .match_by
                .as_ref()
                .unwrap()
                .to_string()
                .as_str()
        );

        let scope_list = &hello.scopes.as_ref().unwrap().scopes.list;

        assert_eq!(3, scope_list.len());

        assert!(scope_list.iter().any(|it| "sdc.ctxt.loc:/sdc.ctxt.loc.detail/r_fac%2F%2F%2Fr_poc%2F%2Fr_bed?fac=r_fac&poc=r_poc&bed=r_bed" == it.to_string().as_str()));
        assert!(scope_list
            .iter()
            .any(|it| "sdc.cdc.type:///130535" == it.to_string().as_str()));
        assert!(scope_list
            .iter()
            .any(|it| "sdc.mds.pkp:1.2.840.10004.20701.1.1" == it.to_string().as_str()));

        let x_addr_list = &hello.x_addrs.as_ref().unwrap();

        assert!(x_addr_list.list.iter().any(|it| {
            "https://192.168.0.105:54415/7a09f8a4-42a0-4aaa-852c-932e7e8401fc"
                == it.to_string().as_str()
        }));

        assert_eq!(1671646711, hello.metadata_version);

        assert!(hello.children.is_empty());
    }

    #[test]
    fn parse_hello() -> anyhow::Result<()> {
        let hello = {
            let input = "        <wsd:Hello xmlns:wsa=\"http://www.w3.org/2005/08/addressing\" xmlns:wsd=\"http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01\">
            <wsa:EndpointReference>
                <wsa:Address>urn:uuid:7a09f8a4-42a0-4aaa-852c-932e7e8401fc</wsa:Address>
            </wsa:EndpointReference>
            <wsd:Types xmlns:dpws=\"http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01\" xmlns=\"http://standards.ieee.org/downloads/11073/11073-20702-2016\">dpws:Device MedicalDevice
        </wsd:Types>
            <wsd:Scopes MatchBy=\"http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01/rfc3986\">
sdc.ctxt.loc:/sdc.ctxt.loc.detail/r_fac%2F%2F%2Fr_poc%2F%2Fr_bed?fac=r_fac&amp;poc=r_poc&amp;bed=r_bed sdc.cdc.type:///130535 sdc.mds.pkp:1.2.840.10004.20701.1.1
        </wsd:Scopes>
            <wsd:XAddrs>https://192.168.0.105:54415/7a09f8a4-42a0-4aaa-852c-932e7e8401fc</wsd:XAddrs>
        <wsd:MetadataVersion>1671646711</wsd:MetadataVersion>
            </wsd:Hello>";

            let mut reader = XmlReader::create_typed(input.as_bytes());
            let mut buf = vec![];

            let hello: Hello = find_start_element_reader!(Hello, HELLO_TAG, reader, buf)?;
            test_hello(&hello.hello);
            hello
        };

        // back to xml, parse it again and test it
        let mut writer = XmlWriter::new(vec![]);
        hello.to_xml_complex(Some(HELLO_TAG_STR), &mut writer, false)?;
        let writer_buf = writer.inner();

        let mut reader = XmlReader::create_typed(writer_buf.as_ref());
        let mut buf = vec![];

        let hello_again: Hello = find_start_element_reader!(Hello, HELLO_TAG, reader, buf)?;
        test_hello(&hello_again.hello);

        Ok(())
    }

    fn test_resolve(resolve: &Resolve) {
        assert_eq!(
            "woop:woop:epr",
            resolve
                .endpoint_references
                .first()
                .unwrap()
                .address
                .value
                .as_str()
        )
    }

    #[test]
    fn parse_resolve() -> anyhow::Result<()> {
        let resolve = {
            let input = "        <wsd:Resolve xmlns:wsa=\"http://www.w3.org/2005/08/addressing\" xmlns:wsd=\"http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01\">
            <wsa:EndpointReference>
                <wsa:Address>woop:woop:epr</wsa:Address>
            </wsa:EndpointReference>
            </wsd:Resolve>";

            let mut reader = XmlReader::create_typed(input.as_bytes());
            let mut buf = vec![];

            let resolve: Resolve = find_start_element_reader!(Resolve, RESOLVE_TAG, reader, buf)?;
            test_resolve(&resolve);
            resolve
        };

        // back to xml, parse it again and test it
        let mut writer = XmlWriter::new(vec![]);
        resolve.to_xml_complex(Some(RESOLVE_TAG_STR), &mut writer, false)?;
        let writer_buf = writer.inner();

        let mut reader = XmlReader::create_typed(writer_buf.as_ref());
        let mut buf = vec![];

        let resolve_again: Resolve = find_start_element_reader!(Resolve, RESOLVE_TAG, reader, buf)?;
        test_resolve(&resolve_again);

        Ok(())
    }

    #[test]
    fn parse_resolve_matches() -> anyhow::Result<()> {
        let resolve_matches = {
            let input = "        <wsd:ResolveMatches xmlns:wsa=\"http://www.w3.org/2005/08/addressing\" xmlns:wsd=\"http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01\">
            <wsd:ResolveMatch>
            <wsa:EndpointReference>
                <wsa:Address>urn:uuid:7a09f8a4-42a0-4aaa-852c-932e7e8401fc</wsa:Address>
            </wsa:EndpointReference>
            <wsd:Types xmlns:dpws=\"http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01\" xmlns=\"http://standards.ieee.org/downloads/11073/11073-20702-2016\">dpws:Device MedicalDevice
        </wsd:Types>
            <wsd:Scopes MatchBy=\"http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01/rfc3986\">
sdc.ctxt.loc:/sdc.ctxt.loc.detail/r_fac%2F%2F%2Fr_poc%2F%2Fr_bed?fac=r_fac&amp;poc=r_poc&amp;bed=r_bed sdc.cdc.type:///130535 sdc.mds.pkp:1.2.840.10004.20701.1.1
        </wsd:Scopes>
            <wsd:XAddrs>https://192.168.0.105:54415/7a09f8a4-42a0-4aaa-852c-932e7e8401fc</wsd:XAddrs>
        <wsd:MetadataVersion>1671646711</wsd:MetadataVersion>
            </wsd:ResolveMatch>
            </wsd:ResolveMatches>";

            let mut reader = XmlReader::create_typed(input.as_bytes());
            let mut buf = vec![];

            let resolve_matches: ResolveMatches =
                find_start_element_reader!(ResolveMatches, RESOLVE_MATCHES_TAG, reader, buf)?;
            test_hello(resolve_matches.resolve_match.as_ref().unwrap());
            resolve_matches
        };

        // back to xml, parse it again and test it
        let mut writer = XmlWriter::new(vec![]);
        resolve_matches.to_xml_complex(Some(RESOLVE_MATCHES_TAG_STR), &mut writer, false)?;
        let writer_buf = writer.inner();

        let mut reader = XmlReader::create_typed(writer_buf.as_ref());
        let mut buf = vec![];

        let resolve_matches_again: ResolveMatches =
            find_start_element_reader!(ResolveMatches, RESOLVE_MATCHES_TAG, reader, buf)?;
        test_hello(resolve_matches_again.resolve_match.as_ref().unwrap());

        Ok(())
    }

    fn test_app_sequence(app_sequence: &AppSequence) {
        assert_eq!(1077004800, app_sequence.instance_id);
        assert_eq!(2, app_sequence.message_number);
        assert!(app_sequence.sequence_id.is_none());
    }

    fn test_app_sequence2(app_sequence: &AppSequence) {
        assert_eq!(1077004800, app_sequence.instance_id);
        assert_eq!(2, app_sequence.message_number);
        assert_eq!(
            "urn:uuid:2f67cde5-bbfe-4e36-b151-6dd0c8273393",
            app_sequence.sequence_id.as_ref().unwrap()
        );
    }

    #[test]
    fn parse_app_sequence() -> anyhow::Result<()> {
        let app_sequence = {
            let input = "<wsd:AppSequence xmlns:wsd=\"http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01\" InstanceId=\"1077004800\" MessageNumber=\"2\"/>";

            let mut reader = XmlReader::create_typed(input.as_bytes());
            let mut buf = vec![];

            let app_sequence: AppSequence =
                find_start_element_reader!(AppSequence, APP_SEQUENCE_TAG, reader, buf)?;
            test_app_sequence(&app_sequence);
            app_sequence
        };

        // back to xml, parse it again and test it
        let mut writer = XmlWriter::new(vec![]);
        app_sequence.to_xml_complex(Some(APP_SEQUENCE_TAG_STR), &mut writer, false)?;
        let writer_buf = writer.inner();

        let mut reader = XmlReader::create_typed(writer_buf.as_ref());
        let mut buf = vec![];

        let app_sequence_again: AppSequence =
            find_start_element_reader!(AppSequence, APP_SEQUENCE_TAG, reader, buf)?;
        test_app_sequence(&app_sequence_again);

        Ok(())
    }

    #[test]
    fn parse_app_sequence2() -> anyhow::Result<()> {
        let app_sequence = {
            let input = "<wsd:AppSequence xmlns:wsd=\"http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01\" InstanceId=\"1077004800\" MessageNumber=\"2\" SequenceId=\"urn:uuid:2f67cde5-bbfe-4e36-b151-6dd0c8273393\"/>";

            let mut reader = XmlReader::create_typed(input.as_bytes());
            let mut buf = vec![];

            let app_sequence: AppSequence =
                find_start_element_reader!(AppSequence, APP_SEQUENCE_TAG, reader, buf)?;
            test_app_sequence2(&app_sequence);
            app_sequence
        };

        // back to xml, parse it again and test it
        let mut writer = XmlWriter::new(vec![]);
        app_sequence.to_xml_complex(Some(APP_SEQUENCE_TAG_STR), &mut writer, false)?;
        let writer_buf = writer.inner();

        let mut reader = XmlReader::create_typed(writer_buf.as_ref());
        let mut buf = vec![];

        let app_sequence_again: AppSequence =
            find_start_element_reader!(AppSequence, APP_SEQUENCE_TAG, reader, buf)?;
        test_app_sequence2(&app_sequence_again);

        Ok(())
    }
}
