use crate::xml::messages::addressing::XmlElement;
use crate::xml::messages::common::{
    soap_element_impl, write_attributes, DO_NOT_CARE_QNAME, WSE_EXPIRES, WSE_EXPIRES_TAG_REF,
    WSE_GET_STATUS, WSE_GET_STATUS_RESPONSE, WSE_RENEW, WSE_RENEW_RESPONSE, WSE_UNSUBSCRIBE,
    WS_EVENTING_NAMESPACE,
};
use crate::xml::messages::duration::XsdDuration;
use protosdc_xml::xml_reader::collect_attributes_reader;
use protosdc_xml::{ComplexXmlTypeWrite, QNameStr, SimpleXmlTypeWrite, WriterError, XmlWriter};
use protosdc_xml::{
    ExpandedName, GenericXmlReaderComplexTypeRead, GenericXmlReaderSimpleTypeRead, ParserError,
};
use quick_xml::events::{BytesStart, Event};
use std::collections::HashMap;
use std::io::BufRead;

#[derive(Clone, Debug, PartialEq)]
pub struct Renew {
    pub attributes: HashMap<ExpandedName, String>,
    pub expires: Option<XsdDuration>,
    pub children: Vec<XmlElement>,
}
soap_element_impl!(Renew, WSE_RENEW);

#[derive(Clone, Debug, PartialEq)]
pub struct RenewResponse {
    pub renew_response: Renew,
}
soap_element_impl!(RenewResponse, WSE_RENEW_RESPONSE);

#[derive(Clone, Debug, PartialEq)]
pub struct GetStatus {
    pub attributes: HashMap<ExpandedName, String>,
    pub children: Vec<XmlElement>,
}
soap_element_impl!(GetStatus, WSE_GET_STATUS);

#[derive(Clone, Debug, PartialEq)]
pub struct Unsubscribe {
    // for convenience, reuse get status, same layout
    pub get_status: GetStatus,
}
soap_element_impl!(Unsubscribe, WSE_UNSUBSCRIBE);

// noop response, does not exist
#[derive(Clone, Debug, PartialEq)]
pub struct UnsubscribeResponse {}
soap_element_impl!(UnsubscribeResponse, WSE_UNSUBSCRIBE);

// noop response, does not exist
#[derive(Clone, Debug, PartialEq)]
pub struct NoopResponse {}
soap_element_impl!(NoopResponse, NOOP_RESPONSE);

#[derive(Clone, Debug, PartialEq)]
pub struct GetStatusResponse {
    pub get_status_response: Renew,
}
soap_element_impl!(GetStatusResponse, WSE_GET_STATUS_RESPONSE);

impl GenericXmlReaderComplexTypeRead for UnsubscribeResponse {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    fn from_xml_complex<B: BufRead>(
        _tag_name: protosdc_xml::QNameSlice,
        _event: &BytesStart,
        _reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        Ok(Self {})
    }
}

impl ComplexXmlTypeWrite for UnsubscribeResponse {
    fn to_xml_complex(
        &self,
        _tag_name: Option<QNameStr>,
        _writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        // placeholders have no content
        Ok(())
    }
}

impl GenericXmlReaderComplexTypeRead for NoopResponse {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    fn from_xml_complex<B: BufRead>(
        _tag_name: protosdc_xml::QNameSlice,
        _event: &BytesStart,
        _reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        Ok(Self {})
    }
}

impl ComplexXmlTypeWrite for NoopResponse {
    fn to_xml_complex(
        &self,
        _tag_name: Option<QNameStr>,
        _writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        // placeholders have no content
        Ok(())
    }
}

impl GenericXmlReaderComplexTypeRead for GetStatus {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
            AnyStart,
            AnyEnd,
        }
        let mut state = State::Root;

        // collect attributes
        let attributes: HashMap<ExpandedName, String> = collect_attributes_reader(event, reader)?;

        state = State::Start;

        let mut children: Vec<XmlElement> = Vec::with_capacity(0);

        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => {
                    match (state, (ns, e.local_name().into_inner())) {
                        (State::Start, _) | (State::AnyEnd, _) => {
                            state = State::AnyStart;
                            children.push(XmlElement::from_xml_complex(
                                DO_NOT_CARE_QNAME,
                                e,
                                reader,
                            )?);
                            state = State::AnyEnd;
                        }
                        (_, _) => Err(ParserError::UnexpectedParserStartState {
                            element_name: match String::from_utf8(
                                e.local_name().into_inner().to_vec(),
                            ) {
                                Ok(it) => it,
                                Err(_) => "FromUtf8Error".to_string(),
                            },
                            parser_state: format!("{:?}", state),
                        })?,
                    }
                }
                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (_, (ns, local_name)) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;

                        return Ok(Self {
                            attributes,
                            children,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref _e))) => Err(ParserError::UnexpectedParserTextEventState {
                    parser_state: format!("{:?}", state),
                })?,
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl ComplexXmlTypeWrite for GetStatus {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_EVENTING_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| WSE_GET_STATUS, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;

        for child in &self.children {
            child.to_xml_complex(None, writer, false)?;
        }
        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

impl GenericXmlReaderComplexTypeRead for GetStatusResponse {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        Ok(Self {
            get_status_response: Renew::from_xml_complex(tag_name, event, reader)?,
        })
    }
}

impl ComplexXmlTypeWrite for GetStatusResponse {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_EVENTING_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| WSE_GET_STATUS_RESPONSE, |(_, it)| it);

        self.get_status_response.to_xml_complex(
            Some((element_namespace, element_tag)),
            writer,
            xsi_type,
        )
    }
}

impl GenericXmlReaderComplexTypeRead for Unsubscribe {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        Ok(Self {
            get_status: GetStatus::from_xml_complex(tag_name, event, reader)?,
        })
    }
}

impl ComplexXmlTypeWrite for Unsubscribe {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_EVENTING_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| WSE_UNSUBSCRIBE, |(_, it)| it);

        self.get_status
            .to_xml_complex(Some((element_namespace, element_tag)), writer, xsi_type)
    }
}

impl GenericXmlReaderComplexTypeRead for RenewResponse {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        Ok(Self {
            renew_response: Renew::from_xml_complex(tag_name, event, reader)?,
        })
    }
}

impl ComplexXmlTypeWrite for RenewResponse {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_EVENTING_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| WSE_RENEW_RESPONSE, |(_, it)| it);

        self.renew_response
            .to_xml_complex(Some((element_namespace, element_tag)), writer, xsi_type)
    }
}

impl GenericXmlReaderComplexTypeRead for Renew {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
            ExpiresStart,
            ExpiresEnd,
            AnyStart,
            AnyEnd,
        }
        let mut state = State::Root;

        // collect attributes
        let attributes: HashMap<ExpandedName, String> = collect_attributes_reader(event, reader)?;

        state = State::Start;

        let mut expires: Option<XsdDuration> = None;
        let mut children: Vec<XmlElement> = Vec::with_capacity(0);

        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => {
                    match (state, (ns, e.local_name().into_inner())) {
                        (State::Start, WSE_EXPIRES_TAG_REF) => {
                            state = State::ExpiresStart;
                        }
                        (State::ExpiresEnd, _) | (State::AnyEnd, _) => {
                            state = State::AnyStart;
                            children.push(XmlElement::from_xml_complex(
                                DO_NOT_CARE_QNAME,
                                e,
                                reader,
                            )?);
                            state = State::AnyEnd;
                        }
                        (_, _) => Err(ParserError::UnexpectedParserStartState {
                            element_name: match String::from_utf8(
                                e.local_name().into_inner().to_vec(),
                            ) {
                                Ok(it) => it,
                                Err(_) => "FromUtf8Error".to_string(),
                            },
                            parser_state: format!("{:?}", state),
                        })?,
                    }
                }
                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (State::ExpiresStart, WSE_EXPIRES_TAG_REF) => {
                        state = State::ExpiresEnd;
                    }
                    (_, (ns, local_name)) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;

                        return Ok(Self {
                            attributes,
                            expires,
                            children,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref e))) => match state {
                    State::ExpiresStart => {
                        let unescaped = e.unescape().map_err(ParserError::QuickXMLError)?;
                        expires = Some(XsdDuration::from_xml_simple(unescaped.as_bytes(), reader)?);
                    }
                    _ => Err(ParserError::UnexpectedParserTextEventState {
                        parser_state: format!("{:?}", state),
                    })?,
                },
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl ComplexXmlTypeWrite for Renew {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_EVENTING_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| WSE_RENEW, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;

        if let Some(expires) = &self.expires {
            writer.write_start(Some(WS_EVENTING_NAMESPACE), WSE_EXPIRES)?;
            let exp = expires.to_xml_simple(writer)?;
            writer.write_bytes(&exp)?;
            writer.write_end(Some(WS_EVENTING_NAMESPACE), WSE_EXPIRES)?;
        }
        for child in &self.children {
            child.to_xml_complex(None, writer, false)?;
        }

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use crate::xml::messages::common::{WSE_RENEW_RESPONSE_TAG, WSE_RENEW_TAG};
    use crate::xml::messages::ws_eventing::renew::{Renew, RenewResponse};
    use protosdc_xml::{find_start_element_reader, GenericXmlReaderComplexTypeRead, XmlReader};
    use protosdc_xml::{ComplexXmlTypeWrite, XmlWriter};

    fn test_renew(renew: &Renew) {
        let expires: String = renew.expires.as_ref().unwrap().clone().into();
        assert_eq!("PT15S", &expires);
    }

    #[test]
    fn round_trip_renew() -> anyhow::Result<()> {
        let renew = {
            let input = "
        <wse:Renew xmlns:wsa=\"http://www.w3.org/2005/08/addressing\" xmlns:wse=\"http://schemas.xmlsoap.org/ws/2004/08/eventing\">
            <wse:Expires>PT15S</wse:Expires>
        </wse:Renew>";

            let mut reader = XmlReader::create_typed(input.as_bytes());
            let mut buf = vec![];
            let renew: Renew = find_start_element_reader!(Renew, WSE_RENEW_TAG, reader, buf)?;
            test_renew(&renew);
            renew
        };

        // back to xml, parse it again and test it
        let mut writer = XmlWriter::new(vec![]);
        renew.to_xml_complex(None, &mut writer, false)?;
        let writer_buf = writer.inner();

        let mut reader = XmlReader::create_typed(writer_buf.as_ref());
        let mut buf = vec![];

        let renew_again: Renew = find_start_element_reader!(Renew, WSE_RENEW_TAG, reader, buf)?;
        test_renew(&renew_again);

        Ok(())
    }

    fn test_renew_response(renew: &RenewResponse) {
        let expires: String = renew
            .renew_response
            .expires
            .as_ref()
            .unwrap()
            .clone()
            .into();
        assert_eq!("PT30S", &expires);
    }

    #[test]
    fn round_trip_renew_response() -> anyhow::Result<()> {
        let renew_response = {
            let input = "
        <wse:RenewResponse xmlns:wsa=\"http://www.w3.org/2005/08/addressing\" xmlns:wse=\"http://schemas.xmlsoap.org/ws/2004/08/eventing\">
            <wse:Expires>PT30S</wse:Expires>
        </wse:RenewResponse>";

            let mut reader = XmlReader::create_typed(input.as_bytes());
            let mut buf = vec![];
            let renew_response: RenewResponse =
                find_start_element_reader!(RenewResponse, WSE_RENEW_RESPONSE_TAG, reader, buf)?;
            test_renew_response(&renew_response);
            renew_response
        };

        // back to xml, parse it again and test it
        let mut writer = XmlWriter::new(vec![]);
        renew_response.to_xml_complex(None, &mut writer, false)?;
        let writer_buf = writer.inner();

        let mut reader = XmlReader::create_typed(writer_buf.as_ref());
        let mut buf = vec![];

        let renew_response_again: RenewResponse =
            find_start_element_reader!(RenewResponse, WSE_RENEW_RESPONSE_TAG, reader, buf)?;
        test_renew_response(&renew_response_again);

        Ok(())
    }
}
