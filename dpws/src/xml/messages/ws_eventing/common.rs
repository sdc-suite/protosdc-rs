use crate::xml::messages::common::{
    write_attributes, WSE_DIALECT, WSE_FILTER, WS_EVENTING_NAMESPACE,
};
use protosdc_xml::xml_reader::collect_attributes_reader;
use protosdc_xml::{
    ComplexXmlTypeWrite, QNameStr, WriterError, XmlElement, XmlReaderComplexXmlTypeRead, XmlWriter,
    DO_NOT_CARE_QNAME,
};
use protosdc_xml::{ExpandedName, GenericXmlReaderComplexTypeRead, ParserError};
use quick_xml::events::{BytesStart, Event};
use std::collections::HashMap;
use std::io::BufRead;

#[derive(Clone, Debug, PartialEq)]
pub struct Filter {
    pub attributes: HashMap<ExpandedName, String>,
    pub dialect: Option<String>,
    pub filters: Option<FilterContent>,
}

#[derive(Clone, Debug, PartialEq)]
pub enum FilterContent {
    Element(XmlElement),
    Text(String),
}

impl GenericXmlReaderComplexTypeRead for Filter {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            StartComplete,
            End,
        }
        let mut state = State::Root;

        // collect attributes
        let mut attributes: HashMap<ExpandedName, String> =
            collect_attributes_reader(event, reader)?;
        // extract known attributes
        let dialect = attributes.remove(&ExpandedName {
            namespace: None,
            local_name: WSE_DIALECT.to_string(),
        });

        // read value
        state = State::Start;

        let mut filters: Option<FilterContent> = None;

        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref _ns, Event::Start(ref e))) => match state {
                    State::Start => {
                        filters = Some(FilterContent::Element(XmlElement::from_xml_complex(
                            DO_NOT_CARE_QNAME,
                            e,
                            reader,
                        )?));
                        state = State::StartComplete;
                    }
                    _ => Err(ParserError::UnexpectedParserStartState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },
                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (_, (ns, local_name)) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            attributes,
                            dialect,
                            filters,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref e))) => match state {
                    State::Start => {
                        let unescaped = &e.unescape().map_err(ParserError::QuickXMLError)?;
                        filters = Some(FilterContent::Text(unescaped.to_string()));

                        state = State::StartComplete;
                    }
                    _ => Err(ParserError::UnexpectedParserTextEventState {
                        parser_state: format!("{:?}", state),
                    })?,
                },
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl ComplexXmlTypeWrite for Filter {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_EVENTING_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| WSE_FILTER, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;
        if let Some(dialect) = &self.dialect {
            writer.add_attribute((WSE_DIALECT, dialect.as_str()))?;
        }
        if let Some(filters) = &self.filters {
            match filters {
                FilterContent::Element(element) => {
                    element.to_xml_complex(None, writer, false)?;
                }
                FilterContent::Text(text) => {
                    writer.write_text(text)?;
                }
            }
        }

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}
