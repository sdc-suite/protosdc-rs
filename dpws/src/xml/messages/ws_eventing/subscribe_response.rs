use crate::xml::messages::addressing::{WsaEndpointReference, XmlElement};
use crate::xml::messages::common::{
    soap_element_impl, write_attributes, DO_NOT_CARE_QNAME, WSE_EXPIRES, WSE_EXPIRES_TAG_REF,
    WSE_SUBSCRIBE_RESPONSE, WSE_SUBSCRIPTION_MANAGER, WSE_SUBSCRIPTION_MANAGER_TAG,
    WSE_SUBSCRIPTION_MANAGER_TAG_REF, WSE_SUBSCRIPTION_MANAGER_TAG_STR, WS_EVENTING_NAMESPACE,
};
use crate::xml::messages::duration::XsdDuration;
use protosdc_xml::xml_reader::collect_attributes_reader;
use protosdc_xml::{ComplexXmlTypeWrite, QNameStr, SimpleXmlTypeWrite, WriterError, XmlWriter};
use protosdc_xml::{
    ExpandedName, GenericXmlReaderComplexTypeRead, GenericXmlReaderSimpleTypeRead, ParserError,
};
use quick_xml::events::{BytesStart, Event};
use std::collections::HashMap;
use std::io::BufRead;

#[derive(Clone, Debug, PartialEq)]
pub struct SubscribeResponse {
    pub attributes: HashMap<ExpandedName, String>,
    pub subscription_manager: WsaEndpointReference,
    pub expires: XsdDuration,
    pub children: Vec<XmlElement>,
}
soap_element_impl!(SubscribeResponse, WSE_SUBSCRIBE_RESPONSE);

impl GenericXmlReaderComplexTypeRead for SubscribeResponse {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
            SubscriptionManagerStart,
            SubscriptionManagerEnd,
            ExpiresStart,
            ExpiresEnd,
            AnyStart,
            AnyEnd,
        }
        let mut state = State::Root;

        // collect attributes
        let attributes: HashMap<ExpandedName, String> = collect_attributes_reader(event, reader)?;

        state = State::Start;

        let mut subscription_manager: Option<WsaEndpointReference> = None;
        let mut expires: Option<XsdDuration> = None;
        let mut children: Vec<XmlElement> = Vec::with_capacity(0);

        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => {
                    match (state, (ns, e.local_name().into_inner())) {
                        (State::Start, WSE_SUBSCRIPTION_MANAGER_TAG_REF) => {
                            state = State::SubscriptionManagerStart;
                            subscription_manager = Some(WsaEndpointReference::from_xml_complex(
                                WSE_SUBSCRIPTION_MANAGER_TAG,
                                e,
                                reader,
                            )?);
                            state = State::SubscriptionManagerEnd;
                        }
                        (State::SubscriptionManagerEnd, WSE_EXPIRES_TAG_REF) => {
                            state = State::ExpiresStart;
                        }
                        (State::ExpiresEnd, _) | (State::AnyEnd, _) => {
                            state = State::AnyStart;
                            children.push(XmlElement::from_xml_complex(
                                DO_NOT_CARE_QNAME,
                                e,
                                reader,
                            )?);
                            state = State::AnyEnd;
                        }
                        (_, _) => Err(ParserError::UnexpectedParserStartState {
                            element_name: match String::from_utf8(
                                e.local_name().into_inner().to_vec(),
                            ) {
                                Ok(it) => it,
                                Err(_) => "FromUtf8Error".to_string(),
                            },
                            parser_state: format!("{:?}", state),
                        })?,
                    }
                }
                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (State::ExpiresStart, WSE_EXPIRES_TAG_REF) => {
                        state = State::ExpiresEnd;
                    }
                    (_, (ns, local_name)) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;

                        return Ok(Self {
                            attributes,
                            subscription_manager: subscription_manager.ok_or(
                                ParserError::MandatoryElementMissing {
                                    element_name: WSE_SUBSCRIPTION_MANAGER.to_string(),
                                },
                            )?,
                            expires: expires.ok_or(ParserError::MandatoryElementMissing {
                                element_name: WSE_EXPIRES.to_string(),
                            })?,
                            children,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref e))) => match state {
                    State::ExpiresStart => {
                        let unescaped = e.unescape().map_err(ParserError::QuickXMLError)?;
                        expires = Some(XsdDuration::from_xml_simple(unescaped.as_bytes(), reader)?);
                    }
                    _ => Err(ParserError::UnexpectedParserTextEventState {
                        parser_state: format!("{:?}", state),
                    })?,
                },
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl ComplexXmlTypeWrite for SubscribeResponse {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(WS_EVENTING_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| WSE_SUBSCRIBE_RESPONSE, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;

        self.subscription_manager.to_xml_complex(
            Some(WSE_SUBSCRIPTION_MANAGER_TAG_STR),
            writer,
            false,
        )?;

        writer.write_start(Some(WS_EVENTING_NAMESPACE), WSE_EXPIRES)?;
        let expires = self.expires.to_xml_simple(writer)?;
        writer.write_bytes(&expires)?;
        writer.write_end(Some(WS_EVENTING_NAMESPACE), WSE_EXPIRES)?;

        for child in &self.children {
            child.to_xml_complex(None, writer, false)?;
        }

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use crate::xml::messages::common::{
        WSE_SUBSCRIBE_RESPONSE_TAG, WSE_SUBSCRIBE_RESPONSE_TAG_STR,
    };
    use crate::xml::messages::ws_eventing::subscribe_response::SubscribeResponse;
    use protosdc_xml::{find_start_element_reader, GenericXmlReaderComplexTypeRead, XmlReader};
    use protosdc_xml::{ComplexXmlTypeWrite, XmlWriter};

    fn test_subscribe_response(subscribe_response: &SubscribeResponse) {
        let expires: String = subscribe_response.expires.clone().into();
        assert_eq!("PT1M", &expires);

        assert_eq!(
            "https://192.168.0.105:58173/f5bcac54-748d-47ce-a2fd-f47906f02a4f/SubscriptionManager",
            subscribe_response
                .subscription_manager
                .address
                .value
                .as_str()
        )
    }

    #[test]
    fn round_trip_subscribe_response() -> anyhow::Result<()> {
        let subscribe_response = {
            let input = "        <wse:SubscribeResponse xmlns:wsa=\"http://www.w3.org/2005/08/addressing\" xmlns:wse=\"http://schemas.xmlsoap.org/ws/2004/08/eventing\">
                <wse:SubscriptionManager>
                    <wsa:Address>https://192.168.0.105:58173/f5bcac54-748d-47ce-a2fd-f47906f02a4f/SubscriptionManager</wsa:Address>
                </wse:SubscriptionManager>
                <wse:Expires>PT1M</wse:Expires>
            </wse:SubscribeResponse>";

            let mut reader = XmlReader::create_typed(input.as_bytes());
            let mut buf = vec![];
            let subscribe_response: SubscribeResponse = find_start_element_reader!(
                SubscribeResponse,
                WSE_SUBSCRIBE_RESPONSE_TAG,
                reader,
                buf
            )?;
            test_subscribe_response(&subscribe_response);
            subscribe_response
        };

        // back to xml, parse it again and test it
        let mut writer = XmlWriter::new(vec![]);
        subscribe_response.to_xml_complex(
            Some(WSE_SUBSCRIBE_RESPONSE_TAG_STR),
            &mut writer,
            false,
        )?;
        let writer_buf = writer.inner();

        let mut reader = XmlReader::create_typed(writer_buf.as_ref());
        let mut buf = vec![];

        let subscribe_response_again: SubscribeResponse =
            find_start_element_reader!(SubscribeResponse, WSE_SUBSCRIBE_RESPONSE_TAG, reader, buf)?;
        test_subscribe_response(&subscribe_response_again);

        Ok(())
    }
}
