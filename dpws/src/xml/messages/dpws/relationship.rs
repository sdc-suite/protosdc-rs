use crate::xml::messages::addressing::XmlElement;
use crate::xml::messages::common::{
    write_attributes, DO_NOT_CARE_QNAME, DPWS_HOSTED_TAG, DPWS_HOSTED_TAG_REF, DPWS_HOSTED_TAG_STR,
    DPWS_HOST_TAG, DPWS_HOST_TAG_REF, DPWS_HOST_TAG_STR, DPWS_NAMESPACE, DPWS_RELATIONSHIP,
    DPWS_TYPE,
};
use crate::xml::messages::dpws::host::{Host, Hosted};
use protosdc_xml::xml_reader::collect_attributes_reader;
use protosdc_xml::{ComplexXmlTypeWrite, QNameStr, WriterError, XmlWriter};
use protosdc_xml::{ExpandedName, GenericXmlReaderComplexTypeRead, ParserError};
use quick_xml::events::{BytesStart, Event};
use std::collections::HashMap;
use std::io::BufRead;

#[derive(Clone, Debug, PartialEq)]
pub struct Relationship {
    pub attributes: HashMap<ExpandedName, String>,
    pub r#type: String,

    // common types
    pub host: Vec<Host>,
    pub hosted: Vec<Hosted>,

    pub children: Vec<XmlElement>,
}

impl GenericXmlReaderComplexTypeRead for Relationship {
    type Extension = Box<dyn protosdc_biceps::types::any::AnyContent + Send + Sync>;
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex<B: BufRead>(
        tag_name: protosdc_xml::QNameSlice,
        event: &BytesStart,
        reader: &mut protosdc_xml::XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError> {
        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
            AnyStart,
            AnyEnd,
        }
        let mut state = State::Root;

        // collect attributes
        let mut attributes: HashMap<ExpandedName, String> =
            collect_attributes_reader(event, reader)?;
        // extract known attributes
        let r#type = attributes
            .remove(&ExpandedName {
                namespace: None,
                local_name: DPWS_TYPE.to_string(),
            })
            .ok_or(ParserError::MandatoryAttributeMissing {
                attribute_name: DPWS_TYPE.to_string(),
            })?;

        // read value
        state = State::Start;

        let mut host: Vec<Host> = vec![];
        let mut hosted: Vec<Hosted> = vec![];
        let mut children: Vec<XmlElement> = Vec::with_capacity(0);

        let mut buf = vec![];

        loop {
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::Start(ref e))) => {
                    match (state, (ns, e.local_name().into_inner())) {
                        (State::Start, DPWS_HOST_TAG_REF) | (State::AnyEnd, DPWS_HOST_TAG_REF) => {
                            state = State::AnyStart;
                            host.push(Host::from_xml_complex(DPWS_HOST_TAG, e, reader)?);
                            state = State::AnyEnd;
                        }
                        (State::Start, DPWS_HOSTED_TAG_REF)
                        | (State::AnyEnd, DPWS_HOSTED_TAG_REF) => {
                            state = State::AnyStart;
                            hosted.push(Hosted::from_xml_complex(DPWS_HOSTED_TAG, e, reader)?);
                            state = State::AnyEnd;
                        }
                        (State::Start, _) | (State::AnyEnd, _) => {
                            state = State::AnyStart;
                            children.push(XmlElement::from_xml_complex(
                                DO_NOT_CARE_QNAME,
                                e,
                                reader,
                            )?);
                            state = State::AnyEnd;
                        }
                        (_, _) => Err(ParserError::UnexpectedParserStartState {
                            element_name: match String::from_utf8(
                                e.local_name().into_inner().to_vec(),
                            ) {
                                Ok(it) => it,
                                Err(_) => "FromUtf8Error".to_string(),
                            },
                            parser_state: format!("{:?}", state),
                        })?,
                    }
                }
                Ok((ref ns, Event::End(ref e))) => match (state, (ns, e.local_name().into_inner()))
                {
                    (_, (ns, local_name)) if &tag_name.0 == ns && tag_name.1 == local_name => {
                        state = State::End;
                        return Ok(Self {
                            attributes,
                            r#type,
                            host,
                            hosted,
                            children,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref _e))) => Err(ParserError::UnexpectedParserTextEventState {
                    parser_state: format!("{:?}", state),
                })?,
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::CData(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl ComplexXmlTypeWrite for Relationship {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => Some(DPWS_NAMESPACE),
        };
        let element_tag = tag_name.map_or_else(|| DPWS_RELATIONSHIP, |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;
        writer.add_attribute((DPWS_TYPE, self.r#type.as_str()))?;

        for host in &self.host {
            host.to_xml_complex(Some(DPWS_HOST_TAG_STR), writer, false)?;
        }
        for hosted in &self.hosted {
            hosted.to_xml_complex(Some(DPWS_HOSTED_TAG_STR), writer, false)?;
        }
        for child in &self.children {
            child.to_xml_complex(None, writer, false)?;
        }

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use crate::xml::messages::common::{DPWS_RELATIONSHIP_TAG, DPWS_RELATIONSHIP_TAG_STR};
    use crate::xml::messages::dpws::relationship::Relationship;
    use protosdc_xml::{find_start_element_reader, GenericXmlReaderComplexTypeRead, XmlReader};
    use protosdc_xml::{ComplexXmlTypeWrite, XmlWriter};

    fn test_relationship(relationship: &Relationship) {
        assert_eq!(
            "http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01/host",
            relationship.r#type.as_str()
        );

        assert_eq!(1, relationship.host.len());
        assert_eq!(2, relationship.hosted.len());
        assert_eq!(0, relationship.children.len());
    }

    #[test]
    fn round_trip_relationship() -> anyhow::Result<()> {
        let relationship = {
            let input = "\
        <dpws:Relationship xmlns:dpws=\"http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01\" Type=\"http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01/host\" xmlns:wsa=\"http://www.w3.org/2005/08/addressing\">
            <dpws:Host>
                <wsa:EndpointReference>
                    <wsa:Address>urn:uuid:7a09f8a4-42a0-4aaa-852c-932e7e8401fc</wsa:Address>
                </wsa:EndpointReference>
                <dpws:Types>dpws:Device</dpws:Types>
            </dpws:Host>
            <dpws:Hosted>
                <wsa:EndpointReference>
                    <wsa:Address>https://192.168.0.105:58173/7a09f8a4-42a0-4aaa-852c-932e7e8401fc/HighPriorityServices</wsa:Address>
                </wsa:EndpointReference>
                <dpws:Types xmlns:sdc=\"http://standards.ieee.org/downloads/11073/11073-20701-2018\">sdc:GetService sdc:SetService sdc:ContainmentTreeService sdc:ContextService sdc:DescriptionEventService sdc:StateEventService sdc:WaveformService</dpws:Types>
                <dpws:ServiceId>my:HighPriorityServices</dpws:ServiceId>
            </dpws:Hosted>
            <dpws:Hosted>
                <wsa:EndpointReference>
                <wsa:Address>https://192.168.0.105:58173/7a09f8a4-42a0-4aaa-852c-932e7e8401fc/LowPriorityServices</wsa:Address>
                </wsa:EndpointReference>
                <dpws:Types></dpws:Types>
                <dpws:ServiceId>my:LowPriorityServices</dpws:ServiceId>
            </dpws:Hosted>
        </dpws:Relationship>
    ";
            let mut reader = XmlReader::create_typed(input.as_bytes());
            let mut buf = vec![];
            let relationship: Relationship =
                find_start_element_reader!(Relationship, DPWS_RELATIONSHIP_TAG, reader, buf)?;
            test_relationship(&relationship);
            relationship
        };

        // back to xml, parse it again and test it
        let mut writer = XmlWriter::new(vec![]);
        relationship.to_xml_complex(Some(DPWS_RELATIONSHIP_TAG_STR), &mut writer, false)?;
        let writer_buf = writer.inner();

        let mut reader = XmlReader::create_typed(writer_buf.as_ref());
        let mut buf = vec![];

        let relationship_again: Relationship =
            find_start_element_reader!(Relationship, DPWS_RELATIONSHIP_TAG, reader, buf)?;
        test_relationship(&relationship_again);

        Ok(())
    }
}
