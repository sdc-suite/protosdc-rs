use protosdc_biceps::types::AnyContent;
use protosdc_xml::xml_reader::ReplacementExtensionHandler;
use protosdc_xml::ExtensionHandler;
use std::io::BufRead;

/// Factory to create [ExtensionHandler]s which allow the XML stack to process
/// additional extension types.
pub trait ExtensionHandlerFactory: Clone {
    fn create<B: BufRead>(
        &self,
    ) -> Box<dyn ExtensionHandler<B, Box<dyn AnyContent + Send + Sync>> + Send + Sync>;
}

/// [ExtensionHandlerFactory] which creates [ReplacementExtensionHandler]s that
/// skip unknown elements.
#[derive(Clone, Debug)]
pub struct ReplacementExtensionHandlerFactory;

impl ExtensionHandlerFactory for ReplacementExtensionHandlerFactory {
    fn create<B: BufRead>(
        &self,
    ) -> Box<dyn ExtensionHandler<B, Box<dyn AnyContent + Send + Sync>> + Send + Sync> {
        Box::new(ReplacementExtensionHandler)
    }
}
