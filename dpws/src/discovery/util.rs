use std::net::{IpAddr, Ipv4Addr, SocketAddr};

use log::error;
use tokio::sync::mpsc::Sender;
use tokio::sync::oneshot;

use crate::discovery::consumer::discovery_consumer::DpwsDiscoveryError;
use network::udp_binding::{OutboundUdpMessage, UdpBindingError, UdpMessage};
use protosdc::discovery::common::error::DiscoveryError;

pub(crate) async fn send_multicast(
    udp_sender: Sender<UdpMessage>,
    message: Vec<u8>,
) -> Result<(), DpwsDiscoveryError> {
    let (response_sender, response_receiver) = oneshot::channel();

    udp_sender
        .send(UdpMessage::OutboundMulticast {
            message: OutboundUdpMessage {
                data: message,
                // address is ignored for multicast, set localhost
                address: SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 8080),
            },
            response: Some(response_sender),
        })
        .await
        .map_err(|err| error!("Unexpected error sending multicast message: {}", err))
        .ok();

    response_receiver
        .await
        .map_err(|err| DiscoveryError::UDPError(UdpBindingError::OneShotError(err)))
        .map_err(DpwsDiscoveryError::DiscoveryError)?
        .map_err(DiscoveryError::UDPError)
        .map_err(DpwsDiscoveryError::DiscoveryError)
}
