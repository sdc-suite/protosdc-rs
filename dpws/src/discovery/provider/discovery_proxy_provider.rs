use crate::discovery::actions;
use crate::soap::common::SoapError;
use crate::soap::soap_client::SoapClient;
use crate::xml::common::SoapHeaderBuilder;
use crate::xml::messages::discovery::{Bye, Hello};
use crate::xml::messages::soap::envelope::SoapMessage;
use crate::xml::messages::ws_eventing::renew::NoopResponse;
use async_trait::async_trait;

/// Represents DPWS (Device Profile for Web Services) discovery proxy provider functionality
/// which can send "Hello" and "Bye" messages to the discovery proxy.
#[async_trait]
pub trait DpwsDiscoveryProxyProvider {
    async fn hello(&self, hello: Hello) -> Result<(), SoapError>;

    async fn bye(&self, bye: Bye) -> Result<(), SoapError>;
}

/// # Warning
/// This implementation does not represent a finished specification, avoid using it.
#[derive(Clone, Debug)]
pub struct DummyDpwsDiscoveryProxyProviderImpl {}

#[async_trait]
impl DpwsDiscoveryProxyProvider for DummyDpwsDiscoveryProxyProviderImpl {
    async fn hello(&self, _hello: Hello) -> Result<(), SoapError> {
        todo!()
    }

    async fn bye(&self, _bye: Bye) -> Result<(), SoapError> {
        todo!()
    }
}

#[derive(Clone)]
pub struct DpwsDiscoveryProxyProviderImpl<S>
where
    S: SoapClient + Clone,
{
    soap_client: S,
}

impl<S> DpwsDiscoveryProxyProviderImpl<S>
where
    S: SoapClient + Clone,
{
    pub(crate) fn new(soap_client: S) -> Self {
        Self { soap_client }
    }
}

#[async_trait]
impl<S> DpwsDiscoveryProxyProvider for DpwsDiscoveryProxyProviderImpl<S>
where
    S: SoapClient + Send + Sync + Clone,
{
    async fn hello(&self, hello: Hello) -> Result<(), SoapError> {
        self.soap_client
            .request_response_opt_body(SoapHeaderBuilder::new().action(actions::HELLO), hello)
            .await
            .map(|_it: (SoapMessage, Option<NoopResponse>)| ())
    }

    async fn bye(&self, bye: Bye) -> Result<(), SoapError> {
        self.soap_client
            .request_response_opt_body(SoapHeaderBuilder::new().action(actions::BYE), bye)
            .await
            .map(|_it: (SoapMessage, Option<NoopResponse>)| ())
    }
}
