use crate::http::server::acceptor::TlsData;
use crate::http::server::common::{
    ContextAddresses, ContextMessage, RegistrationResult, RegistryError, Server, ServerAddresses,
    ServerError, ServerRegistry,
};
use async_trait::async_trait;
use axum::extract::connect_info::{ConnectInfo, Connected};
use axum::extract::{Path, State};
use axum::response::{Html, IntoResponse, Response};
use axum::routing::{get, post};
use axum::{extract, Extension, Router};

#[cfg(all(not(feature = "tls_openssl"), feature = "tls_rustls"))]
use axum_server::tls_rustls::{RustlsAcceptor, RustlsConfig};

use crate::http::server::axum_server::handle::Handle;
use crate::http::server::axum_server::server;
use crate::http::server::axum_server::server::IncomingStream;
use common::crypto::CryptoConfig;
use common::{Service, ServiceDelegate, ServiceError, ServiceState};
use futures::future::Either;
use http::{Request, StatusCode};
use http_body_util::BodyExt;
use log::{debug, error, info, warn};
use std::collections::HashMap;
use std::future::Future;
use std::net::SocketAddr;
use std::sync::Arc;
use thiserror::Error;
use tokio::sync::mpsc::Sender;
use tokio::sync::{mpsc, oneshot, RwLock};
use tokio::task::JoinHandle;
use tokio_rustls::rustls::pki_types::CertificateDer;
#[cfg(all(not(feature = "tls_openssl"), feature = "tls_rustls"))]
use tokio_rustls::rustls::ServerConfig;

/// Represents the maximum number of requests that can currently be handled by the
/// axum server and registry.
const BUFFER_SIZE: usize = 150;

type HandlerAccess = Arc<RwLock<AxumServerHandler>>;

type RegistryMap = HashMap<String, Sender<ContextMessage>>;

#[derive(Debug)]
pub struct AxumRegistry {
    _msg_task: JoinHandle<()>,
    sender: Sender<AxumRegistryAccessMessage>,
}

#[derive(Debug)]
enum AxumRegistryAccessMessage {
    Register {
        addr: SocketAddr,
        response: oneshot::Sender<Result<AxumServerAccess, RegistryError>>,
    },
}

#[derive(Clone, Debug)]
pub struct AxumRegistryAccess {
    sender: Sender<AxumRegistryAccessMessage>,
}

#[derive(Debug)]
pub struct AxumServer {
    service_delegate: ServiceDelegate,
    _msg_task: JoinHandle<()>,
    sender: Sender<AxumServerAccessMessage>,
    _server_task: JoinHandle<Result<(), std::io::Error>>,
    server_handler: HandlerAccess,
}

#[derive(Debug)]
enum AxumServerAccessMessage {
    GetAddress {
        response: oneshot::Sender<Option<ServerAddresses>>,
    },
    RegisterContextPath {
        context_path: String,
        response: oneshot::Sender<Result<RegistrationResult, ServerError>>,
    },
    UnregisterContextPath {
        context_path: String,
        response: oneshot::Sender<Result<(), ServerError>>,
    },
    Shutdown,
}

#[derive(Clone, Debug)]
pub struct AxumServerAccess {
    sender: Sender<AxumServerAccessMessage>,
}

impl AxumServerAccess {
    async fn send_command<ResponseType>(
        &self,
        command: AxumServerAccessMessage,
        rx: oneshot::Receiver<ResponseType>,
    ) -> Result<ResponseType, ServerError> {
        self.sender
            .send(command)
            .await
            .map_err(|err| ServerError::Other(anyhow::Error::from(err)))?;

        rx.await
            .map_err(|err| ServerError::Other(anyhow::Error::from(err)))
    }
}

#[async_trait]
impl Server for AxumServerAccess {
    async fn get_address(&self) -> Option<ServerAddresses> {
        let (tx, rx) = oneshot::channel();
        self.send_command(AxumServerAccessMessage::GetAddress { response: tx }, rx)
            .await
            .unwrap_or(None)
    }

    async fn register_context_path(
        &self,
        context_path: &str,
    ) -> Result<RegistrationResult, ServerError> {
        let (tx, rx) = oneshot::channel();
        self.send_command(
            AxumServerAccessMessage::RegisterContextPath {
                context_path: context_path.to_string(),
                response: tx,
            },
            rx,
        )
        .await?
    }

    async fn unregister_context_path(&self, context_path: &str) -> Result<(), ServerError> {
        let (tx, rx) = oneshot::channel();
        self.send_command(
            AxumServerAccessMessage::UnregisterContextPath {
                context_path: context_path.to_string(),
                response: tx,
            },
            rx,
        )
        .await?
    }
}

#[derive(Debug, Default)]
pub struct AxumServerHandler {
    context_path_registry: RegistryMap,
}

impl AxumRegistry {
    pub fn new(crypto: Option<CryptoConfig>) -> Self {
        let mut registry: HashMap<SocketAddr, AxumServer> = HashMap::new();

        let (tx, mut rx) = mpsc::channel(BUFFER_SIZE);

        let task = tokio::spawn(async move {
            while let Some(msg) = rx.recv().await {
                match msg {
                    AxumRegistryAccessMessage::Register { addr, response } => {
                        let server = init_server(&mut registry, &crypto, addr).await;
                        if response.send(server).is_err() {
                            error!("Could not send server access, requestee dropped");
                        }
                    }
                }
            }
        });

        Self {
            _msg_task: task,
            sender: tx,
        }
    }

    pub fn access(&self) -> AxumRegistryAccess {
        AxumRegistryAccess {
            sender: self.sender.clone(),
        }
    }
}

#[async_trait]
impl ServerRegistry<AxumServerAccess> for AxumRegistryAccess {
    async fn init_server(&self, address: SocketAddr) -> Result<AxumServerAccess, RegistryError> {
        let (tx, rx) = oneshot::channel();
        self.sender
            .send(AxumRegistryAccessMessage::Register {
                addr: address,
                response: tx,
            })
            .await
            .map_err(|err| {
                RegistryError::ServiceError(ServiceError::Other(anyhow::Error::from(err)))
            })?;
        match rx.await {
            Ok(o) => o,
            Err(err) => Err(RegistryError::ServiceError(ServiceError::Other(
                anyhow::Error::from(err),
            ))),
        }
    }
}

async fn init_server(
    registry: &mut HashMap<SocketAddr, AxumServer>,
    crypto: &Option<CryptoConfig>,
    address: SocketAddr,
) -> Result<AxumServerAccess, RegistryError> {
    // find available server or create a new one
    // TODO: Check crypto equality too
    if registry.get(&address).is_none() {
        let server = AxumServer::new(address, crypto.clone()).await?;
        let actual_address = *server
            .get_address()
            .await
            .ok_or(RegistryError::NoAddressError)?
            .socket_addr();
        registry.insert(actual_address, server);
        Ok(registry.get_mut(&actual_address).unwrap().access()) // must exist now
    } else {
        Ok(registry.get_mut(&address).unwrap().access()) // must exist
    }
}

#[derive(Debug, Error)]
enum AxumServerHandlerError {
    #[error("Could not forward the request {path} to the handler")]
    CouldNotForwardRequest { path: String },

    #[error("Could not forward the request {id} to the handler")]
    UnknownId { id: String },
}

impl IntoResponse for AxumServerHandlerError {
    fn into_response(self) -> Response {
        warn!("{:?}", &self);
        match self {
            AxumServerHandlerError::CouldNotForwardRequest { .. } => {
                (StatusCode::INTERNAL_SERVER_ERROR, format!("{:?}", self)).into_response()
            }
            AxumServerHandlerError::UnknownId { .. } => {
                (StatusCode::BAD_REQUEST, format!("{:?}", self)).into_response()
            }
        }
    }
}

impl AxumServerHandler {
    async fn message(
        &self,
        path: String,
        request: extract::Request,
        local_addr: SocketAddr,
        remote_addr: SocketAddr,
        peer_certificates: Option<Vec<CertificateDer<'static>>>,
    ) -> impl Future<Output = impl IntoResponse> {
        match self.context_path_registry.get(&path) {
            Some(sender) => {
                let (tx, rx) = oneshot::channel();

                let (a, b) = request.into_parts();
                match sender
                    .send(ContextMessage {
                        request: Request::from_parts(a, b.boxed_unsync()),
                        receiver_addr: local_addr,
                        sender_addr: remote_addr,
                        peer_certificates,
                        response_sender: tx,
                    })
                    .await
                {
                    Ok(_) => {}
                    Err(_) => {
                        return Either::Left(async {
                            AxumServerHandlerError::CouldNotForwardRequest { path }.into_response()
                        })
                    }
                }
                Either::Right(Either::Left(async {
                    match rx.await {
                        Ok(resp) => resp.into_response(),
                        Err(_) => {
                            AxumServerHandlerError::CouldNotForwardRequest { path }.into_response()
                        }
                    }
                }))
            }
            None => Either::Right(Either::Right(async {
                AxumServerHandlerError::UnknownId { id: path }.into_response()
            })),
        }
    }
}

#[derive(Clone, Debug)]
struct MyConnectInfo {
    local: SocketAddr,
    remote: SocketAddr,
}

impl Connected<IncomingStream> for MyConnectInfo {
    fn connect_info(target: IncomingStream) -> Self {
        MyConnectInfo {
            local: *target
                .local_addr()
                .as_ref()
                .expect("no local address in tcp stream, broken"),
            remote: target.remote_addr(),
        }
    }
}

async fn handler_context_path(
    ConnectInfo(addr): ConnectInfo<MyConnectInfo>,
    State(handler_access): State<HandlerAccess>,
    Path(context_path): Path<String>,
    request: extract::Request,
) -> impl IntoResponse {
    debug!(
        "Access to {:?} {} from {:?}",
        addr.local, context_path, addr.remote
    );
    let processing = {
        let handler = handler_access.read().await;
        handler
            .message(context_path, request, addr.local, addr.remote, None)
            .await
    };
    processing.await
}

async fn handler_context_path_tls(
    ConnectInfo(addr): ConnectInfo<MyConnectInfo>,
    State(handler_access): State<HandlerAccess>,
    Path(context_path): Path<String>,
    Extension(tls_data): Extension<TlsData>,
    request: extract::Request,
) -> impl IntoResponse {
    debug!(
        "access to {:?} {} from {:?} via TLS",
        addr.local, context_path, addr.remote
    );
    let processing = {
        let handler = handler_access.read().await;
        handler
            .message(
                context_path,
                request,
                addr.local,
                addr.remote,
                tls_data.peer_certificates,
            )
            .await
    };
    processing.await
}

async fn fallback() -> impl IntoResponse {
    Html("<h1>This page does not exist.</h1>\
    If it ever did, I do not know. Here is a joke for you: Have you heard? They don't make 2m tape measures any longer.")
}

#[async_trait]
impl Service for AxumServer {
    async fn start(&mut self) -> Result<(), ServiceError> {
        self.service_delegate.started().await?;
        Ok(())
    }

    async fn stop(&mut self) -> Result<(), ServiceError> {
        self.service_delegate.stop().await?;

        self.sender
            .send(AxumServerAccessMessage::Shutdown)
            .await
            .ok(); // result is don't care
        info!("Server shutting down");

        self.service_delegate.stopped().await?;
        Ok(())
    }

    fn is_running(&self) -> bool {
        self.service_delegate.is_running()
    }

    fn state(&self) -> &ServiceState {
        self.service_delegate.state()
    }
}

impl AxumServer {
    async fn new(
        requested_addr: SocketAddr,
        crypto_config: Option<CryptoConfig>,
    ) -> Result<Self, ServerError> {
        let server_handler = HandlerAccess::default();

        let app = match &crypto_config {
            Some(_) => Router::new()
                .route("/*context_path", get(handler_context_path_tls))
                .route("/*context_path", post(handler_context_path_tls))
                .fallback(fallback)
                .with_state(server_handler.clone()),
            None => Router::new()
                .route("/*context_path", get(handler_context_path))
                .route("/*context_path", post(handler_context_path))
                .fallback(fallback)
                .with_state(server_handler.clone()),
        };

        let handle = Handle::new();
        let server_handle = handle.clone();

        #[cfg(all(not(feature = "tls_openssl"), feature = "tls_rustls"))]
        let crypto = {
            info!("Initializing crypto with rustls");
            if let Some(crypto) = &crypto_config {
                let provider_certs: ServerConfig = crypto.clone().try_into()?;
                let axum_tls = RustlsConfig::from_config(Arc::new(provider_certs));
                Some(axum_tls)
            } else {
                None
            }
        };

        #[cfg(feature = "tls_openssl")]
        let crypto = {
            info!("Initializing crypto with openssl");
            if let Some(crypto) = &crypto_config {
                let acceptor: openssl::ssl::SslAcceptorBuilder = crypto.clone().try_into()?;
                Some(acceptor)
            } else {
                None
            }
        };

        let server_task = match crypto {
            None => tokio::spawn(
                server::bind(requested_addr)
                    .handle(server_handle)
                    .serve(app.into_make_service_with_connect_info::<MyConnectInfo>()),
            ),
            #[cfg(all(not(feature = "tls_openssl"), feature = "tls_rustls"))]
            Some(axum_tls) => tokio::spawn({
                server::bind(requested_addr)
                    .acceptor(crate::http::server::acceptor::CustomRustlsAcceptor::new(
                        RustlsAcceptor::new(axum_tls),
                    ))
                    .handle(server_handle)
                    .serve(app.into_make_service_with_connect_info::<MyConnectInfo>())
            }),
            #[cfg(feature = "tls_openssl")]
            Some(openssl_tls) => tokio::spawn({
                let openssl_config: axum_server::tls_openssl::OpenSSLConfig =
                    openssl_tls.try_into().map_err(|err| {
                        ServerError::Other(anyhow::anyhow!(
                            "Could not convert crypto config: {:?}",
                            err
                        ))
                    })?;
                let openssl_acceptor =
                    axum_server::tls_openssl::OpenSSLAcceptor::new(openssl_config);

                server::bind(requested_addr)
                    .acceptor(crate::http::server::acceptor::CustomOpensslAcceptor::new(
                        openssl_acceptor,
                    ))
                    .handle(server_handle)
                    .serve(app.into_make_service_with_connect_info::<MyConnectInfo>())
            }),
        };

        let server_addr = handle.listening().await.ok_or_else(|| {
            ServerError::Other(anyhow::anyhow!(
                "Listening failed to bind to {}, is there another instance running?",
                requested_addr
            ))
        })?;

        let actual_addr = match &crypto_config {
            None => ServerAddresses::Insecure(server_addr),
            Some(_) => ServerAddresses::Secure(server_addr),
        };

        let (tx, mut rx) = mpsc::channel(BUFFER_SIZE);

        let msg_task = tokio::spawn(async move {
            while let Some(msg) = rx.recv().await {
                match msg {
                    AxumServerAccessMessage::GetAddress { response } => {
                        response.send(Some(actual_addr.clone())).ok(); // result is don't care
                    }
                    AxumServerAccessMessage::RegisterContextPath {
                        context_path,
                        response,
                    } => {
                        response
                            .send(
                                register_context_path(&actual_addr, &context_path, &server_handler)
                                    .await,
                            )
                            .ok(); // result is don't care
                    }
                    AxumServerAccessMessage::UnregisterContextPath {
                        context_path,
                        response,
                    } => {
                        response
                            .send(unregister_context_path(&context_path, &server_handler).await)
                            .ok(); // result is don't care
                    }
                    AxumServerAccessMessage::Shutdown => {
                        handle.shutdown();
                    }
                }
            }
        });

        Ok(Self {
            service_delegate: Default::default(),
            sender: tx,
            _msg_task: msg_task,
            _server_task: server_task,
            server_handler: HandlerAccess::default(),
        })
    }

    fn access(&self) -> AxumServerAccess {
        AxumServerAccess {
            sender: self.sender.clone(),
        }
    }
}

async fn register_context_path(
    address: &ServerAddresses,
    context_path: &str,
    server_handler: &HandlerAccess,
) -> Result<RegistrationResult, ServerError> {
    let addr = address.with_context_path(context_path)?;
    info!(
        "Registering new context path {} with id {}",
        addr, context_path
    );

    let (tx, rx) = mpsc::channel(BUFFER_SIZE);
    {
        // critical path
        let mut handler = server_handler.write().await;
        let registry = &mut handler.context_path_registry;

        if registry.get(context_path).is_some() {
            return Err(ServerError::ContextAlreadyRegistered {
                path: context_path.to_string(),
            });
        }
        registry.insert(context_path.to_string(), tx);
    }

    info!("Context path {} with id {} registered", addr, context_path);

    Ok(RegistrationResult {
        address: ContextAddresses { address: addr },
        channel: rx,
    })
}

async fn unregister_context_path(
    context_path: &str,
    server_handler: &HandlerAccess,
) -> Result<(), ServerError> {
    let mut handler = server_handler.write().await;
    let registry = &mut handler.context_path_registry;

    if registry.remove(context_path).is_none() {
        return Err(ServerError::UnknownContextPath);
    }

    Ok(())
}

#[async_trait]
impl Server for AxumServer {
    async fn get_address(&self) -> Option<ServerAddresses> {
        self.access().get_address().await
    }

    async fn register_context_path(
        &self,
        context_path: &str,
    ) -> Result<RegistrationResult, ServerError> {
        self.access().register_context_path(context_path).await
    }

    async fn unregister_context_path(&self, context_path: &str) -> Result<(), ServerError> {
        let mut handler = self.server_handler.write().await;
        let registry = &mut handler.context_path_registry;

        if registry.remove(context_path).is_none() {
            return Err(ServerError::UnknownContextPath);
        }

        Ok(())
    }
}

#[cfg(test)]
mod test {
    use crate::http::server::acceptor::get_x509;
    use crate::http::server::axum::AxumRegistry;
    use crate::http::server::common::{ContextMessageResponse, Server, ServerRegistry};
    use axum::response::Html;
    use bytes::Bytes;
    use common::crypto::der_to_pem;
    use common_test::crypto::generate_certificates;
    use http::Response;
    use hyper::StatusCode;
    use log::info;
    use std::net::SocketAddr;
    use tokio_rustls::rustls::ClientConfig;

    pub fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[tokio::test]
    async fn test_startup() -> anyhow::Result<()> {
        init();
        let registry = AxumRegistry::new(None);

        let registry_access = registry.access();

        let addr1 = {
            let server = registry_access
                .init_server(SocketAddr::from(([127, 0, 0, 1], 0)))
                .await?;

            server.get_address().await
        };
        let server = {
            let server2 = registry_access
                .init_server(addr1.clone().unwrap().into())
                .await?;

            assert_eq!(addr1, server2.get_address().await);
            server2
        };

        let mut result = server
            .register_context_path("subscription/test_sub")
            .await?;

        let _tsk = tokio::spawn(async move {
            let msg = result.channel.recv().await.unwrap();
            info!("tsk msg");

            let response = Response::new(Bytes::from(Html("Good, how about you?").0));
            msg.response_sender
                .send(ContextMessageResponse {
                    status_code: StatusCode::CREATED,
                    payload: response,
                })
                .unwrap();
        });

        let target_addr = result.address.address.to_string();

        info!("Get request to {}", target_addr);

        // welcome to proxy hell, where tests can fail because
        // reqwest respects the system proxy... ╭( ๐_๐)╮
        let client = reqwest::Client::builder()
            .no_proxy()
            .build()
            .expect("Could not build client?");

        let response = client.get(&target_addr).send().await?;

        info!("Response: {:?}", response);

        assert_eq!(StatusCode::CREATED, response.status());
        assert_eq!("Good, how about you?", response.text().await?.as_str());

        Ok(())
    }

    #[tokio::test]
    async fn test_mutual_tls() -> anyhow::Result<()> {
        init();
        let crypto = generate_certificates()?;

        let consumer_pem =
            String::from_utf8(crypto.consumer.public.clone()).expect("Serialize failed");
        let consumer_subject = crypto.consumer.subject.clone();

        let registry = AxumRegistry::new(Some(crypto.provider_crypto_config()));

        let registry_access = registry.access();

        let addr1 = {
            let server = registry_access
                .init_server(SocketAddr::from(([127, 0, 0, 1], 0)))
                .await?;

            server.get_address().await
        };
        let server = {
            let server2 = registry_access
                .init_server(addr1.clone().unwrap().into())
                .await?;

            assert_eq!(addr1, server2.get_address().await);
            server2
        };

        let mut result = server
            .register_context_path("subscription/test_sub")
            .await?;

        let _tsk = tokio::spawn(async move {
            let msg = result.channel.recv().await.unwrap();
            info!("tsk msg");

            let peer_chain = msg.peer_certificates.expect("No certs?");
            assert_eq!(1, peer_chain.len());

            let peer_cert = peer_chain.first().expect("cert");

            let peer_x509_cert = get_x509(peer_cert).expect("peer cert");

            assert_eq!(
                consumer_subject,
                peer_x509_cert
                    .subject
                    .iter_common_name()
                    .next()
                    .expect("cert cn")
                    .as_str()
                    .expect("str")
                    .to_string()
            );

            let peer_pem_string = der_to_pem(peer_cert.to_vec());

            assert_eq!(consumer_pem, peer_pem_string);

            let response = Response::new(Bytes::from(Html("Good, how about you?").0));
            msg.response_sender
                .send(ContextMessageResponse {
                    status_code: StatusCode::CREATED,
                    payload: response,
                })
                .unwrap();
        });

        let target_addr = result.address.address.to_string();

        info!("Get request to {}", target_addr);

        let client_config: ClientConfig = crypto.consumer_crypto_config().try_into()?;
        // welcome to proxy hell, where tests can fail because
        // reqwest respects the system proxy... ╭( ๐_๐)╮
        let client = reqwest::Client::builder()
            .no_proxy()
            .use_preconfigured_tls(client_config)
            .build()
            .expect("Could not build client?");

        let response = client.get(&target_addr).send().await?;

        info!("Response: {:?}", response);

        assert_eq!(StatusCode::CREATED, response.status());
        assert_eq!("Good, how about you?", response.text().await?.as_str());

        let client_no_crypto = reqwest::Client::builder()
            .no_proxy()
            .build()
            .expect("Could not build client?");

        let response = client_no_crypto.get(&target_addr).send().await;
        assert!(response.is_err());

        Ok(())
    }
}
