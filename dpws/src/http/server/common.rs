use crate::soap::common::{create_soap_http_message, SOAP_FAULT_ACTION_STR};
use crate::xml::common::SoapHeaderBuilder;
use crate::xml::messages::soap::envelope::SoapMessage;
use crate::xml::messages::soap::fault::Fault;
use async_trait::async_trait;
use axum::response::{IntoResponse, Response};
use axum::Error;
use bytes::Bytes;
use common::crypto::CryptoError;
use common::ServiceError;
use http::{Request, Response as HttpResponse, StatusCode, Uri};
use protosdc_xml::XmlWriter;
use std::net::SocketAddr;
use thiserror::Error;
use tokio::sync::oneshot;
use tokio_rustls::rustls::pki_types::CertificateDer;

#[derive(Debug, Error)]
pub enum HttpError {
    #[error("Error with status code {status_code} and body {body:?}")]
    WithBody {
        status_code: StatusCode,
        body: Bytes,
    },
    #[error("Error with status code {status_code}")]
    Status { status_code: StatusCode },
}

#[derive(Debug, Error)]
pub enum ServerError {
    #[error("Context Path is not registered at the server and cannot be unregistered.")]
    UnknownContextPath,
    #[error("Context Path {path} is already registered at the server.")]
    ContextAlreadyRegistered { path: String },
    #[error(transparent)]
    UriError(#[from] http::uri::InvalidUri),
    #[error(transparent)]
    HttpError(#[from] http::Error),
    #[error("Service is not running")]
    ServiceNotRunning,
    #[error(transparent)]
    CryptoError(#[from] CryptoError),
    #[error(transparent)]
    Other(#[from] anyhow::Error),
}

#[derive(Debug, Error)]
pub enum RegistryError {
    #[error(transparent)]
    UriError(http::uri::InvalidUri),
    #[error(transparent)]
    ServerError(#[from] ServerError),
    #[error(transparent)]
    ServiceError(#[from] ServiceError),
    #[error("The server has not received an address, indicating an initialization error")]
    NoAddressError,
}

impl IntoResponse for HttpError {
    fn into_response(self) -> Response {
        match self {
            HttpError::WithBody { status_code, body } => (status_code, body).into_response(),
            HttpError::Status { status_code } => status_code.into_response(),
        }
    }
}

#[async_trait]
pub trait HttpHandler<E>
where
    E: IntoResponse,
{
    async fn handle(input: Bytes) -> Result<E, HttpError>;
}

#[derive(Clone, Debug, PartialEq)]
pub enum ServerAddresses {
    Insecure(SocketAddr),
    Secure(SocketAddr),
}

impl ServerAddresses {
    pub(crate) fn with_context_path(&self, path: &str) -> Result<Uri, ServerError> {
        let (scheme, addr) = match self {
            ServerAddresses::Insecure(addr) => ("http", addr),
            ServerAddresses::Secure(addr) => ("https", addr),
        };

        let authority = format!("{}:{}", addr.ip(), addr.port());

        let full_path = format!("/{}", path);

        Ok(Uri::builder()
            .scheme(scheme)
            .authority(authority)
            .path_and_query(full_path)
            .build()?)
    }

    pub(crate) fn socket_addr(&self) -> &SocketAddr {
        match self {
            ServerAddresses::Insecure(x) => x,
            ServerAddresses::Secure(x) => x,
        }
    }
}

impl From<ServerAddresses> for SocketAddr {
    fn from(val: ServerAddresses) -> Self {
        match val {
            ServerAddresses::Insecure(x) => x,
            ServerAddresses::Secure(x) => x,
        }
    }
}

#[derive(Clone, Debug)]
pub struct ContextAddresses {
    pub address: Uri,
}

pub(crate) type BoxBody = http_body_util::combinators::UnsyncBoxBody<Bytes, Error>;

/// Represents an HTTP request with its context, i.e. connection details, and a sender to send the
/// response.
#[derive(Debug)]
pub struct ContextMessage {
    pub request: Request<BoxBody>,
    pub receiver_addr: SocketAddr,
    pub sender_addr: SocketAddr,
    pub peer_certificates: Option<Vec<CertificateDer<'static>>>,
    pub response_sender: oneshot::Sender<ContextMessageResponse>,
}

#[derive(Debug)]
pub struct ContextMessageResponse {
    pub status_code: StatusCode,
    pub payload: HttpResponse<Bytes>,
}

impl From<Fault> for ContextMessageResponse {
    fn from(value: Fault) -> Self {
        let header = SoapHeaderBuilder::new()
            .action(SOAP_FAULT_ACTION_STR)
            .build();

        let msg = SoapMessage { header };

        let mut writer = XmlWriter::new(vec![]);
        msg.to_xml_complex(&mut writer, value)
            .expect("Could not convert fault to xml");
        let writer_buf = writer.into_inner();

        let payload = create_soap_http_message(writer_buf.into());
        Self {
            status_code: StatusCode::BAD_REQUEST,
            payload,
        }
    }
}

impl IntoResponse for ContextMessageResponse {
    fn into_response(self) -> Response {
        let (mut parts, bytes) = self.payload.into_parts();
        parts.status = self.status_code;
        Response::from_parts(parts, axum_core::body::Body::from(bytes))
    }
}

#[derive(Debug)]
pub struct RegistrationResult {
    pub address: ContextAddresses,
    pub channel: tokio::sync::mpsc::Receiver<ContextMessage>,
}

#[async_trait]
pub trait Server {
    /// None if server is not running
    async fn get_address(&self) -> Option<ServerAddresses>;
    async fn register_context_path(
        &self,
        context_path: &str,
    ) -> Result<RegistrationResult, ServerError>;

    async fn unregister_context_path(&self, context_path: &str) -> Result<(), ServerError>;
}

#[async_trait]
pub trait ServerRegistry<E>
where
    E: Server,
{
    async fn init_server(&self, address: SocketAddr) -> Result<E, RegistryError>;
}
