use crate::http::client::common::{HttpClient, HttpClientError};
use async_trait::async_trait;
use bytes::Bytes;
use common::crypto::{CryptoConfig, CryptoError};
use http::{HeaderMap, Response};
use log::error;
use reqwest::ResponseBuilderExt;
use std::time::Duration;
use thiserror::Error;
use tokio::sync::mpsc::{channel, Sender};
use tokio::sync::oneshot;
use tokio::task::JoinHandle;
use tokio_rustls::rustls::ClientConfig;

const REQUEST_TIMEOUT: Duration = Duration::from_secs(5);
const CONNECTION_TIMEOUT: Duration = Duration::from_secs(10);

#[derive(Debug)]
pub struct ReqwestHttpClient {
    _client: reqwest::Client,
    sender: Sender<ReqwestClientCommand>,
    _client_task: JoinHandle<()>,
}

enum Command {
    Get {
        uri: String,
    },
    Post {
        uri: String,
        headers: HeaderMap,
        body: Bytes,
    },
}

struct ReqwestClientCommand {
    sender: oneshot::Sender<Result<Response<Bytes>, HttpClientError>>,
    command: Command,
}

#[derive(Clone, Debug)]
pub struct ReqwestClientAccess {
    sender: Sender<ReqwestClientCommand>,
}

#[derive(Debug, Error)]
pub enum ReqwestClientError {
    #[error(transparent)]
    CryptoError(#[from] CryptoError),
    #[error(transparent)]
    ReqwestError(#[from] reqwest::Error),
}

async fn get(client: &mut reqwest::Client, url: &str) -> Result<Response<Bytes>, HttpClientError> {
    let response = client.get(url).send().await?;

    reqwest_response_to_http_response(response).await
}

async fn post(
    client: &mut reqwest::Client,
    url: &str,
    headers: HeaderMap,
    body: Bytes,
) -> Result<Response<Bytes>, HttpClientError> {
    let response = client.post(url).headers(headers).body(body).send().await?;

    reqwest_response_to_http_response(response).await
}

impl ReqwestHttpClient {
    pub fn new(crypto: Option<CryptoConfig>) -> Result<Self, ReqwestClientError> {
        let client_config: Option<ClientConfig> = crypto
            .map(|it| it.try_into())
            .map_or(Ok(None), |r| r.map(Some))?;

        let client_builder = reqwest::Client::builder()
            .timeout(REQUEST_TIMEOUT)
            .https_only(client_config.is_some()) // require tls if we have crypto
            .no_proxy()
            .http1_only() // SDC is http/1.1 only
            .tls_built_in_root_certs(false) // only use explicitly added certs
            .pool_idle_timeout(CONNECTION_TIMEOUT)
            .pool_max_idle_per_host(1); // only one connection per host

        let client_builder = if let Some(crypto_cfg) = client_config {
            client_builder.use_preconfigured_tls(crypto_cfg)
        } else {
            client_builder
        };

        let client = client_builder.build()?;

        let (tx, mut rx) = channel::<ReqwestClientCommand>(100);
        let task_client = client.clone();
        let processing_task = tokio::spawn(async move {
            let mut tsk_client = task_client;

            while let Some(msg) = rx.recv().await {
                let result = match msg.command {
                    Command::Get { uri } => get(&mut tsk_client, &uri).await,
                    Command::Post { uri, headers, body } => {
                        post(&mut tsk_client, &uri, headers, body).await
                    }
                };
                match msg.sender.send(result) {
                    Ok(_) => {}
                    Err(err) => error!("Error sending response: {:?}", err),
                }
            }
        });

        Ok(Self {
            _client: client,
            sender: tx,
            _client_task: processing_task,
        })
    }

    pub fn get_access(&self) -> ReqwestClientAccess {
        ReqwestClientAccess {
            sender: self.sender.clone(),
        }
    }
}

// TODO: this really isn't ideal, this transformation serves no purpose but to fulfill the interface
//  and cause some work
async fn reqwest_response_to_http_response(
    response: reqwest::Response,
) -> Result<Response<Bytes>, HttpClientError> {
    let mut transformed_response_builder = Response::builder();
    transformed_response_builder = response
        .headers()
        .iter()
        .fold(transformed_response_builder, |builder, (key, value)| {
            builder.header(key, value)
        });
    transformed_response_builder = transformed_response_builder
        .url(response.url().clone())
        .version(response.version())
        .status(response.status());

    Ok(transformed_response_builder.body(response.bytes().await?)?)
}

impl ReqwestClientAccess {
    async fn send_command(&self, command: Command) -> Result<Response<Bytes>, HttpClientError> {
        let (tx, rx) = oneshot::channel();
        self.sender
            .send(ReqwestClientCommand {
                sender: tx,
                command,
            })
            .await
            .map_err(|_| HttpClientError::NotResponding)?;

        match rx.await {
            Ok(ok) => ok,
            Err(_) => Err(HttpClientError::NotResponding),
        }
    }
}

#[async_trait]
impl HttpClient for ReqwestClientAccess {
    async fn get(&self, url: &str) -> Result<Response<Bytes>, HttpClientError> {
        self.send_command(Command::Get {
            uri: url.to_string(),
        })
        .await
    }

    async fn post(
        &self,
        headers: HeaderMap,
        url: &str,
        body: Bytes,
    ) -> Result<Response<Bytes>, HttpClientError> {
        self.send_command(Command::Post {
            uri: url.to_string(),
            headers,
            body,
        })
        .await
    }
}
