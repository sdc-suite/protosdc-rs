use async_trait::async_trait;
use bytes::Bytes;
use http::Response;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum HttpClientError {
    #[error(transparent)]
    ReqwestError(#[from] reqwest::Error),
    #[error(transparent)]
    HttpError(#[from] http::Error),
    #[error("Client is not responding")]
    NotResponding,
}

#[async_trait]
pub trait HttpClient {
    async fn get(&self, url: &str) -> Result<Response<Bytes>, HttpClientError>;
    async fn post(
        &self,
        headers: http::HeaderMap,
        url: &str,
        body: Bytes,
    ) -> Result<Response<Bytes>, HttpClientError>;
}
