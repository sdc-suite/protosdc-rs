use std::error::Error;
use vergen::EmitBuilder;

fn main() -> Result<(), Box<dyn Error>> {
    // Emit the instructions
    EmitBuilder::builder()
        // .idempotent()
        .git_sha(false)
        .git_branch()
        .emit_and_set()?;
    Ok(())
}
