use std::net::SocketAddr;
use std::time::Duration;

use async_trait::async_trait;
use bytes::Bytes;
use http::{header, Response, StatusCode};
use log::info;

use common::oneshot_log_error;
use common::test_util::init_logging;
use dpws::common::extension_handling::ReplacementExtensionHandlerFactory;
use dpws::http::client::reqwest::ReqwestHttpClient;
use dpws::http::server::axum::AxumRegistry;
use dpws::http::server::common::{ContextMessage, ContextMessageResponse};
use dpws::soap::common::MEDIA_TYPE_SOAP_HEADER;
use dpws::soap::dpws::client::event_sink::{EventSink, EventSinkImpl};
use dpws::soap::dpws::client::notification_sink::{NotificationSink, NotificationSinkError};
use dpws::soap::dpws::subscription_manager::SinkSubscriptionManagerImpl;
use dpws::soap::soap_client::SoapClientImpl;
use dpws::xml::messages::biceps_ext::BICEPS_ACTION_EPISODIC_METRIC_REPORT;
use dpws::xml::messages::common::DPWS_ACTION_DIALECT;
use dpws::xml::messages::ws_eventing::common::{Filter, FilterContent};

#[tokio::test]
#[ignore]
async fn test_subscribe() -> anyhow::Result<()> {
    init_logging();

    let http_client = ReqwestHttpClient::new(None)?;

    let target =
        "http://192.168.0.113:60310/7a09f8a4-42a0-4aaa-852c-932e7e8401fd/HighPriorityServices";

    let soap_client = SoapClientImpl::new_string(
        http_client.get_access(),
        target,
        ReplacementExtensionHandlerFactory,
    );

    let registry = AxumRegistry::new(None);

    let event_sink: EventSinkImpl<_, _, _, SinkSubscriptionManagerImpl> = EventSinkImpl::new(
        soap_client,
        SocketAddr::from(([192, 168, 0, 113], 0)),
        registry.access(),
    );

    let desired_actions = BICEPS_ACTION_EPISODIC_METRIC_REPORT;

    let notification_sink = PrintingNotificationSink {};

    let duration = Duration::from_secs(360);

    let filter = Filter {
        attributes: Default::default(),
        dialect: Some(DPWS_ACTION_DIALECT.to_string()),
        filters: Some(FilterContent::Text(desired_actions.to_string())),
    };

    let sub1 = event_sink
        .subscribe(Some(filter.clone()), duration, notification_sink.clone())
        .await?;

    // wait for some events to happen
    tokio::time::sleep(Duration::from_secs(2)).await;

    event_sink.unsubscribe(&sub1.subscription_id).await?;
    // event_sink.unsubscribe_all().await?;

    let sub2 = event_sink
        .subscribe(Some(filter), duration, notification_sink.clone())
        .await?;

    // wait for some events to happen
    tokio::time::sleep(Duration::from_secs(2)).await;

    event_sink.unsubscribe(&sub2.subscription_id).await?;

    Ok(())
}

#[derive(Clone, Debug)]
struct PrintingNotificationSink {}

#[async_trait]
impl NotificationSink for PrintingNotificationSink {
    async fn receive_notification(
        &self,
        notification: ContextMessage,
    ) -> Result<(), NotificationSinkError> {
        info!("Received notification");

        let mut payload: Response<Bytes> = Default::default();

        payload
            .headers_mut()
            .insert(header::ACCEPT, MEDIA_TYPE_SOAP_HEADER.clone());
        payload
            .headers_mut()
            .insert(header::CONTENT_TYPE, MEDIA_TYPE_SOAP_HEADER.clone());

        oneshot_log_error(
            notification.response_sender,
            ContextMessageResponse {
                status_code: StatusCode::ACCEPTED,
                payload,
            },
        );

        Ok(())
    }
}
