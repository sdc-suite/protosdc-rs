use std::net::IpAddr;

use log::info;

use biceps::common::access::mdib_access::MdibAccess;
use common::test_util::init_logging;
use common::Service;
use dpws::consumer::services::{get_context_states, get_md_description, get_md_state, get_mdib};
use dpws::soap::dpws::device::hosting_service::HostingService;
use network::platform::test_util::determine_adapter_address_for_multicast;
use network::udp_binding::IPV4_MULTICAST_ADDRESS;

use crate::test_util::{connect_consumer, create_device};

#[cfg(test)]
pub mod test_util;

#[tokio::test]
async fn test_get_service() -> anyhow::Result<()> {
    init_logging();
    // find address which does ipv4 multicast
    let multicast_address = IpAddr::V4(IPV4_MULTICAST_ADDRESS);
    let adapter_address = determine_adapter_address_for_multicast(multicast_address).await;

    let mut device = create_device(adapter_address, None, None).await?;
    device.start().await?;

    let to_connect = device.hosting_service.endpoint_reference().await;
    let remote_device = connect_consumer(adapter_address, to_connect, None).await?;
    {
        let mdib_version = remote_device.remote_mdib.mdib_version().await;
        info!("Remote Device Mdib Version: {:?}", mdib_version);
    }

    // get all entities
    let all_entities = device.local_mdib.entities_by_type(|_| true).await;

    let empty: &[&str] = &[];

    let hosting = remote_device.hosting_service.lock().await;
    let get_mdib_response = get_mdib(&*hosting).await?;
    let get_md_description_response = get_md_description(&*hosting, empty).await?;
    let get_md_state_response = get_md_state(&*hosting, empty).await?;
    let get_context_states_response = get_context_states(&*hosting, empty).await?;

    common_test::tests::get::get_tests::test_get_service(
        &all_entities,
        &remote_device.remote_mdib,
        &get_mdib_response,
        &get_md_description_response,
        &get_md_state_response,
        &get_context_states_response,
    )
    .await
}

#[tokio::test]
async fn test_metric_change() -> anyhow::Result<()> {
    init_logging();

    // find address which does ipv4 multicast
    let multicast_address = IpAddr::V4(IPV4_MULTICAST_ADDRESS);
    let adapter_address = determine_adapter_address_for_multicast(multicast_address).await;

    let mut device = create_device(adapter_address, None, None).await?;
    device.start().await?;

    let to_connect = device.hosting_service.endpoint_reference().await;
    let remote_device = connect_consumer(adapter_address, to_connect, None).await?;
    {
        let mdib_version = remote_device.remote_mdib.mdib_version().await;
        info!("Remote Device Mdib Version: {:?}", mdib_version);
    }

    common_test::tests::metric_change::metric_change_tests::test_metric_change(
        remote_device.remote_mdib.clone(),
        device.local_mdib.clone(),
    )
    .await
}
