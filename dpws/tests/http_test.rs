use bytes::Bytes;
use common::test_util::init_logging;
use dpws::common::extension_handling::ReplacementExtensionHandlerFactory;
use dpws::http::client::common::HttpClient;
use dpws::http::client::reqwest::ReqwestHttpClient;
use dpws::http::server::axum::AxumRegistry;
use dpws::http::server::common::{ContextMessageResponse, Server, ServerRegistry};
use dpws::soap::common::WSA_ACTION_GET_METADATA_REQUEST_STR;
use dpws::soap::soap_client::{SoapClient, SoapClientImpl};
use dpws::xml::common::SoapHeaderBuilder;
use dpws::xml::messages::soap::envelope::SoapMessage;
use dpws::xml::messages::ws_metadata_exchange::get_metadata::GetMetadata;
use dpws::xml::messages::ws_metadata_exchange::metadata::Metadata;
use http::{HeaderMap, StatusCode};
use std::net::SocketAddr;

#[tokio::test]
async fn test_unencrypted() -> anyhow::Result<()> {
    init_logging();

    let test_path = "eine/biene/mit/zwei/bergen/und/nem/foto/atellier";

    let registry = AxumRegistry::new(None);

    let server = registry
        .access()
        .init_server(SocketAddr::from(([127, 0, 0, 1], 0)))
        .await?;

    let path = server.register_context_path(test_path).await?;
    let path_string = path.address.address.to_string();
    let mut path_channel = path.channel;

    tokio::spawn(async move {
        if let Some(msg) = path_channel.recv().await {
            msg.response_sender
                .send(ContextMessageResponse {
                    status_code: StatusCode::IM_A_TEAPOT,
                    payload: Default::default(),
                })
                .expect("Unlucky");
        }
    });

    let client = ReqwestHttpClient::new(None)?;

    let client_access = client.get_access();

    let response = client_access
        .post(HeaderMap::default(), &path_string, Bytes::new())
        .await?;

    assert_eq!(StatusCode::IM_A_TEAPOT, response.status());

    Ok(())
}

#[tokio::test]
#[ignore]
async fn test_soap_client() -> anyhow::Result<()> {
    let http_client = ReqwestHttpClient::new(None)?;

    let target =
        "http://192.168.0.113:59381/7a09f8a4-42a0-4aaa-852c-932e7e8401fd/HighPriorityServices";

    let soap_client = SoapClientImpl::new_string(
        http_client.get_access(),
        target,
        ReplacementExtensionHandlerFactory,
    );

    let header = SoapHeaderBuilder::new().action(WSA_ACTION_GET_METADATA_REQUEST_STR);

    let get_metadata = GetMetadata {
        attributes: Default::default(),
        dialect: None,
        identifier: None,
    };

    let response: (SoapMessage, Metadata) =
        soap_client.request_response(header, get_metadata).await?;

    println!("{:?}", response.1);

    Ok(())
}
