use std::collections::HashSet;
#[cfg(test)]
use std::net::{IpAddr, SocketAddr};

use anyhow::Result;
use biceps::common::access::mdib_access::MdibAccess;
use biceps::provider::access::local_mdib_access::LocalMdibAccess;
use biceps::test_util::mdib::mdib_modification_presets;
use common::crypto::CryptoConfig;
use dpws::common::extension_handling::ReplacementExtensionHandlerFactory;
use dpws::consumer::sco::DpwsSetServiceHandler;
use dpws::consumer::sdc_consumer::{ConsumerConfigBuilder, SdcRemoteDevice};
use dpws::consumer::sdc_remote_device_connector;
use dpws::consumer::sdc_remote_device_connector::{
    ConnectConfiguration, ALL_EPISODIC_AND_WAVEFORM_REPORTS,
};
use dpws::discovery::constants::WS_DISCOVERY_PORT;
use dpws::discovery::consumer::discovery_consumer::DpwsDiscoveryConsumerImpl;
use dpws::discovery::provider::discovery_provider::{
    DpwsDiscoveryProcessorImpl, DpwsDiscoveryProviderImpl,
};
use dpws::discovery::provider::discovery_proxy_provider::DpwsDiscoveryProxyProviderImpl;
use dpws::http::client::reqwest::{ReqwestClientAccess, ReqwestHttpClient};
use dpws::http::server::axum::AxumRegistry;
use dpws::provider::sdc_provider::SdcDevice;
use dpws::provider::sdc_provider_plugin::TypesScopesPlugin;
use dpws::soap::dpws::client::consumer::{
    Consumer, ConsumerHostedService, ConsumerHostingService, ConsumerImpl, ReqwestSoapClient,
};
use dpws::soap::dpws::device::dpws_device::DeviceSettings;
use dpws::soap::dpws::device::hosting_service::HostingServiceImpl;
use dpws::xml::messages::addressing::WsaEndpointReference;
use dpws::xml::messages::common::{DPWS_DEVICE_TYPE, MDPWS_MEDICAL_DEVICE_TYPE};
use log::{debug, info};

use dpws::soap::dpws::client::hosting_service::HostingService;
use dpws::soap::soap_client::SoapClientImpl;

use network::udp_binding::{create_udp_binding, UdpBindingImpl};
use protosdc::common::uuid::generate_provider_epr_uuid;
use protosdc::consumer::remote_device::DefaultRemoteMdibAccessImpl;
use protosdc::consumer::sco::sco_controller::ScoController;
use protosdc::consumer::sco::sco_transaction::ScoTransactionImpl;
use protosdc::provider::device::DefaultLocalMdibAccessImpl;
use protosdc::provider::localization_storage::LocalizationStorageImpl;
use protosdc::provider::sco::{DummyOperationInvocationReceiver, OperationInvocationReceiver};

pub type DiscoveryProvider = DpwsDiscoveryProviderImpl<
    UdpBindingImpl,
    DpwsDiscoveryProcessorImpl,
    DpwsDiscoveryProxyProviderImpl<
        SoapClientImpl<ReqwestClientAccess, ReplacementExtensionHandlerFactory>,
    >,
>;

pub async fn create_device_generic<T>(
    receiver: T,
    adapter_address: IpAddr,
    crypto_config: Option<CryptoConfig>,
    localization: Option<LocalizationStorageImpl>,
) -> Result<
    SdcDevice<
        DiscoveryProvider,
        DefaultLocalMdibAccessImpl,
        T,
        HostingServiceImpl,
        TypesScopesPlugin,
    >,
>
where
    T: 'static + OperationInvocationReceiver<DefaultLocalMdibAccessImpl> + Sync + Send + Clone,
{
    let device_settings = DeviceSettings::builder()
        .network_interface(SocketAddr::from((adapter_address, 0)))
        .crypto(crypto_config.clone())
        .endpoint_reference(
            WsaEndpointReference::builder()
                .address(generate_provider_epr_uuid())
                .build(),
        )
        .types(HashSet::from_iter(vec![
            MDPWS_MEDICAL_DEVICE_TYPE.into(),
            DPWS_DEVICE_TYPE.into(),
        ]))
        .extension_handler_factory_default()
        .build();

    let mdib_data = mdib_modification_presets();

    let scopes_updater = TypesScopesPlugin::default();

    let mut device = SdcDevice::new(receiver, device_settings, scopes_updater, localization)
        .await
        .expect("Could not initialize provider");

    device.local_mdib.write_description(mdib_data).await?;

    Ok(device)
}

pub async fn create_device(
    adapter_address: IpAddr,
    crypto_config: Option<CryptoConfig>,
    localization: Option<LocalizationStorageImpl>,
) -> Result<
    SdcDevice<
        DiscoveryProvider,
        DefaultLocalMdibAccessImpl,
        DummyOperationInvocationReceiver,
        HostingServiceImpl,
        TypesScopesPlugin,
    >,
> {
    create_device_generic(
        DummyOperationInvocationReceiver {},
        adapter_address,
        crypto_config,
        localization,
    )
    .await
}

pub async fn connect_consumer(
    adapter_address: IpAddr,
    endpoint: WsaEndpointReference,
    crypto_config: Option<CryptoConfig>,
) -> Result<
    SdcRemoteDevice<
        DefaultRemoteMdibAccessImpl,
        ConsumerHostingService<ReplacementExtensionHandlerFactory>,
        ConsumerHostedService<ReplacementExtensionHandlerFactory>,
        ReqwestSoapClient<ReplacementExtensionHandlerFactory>,
        ReqwestSoapClient<ReplacementExtensionHandlerFactory>,
        ScoController<DpwsSetServiceHandler<ReqwestSoapClient<ReplacementExtensionHandlerFactory>>>,
        ScoTransactionImpl,
        ReplacementExtensionHandlerFactory,
    >,
> {
    debug!("Connecting consumer");

    let udp_binding = create_udp_binding(adapter_address, Some(WS_DISCOVERY_PORT)).await?;
    let discovery_consumer = DpwsDiscoveryConsumerImpl::new(udp_binding);

    let http_client = ReqwestHttpClient::new(crypto_config.clone())?;
    let registry = AxumRegistry::new(crypto_config);

    let consumer = ConsumerImpl::new(
        discovery_consumer,
        http_client.get_access(),
        registry.access(),
        ReplacementExtensionHandlerFactory,
    );

    let hosting_service = consumer
        .connect(endpoint.address.value.as_str(), None)
        .await?;

    info!("{:?}", hosting_service.this_model());
    info!("{:?}", hosting_service.types());

    let connect_configuration = ConnectConfiguration::new(
        ALL_EPISODIC_AND_WAVEFORM_REPORTS
            .iter()
            .map(|it| it.to_string())
            .collect(),
        Vec::with_capacity(0),
    )?;

    let consumer_config = ConsumerConfigBuilder::default()
        .default_extension_handler()
        .build();

    #[allow(clippy::type_complexity)] // just test code, but indeed quite complex
    // subscribe to episodic reports, find the correct hosted service first
    let remote_device: SdcRemoteDevice<
        DefaultRemoteMdibAccessImpl,
        ConsumerHostingService<ReplacementExtensionHandlerFactory>,
        ConsumerHostedService<ReplacementExtensionHandlerFactory>,
        ReqwestSoapClient<ReplacementExtensionHandlerFactory>,
        ReqwestSoapClient<ReplacementExtensionHandlerFactory>,
        ScoController<DpwsSetServiceHandler<ReqwestSoapClient<ReplacementExtensionHandlerFactory>>>,
        ScoTransactionImpl,
        ReplacementExtensionHandlerFactory,
    > = sdc_remote_device_connector::connect::<
        _,
        _,
        _,
        _,
        ScoController<DpwsSetServiceHandler<ReqwestSoapClient<ReplacementExtensionHandlerFactory>>>,
        ScoTransactionImpl,
        ReplacementExtensionHandlerFactory,
    >(hosting_service, connect_configuration, consumer_config)
    .await?;

    let mdib_version = remote_device.remote_mdib.mdib_version().await;
    debug!("Remote Device Mdib Version: {:?}", mdib_version);

    Ok(remote_device)
}
