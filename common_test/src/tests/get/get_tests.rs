use biceps::common::mdib_entity::{Entity, EntityBase, MdibEntity};
use biceps::consumer::access::remote_mdib_access::RemoteMdibAccess;
use protosdc_biceps::biceps::{
    GetContextStatesResponse, GetMdDescriptionResponse, GetMdStateResponse, GetMdibResponse,
};

pub async fn test_get_service<REM>(
    all_entities: &[MdibEntity],
    remote_mdib: &REM,
    get_mdib_response: &GetMdibResponse,
    get_md_description: &GetMdDescriptionResponse,
    get_md_state: &GetMdStateResponse,
    get_context_states: &GetContextStatesResponse,
) -> anyhow::Result<()>
where
    REM: RemoteMdibAccess,
{
    assert!(!all_entities.is_empty());

    for entity in all_entities {
        assert!(remote_mdib.entity(&entity.entity.handle()).await.is_some())
    }

    let num_mds = all_entities
        .iter()
        .filter(|it| matches!(it.entity, Entity::Mds(..)))
        .count();
    assert!(num_mds > 0);

    let num_states = all_entities
        .iter()
        .flat_map(|it| it.resolve_states())
        .count();
    assert!(num_states > 0);

    let num_context_states = all_entities
        .iter()
        .flat_map(|it| it.resolve_context_states())
        .count();
    assert!(num_context_states > 0);

    // check get mdib works
    {
        let md_description = get_mdib_response
            .mdib
            .md_description
            .as_ref()
            .expect("No MdDescription present");
        assert_eq!(num_mds, md_description.mds.len());
        let md_state = get_mdib_response
            .mdib
            .md_state
            .as_ref()
            .expect("No MdState present");
        assert_eq!(num_states, md_state.state.len());
    }
    // check get md description works
    {
        assert_eq!(num_mds, get_md_description.md_description.mds.len());
    }
    // check get md state works
    {
        assert_eq!(num_states, get_md_state.md_state.state.len());
    }
    // check get context states
    {
        assert_eq!(num_context_states, get_context_states.context_state.len());
    }

    Ok(())
}
