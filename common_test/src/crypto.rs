use anyhow::Result;
use rcgen::{BasicConstraints, Certificate, DnType, ExtendedKeyUsagePurpose, IsCa, RcgenError};

use common::crypto::CryptoConfig;

fn generate_ca_cert() -> Result<Certificate, RcgenError> {
    let mut params = rcgen::CertificateParams::new(vec!["protosdc-rs-root".to_string()]);
    params.is_ca = IsCa::Ca(BasicConstraints::Constrained(1));

    Certificate::from_params(params)
}

// PEM key pair
pub struct KeyPair {
    pub public: Vec<u8>,
    pub private: Vec<u8>,

    pub certificate: Certificate,
    pub subject: String,
}

pub struct GeneratedCerts {
    pub ca_public: Vec<u8>,
    pub consumer: KeyPair,
    pub provider: KeyPair,
}

impl GeneratedCerts {
    pub fn provider_crypto_config(&self) -> CryptoConfig {
        CryptoConfig::new_from_memory(
            self.provider.private.clone(),
            self.provider.public.clone(),
            self.ca_public.clone(),
            None,
        )
    }

    pub fn consumer_crypto_config(&self) -> CryptoConfig {
        CryptoConfig::new_from_memory(
            self.consumer.private.clone(),
            self.consumer.public.clone(),
            self.ca_public.clone(),
            None,
        )
    }
}

fn generate_certs(dpws: bool) -> Result<GeneratedCerts, RcgenError> {
    let ca_cert = generate_ca_cert()?;

    let consumer = {
        let subject = "protosdc-rs-consumer";
        let mut consumer_params = rcgen::CertificateParams::new(vec![subject.to_string()]);
        consumer_params
            .extended_key_usages
            .push(ExtendedKeyUsagePurpose::ClientAuth);
        if dpws {
            consumer_params
                .extended_key_usages
                .push(ExtendedKeyUsagePurpose::ServerAuth);
        }
        consumer_params
            .distinguished_name
            .push(DnType::CommonName, subject.to_string());

        let cert = Certificate::from_params(consumer_params)?;

        // sign
        KeyPair {
            public: cert.serialize_pem_with_signer(&ca_cert)?.into_bytes(),
            private: cert.serialize_private_key_pem().into_bytes(),
            certificate: cert,
            subject: subject.to_string(),
        }
    };

    let provider = {
        let subject = "protosdc-rs-provider";
        let mut provider_params = rcgen::CertificateParams::new(vec![subject.to_string()]);
        provider_params
            .extended_key_usages
            .push(ExtendedKeyUsagePurpose::ServerAuth);
        if dpws {
            provider_params
                .extended_key_usages
                .push(ExtendedKeyUsagePurpose::ClientAuth);
        }
        provider_params
            .distinguished_name
            .push(DnType::CommonName, subject.to_string());

        let cert = Certificate::from_params(provider_params)?;

        // sign
        KeyPair {
            public: cert.serialize_pem_with_signer(&ca_cert)?.into_bytes(),
            private: cert.serialize_private_key_pem().into_bytes(),
            certificate: cert,
            subject: subject.to_string(),
        }
    };

    Ok(GeneratedCerts {
        ca_public: ca_cert.serialize_pem()?.into_bytes(),
        consumer,
        provider,
    })
}

pub fn generate_certificates() -> Result<GeneratedCerts, RcgenError> {
    generate_certs(false)
}

pub fn generate_dpws_certificates() -> Result<GeneratedCerts, RcgenError> {
    generate_certs(true)
}
