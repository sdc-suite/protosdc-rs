#[cfg(target_os = "linux")]
mod unix;

#[cfg(target_os = "linux")]
pub use unix::*;

#[cfg(target_os = "macos")]
mod unix;

#[cfg(target_os = "macos")]
pub use unix::*;

#[cfg(target_os = "windows")]
mod windows;

#[cfg(target_os = "windows")]
pub use windows::*;
