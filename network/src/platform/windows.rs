use crate::common::{compare_address, ip_address_to_socket_address};
use crate::udp_binding::UdpBindingError;
use log::debug;
use socket2::Socket;
use std::net::{IpAddr, Ipv4Addr, Ipv6Addr, SocketAddr, SocketAddrV4, SocketAddrV6};

/// Windows binds the socket to `INADDR_ANY`/`in6addr_any` and filters through the join later on.
///
/// see https://msdn.microsoft.com/en-us/library/windows/desktop/ms737550(v=vs.85).aspx
pub fn bind_multicast(socket: &Socket, addr: &SocketAddr) -> Result<(), UdpBindingError> {
    debug!("(Windows) binding {:?} to multicast {:?}", socket, addr);
    let addr = match *addr {
        SocketAddr::V4(addr) => SocketAddr::new(Ipv4Addr::new(0, 0, 0, 0).into(), addr.port()),
        SocketAddr::V6(addr) => {
            SocketAddr::new(Ipv6Addr::new(0, 0, 0, 0, 0, 0, 0, 0).into(), addr.port())
        }
    };
    socket
        .bind(&socket2::SockAddr::from(addr))
        .map_err(|source| UdpBindingError::MulticastBindFailed {
            source,
            address: addr.to_string(),
        })?;
    Ok(())
}

pub fn get_socket_addr_for_ip(adapter_address: IpAddr) -> Option<SocketAddr> {
    let interfaces = ipconfig::get_adapters().expect("Could not retrieve interfaces");
    let adapter_socket_addr: SocketAddr = ip_address_to_socket_address(adapter_address, None);

    // retrieve the SocketAddr from the adapter since we need the correct scope_id
    let mut selected_adapter_socket_addr_opt = None;
    for adapter in interfaces {
        debug!("Checking interface");
        debug!("  name: {:?}", adapter.adapter_name());
        debug!("  friendly name: {:?}", adapter.friendly_name());
        debug!("  addresses: {:?}", adapter.ip_addresses());
        debug!("  scope_id: {:?}", adapter.ipv6_if_index());

        for address in adapter.ip_addresses() {
            let socket_addr = ip_addr_to_socket_addr(address, adapter.ipv6_if_index());
            if compare_address(Some(socket_addr), &adapter_socket_addr) {
                debug!(
                    "It's a match! Interface {:?} chosen, SocketAddr {:?}",
                    &adapter.adapter_name(),
                    socket_addr
                );
                selected_adapter_socket_addr_opt = Some(socket_addr);
                break;
            }
        }
    }

    selected_adapter_socket_addr_opt
}

fn ip_addr_to_socket_addr(ip: &IpAddr, scope_id: u32) -> SocketAddr {
    match ip {
        IpAddr::V4(addr) => SocketAddr::V4(SocketAddrV4::new(*addr, 0)),
        IpAddr::V6(addr) => SocketAddr::V6(SocketAddrV6::new(*addr, 0, 0, scope_id)),
    }
}

#[cfg(any(feature = "test-utilities", test))]
pub mod test_util {
    use ipconfig::OperStatus;
    use std::net::IpAddr;

    use crate::udp_binding::test_util::test_adapter_address_working_multicast;
    use crate::udp_binding::PROTOSDC_MULTICAST_PORT;
    use log::debug;

    /// Returns the address of an adapter which can be used to send to the provided multicast
    /// address without OS errors.
    ///
    /// Note: Since this is test code, instead of returning an error, this will just panic if no
    ///       adapters are available.
    ///
    /// # Arguments
    ///
    /// * `multicast_address`: to get appropriate adapter for
    ///
    /// returns: IpAddr address of the adapter which can be used for multicast to the address.
    pub async fn determine_adapter_address_for_multicast(multicast_address: IpAddr) -> IpAddr {
        let mut adapters = ipconfig::get_adapters().expect("Could not retrieve adapters");

        // filter out interfaces without connection
        adapters = adapters
            .into_iter()
            .filter(|it| it.oper_status() == OperStatus::IfOperStatusUp)
            .collect();

        for adapter in adapters {
            for address in adapter.ip_addresses() {
                match (address, multicast_address) {
                    (IpAddr::V6(addr), IpAddr::V6(_)) => {
                        if test_adapter_address_working_multicast(
                            address.clone(),
                            multicast_address,
                            adapter.ipv6_if_index(),
                            PROTOSDC_MULTICAST_PORT,
                        )
                        .await
                        {
                            return IpAddr::V6(*addr);
                        }
                    }
                    (IpAddr::V4(addr), IpAddr::V4(_)) => {
                        if test_adapter_address_working_multicast(
                            address.clone(),
                            multicast_address,
                            0,
                            PROTOSDC_MULTICAST_PORT,
                        )
                        .await
                        {
                            return IpAddr::V4(*addr);
                        }
                    }
                    // nothing to do there
                    (_, _) => {
                        debug!("Discarding address {:?}", address)
                    }
                }
            }
        }

        panic!(
            "Could not find matching network interface for multicast address {:?}",
            &multicast_address
        );
    }
}
