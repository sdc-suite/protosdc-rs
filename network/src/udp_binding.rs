use std::fmt;
use std::fmt::Formatter;
use std::net::{IpAddr, Ipv4Addr, Ipv6Addr, SocketAddr, SocketAddrV4, SocketAddrV6, UdpSocket};
use std::sync::Arc;
use std::time::Duration;

use async_trait::async_trait;
use bytes::Bytes;
use futures_util::sink::SinkExt;
use futures_util::stream::{SplitSink, SplitStream, StreamExt};
use log::{debug, error, info, warn};
use socket2::{Domain, Protocol, SockAddr, Socket, Type};
use thiserror::Error;
use tokio::net::UdpSocket as TokioUdpSocket;
use tokio::sync::broadcast::{
    channel as broadcast_channel, Receiver as BroadcastReceiver, Sender as BroadcastSender,
};
use tokio::sync::mpsc::{channel, Receiver, Sender};
use tokio::sync::oneshot::Sender as OneShotSender;
use tokio::sync::Mutex;
use tokio::task;
use tokio_util::codec::BytesCodec;
use tokio_util::sync::{CancellationToken, DropGuard};
use tokio_util::udp::UdpFramed;

use crate::platform::{bind_multicast, get_socket_addr_for_ip};

pub const MULTICAST_MAX_SIZE: usize = 4096;
pub const IPV4_MULTICAST_ADDRESS: Ipv4Addr = Ipv4Addr::new(239, 255, 255, 250);
pub const IPV6_MULTICAST_ADDRESS: Ipv6Addr = Ipv6Addr::new(0xFF02, 0, 0, 0, 0, 0, 0, 0xC);
pub const PROTOSDC_MULTICAST_PORT: u16 = 6464;

pub const CHANNEL_SIZE: usize = 100;

type UdpSink = SplitSink<UdpFramed<BytesCodec>, (Bytes, SocketAddr)>;

#[derive(Debug, Error)]
pub enum UdpBindingError {
    #[error("Could not retrieve available adapters: {source:?}")]
    AdapterError { source: anyhow::Error },

    #[error("Could not find matching adapter for {address:?}")]
    UnknownAdapterAddress { address: IpAddr },

    #[error("Binding to multicast address {address:?} failed")]
    MulticastBindFailed {
        source: std::io::Error,
        address: String,
    },

    #[error("Binding cannot be reconfigured when it is already running")]
    BindingAlreadyRunning,

    /// Represents all other cases of `std::io::Error`.
    #[error(transparent)]
    IOError(#[from] std::io::Error),

    #[error(transparent)]
    OneShotError(#[from] tokio::sync::oneshot::error::RecvError),
}

#[derive(Debug)]
pub enum UdpMessage {
    OutboundMulticast {
        message: OutboundUdpMessage,
        response: Option<OneShotSender<Result<(), UdpBindingError>>>,
    },
    OutboundUnicast {
        message: OutboundUdpMessage,
        response: Option<OneShotSender<Result<(), UdpBindingError>>>,
    },
}

fn get_ipv4_multicast_address(port: u16) -> SocketAddr {
    SocketAddr::from(SocketAddrV4::new(IPV4_MULTICAST_ADDRESS, port))
}

fn get_ipv6_multicast_address(adapter_index: u32, port: u16) -> SocketAddr {
    SocketAddr::from(SocketAddrV6::new(
        IPV6_MULTICAST_ADDRESS,
        port,
        0,
        adapter_index,
    ))
}

// this will be common for all our sockets
pub(crate) fn new_socket(address: &SocketAddr) -> Result<Socket, UdpBindingError> {
    let domain = if address.is_ipv4() {
        Domain::IPV4
    } else {
        Domain::IPV6
    };

    debug!("Creating new {:?} socket for {:?}", domain, address);

    let socket =
        Socket::new(domain, Type::DGRAM, Some(Protocol::UDP)).map_err(UdpBindingError::IOError)?;

    // TokioUdpSocket requires nonblocking socket, but doesn't check for it. See docs.
    socket.set_nonblocking(true)?;
    socket.set_reuse_address(true)?;

    #[cfg(unix)] // this is currently restricted to Unix's in socket2
    socket.set_reuse_port(true)?;

    Ok(socket)
}

async fn join_multicast(
    multicast_addr: SocketAddr,
    adapter_addr: SocketAddr,
    adapter_index: u32,
) -> Result<UdpSocket, UdpBindingError> {
    let ip_addr = multicast_addr.ip();

    let socket = new_socket(&adapter_addr)?;

    // depending on the IP protocol we have slightly different work
    match ip_addr {
        IpAddr::V4(ref mdns_v4) => {
            // join to the multicast address, with all interfaces
            let adapter_addr = match adapter_addr {
                SocketAddr::V4(v4) => v4,
                SocketAddr::V6(_) => panic!("No"),
            };

            info!("Joining {:?} to {:?}", adapter_addr.ip(), mdns_v4);
            socket.join_multicast_v4(mdns_v4, adapter_addr.ip())?;
        }
        IpAddr::V6(ref mdns_v6) => {
            // join to the multicast address, with all interfaces (ipv6 uses indexes not addresses)
            info!(
                "Joining {:?} ({}) to {:?}",
                adapter_addr.ip(),
                adapter_index,
                mdns_v6
            );
            socket.join_multicast_v6(mdns_v6, adapter_index)?;
            socket.set_only_v6(true)?;
        }
    };

    // bind us to the socket address.
    bind_multicast(&socket, &multicast_addr)?;

    // arbitrary sleep to allow for igmp join to work
    debug!("Doing arbitrary sleep for igmp join");
    tokio::time::sleep(Duration::from_millis(2000)).await;
    debug!("Done waiting");

    // convert to standard sockets
    Ok(socket.into())
}

/// Creates a new `UdpBindingImpl` to use for multicast and unicast discovery
///
/// # Arguments
///
/// * `adapter_address`: to bind discovery to
/// * `discovery_port`: to use for multicast, defaults to `PROTOSDC_MULTICAST_PORT` if None
///
/// returns: Result<UdpBindingImpl, UdpBindingError>
pub async fn create_udp_binding(
    adapter_address: IpAddr,
    discovery_port: Option<u16>,
) -> Result<UdpBindingImpl, UdpBindingError> {
    let actual_port = discovery_port.unwrap_or(PROTOSDC_MULTICAST_PORT);

    info!(
        "Creating UDP binding for {:?}:{}",
        &adapter_address, actual_port
    );
    // determine adapter to use
    let selected_adapter_socket_addr_opt = get_socket_addr_for_ip(adapter_address);

    let selected_adapter_socket_addr =
        selected_adapter_socket_addr_opt.ok_or(UdpBindingError::UnknownAdapterAddress {
            address: adapter_address,
        })?;

    let adapter_index = match selected_adapter_socket_addr {
        SocketAddr::V4(_) => 0,
        SocketAddr::V6(sock) => sock.scope_id(),
    };

    // select appropriate multicast address based on the adapter we received
    let (ip_addr, multicast_address) =
        get_multicast_address_from_address(adapter_address, adapter_index, actual_port).await;

    let multicast_socket = join_multicast(multicast_address, ip_addr, adapter_index).await?;

    // create unicast socket
    let unicast_adapter_addr = match selected_adapter_socket_addr {
        SocketAddr::V4(_) => selected_adapter_socket_addr,
        SocketAddr::V6(mut addr) => {
            addr.set_scope_id(adapter_index);
            SocketAddr::V6(addr)
        }
    };

    let unicast_socket = new_socket(&selected_adapter_socket_addr)?;
    let unicast_sock_addr = &SockAddr::from(unicast_adapter_addr);
    info!("Binding UDP Socket to {:?}", &unicast_sock_addr);
    unicast_socket.bind(unicast_sock_addr)?;

    let unicast_socket_addr = unicast_socket
        .local_addr()
        .map_err(UdpBindingError::IOError)?
        .as_socket()
        .unwrap();
    info!("Bound unicast socket to {:?}", &unicast_socket_addr);

    info!("Created UDP binding for {:?}", &adapter_address);

    debug!("Created UDP binding for {:?}:", &adapter_address);
    debug!("Unicast Address: {:?}", &unicast_socket_addr);
    debug!("Multicast Address: {:?}", &multicast_address);
    debug!("Selected SocketAddr: {:?}", &selected_adapter_socket_addr);

    Ok(UdpBindingImpl::new(
        unicast_socket.into(),
        unicast_socket_addr,
        multicast_socket,
        multicast_address,
    ))
}

/// Selects appropriate adapter and multicast address based on the input adapter address
///
/// # Arguments
///
/// * `adapter_address`: address of the adapter to use for multicast
/// * `adapter_index`: index of the adapter to use for multicast, needed for IPv6 scopes
///
/// returns: (SocketAddr, SocketAddr)
///  1. parameter is the adapter address with the multicast port and correct scope
///  2. parameter is the multicast address with correct scope
pub(crate) async fn get_multicast_address_from_address(
    adapter_address: IpAddr,
    adapter_index: u32,
    port: u16,
) -> (SocketAddr, SocketAddr) {
    match adapter_address {
        IpAddr::V4(ip_addr) => (
            SocketAddr::from(SocketAddrV4::new(ip_addr, port)),
            get_ipv4_multicast_address(port),
        ),
        IpAddr::V6(ip_addr) => (
            SocketAddr::from(SocketAddrV6::new(ip_addr, port, 0, adapter_index)),
            get_ipv6_multicast_address(adapter_index, port),
        ),
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct UdpHeader {
    pub source_port: u16,
    pub destination_port: u16,
    pub source_address: IpAddr,
    pub destination_address: IpAddr,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct OutboundUdpMessage {
    pub data: Vec<u8>,
    pub address: SocketAddr,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct InboundUdpMessage {
    pub data: Vec<u8>,
    pub udp_header: UdpHeader,
}

/// Trait which describes an asynchronous UDP binding.
///
/// Inbound messages are available through a broadcast receiver, while sending messages can be done
/// through a message channel or through provided methods. The channel allows a "fire-and-forget"
/// approach, but can also provide return values through one-shot responses.
#[async_trait]
pub trait UdpBinding: Send + Sync {
    /// Subscribes to inbound UDP messages.
    fn subscribe(&self) -> BroadcastReceiver<InboundUdpMessage>;

    /// Returns a sender which can be used to send UDP messages via message passing.
    fn get_message_channel_sender(&self) -> Sender<UdpMessage>;

    /// Sends a message to the multicast address this binding is assigned to.
    ///
    /// # Arguments
    ///
    /// * `udp_message`: A message to be sent to the assigned multicast address,
    ///                  the message in the address will be ignored
    ///
    /// returns: Result<(), UdpBindingError>
    async fn send_message_multicast(
        &mut self,
        udp_message: OutboundUdpMessage,
    ) -> Result<(), UdpBindingError>;

    /// Sends a unicast UDP message.
    ///
    /// # Arguments
    ///
    /// * `udp_message`: A message to be sent.
    ///
    /// returns: Result<(), UdpBindingError>
    async fn send_message_unicast(
        &mut self,
        udp_message: OutboundUdpMessage,
    ) -> Result<(), UdpBindingError>;
}

#[derive(Debug)]
struct UdpBindingImplInternal {
    stream: SplitStream<UdpFramed<BytesCodec>>,
    sender: BroadcastSender<InboundUdpMessage>,
}

#[derive(Debug)]
struct UdpBindingImplTasks {
    _task_drop_guard: DropGuard,
    _unicast_background_task: task::JoinHandle<()>,
    _multicast_background_task: task::JoinHandle<()>,
    _message_channel_task: task::JoinHandle<()>,
}

#[derive(Clone, Debug)]
pub struct UdpBindingImpl {
    unicast_sink: Arc<Mutex<UdpSink>>,
    _multicast_sink: Arc<Mutex<UdpSink>>,

    unicast_addr: SocketAddr,
    multicast_addr: SocketAddr,

    udp_sender: BroadcastSender<InboundUdpMessage>,

    _tasks: Arc<Mutex<UdpBindingImplTasks>>,

    message_channel_sender: Sender<UdpMessage>,
}

impl fmt::Display for UdpBindingImpl {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "UdpBindingImpl (unicast socket {:?}, multicast socket {:?})",
            self.unicast_addr, self.multicast_addr
        )
    }
}

impl UdpBindingImpl {
    pub fn new(
        unicast_socket: UdpSocket,
        unicast_addr: SocketAddr,
        multicast_socket: UdpSocket,
        multicast_addr: SocketAddr,
    ) -> UdpBindingImpl {
        let unicast_tokio_socket = TokioUdpSocket::from_std(unicast_socket)
            .expect("Could not convert unicast UdpSocket to tokio::UdpSocket");
        let unicast_framed = UdpFramed::new(unicast_tokio_socket, BytesCodec::new());
        let (unicast_sink, unicast_stream) = unicast_framed.split();

        let multicast_tokio_socket = TokioUdpSocket::from_std(multicast_socket)
            .expect("Could not convert multicast UdpSocket to tokio::UdpSocket");
        let multicast_framed = UdpFramed::new(multicast_tokio_socket, BytesCodec::new());
        let (multicast_sink, multicast_stream) = multicast_framed.split();

        let (message_sender, message_receiver) = channel(CHANNEL_SIZE);
        let (udp_sender, _) = broadcast_channel(CHANNEL_SIZE);

        let multicast_data = UdpBindingImplInternal {
            stream: multicast_stream,
            sender: udp_sender.clone(),
        };

        let unicast_data = UdpBindingImplInternal {
            stream: unicast_stream,
            sender: udp_sender.clone(),
        };

        let arc_unicast_sink = Arc::new(Mutex::new(unicast_sink));

        let task_cancellation_token = CancellationToken::new();

        let message_channel_task = tokio::spawn(Self::_process_messages(
            message_receiver,
            arc_unicast_sink.clone(),
            multicast_addr,
            task_cancellation_token.clone(),
        ));

        UdpBindingImpl {
            unicast_sink: arc_unicast_sink,
            _multicast_sink: Arc::new(Mutex::new(multicast_sink)),
            unicast_addr,
            multicast_addr,
            udp_sender,

            _tasks: Arc::new(Mutex::new(UdpBindingImplTasks {
                _unicast_background_task: task::spawn(Self::_process_stream(
                    unicast_data,
                    unicast_addr,
                    task_cancellation_token.clone(),
                )),
                _multicast_background_task: task::spawn(Self::_process_stream(
                    multicast_data,
                    multicast_addr,
                    task_cancellation_token.clone(),
                )),

                _task_drop_guard: task_cancellation_token.drop_guard(),

                _message_channel_task: message_channel_task,
            })),

            message_channel_sender: message_sender,
        }
    }

    async fn _process_stream(
        mut internal: UdpBindingImplInternal,
        local_address: SocketAddr,
        cancellation_token: CancellationToken,
    ) {
        let sender = &internal.sender;
        loop {
            tokio::select! {
                _ = cancellation_token.cancelled() => {
                    debug!("_process_stream cancelled");
                    return
                }
                msg = internal.stream.next() => {
                    match msg {
                        None => {
                            debug!("_process_stream received none element, exiting");
                            return
                        }
                        Some(item) => {
                            match item {
                                Ok((n, addr)) => {
                                    if addr != local_address {
                                        // udp.send_to(&buf[..count], &src).await?;
                                        debug!("{:?} bytes message from {:?}", n, addr);
                                        // try printing it as a string
                                        let payload = n.to_vec();

                                        let msg = InboundUdpMessage {
                                            data: payload,
                                            udp_header: UdpHeader {
                                                source_port: addr.port(),
                                                destination_port: local_address.port(),
                                                source_address: addr.ip(),
                                                destination_address: local_address.ip(),
                                            },
                                        };

                                        match sender.send(msg) {
                                            Ok(_) => {}
                                            Err(err) => {
                                                debug!("Could not send received UDP message via channel, no receivers {:?}", err)
                                            }
                                        }
                                    }
                                }
                                Err(e) => {
                                    warn!("Error receiving incoming UDP message: {:?}", e);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    async fn _process_messages(
        mut receiver: Receiver<UdpMessage>,
        sink: Arc<Mutex<UdpSink>>,
        multicast_address: SocketAddr,
        cancellation_token: CancellationToken,
    ) {
        loop {
            tokio::select! {
                _ = cancellation_token.cancelled() => {
                    receiver.close();
                    debug!("_process_messages cancelled");
                    return
                }
                msg = receiver.recv() => {
                    match msg {
                        None => {
                            debug!("_process_messages received none element, exiting");
                            return
                        },
                        Some(message) => {
                            match message {
                                UdpMessage::OutboundMulticast { message, response } => {
                                    let mut s = sink.lock().await;
                                    let result = send_message_multicast(&mut s, message, multicast_address).await;
                                    match response {
                                        None => {}
                                        // don't care about result, if the receiver was dropped, the caller doesn't care
                                        #[allow(unused_must_use)]
                                        Some(sender) => {
                                            sender.send(result);
                                        }
                                    }
                                }
                                UdpMessage::OutboundUnicast { message, response } => {
                                    let mut s = sink.lock().await;
                                    let result = send_message_unicast(&mut s, message).await;
                                    match response {
                                        None => {}
                                        // don't care about result, if the receiver was dropped, the caller doesn't care
                                        #[allow(unused_must_use)]
                                        Some(sender) => {
                                            sender.send(result);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

pub(crate) async fn send_message_multicast(
    sink: &mut UdpSink,
    udp_message: OutboundUdpMessage,
    multicast_address: SocketAddr,
) -> Result<(), UdpBindingError> {
    sink.send((Bytes::from(udp_message.data), multicast_address))
        .await
        .map_err(|source| source.into())
}

pub(crate) async fn send_message_unicast(
    sink: &mut UdpSink,
    udp_message: OutboundUdpMessage,
) -> Result<(), UdpBindingError> {
    sink.send((Bytes::from(udp_message.data), udp_message.address))
        .await
        .map_err(|source| source.into())
}

#[async_trait]
impl UdpBinding for UdpBindingImpl {
    fn subscribe(&self) -> BroadcastReceiver<InboundUdpMessage> {
        self.udp_sender.subscribe()
    }

    fn get_message_channel_sender(&self) -> Sender<UdpMessage> {
        self.message_channel_sender.clone()
    }

    async fn send_message_multicast(
        &mut self,
        udp_message: OutboundUdpMessage,
    ) -> Result<(), UdpBindingError> {
        let mut sink = self.unicast_sink.lock().await;
        send_message_multicast(&mut sink, udp_message, self.multicast_addr).await
    }

    async fn send_message_unicast(
        &mut self,
        udp_message: OutboundUdpMessage,
    ) -> Result<(), UdpBindingError> {
        let mut sink = self.unicast_sink.lock().await;
        send_message_unicast(&mut sink, udp_message).await
    }
}

#[cfg(any(feature = "test-utilities", test))]
pub mod test_util {
    use std::net::{IpAddr, UdpSocket};

    use crate::common::ip_address_to_socket_address;
    use log::{error, info};
    use socket2::SockAddr;

    use crate::udp_binding::{get_multicast_address_from_address, join_multicast, new_socket};

    /// Tests whether a provided adapter is able to send multicast messages without OS errors.
    ///
    /// # Arguments
    ///
    /// * `adapter_address`: address of the adapter to test
    /// * `multicast_address`: multicast address to send to
    /// * `adapter`: adapter to test
    ///
    /// returns: bool
    pub(crate) async fn test_adapter_address_working_multicast(
        adapter_address: IpAddr,
        _multicast_address: IpAddr,
        adapter_idx: u32,
        port: u16,
    ) -> bool {
        let (ip_addr, actual_multicast_address) =
            get_multicast_address_from_address(adapter_address, adapter_idx, port).await;
        let unicast_adapter_addr = ip_address_to_socket_address(adapter_address, Some(adapter_idx));

        let sock = new_socket(&unicast_adapter_addr)
            .unwrap_or_else(|_| panic!("Could not create socket for {:?}", &unicast_adapter_addr));

        let unicast_sock_addr = &SockAddr::from(unicast_adapter_addr);
        info!("Testing multicast on UDP socket on address {:?}", &ip_addr);
        match sock.bind(unicast_sock_addr) {
            Ok(_) => {}
            Err(err) => {
                error!("Could not bind unicast socket: {:?}", err);
                return false;
            }
        }

        let unicast_socket: UdpSocket = sock.into();
        let unicast_socket_addr = match unicast_socket.local_addr() {
            Ok(it) => it,
            Err(err) => {
                error!("Could not retrieve local address: {:?}", err);
                return false;
            }
        };

        let test_data = "protosdc-rs-socket-test-payload".as_bytes();

        let sent = unicast_socket.send_to(test_data, actual_multicast_address);

        info!(
            "Sent test data using adapter {:?} to address {:?} passed: {:?}",
            &unicast_socket_addr,
            actual_multicast_address,
            sent.is_ok()
        );

        let sent_ok = sent.map_err(|err| error!("{:?}", &err)).is_ok();

        // try joining multicast as well
        let join_ok = join_multicast(actual_multicast_address, ip_addr, adapter_idx)
            .await
            .map_err(|err| error!("{:?}", &err))
            .is_ok();

        sent_ok && join_ok
    }
}

#[cfg(test)]
pub mod tests {
    use std::net::{IpAddr, SocketAddr};
    use std::time::Duration;

    use anyhow::Result;
    use log::{error, info};
    use tokio::{task, time};
    use tokio_util::sync::CancellationToken;
    use uuid::Uuid;

    use crate::platform;

    use crate::udp_binding::{
        create_udp_binding, OutboundUdpMessage, UdpBinding, IPV4_MULTICAST_ADDRESS,
        IPV6_MULTICAST_ADDRESS, PROTOSDC_MULTICAST_PORT,
    };

    const TEST_TIMEOUT: Duration = Duration::from_secs(10);

    fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[tokio::test]
    async fn test_multicast_v4() -> Result<()> {
        init();

        test_multicast(IpAddr::V4(IPV4_MULTICAST_ADDRESS), PROTOSDC_MULTICAST_PORT).await
    }

    #[tokio::test]
    async fn test_multicast_v4_custom_port() -> Result<()> {
        init();

        test_multicast(IpAddr::V4(IPV4_MULTICAST_ADDRESS), 3702).await
    }

    #[tokio::test]
    async fn test_multicast_v6() -> Result<()> {
        init();

        test_multicast(IpAddr::V6(IPV6_MULTICAST_ADDRESS), PROTOSDC_MULTICAST_PORT).await
    }

    #[tokio::test]
    async fn test_unicast_v4() -> Result<()> {
        init();

        test_unicast(IpAddr::V4(IPV4_MULTICAST_ADDRESS), PROTOSDC_MULTICAST_PORT).await
    }

    #[tokio::test]
    async fn test_unicast_v4_custom_port() -> Result<()> {
        init();

        test_unicast(IpAddr::V4(IPV4_MULTICAST_ADDRESS), 3702).await
    }

    #[tokio::test]
    async fn test_unicast_v6() -> Result<()> {
        init();

        test_unicast(IpAddr::V6(IPV6_MULTICAST_ADDRESS), PROTOSDC_MULTICAST_PORT).await
    }

    //noinspection DuplicatedCode
    #[tokio::test]
    async fn test_drop_cancels_tasks() -> Result<()> {
        let multicast_address = IpAddr::V4(IPV4_MULTICAST_ADDRESS);
        let adapter_address =
            platform::test_util::determine_adapter_address_for_multicast(multicast_address).await;
        let first_binding = match create_udp_binding(adapter_address, None).await {
            Ok(socket) => socket,
            Err(e) => {
                error!("Could not create UDP binding: {:?}", e);
                panic!("Could not create UDP binding: {:?}", e)
            }
        };

        let ct = CancellationToken::new();
        let dg = ct.drop_guard();

        let mut tasks = first_binding._tasks.lock().await;

        let old_guard = std::mem::replace(&mut tasks._task_drop_guard, dg);

        // check tasks are still running
        assert!(!tasks._message_channel_task.is_finished());
        assert!(!tasks._multicast_background_task.is_finished());
        assert!(!tasks._unicast_background_task.is_finished());

        // drop the old guard
        drop(old_guard);

        // check tasks are done within timeout
        tokio::time::timeout(TEST_TIMEOUT, async {
            // poll tasks frequently
            let mut period = tokio::time::interval(Duration::from_millis(10));
            loop {
                period.tick().await;
                let all = tasks._message_channel_task.is_finished()
                    && tasks._multicast_background_task.is_finished()
                    && tasks._unicast_background_task.is_finished();
                if all {
                    break;
                }
            }
        })
        .await?;
        assert!(tasks._message_channel_task.is_finished());
        assert!(tasks._multicast_background_task.is_finished());
        assert!(tasks._unicast_background_task.is_finished());

        Ok(())
    }

    //noinspection DuplicatedCode
    async fn test_unicast(multicast_address: IpAddr, port: u16) -> Result<()> {
        let adapter_address =
            platform::test_util::determine_adapter_address_for_multicast(multicast_address).await;

        let request_bytes = Uuid::new_v4().as_bytes().to_vec();

        let mut first_binding = match create_udp_binding(adapter_address, Some(port)).await {
            Ok(socket) => socket,
            Err(e) => {
                error!("Could not create UDP binding: {:?}", e);
                panic!("Could not create UDP binding: {:?}", e)
            }
        };

        let second_binding = match create_udp_binding(adapter_address, Some(port)).await {
            Ok(socket) => socket,
            Err(e) => {
                error!("Could not create UDP binding: {:?}", e);
                panic!("Could not create UDP binding: {:?}", e)
            }
        };

        let udp_message_to_send = OutboundUdpMessage {
            data: request_bytes.clone(),
            address: second_binding.unicast_addr,
        };

        info!("Sending {:?}", udp_message_to_send);

        let _first_receiver = first_binding.subscribe();
        let mut second_receiver = second_binding.subscribe();

        let result_join = task::spawn(async move {
            while let Ok(value) = second_receiver.recv().await {
                info!("Received inbound UDP message: {:?}", &value);
                info!("As string: {:?}", String::from_utf8(value.data.clone()));

                if request_bytes == value.data {
                    return Ok(value);
                }
            }
            Err("No inbound message or whatever")
        });

        info!("Sending unicast message {:?}", udp_message_to_send);
        first_binding
            .send_message_unicast(udp_message_to_send.clone())
            .await
            .expect("Could not send multicast message");

        let result = match time::timeout(Duration::from_secs(5), result_join).await {
            Ok(val) => val.unwrap().unwrap(),
            Err(error) => {
                panic!("No inbound message received, timeout. {:?}", error)
            }
        };

        info!(
            "Received data from {:?} as string: {:?}",
            &result.udp_header,
            std::str::from_utf8(&result.data)
        );
        assert_eq!(udp_message_to_send.data, result.data);

        match result.udp_header.source_address {
            IpAddr::V4(addr) => {
                assert!(!addr.is_multicast())
            }
            IpAddr::V6(addr) => {
                assert!(!addr.is_multicast())
            }
        }

        match result.udp_header.destination_address {
            IpAddr::V4(addr) => {
                assert!(!addr.is_multicast())
            }
            IpAddr::V6(addr) => {
                assert!(!addr.is_multicast())
            }
        }

        Ok(())
    }

    //noinspection DuplicatedCode
    async fn test_multicast(multicast_address: IpAddr, port: u16) -> Result<()> {
        let adapter_address =
            platform::test_util::determine_adapter_address_for_multicast(multicast_address).await;

        info!(
            "Using adapter address {:?} for multicast test",
            &adapter_address
        );

        let message_bytes = Uuid::new_v4().as_bytes().to_vec();

        let udp_message_to_send = OutboundUdpMessage {
            data: message_bytes.clone(),
            // not like this matters
            address: SocketAddr::new(adapter_address, PROTOSDC_MULTICAST_PORT),
        };

        let mut binding = match create_udp_binding(adapter_address, Some(port)).await {
            Ok(socket) => socket,
            Err(e) => {
                error!("Could not create UDP binding: {:?}", e);
                panic!("Could not create UDP binding: {:?}", e)
            }
        };

        let mut receiver = binding.subscribe();

        info!("Spawning result task");
        let result_join = task::spawn(async move {
            info!("Result task running");
            while let Ok(value) = receiver.recv().await {
                info!("Received inbound UDP message: {:?}", &value);
                info!("As string: {:?}", String::from_utf8(value.data.clone()));

                if message_bytes == value.data {
                    return Ok(value);
                }
            }
            Err("No inbound message or whatever")
        });

        binding
            .send_message_multicast(udp_message_to_send.clone())
            .await
            .expect("Could not send multicast message");

        let result = match time::timeout(Duration::from_secs(5), result_join).await {
            Ok(val) => val.unwrap().unwrap(),
            Err(error) => {
                panic!("No inbound message received, timeout. {:?}", error)
            }
        };

        assert_eq!(udp_message_to_send.data, result.data);
        Ok(())
    }
}
