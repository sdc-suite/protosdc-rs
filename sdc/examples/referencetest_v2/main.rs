extern crate core;

use std::io::{BufRead, Write};
use std::path::PathBuf;
use std::str::FromStr;

use anyhow::Result;
use clap::{Parser, Subcommand, ValueEnum};
use log::{info, LevelFilter};
use protosdc_biceps::biceps::{GetMdibResponse, Mdib};
use protosdc_biceps::extension::sdpi::{EquipmentIdentifier, Gender};
use protosdc_biceps::types::AnyContent;
use protosdc_xml::{
    ExtensionHandler, ExtensionParserError, GenericXmlReaderComplexTypeRead, XmlReader,
};
use quick_xml::events::BytesStart;
use quick_xml::name::{Namespace, ResolveResult};
use serde::Deserialize;

use common::crypto::CryptoConfig;
use dpws::common::extension_handling::ExtensionHandlerFactory;
use dpws::find_start_element;
use dpws::xml::messages::biceps_ext::GET_MDIB_RESPONSE_TAG;
use protosdc::provider::localization_storage::{InputText, TextWidth};

mod consumer;
mod provider;

#[derive(Clone, Debug)]
pub struct ReferenceTestExtensionHandlerFactory;

impl ExtensionHandlerFactory for ReferenceTestExtensionHandlerFactory {
    fn create<B: BufRead>(
        &self,
    ) -> Box<dyn ExtensionHandler<B, Box<dyn AnyContent + Send + Sync>> + Send + Sync> {
        Box::new(ReferenceTestExtensionHandler)
    }
}

#[derive(Clone, Debug)]
pub struct ReferenceTestExtensionHandler;

const SDPI_NAMESPACE: &[u8] = b"urn:oid:1.3.6.1.4.1.19376.1.6.2.10.1.1.1";
const SDPI_GENDER_NAME: &str = "Gender";
const SDPI_EQUIPMENT_IDENTIFIER_NAME: &str = "EquipmentIdentifier";

const SDPI_GENDER_TAG: (ResolveResult, &[u8]) = (
    ResolveResult::Bound(Namespace(SDPI_NAMESPACE)),
    SDPI_GENDER_NAME.as_bytes(),
);
const SDPI_EQUIPMENT_IDENTIFIER_TAG: (ResolveResult, &[u8]) = (
    ResolveResult::Bound(Namespace(SDPI_NAMESPACE)),
    SDPI_EQUIPMENT_IDENTIFIER_NAME.as_bytes(),
);
impl<B: BufRead> ExtensionHandler<B, Box<dyn AnyContent + Send + Sync>>
    for ReferenceTestExtensionHandler
{
    fn handle(
        &mut self,
        start: &BytesStart,
        reader: &mut XmlReader<B, Box<dyn AnyContent + Send + Sync>>,
    ) -> std::result::Result<Box<dyn AnyContent + Send + Sync>, ExtensionParserError> {
        let resolved_tag = reader.resolve_element(start.name());

        match (resolved_tag.0, resolved_tag.1.into_inner()) {
            SDPI_GENDER_TAG => Ok(Box::new(Gender::from_xml_complex(
                SDPI_GENDER_TAG,
                start,
                reader,
            )?)),
            SDPI_EQUIPMENT_IDENTIFIER_TAG => Ok(Box::new(EquipmentIdentifier::from_xml_complex(
                SDPI_EQUIPMENT_IDENTIFIER_TAG,
                start,
                reader,
            )?)),
            _ => Err(ExtensionParserError::UnknownElement {
                name: format!("{:?}", start.name()),
            })?,
        }
    }
}

/// protosdc-rs-dpws reference test
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Path to config file to load
    #[clap(short, long)]
    config: Option<String>,

    /// Path to the public key to use
    #[clap(long)]
    public_key_path: Option<String>,

    /// Path to the private key to use
    #[clap(long)]
    private_key_path: Option<String>,

    /// Password to decrypt the private key
    #[clap(long)]
    private_key_password: Option<String>,

    /// Path to the ca certificate
    #[clap(long)]
    ca_cert_path: Option<String>,

    /// IP of the adapter to use
    #[clap(short, long)]
    adapter_ip: Option<String>,

    /// Proxy Url
    #[clap(short, long)]
    proxy_url: Option<String>,

    /// The protocol to use for the participant
    #[arg(value_enum)]
    protocol: Option<Protocol>,

    #[clap(subcommand)]
    command: Commands,
}

#[derive(Subcommand, Debug)]
enum Commands {
    /// Run the protosdc-rs reference provider
    Provider {
        /// Port to use for the provider
        #[clap(short, long)]
        port: Option<u16>,

        /// EndpointReference to use for the provider
        #[clap(short, long)]
        epr: Option<String>,

        /// Path to load the mdib from
        #[clap(long)]
        mdib: Option<String>,

        /// Path to load localized text information from
        #[clap(short, long)]
        localized_texts: Option<String>,
    },
    /// Run the protosdc-rs reference consumer
    Consumer {
        /// Characters the EndpointReference of the provider to connect to ends with
        #[clap(long)]
        epr: Option<String>,
    },
}

pub fn file_exists(path: &str) -> bool {
    PathBuf::from_str(path)
        .unwrap_or_else(|_| panic!("Path {} was not valid", &path))
        .exists()
}

async fn load_mdib_xml_file(path: &str) -> Result<Mdib> {
    assert!(file_exists(path), "mdib path does not exist");
    let mdib_file_content = tokio::fs::read(PathBuf::from_str(path).unwrap()).await?;

    let mut reader = XmlReader::create_custom(
        mdib_file_content.as_ref(),
        Box::new(ReferenceTestExtensionHandler),
    );
    let mut buf = vec![];
    let get_mdib_response: GetMdibResponse =
        find_start_element!(GetMdibResponse, GET_MDIB_RESPONSE_TAG, reader, buf)?;

    Ok(get_mdib_response.mdib)
}

#[derive(Debug, Default, Deserialize)]
struct Config {
    provider: ProviderConfig,
    consumer: ConsumerConfig,
}

#[derive(Debug, Default, Deserialize)]
pub struct ConsumerConfig {
    protocol: Protocol,
    epr: String,
    address: String,
    tls: Tls,
    disco_mdpws: Option<String>,
    disco_proto: Option<String>,
}

#[derive(Debug, Default, Deserialize)]
pub struct ProviderConfig {
    protocol: Protocol,
    epr: String,
    address: String,
    port: u16,
    mdib: String,
    tls: Tls,
    disco_mdpws: Option<String>,
    disco_proto: Option<String>,
}

#[derive(Debug, Default, Deserialize)]
struct Tls {
    public_key_file: Option<String>,
    private_key_file: Option<String>,
    ca_cert_file: Option<String>,
    private_key_password: Option<String>,
}

/// Doc comment
#[derive(Clone, Debug, Default, Deserialize, ValueEnum)]
enum Protocol {
    #[default]
    All,
    Mdpws,
    Proto,
}

#[derive(Debug, Deserialize)]
struct TranslationConfig {
    translations: Translations,
}

#[derive(Debug, Deserialize)]
struct Translations {
    texts: Vec<Text>,
}

#[derive(Debug, Deserialize)]
struct Text {
    text: String,
    width: String,
    lang: String,
    r#ref: String,
    lines: u8,
}

impl TryInto<InputText> for Text {
    type Error = anyhow::Error;

    fn try_into(self) -> std::result::Result<InputText, Self::Error> {
        let width = match self.width.as_str() {
            "xs" => TextWidth::XS,
            "s" => TextWidth::S,
            "m" => TextWidth::M,
            "l" => TextWidth::L,
            "xl" => TextWidth::XL,
            "xxl" => TextWidth::XXL,
            _ => return Err(anyhow::Error::msg(format!("Unknown width {}", self.width))),
        };

        Ok(InputText {
            text_ref: self.r#ref,
            lang: self.lang,
            text: self.text,
            width,
            lines: self.lines,
        })
    }
}

async fn load_texts(path: &str) -> Result<Vec<InputText>> {
    assert!(file_exists(path));
    let content = tokio::fs::read_to_string(PathBuf::from_str(path).unwrap()).await?;

    let loaded: TranslationConfig = toml::from_str(content.as_str())?;

    loaded
        .translations
        .texts
        .into_iter()
        .map(|it| it.try_into())
        .collect::<Result<Vec<_>, _>>()
}

async fn load_config(path: &str) -> Result<Config> {
    assert!(file_exists(path));
    let content = tokio::fs::read_to_string(PathBuf::from_str(path).unwrap()).await?;

    let loaded: Config = toml::from_str(content.as_str())?;
    Ok(loaded)
}

async fn get_crypto_config(args: &Tls) -> Option<CryptoConfig> {
    if args.public_key_file.is_none()
        && args.private_key_file.is_none()
        && args.ca_cert_file.is_none()
    {
        info!("No crypto parameters provided");
        return None;
    }

    info!(
        "Running with crypto parameters public {:?} private {:?} ca {:?} password {:?}",
        &args.public_key_file,
        &args.private_key_file,
        &args.ca_cert_file,
        &args.private_key_password
    );
    assert!(file_exists(
        args.public_key_file
            .as_ref()
            .expect("Path to public key file required")
    ));
    assert!(file_exists(
        args.private_key_file
            .as_ref()
            .expect("Path to private key file required")
    ));
    assert!(file_exists(
        args.ca_cert_file
            .as_ref()
            .expect("Path to ca certificate file required")
    ));

    Some(
        CryptoConfig::new(
            &args.private_key_file.as_ref().unwrap().clone(),
            &args.public_key_file.as_ref().unwrap().clone(),
            &args.ca_cert_file.as_ref().unwrap().clone(),
            match &args.private_key_password.as_ref() {
                None => None,
                Some(x) => Some(x),
            },
        )
        .await
        .expect("Could not load certs"),
    )
}

fn init() {
    env_logger::Builder::new()
        .format(|buf, record| {
            writeln!(
                buf,
                "{}:{} {} [{}] - {}",
                record.file().unwrap_or("unknown"),
                record.line().unwrap_or(0),
                chrono::Local::now().format("%Y-%m-%dT%H:%M:%S"),
                record.level(),
                record.args()
            )
        })
        .filter_level(LevelFilter::Info)
        .parse_default_env()
        .init();
}

#[tokio::main]
async fn main() -> Result<()> {
    init();

    let args = Args::parse();

    let mut config = match args.config {
        Some(pth) => load_config(&pth).await?,
        None => Config::default(),
    };

    // update config with command line flags
    if let Some(protocol) = args.protocol {
        config.consumer.protocol = protocol.clone();
        config.provider.protocol = protocol;
    }

    if let Some(ip) = args.adapter_ip {
        config.consumer.address.clone_from(&ip);
        config.provider.address.clone_from(&ip);
    }

    if let Some(path) = args.public_key_path {
        config.consumer.tls.public_key_file = Some(path.clone());
        config.provider.tls.public_key_file = Some(path.clone());
    }

    if let Some(path) = args.private_key_path {
        config.consumer.tls.private_key_file = Some(path.clone());
        config.provider.tls.private_key_file = Some(path.clone());
    }

    if let Some(path) = args.private_key_password {
        config.consumer.tls.private_key_password = Some(path.clone());
        config.provider.tls.private_key_password = Some(path.clone());
    }

    if let Some(path) = args.ca_cert_path {
        config.consumer.tls.ca_cert_file = Some(path.clone());
        config.provider.tls.ca_cert_file = Some(path.clone());
    }

    match &args.command {
        Commands::Provider {
            port,
            epr,
            mdib,
            localized_texts,
        } => {
            let mut provider_config = config.provider;
            // update config with command line flags
            if let Some(x) = epr {
                provider_config.epr.clone_from(x);
            }
            if let Some(x) = port {
                provider_config.port = *x;
            }
            if let Some(x) = mdib {
                provider_config.mdib.clone_from(x);
            }

            let crypto_config = get_crypto_config(&provider_config.tls).await;

            let mdib = load_mdib_xml_file(&provider_config.mdib).await?;

            let localized_texts = match localized_texts {
                None => None,
                Some(location) => Some(load_texts(location.as_str()).await?),
            };

            match &provider_config.protocol {
                Protocol::Mdpws => {
                    provider::run_dpws_provider(
                        &provider_config,
                        crypto_config,
                        mdib,
                        localized_texts,
                        ReferenceTestExtensionHandlerFactory,
                        provider_config.disco_mdpws.clone(),
                    )
                    .await?
                }
                Protocol::Proto => {
                    provider::run_proto_provider(
                        &provider_config,
                        crypto_config,
                        mdib,
                        localized_texts,
                        ReferenceTestExtensionHandlerFactory,
                        provider_config.disco_proto.clone(),
                    )
                    .await?
                }
                Protocol::All => {
                    provider::run_dual_provider(
                        &provider_config,
                        crypto_config,
                        mdib,
                        localized_texts,
                        ReferenceTestExtensionHandlerFactory,
                        provider_config.disco_proto.clone(),
                    )
                    .await?
                }
            }
        }
        Commands::Consumer { epr } => {
            let mut consumer_config = config.consumer;
            // update config with command line flags
            if let Some(x) = epr {
                consumer_config.epr.clone_from(x);
            }

            let crypto_config = get_crypto_config(&consumer_config.tls).await;

            match &consumer_config.protocol {
                Protocol::Mdpws => {
                    consumer::run_consumer_dpws_main(
                        &consumer_config,
                        crypto_config,
                        ReferenceTestExtensionHandlerFactory,
                    )
                    .await?
                }
                Protocol::Proto => {
                    consumer::run_consumer_proto_main(&consumer_config, crypto_config).await?
                }
                Protocol::All => {
                    consumer::run_consumer_dual(
                        &consumer_config,
                        crypto_config,
                        ReferenceTestExtensionHandlerFactory,
                    )
                    .await?
                }
            }
        }
    };

    info!("Done");
    Ok(())
}
