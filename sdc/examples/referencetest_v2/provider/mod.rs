pub mod alert_system_updater;
pub mod component_updater;
pub mod description_updater;
pub mod numeric_metric_updater;
pub mod operation_updater;
pub mod string_metric_updater;
pub mod waveform_updater;

use crate::ProviderConfig;
use alert_system_updater::AlertSystemUpdater;
use async_trait::async_trait;
use biceps::common::access::mdib_access::MdibAccess;
use biceps::common::biceps_util::ContextState;
use biceps::common::biceps_util::{MetricState, MultiState, State};
use biceps::common::context::fallback_instance_identifier::create_fallback_instance_identifier;
use biceps::common::mdib_description_modifications::MdibDescriptionModifications;
use biceps::common::mdib_entity::{Entity, EntityBase, MdibEntity};
use biceps::common::mdib_state_modifications::{
    MdibStateModificationResults, MdibStateModifications,
};
use biceps::common::mdib_version::MdibVersionBuilder;
use biceps::common::storage::mdib_storage::{MdibStorageImpl, WriteStateResult};
use biceps::entity_filter;
use biceps::provider::access::local_mdib_access::{LocalMdibAccess, LocalMdibAccessImpl};
use biceps::test_util::mdib_utils::create_minimal_location_context_state;
use biceps::utilities::localized_text::LocalizedTextBuilder;
use biceps::utilities::modifications::modifications_from_mdib;
use common::crypto::CryptoConfig;
use common::Service;
use component_updater::ComponentUpdater;
use description_updater::DescriptionUpdater;
use dpws::chain_plugins;
use dpws::common::extension_handling::{
    ExtensionHandlerFactory, ReplacementExtensionHandlerFactory,
};
use dpws::discovery::constants::WS_DISCOVERY_PORT;
use dpws::discovery::provider::discovery_provider::{
    DpwsDiscoveryProcessorImpl, DpwsDiscoveryProviderImpl,
};
use dpws::discovery::provider::discovery_proxy_provider::DummyDpwsDiscoveryProxyProviderImpl;
use dpws::provider::sdc_provider::SdcDevice;
use dpws::provider::sdc_provider_plugin::TypesScopesPlugin;
use dpws::soap::dpws::device::dpws_device::DeviceSettings;
use dpws::xml::messages::addressing::{WsaAttributedURIType, WsaEndpointReference};
use dpws::xml::messages::common::{DPWS_DEVICE_TYPE, GLUE_SCOPE, MDPWS_MEDICAL_DEVICE_TYPE};
use futures::FutureExt;
use log::{debug, error, info, warn};
use network::udp_binding::{create_udp_binding, UdpBindingImpl, PROTOSDC_MULTICAST_PORT};
use numeric_metric_updater::NumericMetricUpdater;
use operation_updater::OperationUpdater;
use protosdc::discovery::provider::discovery_provider::DiscoveryProviderImpl;
use protosdc::proto_chain_plugins;
use protosdc::provider::device::SdcDevice as ProtoSdcDevice;
use protosdc::provider::device::ServerConfig;
use protosdc::provider::localization_storage::{InputText, LocalizationStorageImpl};
use protosdc::provider::metadata::{DeviceMetadata, LocalizedString};
use protosdc::provider::plugins::ScopesPlugin;
use protosdc::provider::sco::{
    Context, InvocationCallError, InvocationResponse, OperationInvocationReceiver,
};
use protosdc::provider::sco_util::{
    FailedInvocationStateTypes, NotFailedInvocationStateTypes, ReportBuilder,
};
use protosdc_biceps::biceps::abstract_metric_value_mod::MetricQuality;
use protosdc_biceps::biceps::{
    context_association_mod, invocation_error_mod, measurement_validity_mod, AbstractContextState,
    AbstractContextStateOneOf, AbstractMetricStateOneOf, AbstractMetricValue, AbstractMultiState,
    Activate, ContextAssociation, DistributionSampleArrayMetricState, EnumStringMetricState,
    Handle, InstanceIdentifierOneOf, LocationDetail, Mdib, MeasurementValidity, NumericMetricState,
    PatientContextState, RealTimeSampleArrayMetricState, SetContextState, SetMetricState,
    SetString, SetValue, StringMetricState, StringMetricValue,
};
use sdc::provider::dual_stack_provider::{
    DualStackDiscovery, DualStackProviderConfig, EndpointReferenceAddress, ProviderError,
};
use sdc::provider::sdc_provider::{
    DeviceDiscoveryAccess, DeviceMdibType, DpwsSdcProviderPluginWrapper,
    ProtoSdcProviderPluginWrapper, SdcProvider,
};
use sdc::provider_chain_plugins;
use std::collections::HashSet;
use std::net::{IpAddr, SocketAddr};
use std::str::FromStr;
use std::time::Duration;
use string_metric_updater::StringMetricUpdater;
use tokio::task;
use waveform_updater::WaveformUpdater;

#[derive(Clone)]
struct ExampleProviderOperationInvocationReceiver {}

impl ExampleProviderOperationInvocationReceiver {
    async fn set_string_write<MDIB>(
        request: SetString,
        old_state: StringMetricState,
        is_enum: bool,
        context: &mut Context<MDIB>,
    ) -> Option<WriteStateResult>
    where
        MDIB: 'static + LocalMdibAccess + Send + Sync + Clone,
    {
        info!(
            "Writing {} into {}",
            &request.requested_string_value,
            old_state.descriptor_handle()
        );

        let old_value = match old_state.metric_value {
            None => StringMetricValue {
                abstract_metric_value: AbstractMetricValue {
                    extension_element: None,
                    metric_quality: MetricQuality {
                        extension_element: None,
                        validity_attr: MeasurementValidity {
                            enum_type: measurement_validity_mod::EnumType::Vld,
                        },
                        mode_attr: None,
                        qi_attr: None,
                    },
                    annotation: vec![],
                    start_time_attr: None,
                    stop_time_attr: None,
                    determination_time_attr: None,
                },
                value_attr: None,
            },
            Some(old) => old,
        };

        let new_state = StringMetricState {
            metric_value: Some(StringMetricValue {
                value_attr: Some(request.requested_string_value),
                ..old_value
            }),
            ..old_state
        };

        let modification = MdibStateModifications::Metric {
            metric_states: vec![match is_enum {
                false => new_state.into_abstract_metric_state_one_of(),
                true => EnumStringMetricState {
                    string_metric_state: new_state,
                }
                .into_abstract_metric_state_one_of(),
            }],
        };

        context.mdib_access.write_states(modification).await.ok()
    }
}

#[async_trait]
impl<MDIB> OperationInvocationReceiver<MDIB> for ExampleProviderOperationInvocationReceiver
where
    MDIB: 'static + LocalMdibAccess + Send + Sync + Clone,
{
    async fn activate(
        &self,
        _operation_handle: String,
        mut context: Context<MDIB>,
        _request: Activate,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        let version = context.mdib_access.mdib_version().await;
        let response = context
            .create_response(
                ReportBuilder::create()
                    .mdib_version(version)
                    .success(NotFailedInvocationStateTypes::Wait)
                    .build(),
            )
            .await;

        task::spawn(async move {
            info!("Async finishing task");
            tokio::time::sleep(Duration::from_millis(500)).await;
            {
                let report_data = ReportBuilder::create()
                    .mdib_version(context.mdib_access.mdib_version().await)
                    .success(NotFailedInvocationStateTypes::Start)
                    .build();

                context.send_report(report_data).await;
            }
            tokio::time::sleep(Duration::from_secs(1)).await;
            {
                let report_data = ReportBuilder::create()
                    .mdib_version(context.mdib_access.mdib_version().await)
                    .success(NotFailedInvocationStateTypes::Fin)
                    .build();

                context.send_report(report_data).await;
            }
            info!("Async task finished");
        });
        Ok(response)
    }

    async fn set_context_state(
        &self,
        operation_handle: String,
        mut context: Context<MDIB>,
        mut request: SetContextState,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        let version = context.mdib_access.mdib_version().await;

        let current_entity = context.mdib_access.entity(&operation_handle).await;
        let _set_context_entity = match current_entity {
            Some(MdibEntity {
                entity: Entity::SetContextStateOperation(it),
                ..
            }) => it,
            _ => {
                return Ok(context
                    .create_response(
                        ReportBuilder::create()
                            .mdib_version(version)
                            .failure(FailedInvocationStateTypes::Fail)
                            .error(invocation_error_mod::EnumType::Inv)
                            .error_message(
                                LocalizedTextBuilder::create()
                                    .no_service("Operation is not a SetContextStateOperation")
                                    .lang("en")
                                    .build(),
                            )
                            .build(),
                    )
                    .await)
            }
        };

        if request.proposed_context_state.len() != 1 {
            return Ok(context
                .create_response(
                    ReportBuilder::create()
                        .mdib_version(version)
                        .failure(FailedInvocationStateTypes::Fail)
                        .error(invocation_error_mod::EnumType::Inv)
                        .error_message(
                            LocalizedTextBuilder::create()
                                .no_service("Expected exactly one context state")
                                .lang("en")
                                .build(),
                        )
                        .build(),
                )
                .await);
        }

        let proposed_state = request.proposed_context_state.remove(0);
        let patient = match proposed_state {
            AbstractContextStateOneOf::PatientContextState(patient) => patient,
            _ => {
                return Ok(context
                    .create_response(
                        ReportBuilder::create()
                            .mdib_version(version)
                            .failure(FailedInvocationStateTypes::Fail)
                            .error(invocation_error_mod::EnumType::Inv)
                            .error_message(
                                LocalizedTextBuilder::create()
                                    .no_service("Expected patient context state")
                                    .lang("en")
                                    .build(),
                            )
                            .build(),
                    )
                    .await)
            }
        };

        debug!(
            "Extensions of proposed patient context: {:?}",
            patient
                .abstract_context_state
                .abstract_multi_state
                .abstract_state
                .extension_element
        );

        if patient.descriptor_handle() != patient.state_handle() {
            return Ok(context
                .create_response(
                    ReportBuilder::create()
                        .mdib_version(version)
                        .failure(FailedInvocationStateTypes::Fail)
                        .error(invocation_error_mod::EnumType::Inv)
                        .error_message(
                            LocalizedTextBuilder::create()
                                .no_service(
                                    "Can only add new context states, not update current ones",
                                )
                                .lang("en")
                                .build(),
                        )
                        .build(),
                )
                .await);
        }

        let current_entity = context
            .mdib_access
            .entity(&patient.descriptor_handle())
            .await;

        let _current_patient_context_entity =
            match current_entity {
                Some(MdibEntity {
                    entity: Entity::PatientContext(it),
                    ..
                }) => it,
                _ => return Ok(context
                    .create_response(
                        ReportBuilder::create()
                            .mdib_version(version)
                            .failure(FailedInvocationStateTypes::Fail)
                            .error(invocation_error_mod::EnumType::Inv)
                            .error_message(
                                LocalizedTextBuilder::create()
                                    .no_service(
                                        "Can only add new context states, not update current ones",
                                    )
                                    .lang("en")
                                    .build(),
                            )
                            .build(),
                    )
                    .await),
            };

        let context_for_mdib = PatientContextState {
            abstract_context_state: AbstractContextState {
                abstract_multi_state: AbstractMultiState {
                    handle_attr: Handle {
                        string: uuid::Uuid::new_v4().to_string(),
                    },
                    ..patient.abstract_context_state.abstract_multi_state.clone()
                },
                ..patient.abstract_context_state.clone()
            },
            ..patient
        };

        let mut modifications = vec![];

        // if the new context is assoc, remove any previously associated one
        if context_for_mdib
            .abstract_context_state
            .context_association_attr
            == Some(ContextAssociation {
                enum_type: context_association_mod::EnumType::Assoc,
            })
        {
            let previously_associated = context
                .mdib_access
                .context_states()
                .await
                .into_iter()
                .filter_map(|it| match it {
                    AbstractContextStateOneOf::PatientContextState(pt) => Some(pt),
                    _ => None,
                })
                .find(|it| {
                    it.abstract_context_state.context_association_attr
                        == Some(ContextAssociation {
                            enum_type: context_association_mod::EnumType::Assoc,
                        })
                })
                .map(|it| PatientContextState {
                    abstract_context_state: AbstractContextState {
                        context_association_attr: Some(ContextAssociation {
                            enum_type: context_association_mod::EnumType::Dis,
                        }),
                        ..it.abstract_context_state.clone()
                    },
                    ..it
                });
            if let Some(state) = previously_associated {
                modifications.push(state.into_abstract_context_state_one_of())
            }
        }

        let new_context_handle = context_for_mdib.abstract_context_state.state_handle();

        info!("Handle for new patient context: {}", new_context_handle);

        debug!(
            "Extensions of new patient context: {:?}",
            context_for_mdib
                .abstract_context_state
                .abstract_multi_state
                .abstract_state
                .extension_element
        );

        modifications.push(context_for_mdib.into_abstract_context_state_one_of());

        let write_result = context
            .mdib_access
            .write_states(MdibStateModifications::Context {
                context_states: modifications,
            })
            .await;

        match write_result {
            Ok(_) => Ok(context
                .create_response(
                    ReportBuilder::create()
                        .mdib_version(version)
                        .success(NotFailedInvocationStateTypes::Fin)
                        .operation_target(new_context_handle)
                        .build(),
                )
                .await),
            Err(err) => Ok(context
                .create_response(
                    ReportBuilder::create()
                        .mdib_version(version)
                        .failure(FailedInvocationStateTypes::Fail)
                        .error(invocation_error_mod::EnumType::Inv)
                        .error_message(
                            LocalizedTextBuilder::create()
                                .no_service(err.to_string())
                                .lang("en")
                                .build(),
                        )
                        .build(),
                )
                .await),
        }
    }

    async fn set_metric_state(
        &self,
        operation_handle: String,
        mut context: Context<MDIB>,
        request: SetMetricState,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        let version = context.mdib_access.mdib_version().await;

        let current_entity = context.mdib_access.entity(&operation_handle).await;
        let _set_context_entity = match current_entity {
            Some(MdibEntity {
                entity: Entity::SetMetricStateOperation(it),
                ..
            }) => it,
            _ => {
                return Ok(context
                    .create_response(
                        ReportBuilder::create()
                            .mdib_version(version)
                            .failure(FailedInvocationStateTypes::Fail)
                            .error(invocation_error_mod::EnumType::Inv)
                            .error_message(
                                LocalizedTextBuilder::create()
                                    .no_service("Operation is not a SetMetricStateOperation")
                                    .lang("en")
                                    .build(),
                            )
                            .build(),
                    )
                    .await)
            }
        };

        if request.proposed_metric_state.len() != 2 {
            return Ok(context
                .create_response(
                    ReportBuilder::create()
                        .mdib_version(version)
                        .failure(FailedInvocationStateTypes::Fail)
                        .error(invocation_error_mod::EnumType::Inv)
                        .error_message(
                            LocalizedTextBuilder::create()
                                .no_service("Expected exactly two metric states")
                                .lang("en")
                                .build(),
                        )
                        .build(),
                )
                .await);
        }

        // verify states exist and types match
        let mut updated_states = vec![];
        for state in request.proposed_metric_state {
            let ent = match context.mdib_access.entity(&state.descriptor_handle()).await {
                Some(ent) => ent,
                None => {
                    return Ok(context
                        .create_response(
                            ReportBuilder::create()
                                .mdib_version(version)
                                .failure(FailedInvocationStateTypes::Fail)
                                .error(invocation_error_mod::EnumType::Inv)
                                .error_message(
                                    LocalizedTextBuilder::create()
                                        .no_service(format!(
                                            "No metric with handle {} known",
                                            state.descriptor_handle()
                                        ))
                                        .lang("en")
                                        .build(),
                                )
                                .build(),
                        )
                        .await)
                }
            };

            match (state, ent) {
                (
                    AbstractMetricStateOneOf::StringMetricState(sms),
                    MdibEntity {
                        entity: Entity::StringMetric(sm),
                        ..
                    },
                ) => updated_states.push(
                    StringMetricState {
                        metric_value: sms.metric_value,
                        ..sm.pair.state
                    }
                    .into_abstract_metric_state_one_of(),
                ),
                (
                    AbstractMetricStateOneOf::NumericMetricState(nms),
                    MdibEntity {
                        entity: Entity::NumericMetric(nm),
                        ..
                    },
                ) => {
                    info!("Copying metric value for {}", nms.descriptor_handle());
                    updated_states.push(
                        NumericMetricState {
                            metric_value: nms.metric_value,
                            ..nm.pair.state
                        }
                        .into_abstract_metric_state_one_of(),
                    )
                }
                (
                    AbstractMetricStateOneOf::DistributionSampleArrayMetricState(dsams),
                    MdibEntity {
                        entity: Entity::DistributionSampleArrayMetric(dsam),
                        ..
                    },
                ) => updated_states.push(
                    DistributionSampleArrayMetricState {
                        metric_value: dsams.metric_value,
                        ..dsam.pair.state
                    }
                    .into_abstract_metric_state_one_of(),
                ),
                (
                    AbstractMetricStateOneOf::RealTimeSampleArrayMetricState(rtsams),
                    MdibEntity {
                        entity: Entity::RealTimeSampleArrayMetric(rtsam),
                        ..
                    },
                ) => updated_states.push(
                    RealTimeSampleArrayMetricState {
                        metric_value: rtsams.metric_value,
                        ..rtsam.pair.state
                    }
                    .into_abstract_metric_state_one_of(),
                ),
                (
                    AbstractMetricStateOneOf::EnumStringMetricState(esms),
                    MdibEntity {
                        entity: Entity::EnumStringMetric(esm),
                        ..
                    },
                ) => {
                    // make sure value is valid
                    let allowed: Vec<String> = esm
                        .pair
                        .descriptor
                        .allowed_value
                        .iter()
                        .map(|it| it.value.clone())
                        .collect();

                    match &esms.string_metric_state.metric_value {
                        None => {}
                        Some(value) => match &value.value_attr {
                            None => {}
                            Some(value_attr) => {
                                if !allowed.contains(value_attr) {
                                    return Ok(context
                                        .create_response(
                                            ReportBuilder::create()
                                                .mdib_version(version)
                                                .failure(FailedInvocationStateTypes::Fail)
                                                .error(invocation_error_mod::EnumType::Inv)
                                                .error_message(LocalizedTextBuilder::create()
                                                    .no_service(format!(
                                                        "{} is not an allowed value for descriptor {}",
                                                        value_attr,
                                                        esms.descriptor_handle()
                                                    ))
                                                    .lang("en")
                                                    .build()
                                                )
                                                .build()
                                        )
                                        .await);
                                }
                            }
                        },
                    }
                    updated_states.push(
                        EnumStringMetricState {
                            string_metric_state: StringMetricState {
                                metric_value: esms.string_metric_state.metric_value,
                                ..esm.pair.state.string_metric_state
                            },
                        }
                        .into_abstract_metric_state_one_of(),
                    )
                }
                (a, b) => {
                    return Ok(context
                        .create_response(
                            ReportBuilder::create()
                                .mdib_version(version)
                                .failure(FailedInvocationStateTypes::Fail)
                                .error(invocation_error_mod::EnumType::Inv)
                                .error_message(
                                    LocalizedTextBuilder::create()
                                        .no_service(format!(
                                            "{:?} and {:?} is not an allowed combination",
                                            a, b
                                        ))
                                        .lang("en")
                                        .build(),
                                )
                                .build(),
                        )
                        .await)
                }
            }
        }

        // pick handle for any updated state
        let operation_target = match updated_states.first() {
            None => "".to_string(),
            Some(updated) => updated.descriptor_handle(),
        };

        let write_result = context
            .mdib_access
            .write_states(MdibStateModifications::Metric {
                metric_states: updated_states,
            })
            .await;

        match write_result {
            Ok(_) => Ok(context
                .create_response(
                    ReportBuilder::create()
                        .mdib_version(version)
                        .success(NotFailedInvocationStateTypes::Fin)
                        .operation_target(operation_target)
                        .build(),
                )
                .await),
            Err(err) => Ok(context
                .create_response(
                    ReportBuilder::create()
                        .mdib_version(version)
                        .failure(FailedInvocationStateTypes::Fail)
                        .error(invocation_error_mod::EnumType::Inv)
                        .error_message(
                            LocalizedTextBuilder::create()
                                .no_service(err.to_string())
                                .build(),
                        )
                        .build(),
                )
                .await),
        }
    }

    async fn set_string(
        &self,
        operation_handle: String,
        mut context: Context<MDIB>,
        request: SetString,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        let version = context.mdib_access.mdib_version().await;
        let response = context
            .create_response(
                ReportBuilder::create()
                    .mdib_version(version)
                    .success(NotFailedInvocationStateTypes::Wait)
                    .build(),
            )
            .await;

        task::spawn(async move {
            info!("Async set_string finishing task");
            tokio::time::sleep(Duration::from_secs(1)).await;

            let version = context.mdib_access.mdib_version().await;
            context
                .send_report(
                    ReportBuilder::create()
                        .mdib_version(version)
                        .success(NotFailedInvocationStateTypes::Start)
                        .build(),
                )
                .await;

            // find operation target
            let operation = context
                .mdib_access
                .entity(&request.abstract_set.operation_handle_ref.string)
                .await;
            let operation_target_handle = match operation {
                Some(op) => match op.entity {
                    Entity::SetStringOperation(entity) => Some(
                        entity
                            .pair
                            .descriptor
                            .abstract_operation_descriptor
                            .operation_target_attr
                            .string,
                    ),
                    _ => {
                        error!("Wrong entity kind found");
                        None
                    }
                },
                None => {
                    error!("No entity found");
                    None
                }
            };

            if operation_target_handle.is_none() {
                let error_message = format!(
                    "Could not find operation target {}",
                    &request.abstract_set.operation_handle_ref.string
                );
                warn!("{}", error_message);
                let version = context.mdib_access.mdib_version().await;

                context
                    .send_report(
                        ReportBuilder::create()
                            .mdib_version(version)
                            .failure(FailedInvocationStateTypes::Fail)
                            .error(invocation_error_mod::EnumType::Unkn)
                            .error_message(
                                LocalizedTextBuilder::create()
                                    .no_service(error_message)
                                    .build(),
                            )
                            .build(),
                    )
                    .await;

                return;
            }

            // find target state
            let operation_target_entity = context
                .mdib_access
                .entity(&operation_target_handle.unwrap())
                .await;
            let write_result =
                match operation_target_entity {
                    None => None,
                    Some(entity) => {
                        // write updated state
                        match entity.entity {
                            Entity::StringMetric(ent) => {
                                Self::set_string_write(request, ent.pair.state, false, &mut context)
                                    .await
                            }
                            Entity::EnumStringMetric(ent) => {
                                // validate new value is permitted
                                let permitted =
                                    ent.pair.descriptor.allowed_value.iter().any(|allowed| {
                                        request.requested_string_value == allowed.value
                                    });
                                match permitted {
                                    false => {
                                        error!(
                                            "{} is not an allowed value for {}",
                                            request.requested_string_value,
                                            ent.handle()
                                        );
                                        None
                                    }
                                    true => {
                                        Self::set_string_write(
                                            request,
                                            ent.pair.state.string_metric_state,
                                            true,
                                            &mut context,
                                        )
                                        .await
                                    }
                                }
                            }
                            _ => None,
                        }
                    }
                };

            let version = context.mdib_access.mdib_version().await;
            match write_result {
                None => {
                    let error_message = format!("Invoking operation {} failed", operation_handle);
                    warn!("{}", error_message);

                    context
                        .send_report(
                            ReportBuilder::create()
                                .failure(FailedInvocationStateTypes::Fail)
                                .mdib_version(version)
                                .error(invocation_error_mod::EnumType::Oth)
                                .error_message(
                                    LocalizedTextBuilder::create()
                                        .no_service(error_message)
                                        .build(),
                                )
                                .build(),
                        )
                        .await;
                }
                Some(written) => {
                    let operation_target = match written.states {
                        MdibStateModificationResults::Metric { metric_states } => {
                            metric_states.iter().next().map(|it| {
                                it.1.iter()
                                    .next()
                                    .map(|state| state.descriptor_handle())
                                    .unwrap()
                            })
                        }
                        _ => None,
                    };

                    match operation_target {
                        None => {
                            context
                                .send_report(
                                    ReportBuilder::create()
                                        .failure(FailedInvocationStateTypes::Fail)
                                        .error(invocation_error_mod::EnumType::Oth)
                                        .mdib_version(version)
                                        .error_message(
                                            LocalizedTextBuilder::create()
                                                .no_service("Could not determine operation target")
                                                .build(),
                                        )
                                        .build(),
                                )
                                .await;
                        }
                        Some(target) => {
                            context
                                .send_report(
                                    ReportBuilder::create()
                                        .success(NotFailedInvocationStateTypes::Fin)
                                        .mdib_version(version)
                                        .operation_target(target)
                                        .build(),
                                )
                                .await;
                        }
                    }
                }
            };

            info!("Async set_string task finished");
        });
        Ok(response)
    }

    async fn set_value(
        &self,
        operation_handle: String,
        mut context: Context<MDIB>,
        _request: SetValue,
    ) -> Result<InvocationResponse, (InvocationCallError, Context<MDIB>)> {
        let version = context.mdib_access.mdib_version().await;

        let set_value_entity = match context.mdib_access.entity(&operation_handle).await {
            Some(MdibEntity {
                entity: Entity::SetValueOperation(it),
                ..
            }) => it,
            _ => {
                return Ok(context
                    .create_response(
                        ReportBuilder::create()
                            .failure(FailedInvocationStateTypes::Fail)
                            .error(invocation_error_mod::EnumType::Inv)
                            .error_message(
                                LocalizedTextBuilder::create()
                                    .no_service("Operation is not a SetValueOperation")
                                    .build(),
                            )
                            .mdib_version(version)
                            .build(),
                    )
                    .await);
            }
        };

        let response = context
            .create_response(
                ReportBuilder::create()
                    .mdib_version(version)
                    .success(NotFailedInvocationStateTypes::Fin)
                    .operation_target(
                        set_value_entity
                            .pair
                            .descriptor
                            .abstract_operation_descriptor
                            .operation_target_attr
                            .string,
                    )
                    .build(),
            )
            .await;
        Ok(response)
    }
}

pub async fn run_provider<PROV, D, MDIB>(mut provider: PROV, mdib: Mdib) -> anyhow::Result<()>
where
    PROV: SdcProvider<MDIB, D> + Service,
    MDIB: DeviceMdibType,
    D: DeviceDiscoveryAccess,
{
    // add mdib content
    let modifications = modifications_from_mdib(mdib).await?;

    let mut modification = MdibDescriptionModifications::default();
    modification.add_all(modifications)?;

    provider
        .local_mdib()
        .await
        .write_description(modification)
        .await?;

    provider.start().await?;

    // set location to a cool one
    let context_descriptor_handle = provider
        .local_mdib()
        .await
        .read_transaction(|access| {
            async move {
                let location_contexts = access
                    .entities_by_type(entity_filter!(LocationContext))
                    .await;

                location_contexts
                    .first()
                    .map(|context_entity| context_entity.entity.handle())
            }
            .boxed()
        })
        .await;

    if let Some(location_descriptor_handle) = context_descriptor_handle {
        info!("Association location for {}", location_descriptor_handle);
        let state_handle = uuid::Uuid::new_v4().to_string();

        let mut ctx_state =
            create_minimal_location_context_state(&location_descriptor_handle, &state_handle);

        let location_detail = LocationDetail {
            extension_element: None,
            po_c_attr: Some("Lukas Cares".to_string()),
            room_attr: None,
            bed_attr: None,
            facility_attr: None,
            building_attr: None,
            floor_attr: None,
        };
        let identification = create_fallback_instance_identifier(&location_detail)
            .expect("Could not create fallback instance identifier?");

        ctx_state.location_detail = Some(location_detail);
        ctx_state.abstract_context_state.context_association_attr = Some(ContextAssociation {
            enum_type: context_association_mod::EnumType::Assoc,
        });
        ctx_state
            .abstract_context_state
            .identification
            .push(InstanceIdentifierOneOf::InstanceIdentifier(identification));

        provider
            .local_mdib()
            .await
            .write_states(MdibStateModifications::Context {
                context_states: vec![ctx_state.into_abstract_context_state_one_of()],
            })
            .await
            .expect("Could not write new location state");
    }

    tokio::time::sleep(Duration::from_secs(10000000)).await;

    Ok(())
}

pub async fn run_dual_provider<EHF>(
    config: &ProviderConfig,
    crypto: Option<CryptoConfig>,
    mdib: Mdib,
    localized_texts: Option<Vec<InputText>>,
    _extension_handler_factory: EHF,
    _disco: Option<String>, // TODO: add discovery proxy
) -> anyhow::Result<()> {
    let addr = IpAddr::from_str(&config.address)?;

    let mds1 = "mds_1";

    let scopes_updater = sdc::provider::plugin::ScopesPlugin::builder()
        .constant_scopes(vec![GLUE_SCOPE.to_string()])
        .build();
    let alert_system_updater = AlertSystemUpdater::new(
        // ignore mds1
        HashSet::from([mds1.to_string()]),
    );
    let waveform_updater = WaveformUpdater::default();
    let component_updater = ComponentUpdater::new(
        // ignore mds1
        HashSet::from([mds1.to_string()]),
    );
    let operation_updater = OperationUpdater::new(
        // ignore mds1
        HashSet::from([mds1.to_string()]),
    );
    let descriptor_updater = DescriptionUpdater::default();
    let numeric_metric_updater = NumericMetricUpdater::new(
        // ignore mds1
        HashSet::from([mds1.to_string()]),
    );
    let string_metric_updater = StringMetricUpdater::new(
        // ignore mds1
        HashSet::from([mds1.to_string()]),
    );

    let plugin_chain = provider_chain_plugins!(
        scopes_updater,
        alert_system_updater,
        waveform_updater,
        component_updater,
        operation_updater,
        descriptor_updater,
        numeric_metric_updater,
        string_metric_updater
    );

    let localization_storage: Option<LocalizationStorageImpl> =
        localized_texts.map(LocalizationStorageImpl::new);

    let config = DualStackProviderConfig::builder()
        .mdib(LocalMdibAccessImpl::<MdibStorageImpl, _, _>::new(
            MdibVersionBuilder::default().seq("ab:cd").create(),
        ))
        .plugin(plugin_chain)
        .crypto(crypto)
        .discovery(setup_dual_stack_discovery(addr, config.epr.clone()).await?)
        .endpoint_reference_address(EndpointReferenceAddress(config.epr.clone()))
        .extension_handler(ReplacementExtensionHandlerFactory)
        .operation(ExampleProviderOperationInvocationReceiver {})
        .localization(localization_storage)
        .network_interface(addr)
        .build();

    let provider = config.try_into_provider().await?;

    run_provider(provider, mdib).await
}

use dpws::discovery::common::Endpoint;
use protosdc::provider::common::EndpointData;
use protosdc_proto::discovery::Endpoint as ProtoEndpoint;

async fn setup_dual_stack_discovery(
    adapter: IpAddr,
    provider_epr: String,
) -> Result<
    DualStackDiscovery<
        DpwsDiscoveryProviderImpl<
            UdpBindingImpl,
            DpwsDiscoveryProcessorImpl,
            DummyDpwsDiscoveryProxyProviderImpl,
        >,
        DpwsDiscoveryProcessorImpl,
        DiscoveryProviderImpl<UdpBindingImpl>,
    >,
    ProviderError,
> {
    let dpws_binding = create_udp_binding(adapter, Some(WS_DISCOVERY_PORT)).await?;

    let proto_binding = create_udp_binding(adapter, Some(PROTOSDC_MULTICAST_PORT)).await?;

    let dpws_endpoint: Endpoint = Endpoint {
        scopes: vec![],
        types: vec![],
        endpoint_reference: WsaEndpointReference {
            attributes: Default::default(),
            address: WsaAttributedURIType {
                attributes: Default::default(),
                value: provider_epr.clone(),
            },
            reference_parameters: None,
            metadata: None,
            children: vec![],
        },
        x_addrs: vec![],
        metadata_version: 0,
    };

    let proto_endpoint: EndpointData = ProtoEndpoint {
        endpoint_identifier: provider_epr.clone(),
        scope: vec![],
        physical_address: vec![],
    }
    .into();

    let dpws_processor = DpwsDiscoveryProcessorImpl::new(dpws_endpoint, None);
    let disco: Option<DummyDpwsDiscoveryProxyProviderImpl> = None;
    let dpws_discovery =
        DpwsDiscoveryProviderImpl::new(dpws_binding, dpws_processor.clone(), disco);

    let proto_disco =
        DiscoveryProviderImpl::new(proto_binding, None, proto_endpoint.clone(), None).await?;

    Ok(DualStackDiscovery::new(
        dpws_discovery,
        dpws_processor,
        proto_disco,
        proto_endpoint,
    ))
}

pub async fn run_proto_provider<EHF>(
    config: &ProviderConfig,
    crypto: Option<CryptoConfig>,
    mdib: Mdib,
    localized_texts: Option<Vec<InputText>>,
    _extension_handler_factory: EHF,
    _disco: Option<String>, // TODO: add discovery proxy
) -> anyhow::Result<()>
where
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    let addr = SocketAddr::from_str(&format!("{}:{}", config.address, config.port))?;

    let server_config = ServerConfig {
        crypto_config: crypto,
        server_address: addr,
    };

    let sequence = "prefix:seq";

    let localization_storage: Option<LocalizationStorageImpl> =
        localized_texts.map(LocalizationStorageImpl::new);

    let mds1 = "mds_1";

    let scopes_updater = ScopesPlugin::default();
    let alert_system_updater = AlertSystemUpdater::new(
        // ignore mds1
        HashSet::from([mds1.to_string()]),
    );
    let waveform_updater = WaveformUpdater::default();
    let component_updater = ComponentUpdater::new(
        // ignore mds1
        HashSet::from([mds1.to_string()]),
    );
    let operation_updater = OperationUpdater::new(
        // ignore mds1
        HashSet::from([mds1.to_string()]),
    );
    let descriptor_updater = DescriptionUpdater::default();
    let numeric_metric_updater = NumericMetricUpdater::new(
        // ignore mds1
        HashSet::from([mds1.to_string()]),
    );
    let string_metric_updater = StringMetricUpdater::new(
        // ignore mds1
        HashSet::from([mds1.to_string()]),
    );

    let plugin_chain = proto_chain_plugins!(
        scopes_updater,
        ProtoSdcProviderPluginWrapper::from(alert_system_updater),
        ProtoSdcProviderPluginWrapper::from(waveform_updater),
        ProtoSdcProviderPluginWrapper::from(component_updater),
        ProtoSdcProviderPluginWrapper::from(operation_updater),
        ProtoSdcProviderPluginWrapper::from(descriptor_updater),
        ProtoSdcProviderPluginWrapper::from(numeric_metric_updater),
        ProtoSdcProviderPluginWrapper::from(string_metric_updater)
    );

    let provider = ProtoSdcDevice::new(
        sequence.to_string(),
        server_config,
        ExampleProviderOperationInvocationReceiver {},
        config.epr.clone(),
        Some(
            DeviceMetadata::builder()
                .friendly_name(vec![LocalizedString::builder()
                    .value("protosdc-rs referencetest provider")
                    .locale("en-US")
                    .build()])
                .manufacturer_url("https://protosdc.org")
                .build(),
        ),
        None,
        localization_storage,
        plugin_chain,
    )
    .await?;

    run_provider(provider, mdib).await
}

pub async fn run_dpws_provider<EHF>(
    config: &ProviderConfig,
    crypto: Option<CryptoConfig>,
    mdib: Mdib,
    localized_texts: Option<Vec<InputText>>,
    extension_handler_factory: EHF,
    discovery_proxy_address: Option<String>,
) -> anyhow::Result<()>
where
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    let device_settings: DeviceSettings<EHF> = DeviceSettings::builder()
        .extension_handler_factory(extension_handler_factory)
        .network_interface(SocketAddr::from((
            IpAddr::from_str(&config.address)?,
            config.port,
        )))
        .crypto(crypto.clone())
        .endpoint_reference(
            WsaEndpointReference::builder()
                .address(config.epr.clone())
                .build(),
        )
        .types(HashSet::from_iter(vec![
            MDPWS_MEDICAL_DEVICE_TYPE.into(),
            DPWS_DEVICE_TYPE.into(),
        ]))
        .discovery_proxy_address(discovery_proxy_address)
        .build();

    let mds1 = "mds_1";

    let scopes_updater = TypesScopesPlugin::default();
    let alert_system_updater = AlertSystemUpdater::new(
        // ignore mds1
        HashSet::from([mds1.to_string()]),
    );
    let waveform_updater = WaveformUpdater::default();
    let component_updater = ComponentUpdater::new(
        // ignore mds1
        HashSet::from([mds1.to_string()]),
    );
    let operation_updater = OperationUpdater::new(
        // ignore mds1
        HashSet::from([mds1.to_string()]),
    );
    let descriptor_updater = DescriptionUpdater::default();
    let numeric_metric_updater = NumericMetricUpdater::new(
        // ignore mds1
        HashSet::from([mds1.to_string()]),
    );
    let string_metric_updater = StringMetricUpdater::new(
        // ignore mds1
        HashSet::from([mds1.to_string()]),
    );

    let plugin_chain = chain_plugins!(
        scopes_updater,
        DpwsSdcProviderPluginWrapper::from(alert_system_updater),
        DpwsSdcProviderPluginWrapper::from(waveform_updater),
        DpwsSdcProviderPluginWrapper::from(component_updater),
        DpwsSdcProviderPluginWrapper::from(operation_updater),
        DpwsSdcProviderPluginWrapper::from(descriptor_updater),
        DpwsSdcProviderPluginWrapper::from(numeric_metric_updater),
        DpwsSdcProviderPluginWrapper::from(string_metric_updater)
    );

    let localization_storage: Option<LocalizationStorageImpl> =
        localized_texts.map(LocalizationStorageImpl::new);

    let provider = SdcDevice::new(
        ExampleProviderOperationInvocationReceiver {},
        device_settings,
        plugin_chain,
        localization_storage,
    )
    .await
    .expect("Could not initialize provider");

    run_provider(provider, mdib).await
}
