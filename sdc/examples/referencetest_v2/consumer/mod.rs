use std::cmp;
use std::collections::HashMap;
use std::net::{IpAddr, SocketAddr};
use std::str::FromStr;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::time::{Duration, Instant};

use futures::{future, stream, FutureExt, StreamExt};
use itertools::Itertools;
use log::{debug, error, info, trace, warn};
use protosdc_biceps::biceps::abstract_metric_value_mod::MetricQuality;
use protosdc_biceps::biceps::extension_mod::Item;
use protosdc_biceps::biceps::instance_identifier_mod::{ExtensionAttr, RootAttr};
use protosdc_biceps::biceps::{
    context_association_mod, invocation_state_mod, measurement_validity_mod, operating_mode_mod,
    AbstractAlertStateOneOf, AbstractContextStateOneOf, AbstractDeviceComponentStateOneOf,
    AbstractGet, AbstractMetricStateOneOf, AbstractMetricValue, AbstractOperationStateOneOf,
    AbstractSet, AbstractSetResponseOneOf, CauseInfo, CodedValue, ContextAssociation, Extension,
    GetSupportedLanguages, Handle, HandleRef, InstanceIdentifier, InstanceIdentifierOneOf,
    InvocationState, MeasurementValidity, NumericMetricValue, PatientContextState, SetContextState,
    SetContextStateOperationDescriptor, SetMetricState, SetMetricStateOperationDescriptor,
    SetString, SetValue, Timestamp,
};
use protosdc_biceps::extension::sdpi::{gender_type_mod, Gender, GenderType};
use protosdc_biceps::types::{ProtoAny, ProtoDecimal};
use protosdc_proto::discovery::Endpoint;
use rand::Rng;
use rust_decimal::Decimal;
use strum::IntoEnumIterator;
use strum_macros::{Display, EnumIter};
use tokio::sync::{broadcast, Mutex};
use tokio::task::JoinSet;
use tokio::time::timeout;
use tokio_stream::wrappers::{BroadcastStream, ReceiverStream};

use crate::ConsumerConfig;
use biceps::common::access::mdib_access::MdibAccessEvent;
use biceps::common::biceps_util::{ContextState, Descriptor, MetricState, MultiState, State};
use biceps::common::mdib_entity::{Entity, EntityBase, NumericMetric};
use biceps::common::mdib_version::MdibVersion;
use biceps::consumer::access::remote_mdib_access::RemoteMdibAccess;
use biceps::entity_filter;
use biceps::test_util::mdib_utils::create_minimal_patient_context_state;
use common::crypto::CryptoConfig;
use dpws::common::extension_handling::{
    ExtensionHandlerFactory, ReplacementExtensionHandlerFactory,
};
use dpws::consumer::sco::DpwsSetServiceHandler;
use dpws::consumer::sdc_consumer::{ConsumerConfigBuilder, SdcRemoteDevice};
use dpws::consumer::sdc_remote_device_connector;
use dpws::consumer::sdc_remote_device_connector::{
    ConnectConfiguration, ALL_EPISODIC_AND_WAVEFORM_REPORTS,
};
use dpws::discovery::constants::WS_DISCOVERY_PORT;
use dpws::discovery::consumer::discovery_consumer::{
    DpwsDiscoveryConsumer, DpwsDiscoveryConsumerImpl, DpwsDiscoveryError, DpwsDiscoveryEvent,
    ProbeOptions,
};
use dpws::discovery::consumer::discovery_proxy_consumer::{
    DpwsDiscoveryProxyConsumer, DpwsDiscoveryProxyConsumerImpl,
};
use dpws::http::client::reqwest::{ReqwestClientAccess, ReqwestHttpClient};
use dpws::http::server::axum::{AxumRegistry, AxumRegistryAccess};
use dpws::soap::dpws::client::consumer::{
    Consumer, ConsumerHostedService, ConsumerHostingService, ConsumerImpl, ReqwestSoapClient,
};
use dpws::soap::dpws::client::hosting_service::HostingService;
use dpws::soap::soap_client::SoapClientImpl;
use dpws::xml::messages::addressing::{WsaAttributedURIType, WsaEndpointReference};
use dpws::xml::messages::common::MDPWS_MEDICAL_DEVICE_TYPE;
use dpws::xml::messages::discovery::{ProbeMatch, ProbeMatches};
use network::udp_binding::{
    create_udp_binding, UdpBinding, UdpBindingImpl, PROTOSDC_MULTICAST_PORT,
};
use protosdc::consumer::grpc_consumer::{
    Consumer as ProtoConsumer, ConsumerImpl as ProtoConsumerImpl,
};
use protosdc::consumer::remote_device::DefaultRemoteMdibAccessImpl;
use protosdc::consumer::remote_device_connector;
use protosdc::consumer::sco::sco_controller::ScoController;
use protosdc::consumer::sco::sco_transaction::{ScoTransaction, ScoTransactionImpl};
use protosdc::discovery::consumer::discovery_consumer::{
    DiscoveryConsumer, DiscoveryConsumerImpl, DiscoveryEvent,
};
use sdc::consumer::sdc_consumer::SdcConsumer;

const SET_STRING_VALUE_PREFIX: &str = "protosdc-rs-dpws was here";

#[derive(Debug, Display, PartialEq, Eq, Hash, EnumIter, Clone)]
enum ReferenceTestStep {
    // The Reference Provider sends Hello messages
    Test1a,
    // The Reference Provider answers to Probe and Resolve messages
    Test1b,
    // The Reference Provider answers to TransferGet
    Test2a,
    // The SDCri Reference Provider grants subscription runtime of at most 15 seconds in order to enforce Reference Consumers to send renew requests
    Test2b,
    // The Reference Provider answers to GetMdib
    Test3a,
    // The Reference Provider answers to GetContextStates messages
    //     - The Reference Provider provides at least one location context state
    Test3b,
    // The Reference Provider produces at least 5 numeric metric updates in 30 seconds
    Test4a,
    // The Reference Provider produces at least 5 string metric updates in 30 seconds
    Test4b,
    // The Reference Provider produces at least 5 alert condition updates in 30 seconds
    Test4c,
    // The Reference Provider produces at least 5 alert signal updates in 30 seconds
    Test4d,
    // The Reference Provider provides alert system self checks in accordance to the periodicity defined in the MDIB (at least every 10 seconds)
    Test4e,
    // The Reference Provider provides 3 waveforms x 10 messages per second x 100 samples per message
    Test4f,
    // The Reference Provider provides changes for the following components:
    //     - Clock/Battery object (Component report)
    //     - The Reference Provider provides changes for the VMD/MDS (Component report)
    Test4g,
    // The Reference Provider provides changes for the following operational states:
    //     - Enable/Disable operations (some different than the ones mentioned above) (Operational State Report)
    Test4h,
    // The Reference Provider produces at least 1 update every 10 seconds comprising
    //     - Update Alert condition concept description of type
    //     - Update Alert condition cause-remedy information
    //     - Update Unit of measure (metrics)
    Test5a,
    // The Reference Provider produces at least 1 insertion followed by a deletion every 10 seconds comprising
    //     - Insert a VMD including Channels including metrics
    //     - Remove the VMD
    Test5b,
    // SetContextState
    //     - Payload: 1 Patient Context
    //     - Context state is added to the MDIB including context association and validation
    //     - If there is an associated context already, that context shall be disassociated
    //         - Handle and version information is generated by the provider
    //     - In order to avoid infinite growth of patient contexts, older contexts are allowed to be removed from the MDIB (=ContextAssociation=No)
    Test6b,
    // SetValue: Immediately answers with "finished"
    //     - Finished has to be sent as a report in addition to the response =>
    Test6c,
    // SetString: Initiates a transaction that sends Wait, Start and Finished
    Test6d,
    //  SetMetricStates:
    //     - Payload: 2 Metric States (settings; consider alert limits)
    //     - Immediately sends finished
    //     - Action: Alter values of metrics
    Test6e,
    // Graceful shutdown (at least subscriptions are ended; optionally Bye is sent)
    Test7,
}

#[derive(Debug, PartialEq, Eq, Hash)]
enum TestStatus {
    Initial,
    Passed,
    Failed,
    Skipped { reason: String },
}

impl std::fmt::Display for TestStatus {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let txt = match self {
            TestStatus::Initial => "Initial".to_string(),
            TestStatus::Passed => "Passed".to_string(),
            TestStatus::Failed => "Failed".to_string(),
            TestStatus::Skipped { reason } => format!("Skipped: {reason}"),
        };
        write!(f, "{}", txt)
    }
}

#[derive(Debug)]
pub enum InsertDelete {
    Insert(Instant),
    Delete(Instant),
}

pub async fn find_insert_update_valid(
    locked: &HashMap<String, Vec<InsertDelete>>,
    max_difference: Duration,
) -> Vec<(String, usize)> {
    locked
        .iter()
        .map(|(handle, changes)| {
            (
                handle,
                changes
                    .windows(2)
                    .filter(|it| match (&it[0], &it[1]) {
                        (InsertDelete::Insert(ins), InsertDelete::Delete(del)) => {
                            del.duration_since(*ins) < max_difference
                        }
                        _ => false,
                    })
                    .count(),
            )
        })
        .filter(|it| it.1 > 0)
        .map(|it| (it.0.clone(), it.1))
        .collect_vec()
}

pub async fn collect_description_insert_delete<U>(
    wait_timeout: Duration,
    mdib: U,
) -> anyhow::Result<String>
where
    U: RemoteMdibAccess + Clone,
{
    let changes_by_handle: Arc<Mutex<HashMap<String, Vec<InsertDelete>>>> =
        Arc::new(Mutex::new(HashMap::new()));
    let collection_completed = Arc::new(AtomicBool::new(true));
    let mdib_sub = mdib.subscribe().await;
    info!("Starting description_insert_delete collection");

    let max_difference = Duration::from_secs(10);
    // - 1 for tolerance
    let min_changes: usize = (wait_timeout.as_secs() / max_difference.as_secs()) as usize - 1;

    let receiver_task = ReceiverStream::new(mdib_sub)
        .take_while(|_| future::ready(collection_completed.load(Ordering::Relaxed)))
        .for_each(|it| {
            let changes_by_handle_task = changes_by_handle.clone();
            let collection_completed_task = collection_completed.clone();
            async move {
                if let MdibAccessEvent::DescriptionModification {
                    inserted, deleted, ..
                } = &it
                {
                    debug!(
                        "DescriptionModification event! inserted: {} deleted: {} :)",
                        inserted.len(),
                        deleted.len()
                    );
                    let now = Instant::now();
                    let mut handle_map = changes_by_handle_task.lock().await;

                    {
                        let inserted_vmd = inserted
                            .iter()
                            .filter(|it| matches!(&it.entity, Entity::Vmd(..)))
                            .collect::<Vec<_>>();

                        for vmd in inserted_vmd.iter() {
                            // check that a channel was added with this as parent
                            let channels = inserted
                                .iter()
                                .filter(|it| {
                                    matches!(&it.entity, Entity::Channel(..))
                                        && it.resolve_parent() == Some(vmd.entity.handle())
                                })
                                .collect::<Vec<_>>();

                            let channels_have_metrics = channels.iter().any(|channel| {
                                inserted.iter().any(|ins| {
                                    ins.resolve_parent() == Some(channel.entity.handle())
                                })
                            });

                            if channels_have_metrics {
                                debug!(
                                    "Adding vmd handle {} as inserted with channels and metrics",
                                    vmd.entity.handle()
                                );
                                handle_map
                                    .entry(vmd.entity.handle())
                                    .or_insert(vec![InsertDelete::Insert(now)])
                                    .push(InsertDelete::Insert(now));
                            }
                        }
                    }
                    {
                        let deleted_vmd = deleted
                            .iter()
                            .filter(|it| matches!(&it.entity, Entity::Vmd(..)))
                            .collect::<Vec<_>>();

                        for vmd in deleted_vmd.iter() {
                            debug!("Adding vmd handle {} as deleted", vmd.entity.handle());
                            handle_map
                                .entry(vmd.entity.handle())
                                .or_insert(vec![InsertDelete::Delete(now)])
                                .push(InsertDelete::Delete(now));
                        }
                    }

                    if find_insert_update_valid(&handle_map, max_difference)
                        .await
                        .iter()
                        .map(|it| it.1)
                        .sum::<usize>()
                        >= min_changes
                    {
                        collection_completed_task.store(false, Ordering::Relaxed);
                    }
                }
            }
        });

    let _collected = timeout(wait_timeout, receiver_task).await;
    info!("Collecting description_insert_delete completed");

    let successful_handle = {
        let locked = changes_by_handle.lock().await;
        let updates = find_insert_update_valid(&locked, max_difference).await;

        if updates.iter().map(|it| it.1).sum::<usize>() >= min_changes {
            Some(
                updates
                    .into_iter()
                    .map(|(handle, changes)| format!("{}: {}", handle, changes))
                    .join(", "),
            )
        } else {
            None
        }
    };

    match successful_handle {
        None => {
            let locked = changes_by_handle.lock().await;

            error!("{:?}", locked);
            anyhow::bail!("Not enough updates seen")
        }
        Some(it) => Ok(it),
    }
}

pub async fn collect_metric_updates<U, F>(
    min_updates: i32,
    wait_timeout: Duration,
    mdib: U,
    filter: F,
) -> anyhow::Result<String>
where
    U: RemoteMdibAccess + Clone,
    F: 'static + Fn(&AbstractMetricStateOneOf) -> bool + Clone,
{
    let metric_reports_by_handle: Arc<Mutex<HashMap<String, i32>>> =
        Arc::new(Mutex::new(HashMap::new()));
    let collection_completed = Arc::new(AtomicBool::new(true));
    let mdib_sub = mdib.subscribe().await;
    info!("Starting metric collection");

    let receiver_task = ReceiverStream::new(mdib_sub)
        .take_while(|_| future::ready(collection_completed.load(Ordering::Relaxed)))
        .for_each(|it| {
            let collection_completed_task = collection_completed.clone();
            let metric_reports_by_handle_task = metric_reports_by_handle.clone();
            let f = filter.clone();
            async move {
                if let MdibAccessEvent::MetricStateModification { states, .. } = &it {
                    debug!("metric event! States: {:?} :)", states.len());

                    let mut handle_map = metric_reports_by_handle_task.lock().await;
                    states
                        .iter()
                        .flat_map(|(_, metric_states)| {
                            metric_states
                                .iter()
                                .filter(|it| f(it))
                                .map(|state| state.descriptor_handle())
                        })
                        .for_each(|handle| *handle_map.entry(handle).or_insert(0) += 1);

                    if let Some((handle, up)) = handle_map.iter().find(|it| it.1 >= &min_updates) {
                        info!("Handle {} had {} updates, done collecting", handle, up);
                        collection_completed_task
                            .clone()
                            .store(false, Ordering::Relaxed)
                    }
                };
            }
        });

    let _collected = timeout(wait_timeout, receiver_task).await;
    info!("Collecting metrics completed");

    {
        let successful_entry = {
            metric_reports_by_handle
                .lock()
                .await
                .iter()
                .find(|(_handle, count)| count >= &&min_updates)
                .map(|it| it.0.clone())
        };

        match successful_entry {
            None => {
                metric_reports_by_handle
                    .lock()
                    .await
                    .iter()
                    .for_each(|(handle, count)| warn!("{}: {}", handle, count));
                anyhow::bail!("Not enough updates seen")
            }
            Some(it) => Ok(it),
        }
    }
}

pub async fn collect_waveform_updates<U>(wait_timeout: Duration, mdib: U) -> anyhow::Result<String>
where
    U: RemoteMdibAccess + Clone,
{
    let waveforms_by_handle: Arc<Mutex<HashMap<String, Vec<Instant>>>> =
        Arc::new(Mutex::new(HashMap::new()));
    let collection_completed = Arc::new(AtomicBool::new(true));
    let mdib_sub = mdib.subscribe().await;
    info!("Starting waveform collection");

    let start = Instant::now();

    let receiver_task = ReceiverStream::new(mdib_sub)
        .take_while(|_| future::ready(collection_completed.load(Ordering::Relaxed)))
        .for_each(|it| {
            let metric_reports_by_handle_task = waveforms_by_handle.clone();
            async move {
                if let MdibAccessEvent::WaveformModification { states, .. } = &it {
                    debug!("waveform event! States: {:?}", states.len());

                    let mut handle_map = metric_reports_by_handle_task.lock().await;
                    states
                        .iter()
                        // only accept states with 100 samples
                        .filter(|state| {
                            state
                                .metric_value
                                .as_ref()
                                .map(|it| {
                                    it.samples_attr
                                        .as_ref()
                                        .map(|it| it.decimal.len() >= 100)
                                        .unwrap_or(false)
                                })
                                .unwrap_or(false)
                        })
                        .for_each(|handle| {
                            debug!("Handle {} has >= 100 samples", handle.descriptor_handle());
                            handle_map
                                .entry(handle.descriptor_handle())
                                .or_insert(vec![])
                                .push(Instant::now())
                        });
                };
            }
        });

    let _collected = timeout(wait_timeout, receiver_task).await;
    let stop = Instant::now();
    info!("Collecting metrics completed");

    // expected updates given passed time, allow 10% less per second
    let min_num_updates = stop.duration_since(start).as_secs() * 9;

    {
        let successful_entries = {
            waveforms_by_handle
                .lock()
                .await
                .iter()
                .filter(|(_handle, timestamps)| timestamps.len() as u64 >= min_num_updates)
                .map(|it| it.0.clone())
                .collect_vec()
        };

        if successful_entries.len() < 3 {
            warn!("Expected number of updates {}", min_num_updates);
            let handles = waveforms_by_handle
                .lock()
                .await
                .iter()
                .map(|it| format!("{}: {}", it.0, it.1.len()))
                .collect_vec();
            info!("Number of updates per handle {}", handles.join(" - "));
            waveforms_by_handle
                .lock()
                .await
                .iter()
                .for_each(|(handle, count)| warn!("{}: {}, {:?}", handle, count.len(), count));
            anyhow::bail!("Not enough updates seen")
        }

        Ok(successful_entries.iter().join(", "))
    }
}

pub async fn collect_alert_updates<U, F>(
    min_updates: i32,
    wait_timeout: Duration,
    mdib: U,
    filter: F,
) -> anyhow::Result<String>
where
    U: RemoteMdibAccess + Clone,
    F: 'static + Fn(&AbstractAlertStateOneOf) -> bool + Clone,
{
    let alert_reports_by_handle: Arc<Mutex<HashMap<String, i32>>> =
        Arc::new(Mutex::new(HashMap::new()));
    let collection_completed = Arc::new(AtomicBool::new(true));
    let mdib_sub = mdib.subscribe().await;

    info!("Starting alert collection");

    let receiver_task = ReceiverStream::new(mdib_sub)
        .take_while(|_| future::ready(collection_completed.load(Ordering::Relaxed)))
        .for_each(|it| {
            let collection_completed_task = collection_completed.clone();
            let metric_reports_by_handle_task = alert_reports_by_handle.clone();
            let f = filter.clone();
            async move {
                if let MdibAccessEvent::AlertStateModification { states, .. } = &it {
                    debug!("alert event! States: {:?} :)", states.len());

                    let mut handle_map = metric_reports_by_handle_task.lock().await;
                    states
                        .iter()
                        .flat_map(|(_, metric_states)| {
                            metric_states
                                .iter()
                                .filter(|it| f(it))
                                .map(|state| state.descriptor_handle())
                        })
                        .for_each(|handle| *handle_map.entry(handle).or_insert(0) += 1);

                    if handle_map.values().any(|it| it >= &min_updates) {
                        collection_completed_task
                            .clone()
                            .store(false, Ordering::Relaxed)
                    }
                };
            }
        });

    let _collected = timeout(wait_timeout, receiver_task).await;
    info!("Collecting alerts completed");

    {
        let successful_entry = {
            alert_reports_by_handle
                .lock()
                .await
                .iter()
                .find(|(_handle, count)| count >= &&min_updates)
                .map(|it| it.0.clone())
        };

        match successful_entry {
            None => {
                alert_reports_by_handle
                    .lock()
                    .await
                    .iter()
                    .for_each(|(handle, count)| warn!("{}: {}", handle, count));
                anyhow::bail!("Not enough updates seen")
            }
            Some(it) => Ok(it),
        }
    }
}

pub type AlertSystemMap = HashMap<String, Vec<(Instant, Option<Timestamp>)>>;

pub async fn check_alert_system_update_period<U>(
    min_updates: usize,
    max_allowed_difference: Duration,
    wait_timeout: Duration,
    mdib: U,
) -> anyhow::Result<String>
where
    U: RemoteMdibAccess + Clone,
{
    let alert_reports_by_handle: Arc<Mutex<AlertSystemMap>> = Arc::new(Mutex::new(HashMap::new()));
    let mdib_sub = mdib.subscribe().await;

    info!("Starting alert system updates collection");

    let receiver_task = ReceiverStream::new(mdib_sub).for_each(|it| {
        let alert_reports_by_handle_task = alert_reports_by_handle.clone();
        async move {
            if let MdibAccessEvent::AlertStateModification { states, .. } = &it {
                debug!("alert event! States: {:?} :)", states.len());
                let now = Instant::now();

                let mut handle_map = alert_reports_by_handle_task.lock().await;
                states
                    .iter()
                    .flat_map(|(_, metric_states)| {
                        metric_states
                            .iter()
                            .filter_map(|it| match it {
                                AbstractAlertStateOneOf::AlertSystemState(s) => Some(s),
                                _ => None,
                            })
                            .map(|state| {
                                (
                                    state.descriptor_handle(),
                                    state.last_self_check_attr.clone(),
                                )
                            })
                    })
                    .for_each(|(handle, last_self_check)| {
                        handle_map
                            .entry(handle)
                            .or_insert(vec![(now, last_self_check.clone())])
                            .push((now, last_self_check))
                    });
            };
        }
    });

    let _collected = timeout(wait_timeout, receiver_task).await;
    info!("Stopping alert system updates collection");

    {
        let locked = alert_reports_by_handle.lock().await;
        let successful_entry = locked.iter().find(|(handle, instants)| {
            if instants.len() >= min_updates {
                let filtered_instants = instants
                    .iter()
                    // remove not updated self checks
                    .dedup_by(|a, b| a.1 == b.1)
                    .collect::<Vec<_>>();

                let max_difference = filtered_instants
                    .windows(2)
                    .map(|data| data[1].0.duration_since(data[0].0))
                    .max();
                if let Some(d) = max_difference {
                    debug!("Max diff for {}: {}s", handle, d.as_secs());
                    trace!("{:?}", instants);
                    trace!("{:?}", filtered_instants);
                    d <= max_allowed_difference
                } else {
                    false
                }
            } else {
                false
            }
        });

        match successful_entry {
            None => {
                anyhow::bail!("Either not enough updates were seen or they violated the max allowed difference")
            }
            Some(it) => Ok(it.0.clone()),
        }
    }
}

pub async fn check_component_updates<U>(wait_timeout: Duration, mdib: U) -> anyhow::Result<String>
where
    U: RemoteMdibAccess + Clone,
{
    let saw_clock_battery_update: Arc<std::sync::Mutex<Option<String>>> =
        Arc::new(std::sync::Mutex::new(None));
    let saw_vmd_mds_update: Arc<std::sync::Mutex<Option<String>>> =
        Arc::new(std::sync::Mutex::new(None));

    let mdib_sub = mdib.subscribe().await;

    info!("Starting component update collection");

    let receiver_task = ReceiverStream::new(mdib_sub)
        .take_while(|_| async {
            saw_clock_battery_update.lock().unwrap().is_none()
                || saw_vmd_mds_update.lock().unwrap().is_none()
        })
        .for_each(|it| {
            let saw_clock_battery_update_task = saw_clock_battery_update.clone();
            let saw_vmd_mds_update_task = saw_vmd_mds_update.clone();
            async move {
                if let MdibAccessEvent::ComponentStateModification { states, .. } = &it {
                    debug!("component event! States: {:?} :)", states.len());
                    states.iter().for_each(|(_mds_handle, components)| {
                        components.iter().for_each(|component| match component {
                            AbstractDeviceComponentStateOneOf::BatteryState(it) => {
                                saw_clock_battery_update_task
                                    .lock()
                                    .unwrap()
                                    .replace(it.descriptor_handle());
                            }
                            AbstractDeviceComponentStateOneOf::ClockState(it) => {
                                saw_clock_battery_update_task
                                    .lock()
                                    .unwrap()
                                    .replace(it.descriptor_handle());
                            }
                            AbstractDeviceComponentStateOneOf::VmdState(it) => {
                                saw_vmd_mds_update_task
                                    .lock()
                                    .unwrap()
                                    .replace(it.descriptor_handle());
                            }
                            AbstractDeviceComponentStateOneOf::MdsState(it) => {
                                saw_vmd_mds_update_task
                                    .lock()
                                    .unwrap()
                                    .replace(it.descriptor_handle());
                            }
                            _ => {}
                        })
                    });
                };
            }
        });

    let _collected = timeout(wait_timeout, receiver_task).await;
    info!("Stopping component update collection");

    let clock_battery = saw_clock_battery_update.lock().unwrap().take();
    let vmd_mds = saw_vmd_mds_update.lock().unwrap().take();

    match (clock_battery, vmd_mds) {
        (Some(clock_battery), Some(vmd_mds)) => Ok(format!(
            "clock/battery: {} - vmd/mds: {}",
            clock_battery, vmd_mds
        )),
        other => anyhow::bail!(format!("Not enough updates were seen. {:?}", other)),
    }
}

pub async fn check_operation_enabled_disabled<U>(
    wait_timeout: Duration,
    mdib: U,
) -> anyhow::Result<String>
where
    U: RemoteMdibAccess + Clone,
{
    let operating_mode_by_handle: Arc<Mutex<HashMap<String, Vec<operating_mode_mod::EnumType>>>> =
        Arc::new(Mutex::new(HashMap::new()));
    let mdib_sub = mdib.subscribe().await;

    info!("Starting operation updates collection");

    let receiver_task = ReceiverStream::new(mdib_sub).for_each(|it| {
        let operating_mode_by_handle_task = operating_mode_by_handle.clone();
        async move {
            if let MdibAccessEvent::OperationStateModification { states, .. } = &it {
                debug!("operation event! States: {:?} :)", states.len());

                let mut handle_map = operating_mode_by_handle_task.lock().await;
                states
                    .iter()
                    .flat_map(|(_, metric_states)| {
                        metric_states.iter().filter_map(|it| match it {
                            AbstractOperationStateOneOf::ActivateOperationState(s) => Some((
                                it.descriptor_handle(),
                                s.abstract_operation_state
                                    .operating_mode_attr
                                    .enum_type
                                    .clone(),
                            )),
                            AbstractOperationStateOneOf::SetAlertStateOperationState(s) => Some((
                                it.descriptor_handle(),
                                s.abstract_operation_state
                                    .operating_mode_attr
                                    .enum_type
                                    .clone(),
                            )),
                            AbstractOperationStateOneOf::SetComponentStateOperationState(s) => {
                                Some((
                                    it.descriptor_handle(),
                                    s.abstract_operation_state
                                        .operating_mode_attr
                                        .enum_type
                                        .clone(),
                                ))
                            }
                            AbstractOperationStateOneOf::SetContextStateOperationState(s) => {
                                Some((
                                    it.descriptor_handle(),
                                    s.abstract_operation_state
                                        .operating_mode_attr
                                        .enum_type
                                        .clone(),
                                ))
                            }
                            AbstractOperationStateOneOf::SetMetricStateOperationState(s) => Some((
                                it.descriptor_handle(),
                                s.abstract_operation_state
                                    .operating_mode_attr
                                    .enum_type
                                    .clone(),
                            )),
                            AbstractOperationStateOneOf::SetStringOperationState(s) => Some((
                                it.descriptor_handle(),
                                s.abstract_operation_state
                                    .operating_mode_attr
                                    .enum_type
                                    .clone(),
                            )),
                            AbstractOperationStateOneOf::SetValueOperationState(s) => Some((
                                it.descriptor_handle(),
                                s.abstract_operation_state
                                    .operating_mode_attr
                                    .enum_type
                                    .clone(),
                            )),
                            _ => None,
                        })
                    })
                    .for_each(|(handle, mode)| {
                        handle_map
                            .entry(handle)
                            .or_insert(vec![mode.clone()])
                            .push(mode)
                    });
            };
        }
    });

    let _collected = timeout(wait_timeout, receiver_task).await;
    info!("Stopping operation updates collection");

    {
        let locked = operating_mode_by_handle.lock().await;
        let successful_entry = locked
            .iter()
            .find(|(_handle, instants)| instants.windows(2).any(|it| it[0] != it[1]));

        match successful_entry {
            None => {
                anyhow::bail!("Either not enough updates were seen or they violated the max allowed difference")
            }
            Some(it) => Ok(it.0.clone()),
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct DescriptionAlertUpdate {
    handle: String,
    alert_type: Option<CodedValue>,
    alert_cause_info: Vec<CauseInfo>,
}

#[derive(Clone, Debug, PartialEq)]
pub struct DescriptionMetricUpdate {
    handle: String,
    metric_unit: CodedValue,
}

#[derive(Clone, Debug, PartialEq)]
pub struct DescriptionUpdateCombo {
    time: Instant,
    alert_handle: String,
    alert_type: Option<CodedValue>,
    alert_cause_info: Vec<CauseInfo>,
    metric_handle: String,
    metric_unit: CodedValue,
}

impl DescriptionUpdateCombo {
    fn from(
        time: Instant,
        alert: &DescriptionAlertUpdate,
        metric: &DescriptionMetricUpdate,
    ) -> Self {
        Self {
            time,
            alert_handle: alert.handle.clone(),
            alert_type: alert.alert_type.clone(),
            alert_cause_info: alert.alert_cause_info.clone(),
            metric_handle: metric.handle.clone(),
            metric_unit: metric.metric_unit.clone(),
        }
    }

    fn matches(&self, other: &Self) -> bool {
        self.alert_handle == other.alert_handle && self.metric_handle == other.metric_handle
    }

    fn has_required_changes(&self, other: &Self) -> bool {
        self.metric_unit != other.metric_unit
            && self.alert_type != other.alert_type
            && self.alert_cause_info != other.alert_cause_info
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct DescriptionUpdate {
    time: Instant,
    alert_updates: Vec<DescriptionAlertUpdate>,
    metric_updates: Vec<DescriptionMetricUpdate>,
}

pub async fn check_description_updates<U>(wait_timeout: Duration, mdib: U) -> anyhow::Result<String>
where
    U: RemoteMdibAccess + Clone,
{
    let description_updates: Arc<Mutex<Vec<DescriptionUpdateCombo>>> =
        Arc::new(Mutex::new(Vec::new()));
    let mdib_sub = mdib.subscribe().await;
    let description_updates_task_handle = description_updates.clone();

    info!("Starting description updates collection");
    let receiver_task = ReceiverStream::new(mdib_sub).for_each(|it| {
        let description_updates_task = description_updates_task_handle.clone();
        async move {
            if let MdibAccessEvent::DescriptionModification { updated, .. } = &it {
                info!("description event! States: {:?} :)", updated.len());
                let now = Instant::now();

                let mut update_vec = description_updates_task.lock().await;

                let alert_updates = updated
                    .iter()
                    .filter_map(|it| match &it.entity {
                        Entity::AlertCondition(it) => Some(it.pair.descriptor.clone()),
                        Entity::LimitAlertCondition(it) => {
                            Some(it.pair.descriptor.alert_condition_descriptor.clone())
                        }
                        _ => None,
                    })
                    .map(|it| DescriptionAlertUpdate {
                        handle: it.handle(),
                        alert_type: it
                            .abstract_alert_descriptor
                            .abstract_descriptor
                            .r#type
                            .clone(),
                        alert_cause_info: it.cause_info,
                    })
                    .collect::<Vec<_>>();

                let metric_updates = updated
                    .iter()
                    .filter_map(|it| match &it.entity {
                        Entity::StringMetric(it) => {
                            Some(it.pair.descriptor.abstract_metric_descriptor.clone())
                        }
                        Entity::EnumStringMetric(it) => Some(
                            it.pair
                                .descriptor
                                .string_metric_descriptor
                                .abstract_metric_descriptor
                                .clone(),
                        ),
                        Entity::NumericMetric(it) => {
                            Some(it.pair.descriptor.abstract_metric_descriptor.clone())
                        }
                        Entity::RealTimeSampleArrayMetric(it) => {
                            Some(it.pair.descriptor.abstract_metric_descriptor.clone())
                        }
                        Entity::DistributionSampleArrayMetric(it) => {
                            Some(it.pair.descriptor.abstract_metric_descriptor.clone())
                        }
                        _ => None,
                    })
                    .map(|it| DescriptionMetricUpdate {
                        handle: it.handle(),
                        metric_unit: it.unit,
                    })
                    .collect::<Vec<_>>();

                let merged = alert_updates
                    .into_iter()
                    .flat_map(|alert| {
                        metric_updates
                            .iter()
                            .map(move |metric| DescriptionUpdateCombo::from(now, &alert, metric))
                    })
                    .collect_vec();

                update_vec.extend(merged)
            };
        }
    });

    let _collected = timeout(wait_timeout, receiver_task).await;
    info!("Stopping description updates collection");

    let matching_changes = {
        let mut locked = description_updates.lock().await;

        // find combination that holds
        loop {
            if locked.is_empty() {
                break None;
            }
            let first = locked.remove(0);

            // find all identical combinations
            let mut matching_elements = locked
                .iter_mut()
                .filter(|it| first.matches(it))
                .map(|it| it.clone())
                .collect_vec();

            // drop matching from locked
            locked.retain(|it| !matching_elements.contains(it));

            // check if matching and first together fulfill the requirement
            matching_elements.insert(0, first);

            // remove
            let matching_changes = matching_elements
                .windows(2)
                .filter_map(|it| {
                    let a = &it[0];
                    let b = &it[1];
                    match a.has_required_changes(b) {
                        true => Some((a.clone(), b.clone())),
                        false => None,
                    }
                })
                .collect::<Vec<_>>();

            if matching_changes.len() >= 3 {
                break Some(matching_changes);
            }
        }
    };

    match matching_changes {
        None => {
            anyhow::bail!("Not enough description updates were seen")
        }
        Some(changes) => Ok(changes
            .into_iter()
            .map(|it| format!("Valid change from\n{:#?}\nto\n{:#?}", it.0, it.1))
            .join(" - ")),
    }
}

async fn run_set_value<CONSUMER, T, MDIB>(
    consumer: &mut CONSUMER,
    handle: &str,
    value: Decimal,
) -> bool
where
    CONSUMER: SdcConsumer<MDIB, T>,
    T: ScoTransaction,
    MDIB: RemoteMdibAccess + Clone + Send + Sync,
{
    let call_result = consumer
        .set_value(SetValue {
            abstract_set: AbstractSet {
                extension_element: None,
                operation_handle_ref: HandleRef {
                    string: handle.to_string(),
                },
            },
            requested_numeric_value: ProtoDecimal::from(value),
        })
        .await;

    let call = match call_result {
        Ok(it) => it,
        Err(err) => {
            error!("{:?}", err);
            return false;
        }
    };

    let immediate_response = call.response();

    match immediate_response {
        AbstractSetResponseOneOf::SetValueResponse(response) => {
            let is_fin = matches!(
                response
                    .abstract_set_response
                    .invocation_info
                    .invocation_state
                    .enum_type,
                invocation_state_mod::EnumType::Fin
            );
            if !is_fin {
                error!(
                    "set value finished with {:?}",
                    response
                        .abstract_set_response
                        .invocation_info
                        .invocation_state
                        .enum_type
                );
                error!(
                    "err {:?}",
                    response
                        .abstract_set_response
                        .invocation_info
                        .invocation_error
                );
                error!(
                    "msg {:?}",
                    response
                        .abstract_set_response
                        .invocation_info
                        .invocation_error_message
                );
            }
            is_fin
        }
        _ => false,
    }
}

async fn run_set_string<CONSUMER, T, MDIB>(
    consumer: &mut CONSUMER,
    operation_handle: &str,
    value: String,
) -> bool
where
    CONSUMER: SdcConsumer<MDIB, T>,
    T: ScoTransaction,
    MDIB: RemoteMdibAccess + Clone + Send + Sync,
{
    let call_result = consumer
        .set_string(SetString {
            abstract_set: AbstractSet {
                extension_element: None,
                operation_handle_ref: HandleRef {
                    string: operation_handle.to_string(),
                },
            },
            requested_string_value: value,
        })
        .await;

    let call = match call_result {
        Ok(it) => it,
        Err(err) => {
            error!("{:?}", err);
            return false;
        }
    };

    let reports = call.wait_for_final_report().await;

    if reports.len() != 3 {
        warn!("Did not receive 3 reports for {}", operation_handle);
        return false;
    }

    if reports[0]
        .report_part
        .invocation_info
        .invocation_state
        .enum_type
        != invocation_state_mod::EnumType::Wait
    {
        warn!(
            "First report for {} was not Wait, was {:?}",
            operation_handle,
            reports[0]
                .report_part
                .invocation_info
                .invocation_state
                .enum_type
        );
        return false;
    }

    if reports[1]
        .report_part
        .invocation_info
        .invocation_state
        .enum_type
        != invocation_state_mod::EnumType::Start
    {
        warn!(
            "Second report for {} was not Start, was {:?}",
            operation_handle,
            reports[1]
                .report_part
                .invocation_info
                .invocation_state
                .enum_type
        );
        return false;
    }

    if reports[2]
        .report_part
        .invocation_info
        .invocation_state
        .enum_type
        != invocation_state_mod::EnumType::Fin
    {
        warn!(
            "Third report for {} was not Fin, was {:?}",
            operation_handle,
            reports[2]
                .report_part
                .invocation_info
                .invocation_state
                .enum_type
        );
        return false;
    }

    true
}

async fn run_set_context_state<CONSUMER, T, MDIB>(
    consumer: &mut CONSUMER,
    handle: &str,
    value: AbstractContextStateOneOf,
) -> Option<MdibVersion>
where
    CONSUMER: SdcConsumer<MDIB, T>,
    T: ScoTransaction,
    MDIB: RemoteMdibAccess + Clone + Send + Sync,
{
    let call_result = consumer
        .set_context_state(SetContextState {
            abstract_set: AbstractSet {
                extension_element: None,
                operation_handle_ref: HandleRef {
                    string: handle.to_string(),
                },
            },
            proposed_context_state: vec![value],
        })
        .await;

    // info!("Call_result {:?}", call_result);

    let call = match call_result {
        Ok(it) => it,
        Err(err) => {
            error!("{:?}", err);
            return None;
        }
    };

    let reports = call.wait_for_final_report().await;

    match reports.last() {
        None => None,
        Some(r) => {
            let success = r.report_part.invocation_info.invocation_state
                == InvocationState {
                    enum_type: invocation_state_mod::EnumType::Fin,
                }
                || r.report_part.invocation_info.invocation_state
                    == InvocationState {
                        enum_type: invocation_state_mod::EnumType::FinMod,
                    };

            if success {
                Some(r.mdib_version.clone())
            } else {
                None
            }
        }
    }
}

async fn run_set_metric_state<CONSUMER, T, MDIB>(
    consumer: &mut CONSUMER,
    handle: &str,
    values: Vec<AbstractMetricStateOneOf>,
) -> bool
where
    CONSUMER: SdcConsumer<MDIB, T>,
    T: ScoTransaction,
    MDIB: RemoteMdibAccess + Clone + Send + Sync,
{
    info!("Invocation of {} with {:?}", handle, values);
    // subscribe to observe for change
    let subscription = consumer.remote_mdib().await.subscribe().await;

    let change = timeout(Duration::from_secs(5), {
        let expected_states = values.clone();
        async move {
            ReceiverStream::new(subscription)
                .filter_map(move |change| match change {
                    MdibAccessEvent::MetricStateModification { states, .. } => {
                        expected_states.iter().all(|expected_state| {
                            states.values().any(|new_states| {
                                new_states.iter().any(|new_state| {
                                    match (expected_state, new_state) {
                                        (
                                            AbstractMetricStateOneOf::StringMetricState(ref string),
                                            AbstractMetricStateOneOf::StringMetricState(
                                                ref new_string,
                                            ),
                                        ) => {
                                            string.descriptor_handle()
                                                == new_string.descriptor_handle()
                                                && string.metric_value == new_string.metric_value
                                        }
                                        (
                                            AbstractMetricStateOneOf::NumericMetricState(
                                                ref numeric,
                                            ),
                                            AbstractMetricStateOneOf::NumericMetricState(
                                                ref new_numeric,
                                            ),
                                        ) => {
                                            let res = numeric.descriptor_handle()
                                                == new_numeric.descriptor_handle()
                                                && numeric.metric_value == new_numeric.metric_value;
                                            info!(
                                                "Matching {} and {}: {}",
                                                numeric.descriptor_handle(),
                                                new_numeric.descriptor_handle(),
                                                res
                                            );
                                            if numeric.descriptor_handle()
                                                == new_numeric.descriptor_handle()
                                            {
                                                debug!("Old {:#?}", numeric.metric_value);
                                                debug!("New {:#?}", new_numeric.metric_value);
                                            }
                                            res
                                        }
                                        _ => false,
                                    }
                                })
                            })
                        });
                        future::ready(Some(states))
                    }
                    _ => future::ready(None),
                })
                .next()
                .await
        }
    });

    let call_result = consumer
        .set_metric_state(SetMetricState {
            abstract_set: AbstractSet {
                extension_element: None,
                operation_handle_ref: HandleRef {
                    string: handle.to_string(),
                },
            },
            proposed_metric_state: values,
        })
        .await;

    let call = match call_result {
        Ok(it) => it,
        Err(err) => {
            error!("{:?}", err);
            return false;
        }
    };

    let reports = call.wait_for_final_report().await;

    let mdib_change = change.await;

    match reports.last() {
        None => false,
        Some(r) => {
            (r.report_part.invocation_info.invocation_state.enum_type
                == invocation_state_mod::EnumType::Fin
                || r.report_part.invocation_info.invocation_state.enum_type
                    == invocation_state_mod::EnumType::FinMod)
                && mdib_change.is_ok()
                && mdib_change.unwrap().is_some()
        }
    }
}

async fn execute_set_metric_state<CONSUMER, T, MDIB>(consumer: &mut CONSUMER) -> TestStatus
where
    CONSUMER: SdcConsumer<MDIB, T>,
    T: ScoTransaction,
    MDIB: RemoteMdibAccess + Clone + Send + Sync,
{
    let mut rnd = rand::thread_rng();

    let set_metric_state_ops = {
        consumer
            .remote_mdib()
            .await
            .entities_by_type(entity_filter!(SetMetricStateOperation))
            .await
    };
    if set_metric_state_ops.is_empty() {
        warn!(
            "{:?}",
            consumer
                .remote_mdib()
                .await
                .entity("set_metric_0.sco.vmd_1.mds_0")
                .await
        );

        TestStatus::Skipped {
            reason: "No set metric state operation found".to_string(),
        }
    } else {
        // check target is a channel with exactly two numeric metrics
        let mdib = consumer.remote_mdib().await;
        let applicable_metrics = stream::iter(set_metric_state_ops)
            .filter_map(|op| async {
                match op.entity {
                    Entity::SetMetricStateOperation(x) => Some(x.pair.descriptor),
                    _ => None,
                }
            })
            .filter_map(|op: SetMetricStateOperationDescriptor| {
                let filter_map_mdib = mdib.clone();
                async move {
                    let handle = op
                        .abstract_set_state_operation_descriptor
                        .abstract_operation_descriptor
                        .operation_target_attr
                        .string
                        .clone();
                    let target_entity = filter_map_mdib.entity(handle.as_str()).await;
                    match target_entity {
                        Some(ent) => match ent.entity {
                            Entity::Channel(channel) => {
                                let collected = stream::iter(channel.children)
                                    .filter_map(|child_handle| {
                                        let inner_mdib = filter_map_mdib.clone();
                                        async move {
                                            let target_entity =
                                                inner_mdib.entity(child_handle.as_str()).await;

                                            match target_entity.map(|it| it.entity) {
                                                Some(Entity::NumericMetric(numeric)) => {
                                                    Some(*numeric)
                                                }
                                                _ => None,
                                            }
                                        }
                                    })
                                    .collect::<Vec<NumericMetric>>()
                                    .await;

                                if collected.len() == 2 {
                                    Some((op.handle(), collected))
                                } else {
                                    None
                                }
                            }
                            _ => None,
                        },
                        None => None,
                    }
                }
            })
            .boxed()
            .next()
            .await;

        let (operation_handle, mut target_metric_1, mut target_metric_2): (
            String,
            NumericMetric,
            NumericMetric,
        ) = match applicable_metrics {
            Some((operation_handle, mut target_metrics)) => (
                operation_handle,
                target_metrics.pop().unwrap(),
                target_metrics.pop().unwrap(),
            ),
            None => {
                return TestStatus::Skipped {
                    reason: "No suitable combination of numeric metrics found".to_string(),
                };
            }
        };

        let target_value = rnd.gen_range(0..1000);

        target_metric_1.pair.state.metric_value = Some(NumericMetricValue {
            abstract_metric_value: AbstractMetricValue {
                extension_element: None,
                metric_quality: MetricQuality {
                    extension_element: None,
                    validity_attr: MeasurementValidity {
                        enum_type: measurement_validity_mod::EnumType::Qst,
                    },
                    mode_attr: None,
                    qi_attr: None,
                },
                annotation: vec![],
                start_time_attr: None,
                stop_time_attr: None,
                determination_time_attr: None,
            },
            value_attr: Some(target_value.into()),
        });

        target_metric_2.pair.state.metric_value = Some(NumericMetricValue {
            abstract_metric_value: AbstractMetricValue {
                extension_element: None,
                metric_quality: MetricQuality {
                    extension_element: None,
                    validity_attr: MeasurementValidity {
                        enum_type: measurement_validity_mod::EnumType::Qst,
                    },
                    mode_attr: None,
                    qi_attr: None,
                },
                annotation: vec![],
                start_time_attr: None,
                stop_time_attr: None,
                determination_time_attr: None,
            },
            value_attr: Some(target_value.into()),
        });

        let success = timeout(
            Duration::from_secs(10),
            run_set_metric_state(
                consumer,
                operation_handle.as_str(),
                vec![
                    target_metric_1
                        .pair
                        .state
                        .into_abstract_metric_state_one_of(),
                    target_metric_2
                        .pair
                        .state
                        .into_abstract_metric_state_one_of(),
                ],
            ),
        )
        .await
        .unwrap_or(false);

        match success {
            true => TestStatus::Passed,
            false => TestStatus::Failed,
        }
    }
}

async fn execute_set_context_state<CONSUMER, T, MDIB>(consumer: &mut CONSUMER) -> TestStatus
where
    CONSUMER: SdcConsumer<MDIB, T>,
    T: ScoTransaction,
    MDIB: RemoteMdibAccess + Clone + Send + Sync,
{
    let remote_mdib = consumer.remote_mdib().await;
    let set_context_state_ops = {
        remote_mdib
            .entities_by_type(entity_filter!(SetContextStateOperation))
            .await
    };
    if set_context_state_ops.is_empty() {
        TestStatus::Skipped {
            reason: "No set context state operation found".to_string(),
        }
    } else {
        let validator_to_set = InstanceIdentifier {
            extension_element: None,
            r#type: None,
            identifier_name: vec![],
            root_attr: Some(RootAttr {
                any_u_r_i: "https://protosdc.org"
                    .to_string()
                    .try_into()
                    .expect("Weird"),
            }),
            extension_attr: Some(ExtensionAttr {
                string: "trust_me_bro".to_string(),
            }),
        };

        let mut patient_context_to_set =
            create_minimal_patient_context_state("placeholder", "placeholder_state");
        patient_context_to_set
            .abstract_context_state
            .context_association_attr = Some(ContextAssociation {
            enum_type: context_association_mod::EnumType::Assoc,
        });
        patient_context_to_set.abstract_context_state.validator =
            vec![InstanceIdentifierOneOf::InstanceIdentifier(
                validator_to_set.clone(),
            )];
        // set the gender for funsies
        patient_context_to_set
            .abstract_context_state
            .abstract_multi_state
            .abstract_state
            .extension_element = Some(Extension {
            item: vec![Item {
                must_understand: false,
                extension_data: ProtoAny {
                    data: Box::new(Gender {
                        gender_type: GenderType {
                            enum_type: gender_type_mod::EnumType::Other,
                        },
                        must_understand_attr: None,
                    }),
                },
            }],
        });

        let mut set_context_state_success = vec![];

        for op in set_context_state_ops.into_iter() {
            let patient_context_to_set_inner = patient_context_to_set.clone();
            info!("Invoking set context state value {}", &op.entity.handle());
            if let Entity::SetContextStateOperation(it) = &op.entity {
                let desc: &SetContextStateOperationDescriptor = &it.pair.descriptor;
                let target_descriptor_handle = &desc
                    .abstract_set_state_operation_descriptor
                    .abstract_operation_descriptor
                    .operation_target_attr
                    .string;

                let mut cool_patient_context: PatientContextState =
                    patient_context_to_set_inner.clone();
                cool_patient_context
                    .abstract_context_state
                    .abstract_multi_state
                    .abstract_state
                    .descriptor_handle_attr = HandleRef {
                    string: target_descriptor_handle.clone(),
                };
                cool_patient_context
                    .abstract_context_state
                    .abstract_multi_state
                    .handle_attr = Handle {
                    string: target_descriptor_handle.clone(),
                };

                info!(
                    "cool_patient_context ext: {:?}",
                    cool_patient_context
                        .abstract_context_state
                        .abstract_multi_state
                        .abstract_state
                        .extension_element
                );

                let result = timeout(
                    Duration::from_secs(10),
                    run_set_context_state(
                        consumer,
                        &op.entity.handle(),
                        cool_patient_context
                            .clone()
                            .into_abstract_context_state_one_of(),
                    ),
                )
                .await
                .unwrap_or(None);
                if let Some(version) = result {
                    set_context_state_success.push((cool_patient_context, version))
                }
            }
        }

        // check mdib for change
        let success = match set_context_state_success.pop() {
            Some((ptx, version)) => {
                info!(
                        "Checking for presence of added patient context for {} starting from mdib version {}",
                        &ptx.descriptor_handle(),
                        version
                    );
                // wait for remote_mdib version to be >= version
                if timeout(
                    Duration::from_secs(10),
                    wait_for_mdib_version(&remote_mdib, &version),
                )
                .await
                .is_err()
                {
                    warn!("Waiting for mdib to reach version {version} timed out.")
                }
                info!(
                    "Reached mdib version {}, is now at {}",
                    version,
                    remote_mdib.mdib_version().await
                );
                remote_mdib
                        .entity(ptx.descriptor_handle().as_str())
                        .await
                        .map(|it| match it.entity {
                            Entity::PatientContext(ctx_entity) => ctx_entity
                                .pair
                                .states
                                .iter()
                                .any(|state: &PatientContextState| {
                                    let correct_association = state.abstract_context_state.context_association_attr
                                        == patient_context_to_set
                                        .abstract_context_state
                                        .context_association_attr;

                                    let correct_validator = state.abstract_context_state.validator.iter().any(
                                            |val| match val {
                                                InstanceIdentifierOneOf::InstanceIdentifier(ii) => {
                                                    ii.root_attr == validator_to_set.root_attr
                                                        && ii.extension_attr
                                                            == validator_to_set.extension_attr
                                                }
                                                _ => false,
                                            },
                                        );

                                    info!("Context {} - Association correct {} - Validator correct {}", state.state_handle(), correct_association, correct_validator);
                                    correct_association && correct_validator
                                }),
                            desc => {
                                error!("Descriptor with handle {} in the mdib was not patient, was entity {:?}", ptx.descriptor_handle(), desc);
                                false
                            },
                        })
                        .unwrap_or_else(|| {
                            error!("Did not find descriptor with handle {} in the mdib", ptx.descriptor_handle());
                            false
                        })
            }
            None => false,
        };

        match success {
            true => TestStatus::Passed,
            false => TestStatus::Failed,
        }
    }
}

async fn wait_for_mdib_version<MDIB>(mdib: &MDIB, mdib_version: &MdibVersion)
where
    MDIB: RemoteMdibAccess + Clone + Send + Sync,
{
    // check mdib periodically for version
    let mut interval = tokio::time::interval(Duration::from_millis(100));

    loop {
        let current_version = mdib.mdib_version().await;

        match current_version.cmp_to(mdib_version) {
            None => warn!("Cannot compare mdib version {current_version} and {mdib_version}"),
            Some(cmp::Ordering::Equal) | Some(cmp::Ordering::Greater) => break,
            _ => {}
        }

        interval.tick().await;
    }
}

pub async fn run_consumer<CONSUMER, T, MDIB>(mut consumer: CONSUMER) -> anyhow::Result<()>
where
    CONSUMER: SdcConsumer<MDIB, T>,
    T: ScoTransaction,
    MDIB: RemoteMdibAccess + Clone + Send + Sync + 'static,
{
    let mut result_map: HashMap<ReferenceTestStep, TestStatus> = HashMap::new();
    ReferenceTestStep::iter().for_each(|it| {
        result_map.insert(it, TestStatus::Initial);
    });

    // we're connected, that means some success
    result_map.insert(ReferenceTestStep::Test1b, TestStatus::Passed);
    result_map.insert(ReferenceTestStep::Test2a, TestStatus::Passed);
    result_map.insert(ReferenceTestStep::Test3a, TestStatus::Passed);

    let supported_languages = consumer
        .get_supported_languages(GetSupportedLanguages {
            abstract_get: AbstractGet {
                extension_element: None,
            },
        })
        .await;

    match supported_languages {
        Ok(response) => info!(
            "Supported languages: {}",
            response.lang.into_iter().map(|it| it.language).join(", ")
        ),
        Err(err) => warn!("Could not retrieve supported languages: {:?}", err),
    }

    let get_context_states_response = consumer.get_context_states(vec![]).await;

    match get_context_states_response {
        Ok(ctx) => {
            info!("Received GetContextStatesResponse");
            let has_location = ctx
                .context_state
                .iter()
                .any(|it| matches!(it, AbstractContextStateOneOf::LocationContextState(..)));
            match has_location {
                true => result_map.insert(ReferenceTestStep::Test3b, TestStatus::Passed),
                false => {
                    error!("No location context found");
                    result_map.insert(ReferenceTestStep::Test3b, TestStatus::Failed)
                }
            };
        }
        Err(err) => {
            error!("{}", err);
            result_map.insert(ReferenceTestStep::Test3b, TestStatus::Failed);
        }
    }

    let mut metric_join_set = JoinSet::new();

    // expect numeric metric updates
    let numeric_updates = collect_metric_updates(
        5,
        Duration::from_secs(30),
        consumer.remote_mdib().await.clone(),
        |it| matches!(it, AbstractMetricStateOneOf::NumericMetricState(..)),
    )
    .then(|it| future::ready((ReferenceTestStep::Test4a, it)));
    metric_join_set.spawn(numeric_updates);

    // expect string metric updates
    let string_updates = collect_metric_updates(
        5,
        Duration::from_secs(30),
        consumer.remote_mdib().await.clone(),
        |it| matches!(it, AbstractMetricStateOneOf::StringMetricState(..)),
    )
    .then(|it| future::ready((ReferenceTestStep::Test4b, it)));
    metric_join_set.spawn(string_updates);

    let alert_condition_updates = collect_alert_updates(
        5,
        Duration::from_secs(30),
        consumer.remote_mdib().await.clone(),
        |it| {
            matches!(
                it,
                AbstractAlertStateOneOf::AlertConditionState(..)
                    | AbstractAlertStateOneOf::LimitAlertConditionState(..)
            )
        },
    )
    .then(|it| future::ready((ReferenceTestStep::Test4c, it)));
    metric_join_set.spawn(alert_condition_updates);

    let alert_signal_updates = collect_alert_updates(
        5,
        Duration::from_secs(30),
        consumer.remote_mdib().await.clone(),
        |it| matches!(it, AbstractAlertStateOneOf::AlertSignalState(..)),
    )
    .then(|it| future::ready((ReferenceTestStep::Test4d, it)));
    metric_join_set.spawn(alert_signal_updates);

    let alert_system_updates = check_alert_system_update_period(
        3,
        Duration::from_secs(10),
        Duration::from_secs(30),
        consumer.remote_mdib().await.clone(),
    )
    .then(|it| future::ready((ReferenceTestStep::Test4e, it)));
    metric_join_set.spawn(alert_system_updates);

    let waveform_updates = collect_waveform_updates(
        Duration::from_secs(30),
        consumer.remote_mdib().await.clone(),
    )
    .then(|it| future::ready((ReferenceTestStep::Test4f, it)));
    metric_join_set.spawn(waveform_updates);

    let component_updates = check_component_updates(
        Duration::from_secs(30),
        consumer.remote_mdib().await.clone(),
    )
    .then(|it| {
        info!("component: {:?}", it);
        future::ready((ReferenceTestStep::Test4g, it))
    });
    metric_join_set.spawn(component_updates);

    let operation_updates = check_operation_enabled_disabled(
        Duration::from_secs(30),
        consumer.remote_mdib().await.clone(),
    )
    .then(|it| {
        info!("operation: {:?}", it);
        future::ready((ReferenceTestStep::Test4h, it))
    });
    metric_join_set.spawn(operation_updates);

    let description_updates = check_description_updates(
        Duration::from_secs(40),
        consumer.remote_mdib().await.clone(),
    )
    .then(|it| {
        match &it {
            Ok(v) => debug!("description: {}", v),
            Err(err) => error!("description: {:?}", err),
        }
        future::ready((ReferenceTestStep::Test5a, it))
    });
    metric_join_set.spawn(description_updates);

    let description_insert_remove = collect_description_insert_delete(
        Duration::from_secs(40),
        consumer.remote_mdib().await.clone(),
    )
    .then(|it| {
        match &it {
            Ok(v) => info!("description ins del: {}", v),
            Err(err) => error!("description ins del: {:?}", err),
        }
        future::ready((ReferenceTestStep::Test5b, it))
    });
    metric_join_set.spawn(description_insert_remove);

    // run set value
    let set_value_ops = {
        consumer
            .remote_mdib()
            .await
            .entities_by_type(entity_filter!(SetValueOperation))
            .await
    };
    if set_value_ops.is_empty() {
        result_map.insert(
            ReferenceTestStep::Test6c,
            TestStatus::Skipped {
                reason: "No set value operations found".to_string(),
            },
        );
    } else {
        let mut set_value_result = false;

        for op in set_value_ops.into_iter() {
            info!("Invoking set value {}", &op.entity.handle());
            set_value_result = set_value_result
                || timeout(
                    Duration::from_secs(10),
                    run_set_value(&mut consumer, &op.entity.handle(), Decimal::from(50)),
                )
                .await
                .unwrap_or(false)
        }

        info!("Invoking set value done");
        result_map.insert(
            ReferenceTestStep::Test6c,
            match set_value_result {
                true => TestStatus::Passed,
                false => TestStatus::Failed,
            },
        );
    }

    // run set string
    let set_string_ops = {
        consumer
            .remote_mdib()
            .await
            .entities_by_type(entity_filter!(SetStringOperation))
            .await
    };
    if set_string_ops.is_empty() {
        result_map.insert(
            ReferenceTestStep::Test6d,
            TestStatus::Skipped {
                reason: "No set string operations found".to_string(),
            },
        );
    } else {
        let mdib = consumer.remote_mdib().await;
        let mut set_string_result = false;
        for op in set_string_ops.into_iter() {
            info!("Invoking set string {}", &op.entity.handle());
            if let Entity::SetStringOperation(it) = op.entity {
                let operation_target_handle: String = it
                    .pair
                    .descriptor
                    .abstract_operation_descriptor
                    .operation_target_attr
                    .string
                    .clone();

                let expected_value = match mdib
                    .entity(operation_target_handle.as_str())
                    .await
                    .map(|it| it.entity)
                {
                    Some(Entity::EnumStringMetric(x)) => x
                        .pair
                        .descriptor
                        .allowed_value
                        .first()
                        .expect("No allowed value found")
                        .value
                        .clone(),
                    _ => format!(
                        "{} {:4}",
                        SET_STRING_VALUE_PREFIX,
                        uuid::Uuid::new_v4().to_string()
                    ),
                };

                set_string_result = set_string_result
                    || timeout(
                        Duration::from_secs(10),
                        run_set_string(&mut consumer, it.handle().as_ref(), expected_value),
                    )
                    .await
                    .unwrap_or(false)
            }
        }

        info!("Invoking set string done");
        result_map.insert(
            ReferenceTestStep::Test6d,
            match set_string_result {
                true => TestStatus::Passed,
                false => TestStatus::Failed,
            },
        );

        // run set context state
        result_map.insert(
            ReferenceTestStep::Test6b,
            execute_set_context_state(&mut consumer).await,
        );

        // run set metric state
        result_map.insert(
            ReferenceTestStep::Test6e,
            execute_set_metric_state(&mut consumer).await,
        );
    }

    // collect futures
    while let Some(res) = metric_join_set.join_next().await {
        let out = res.expect("JoinError?");
        result_map.insert(
            out.0,
            match out.1 {
                Ok(_) => TestStatus::Passed,
                _ => TestStatus::Failed,
            },
        );
    }

    consumer.disconnect().await?;

    ReferenceTestStep::iter()
        .map(|it| (it.clone(), result_map.get(&it)))
        .filter(|(_, result)| result.is_some())
        .for_each(|(test, result)| info!("{}: {}", test, result.unwrap()));

    let has_errors = result_map.iter().any(|it| it.1 == &TestStatus::Failed);
    match has_errors {
        false => Ok(()),
        true => anyhow::bail!("Had failed steps"),
    }
}

async fn run_consumer_dpws_with_discovery<EHF, DISCO>(
    config: &ConsumerConfig,
    extension_handler_factory: EHF,
    discovery: DISCO,
    client: ReqwestHttpClient,
    server: AxumRegistry,
) -> anyhow::Result<()>
where
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
    DISCO: DpwsDiscoveryConsumer + Send + Sync + 'static,
{
    let consumer = ConsumerImpl::new(
        discovery,
        client.get_access(),
        server.access(),
        extension_handler_factory.clone(),
    );

    let hosting_service = consumer.connect(&config.epr, None).await?;

    info!("{:?}", hosting_service.this_model());
    info!("{:?}", hosting_service.types());

    // create a connect configuration in which every report is optional
    let connect_configuration = ConnectConfiguration::new(
        vec![],
        ALL_EPISODIC_AND_WAVEFORM_REPORTS
            .iter()
            .map(|it| it.to_string())
            .collect(),
    )?;

    let consumer_config = ConsumerConfigBuilder::default()
        .extension_handler_factory(extension_handler_factory)
        .build();

    // subscribe to episodic reports, find the correct hosted service first
    #[allow(clippy::type_complexity)] // too bad
    let remote_device: SdcRemoteDevice<
        DefaultRemoteMdibAccessImpl,
        ConsumerHostingService<EHF>,
        ConsumerHostedService<EHF>,
        ReqwestSoapClient<EHF>,
        ReqwestSoapClient<EHF>,
        ScoController<DpwsSetServiceHandler<ReqwestSoapClient<EHF>>>,
        ScoTransactionImpl,
        EHF,
    > = sdc_remote_device_connector::connect::<
        _,
        _,
        _,
        _,
        ScoController<DpwsSetServiceHandler<ReqwestSoapClient<EHF>>>,
        ScoTransactionImpl,
        EHF,
    >(hosting_service, connect_configuration, consumer_config)
    .await?;

    let mut watchdog_msg = remote_device.watchdog.subscribe().await;

    let _watchdog_task = tokio::spawn(async move {
        loop {
            if let Ok(msg) = watchdog_msg.recv().await {
                error!("Watchdog detected disconnect: {msg:?}");
            }
        }
    });

    run_consumer(remote_device).await
}

async fn filter_probes_for_epr(
    receiver: broadcast::Receiver<DpwsDiscoveryEvent>,
    epr: &str,
) -> Option<ProbeMatch> {
    let mut stream = BroadcastStream::new(receiver);

    while let Some(msg) = stream.next().await {
        if let Ok(DpwsDiscoveryEvent::ProbeMatches(probe_matches)) = msg {
            if let Some(probe_match) = find_probe_match_for_epr(probe_matches, epr) {
                return Some(probe_match);
            }
        }
    }
    None
}

fn find_probe_match_for_epr(probe_matches: ProbeMatches, epr: &str) -> Option<ProbeMatch> {
    probe_matches
        .probe_match
        .into_iter()
        .find(|it| it.probe_match.endpoint_reference.address.value == epr)
}
async fn run_probe_dpws<DISCO>(
    discovery_consumer: &DISCO,
    epr: &str,
) -> Result<Option<ProbeMatch>, DpwsDiscoveryError>
where
    DISCO: DpwsDiscoveryConsumer,
{
    // also subscribe so we can bail early
    let subscription = discovery_consumer.subscribe();

    let types = &[MDPWS_MEDICAL_DEVICE_TYPE.into()];

    let filter_task = filter_probes_for_epr(subscription, epr);
    let probe_options = ProbeOptions::builder().types(types).build();
    let probe_task = discovery_consumer.probe(probe_options);

    tokio::select! {
        matches = filter_task => {
            Ok(matches)
        }
        task_result = probe_task => {
            Ok(task_result?.into_iter().flat_map(|it| find_probe_match_for_epr(it, epr)).next())
        }
    }
}

pub async fn run_consumer_dpws_main<EHF>(
    config: &ConsumerConfig,
    crypto: Option<CryptoConfig>,
    extension_handler_factory: EHF,
) -> anyhow::Result<()>
where
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    let adapter_address = IpAddr::from_str(&config.address)?;
    let proto_udp = create_udp_binding(adapter_address, Some(WS_DISCOVERY_PORT)).await?;

    run_consumer_dpws(config, crypto, extension_handler_factory, proto_udp).await
}

async fn run_consumer_dpws<EHF>(
    config: &ConsumerConfig,
    crypto: Option<CryptoConfig>,
    extension_handler_factory: EHF,
    udp_binding: UdpBindingImpl,
) -> anyhow::Result<()>
where
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    let adapter_addr = IpAddr::from_str(&config.address)?;

    let http_client = ReqwestHttpClient::new(crypto.clone())?;
    let registry = AxumRegistry::new(crypto);

    match &config.disco_mdpws {
        None => {
            info!("Running with multicast discovery");

            let discovery_consumer = DpwsDiscoveryConsumerImpl::new(udp_binding);
            run_consumer_dpws_with_discovery(
                config,
                extension_handler_factory,
                discovery_consumer,
                http_client,
                registry,
            )
            .await
        }
        Some(addr) => {
            info!("Running with disco at {}", addr);
            // validate address here so it won't fail on subscribe
            let http = http::Uri::from_str(addr).expect("Invalid uri");
            let _socket_addr = SocketAddr::from_str(&format!(
                "{}:{}",
                http.host().unwrap(),
                http.port().unwrap()
            ))
            .expect("No valid socket address");

            let soap_client = SoapClientImpl::new(
                http_client.get_access(),
                WsaEndpointReference {
                    attributes: Default::default(),
                    address: WsaAttributedURIType {
                        attributes: Default::default(),
                        value: addr.clone(),
                    },
                    reference_parameters: None,
                    metadata: None,
                    children: vec![],
                },
                ReplacementExtensionHandlerFactory,
            );

            let disco = DpwsDiscoveryProxyConsumerImpl::new(
                addr.clone(),
                SocketAddr::new(adapter_addr, 0),
                soap_client,
                registry.access(),
            );

            let mut disco_messages = DpwsDiscoveryProxyConsumer::subscribe(&disco).await?;
            let disco_task = tokio::spawn(async move {
                loop {
                    if let Ok(msg) = disco_messages.recv().await {
                        info!("Received disco notification {:?}", msg)
                    }
                }
            });

            let result = run_consumer_dpws_with_discovery(
                config,
                extension_handler_factory,
                disco,
                http_client,
                registry,
            )
            .await;

            disco_task.abort();
            result
        }
    }
}

pub async fn run_consumer_proto_main(
    config: &ConsumerConfig,
    crypto: Option<CryptoConfig>,
) -> anyhow::Result<()> {
    let adapter_address = IpAddr::from_str(&config.address)?;
    let proto_udp = create_udp_binding(adapter_address, Some(PROTOSDC_MULTICAST_PORT)).await?;

    if let Some(endpoint) = run_proto_discovery(config, crypto.clone(), proto_udp.clone()).await? {
        run_consumer_proto(crypto, endpoint).await
    } else {
        Err(anyhow::anyhow!("No endpoint found"))
    }
}

async fn run_consumer_proto(
    crypto: Option<CryptoConfig>,
    endpoint: Endpoint,
) -> anyhow::Result<()> {
    let modified_endpoint = match &crypto.is_some() {
        true => ProtoConsumerImpl::add_https_prefix_to_endpoint(endpoint),
        false => ProtoConsumerImpl::add_http_prefix_to_endpoint(endpoint),
    };

    let mut consumer = ProtoConsumerImpl::default();
    consumer.connect(modified_endpoint.clone(), crypto).await?;

    let remote_device = remote_device_connector::connect(consumer).await?;
    {
        let mdib_version = remote_device.mdib_version().await;
        info!("Remote Device Mdib Version: {}", mdib_version);
    }

    run_consumer(remote_device).await?;

    Ok(())
}

async fn run_proto_discovery<UDP>(
    consumer_config: &ConsumerConfig,
    crypto_config: Option<CryptoConfig>,
    udp_binding_impl: UDP,
) -> Result<Option<Endpoint>, anyhow::Error>
where
    UDP: UdpBinding + Send + Sync + 'static,
{
    let mut discovery_consumer = DiscoveryConsumerImpl::new(
        udp_binding_impl,
        consumer_config.disco_proto.clone(),
        crypto_config,
    )
    .await?;

    let mut subscription = discovery_consumer.subscribe();

    let early_result = async move {
        loop {
            if let Ok(msg) = subscription.recv().await {
                debug!("Received discovery notification {:?}", msg);
                if let DiscoveryEvent::SearchMatch { endpoints, .. } = msg {
                    if let Some(endpoint) = endpoints
                        .into_iter()
                        .find(|it| it.endpoint_identifier == consumer_config.epr)
                    {
                        return Some(endpoint);
                    }
                }
            }
        }
    };

    // alternatively, use .search_endpoint if filtering is implemented correctly
    let search_result = discovery_consumer.search(vec![], None, Some(Duration::from_secs(10)));

    let result = tokio::select! {
        res = early_result => {
            res
        }
        res = search_result => {
            res?.into_iter().find(|it| it.endpoint_identifier == consumer_config.epr)
        }
    };

    info!("Discovery results {:?}", result);

    Ok(result)
}

async fn run_dpws_discovery<UDP>(
    http_client: ReqwestClientAccess,
    server_registry: AxumRegistryAccess,
    udp_binding: UDP,
    adapter_addr: IpAddr,
    disco_mdpws: Option<String>, // TODO: add discovery proxy
    epr: &str,
) -> Result<Option<ProbeMatch>, anyhow::Error>
where
    UDP: UdpBinding + Send + Sync + 'static,
{
    match disco_mdpws {
        None => {
            info!("Running probe with multicast discovery");

            let discovery_consumer = DpwsDiscoveryConsumerImpl::new(udp_binding);
            Ok(run_probe_dpws(&discovery_consumer, epr).await?)
        }
        Some(addr) => {
            info!("Running with disco at {}", addr);
            // validate address here so it won't fail on subscribe
            let http = http::Uri::from_str(&addr).expect("Invalid uri");
            let _socket_addr = SocketAddr::from_str(&format!(
                "{}:{}",
                http.host().unwrap(),
                http.port().unwrap()
            ))
            .expect("No valid socket address");

            let soap_client = SoapClientImpl::new(
                http_client,
                WsaEndpointReference {
                    attributes: Default::default(),
                    address: WsaAttributedURIType {
                        attributes: Default::default(),
                        value: addr.clone(),
                    },
                    reference_parameters: None,
                    metadata: None,
                    children: vec![],
                },
                ReplacementExtensionHandlerFactory,
            );

            let disco = DpwsDiscoveryProxyConsumerImpl::new(
                addr,
                SocketAddr::new(adapter_addr, 0),
                soap_client,
                server_registry,
            );

            Ok(run_probe_dpws(&disco, epr).await?)
        }
    }
}

pub async fn run_consumer_dual<EHF>(
    config: &ConsumerConfig,
    crypto: Option<CryptoConfig>,
    extension_handler_factory: EHF,
) -> anyhow::Result<()>
where
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    let adapter_address = IpAddr::from_str(&config.address)?;

    // run discovery
    let http_client = ReqwestHttpClient::new(crypto.clone())?;
    let registry = AxumRegistry::new(crypto.clone());
    let mdpws_udp = create_udp_binding(adapter_address, Some(WS_DISCOVERY_PORT)).await?;
    let proto_udp = create_udp_binding(adapter_address, Some(PROTOSDC_MULTICAST_PORT)).await?;

    let mdpws_probe = run_dpws_discovery(
        http_client.get_access(),
        registry.access(),
        mdpws_udp.clone(),
        adapter_address,
        config.disco_mdpws.clone(),
        &config.epr,
    );

    let proto_search = run_proto_discovery(config, crypto.clone(), proto_udp.clone());

    tokio::select! {
        mdpws_probe_result = mdpws_probe => {
            info!("MDPWS probe result: {:?}", mdpws_probe_result);
            run_consumer_dpws(config, crypto, extension_handler_factory, mdpws_udp).await
        }
        proto_search_result = proto_search => {
            info!("Proto search result: {:?}", proto_search_result);
            if let Ok(Some(endpoint)) = proto_search_result {
                run_consumer_proto(crypto, endpoint).await
            } else {
                Err(anyhow::anyhow!("No endpoint found"))
            }
        }
    }
}
