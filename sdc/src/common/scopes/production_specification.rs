use regex::Regex;

const SCHEME: &str = "sdc.mds.prodspec";
const SCHEME_ESC: &str = r"sdc\.mds\.prodspec";

use biceps::common::builders::InstanceIdentifierBuilder;
use biceps::common::context::uri::{decode_pchar, encode_pchar, UriUtilError};
use const_format::concatcp;
use lazy_static::lazy_static;
use protosdc_biceps::biceps::abstract_device_component_descriptor_mod::ProductionSpecification;
use protosdc_biceps::biceps::{CodeIdentifier, CodedValue, InstanceIdentifierOneOf};
use thiserror::Error;

const UNRESERVED: &str = "[a-zA-Z0-9\\-._~]";
const CHAR: &str = concatcp!("(?:(?:%[a-fA-F0-9]{2})+|(?:", UNRESERVED, ")+)");
pub(crate) const CHAR_SEQUENCE_NZ_REGEX: &str = concatcp!(CHAR, "+");
pub(crate) const CHAR_SEQUENCE_REGEX: &str = concatcp!(CHAR, "*");

const PROD_SPEC_GROUP: &str = "prod_spec";
const ROOT_GROUP: &str = "root";
const EXTENSION_GROUP: &str = "extension";
const INSTANCE_IDENTIFIER_PATTERN: &str = concatcp!(
    "(?P<",
    ROOT_GROUP,
    ">",
    CHAR_SEQUENCE_NZ_REGEX,
    ")(,",
    "(?P<",
    EXTENSION_GROUP,
    ">",
    CHAR_SEQUENCE_NZ_REGEX,
    "))?"
);

const CODE_GROUP: &str = "code";
const CODING_SYSTEM_GROUP: &str = "coding_system";
const CODING_SYSTEM_VERSION_GROUP: &str = "coding_system_version";

const SPEC_TYPE_PATTERN: &str = concatcp!(
    "(?P<",
    CODE_GROUP,
    ">",
    CHAR_SEQUENCE_NZ_REGEX,
    ")",
    // optional start
    r"(,",
    "(?P<",
    CODING_SYSTEM_GROUP,
    ">",
    CHAR_SEQUENCE_NZ_REGEX,
    ")",
    // nested optional start
    r"(,",
    "(?P<",
    CODING_SYSTEM_VERSION_GROUP,
    ">",
    CHAR_SEQUENCE_NZ_REGEX,
    ")",
    // nested optional end
    r")?",
    // optional end
    r")?",
);

const PRODUCTION_SPECIFICATION_PATTERN: &str = concatcp!(
    "^",
    SCHEME_ESC,
    ":",
    "(?P<",
    PROD_SPEC_GROUP,
    ">",
    CHAR_SEQUENCE_REGEX,
    ")",
    ":",
    SPEC_TYPE_PATTERN,
    // begin optional
    r"(:",
    INSTANCE_IDENTIFIER_PATTERN,
    // end optional
    r")?",
    "$"
);

lazy_static! {
    pub(crate) static ref PRODUCTION_SPECIFICATION_PARSER: Regex =
        Regex::new(PRODUCTION_SPECIFICATION_PATTERN).unwrap();
}

#[derive(Debug, Error)]
pub enum ProductionSpecificationMapperError {
    #[error("Could not parse production specification, reason: {0}")]
    ParsingError(String),
    #[error("Regex did not match input")]
    RegexMismatch,
    #[error("Mandatory pattern group missing: {0}")]
    MandatoryPatternGroupMissing(&'static str),
    #[error(transparent)]
    UriUtilError(#[from] UriUtilError),
}

/// Creates a production specification string according to SDPi 1.0 2:A.4.4 Discovery Scopes
///
/// # Arguments
///
/// * `production_specification`: a production specification to convert into a scope string
///
/// returns: String
pub fn create_prod_spec(production_specification: &ProductionSpecification) -> String {
    let mut result = String::new();
    result.push_str(SCHEME);

    // ProductionSpec ':'
    result.push(':');
    result
        .push_str(encode_pchar(production_specification.production_spec.as_str(), false).as_str());

    result.push(':');
    result.push_str(
        encode_pchar(
            production_specification.spec_type.code_attr.string.as_str(),
            false,
        )
        .as_str(),
    );
    if let Some(system) = &production_specification.spec_type.coding_system_attr {
        result.push(',');
        result.push_str(encode_pchar(system.uri.as_str(), false).as_str());
        if let Some(version) = &production_specification
            .spec_type
            .coding_system_version_attr
        {
            result.push(',');
            result.push_str(encode_pchar(version.as_str(), false).as_str());
        }
    }

    if let Some(component_id) = &production_specification.component_id {
        result.push(':');

        let identifier = match component_id {
            InstanceIdentifierOneOf::InstanceIdentifier(it) => it,
            InstanceIdentifierOneOf::OperatingJurisdiction(it) => &it.instance_identifier,
        };

        // TODO: according to spec, this is not optional?
        if let Some(root) = &identifier.root_attr {
            result.push_str(encode_pchar(root.any_u_r_i.uri.as_str(), false).as_str());
        }

        if let Some(extension) = &identifier.extension_attr {
            result.push(',');
            result.push_str(encode_pchar(extension.string.as_str(), false).as_str())
        }
    }

    result
}

/// Parses a Production Specification from a string according to SDPi 1.0 2:A.4.4 Discovery Scopes
///
/// # Arguments
///
/// * `input`: string representing a production specification
///
/// returns: Result<ProductionSpecification, ProductionSpecificationMapperError>
///
/// # Examples
///
/// ```
/// # use sdc::common::scopes::production_specification::parse_prod_spec;
/// let parsed = parse_prod_spec("sdc.mds.prodspec:SPEC%20BRUH:abc").unwrap();
/// ```
pub fn parse_prod_spec(
    input: &str,
) -> Result<ProductionSpecification, ProductionSpecificationMapperError> {
    let matched_identifier = PRODUCTION_SPECIFICATION_PARSER.captures(input);
    match matched_identifier {
        None => Err(ProductionSpecificationMapperError::RegexMismatch),
        Some(matched) => {
            // these must exist now
            let prod_spec = matched.name(PROD_SPEC_GROUP).ok_or(
                ProductionSpecificationMapperError::MandatoryPatternGroupMissing(PROD_SPEC_GROUP),
            )?;
            let code = matched.name(CODE_GROUP).ok_or(
                ProductionSpecificationMapperError::MandatoryPatternGroupMissing(CODE_GROUP),
            )?;

            let decoded_prod_spec = decode_pchar(prod_spec.as_str())?;
            let decoded_code = decode_pchar(code.as_str())?;

            let mut result = ProductionSpecification {
                spec_type: CodedValue {
                    extension_element: None,
                    coding_system_name: vec![],
                    concept_description: vec![],
                    translation: vec![],
                    code_attr: CodeIdentifier {
                        string: decoded_code,
                    },
                    coding_system_attr: None,
                    coding_system_version_attr: None,
                    symbolic_code_name_attr: None,
                },
                production_spec: decoded_prod_spec,
                component_id: None,
            };

            // parse the optional stuff
            if let Some(cs) = matched.name(CODING_SYSTEM_GROUP) {
                result.spec_type.coding_system_attr =
                    Some(decode_pchar(cs.as_str())?.try_into().map_err(|_| {
                        ProductionSpecificationMapperError::ParsingError(
                            "Could not map CodingSystem to ProtoUri".to_string(),
                        )
                    })?);
            };

            if let Some(csv) = matched.name(CODING_SYSTEM_VERSION_GROUP) {
                result.spec_type.coding_system_version_attr = Some(decode_pchar(csv.as_str())?);
            };

            if let Some(root) = matched.name(ROOT_GROUP) {
                let mut builder = InstanceIdentifierBuilder::default()
                    .identifier_root(decode_pchar(root.as_str())?);
                builder = match matched.name(EXTENSION_GROUP) {
                    Some(extension) => {
                        builder.identifier_extension(decode_pchar(extension.as_str())?)
                    }
                    None => builder,
                };
                result.component_id =
                    Some(InstanceIdentifierOneOf::InstanceIdentifier(builder.build()));
            }

            Ok(result)
        }
    }
}

#[cfg(test)]
mod test {
    use crate::common::scopes::production_specification::{
        create_prod_spec, parse_prod_spec, ProductionSpecificationMapperError,
    };
    use biceps::common::builders::InstanceIdentifierBuilder;
    use protosdc_biceps::biceps::abstract_device_component_descriptor_mod::ProductionSpecification;
    use protosdc_biceps::biceps::{CodeIdentifier, CodedValue, InstanceIdentifierOneOf};

    pub fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    pub fn create_production_specification(
        spec: impl Into<String>,
        code: impl Into<String>,
        coding_system: Option<impl Into<String>>,
        coding_system_version: Option<impl Into<String>>,
        root: Option<impl Into<String>>,
        extension: Option<impl Into<String>>,
    ) -> ProductionSpecification {
        ProductionSpecification {
            spec_type: CodedValue {
                extension_element: None,
                coding_system_name: vec![],
                concept_description: vec![],
                translation: vec![],
                code_attr: CodeIdentifier {
                    string: code.into(),
                },
                coding_system_attr: coding_system
                    .map(|it| it.into())
                    .map(|it| it.try_into().unwrap()),
                coding_system_version_attr: coding_system_version.map(|it| it.into()),
                symbolic_code_name_attr: None,
            },
            production_spec: spec.into(),
            component_id: match (root, extension) {
                (None, None) => None,
                otherwise => Some(InstanceIdentifierOneOf::InstanceIdentifier(
                    match otherwise {
                        (Some(r), None) => InstanceIdentifierBuilder::default()
                            .identifier_root(r)
                            .build(),
                        (None, Some(e)) => InstanceIdentifierBuilder::default()
                            .identifier_extension(e)
                            .build(),
                        (Some(r), Some(e)) => InstanceIdentifierBuilder::default()
                            .identifier_root(r)
                            .identifier_extension(e)
                            .build(),
                        _ => panic!("Unreachable"),
                    },
                )),
            },
        }
    }

    fn test_data() -> Vec<(&'static str, ProductionSpecification)> {
        vec![
            (
                "sdc.mds.prodspec:SPEC%20BRUH:abc,systemyolo,versionyolo:whatever,man",
                create_production_specification(
                    "SPEC BRUH",
                    "abc",
                    Some("systemyolo"),
                    Some("versionyolo"),
                    Some("whatever"),
                    Some("man"),
                ),
            ),
            (
                "sdc.mds.prodspec:SPEC%20BRUH:abc,systemyolo,versionyolo:whatever",
                create_production_specification(
                    "SPEC BRUH",
                    "abc",
                    Some("systemyolo"),
                    Some("versionyolo"),
                    Some("whatever"),
                    None::<String>,
                ),
            ),
            (
                "sdc.mds.prodspec:SPEC%20BRUH:abc,systemyolo:whatever",
                create_production_specification(
                    "SPEC BRUH",
                    "abc",
                    Some("systemyolo"),
                    None::<String>,
                    Some("whatever"),
                    None::<String>,
                ),
            ),
            (
                "sdc.mds.prodspec:SPEC%20BRUH:abc",
                create_production_specification(
                    "SPEC BRUH",
                    "abc",
                    None::<String>,
                    None::<String>,
                    None::<String>,
                    None::<String>,
                ),
            ),
        ]
    }

    #[test]
    fn test_from_uri() -> anyhow::Result<()> {
        init();

        for (data, prod_spec) in test_data() {
            assert_eq!(parse_prod_spec(data)?, prod_spec)
        }

        Ok(())
    }

    #[test]
    fn test_to_uri() -> anyhow::Result<()> {
        init();

        for (data, prod_spec) in test_data() {
            assert_eq!(data, create_prod_spec(&prod_spec).as_str())
        }

        Ok(())
    }

    #[test]
    fn test_from_invalid_uri() -> anyhow::Result<()> {
        init();

        let invalid = [
            "sdc.mds.prodspec:SPEC%20BRUH",
            "sdc.mds.prodspec:SPEC%20BRUH:",
            "sdc.mds.prodspec:SPEC%20BRUH:abc,",
            "sdc.mds.prodspec:SPEC%20BRUH:abc,stuff:",
            "sdc.mds.prodspec:SPEC%20BRUH:abc,stuff:,",
            "sdc.mds.proodspec:SPEC%20BRUH:abc,stuff",
            "",
            "sdc.mds.prodspec:SPEC%20BRUH:abc,stuff:aa,bb:c",
            "sdc.mds.prodspec:SPEC%20BRUH:abc,stuff:aa,bb,c",
        ];

        for input in invalid.iter() {
            assert!(matches!(
                parse_prod_spec(input),
                Err(ProductionSpecificationMapperError::RegexMismatch)
            ))
        }

        Ok(())
    }
}
