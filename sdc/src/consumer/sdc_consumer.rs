use std::sync::Arc;

use async_trait::async_trait;
use protosdc_biceps::biceps::{
    AbstractSetOneOf, Activate, GetContextStatesByFilter, GetContextStatesByFilterResponse,
    GetContextStatesByIdentification, GetContextStatesByIdentificationResponse,
    GetContextStatesResponse, GetLocalizedText, GetLocalizedTextResponse, GetMdDescriptionResponse,
    GetMdStateResponse, GetMdibResponse, GetSupportedLanguages, GetSupportedLanguagesResponse,
    SetAlertState, SetComponentState, SetContextState, SetMetricState, SetString, SetValue,
};
use thiserror::Error;

use biceps::consumer::access::remote_mdib_access::RemoteMdibAccess;
use dpws::common::extension_handling::ExtensionHandlerFactory;
use dpws::consumer::sdc_consumer::SdcRemoteDevice;
use dpws::consumer::services;
use dpws::soap::dpws::client::hosted_service::HostedService;
use dpws::soap::dpws::client::hosting_service::HostingService;
use dpws::soap::soap_client::SoapClient;
use protosdc::consumer::grpc_consumer::{Consumer, LocalizationServiceConsumer};
use protosdc::consumer::sco::sco_controller::SetServiceAccess;
use protosdc::consumer::sco::sco_transaction::ScoTransaction;

/// Trait for interacting with SDC Consumer.
#[async_trait]
pub trait SdcConsumer<MDIB, T>
where
    MDIB: RemoteMdibAccess,
    T: ScoTransaction,
{
    // Remote Mdib
    async fn remote_mdib(&self) -> MDIB;

    // GetService
    async fn get_mdib(&self) -> Result<GetMdibResponse, ConsumerError>;

    async fn get_md_state(&self, handles: Vec<String>)
        -> Result<GetMdStateResponse, ConsumerError>;

    async fn get_md_description(
        &self,
        handles: Vec<String>,
    ) -> Result<GetMdDescriptionResponse, ConsumerError>;

    // ContextService
    async fn get_context_states(
        &self,
        handles: Vec<String>,
    ) -> Result<GetContextStatesResponse, ConsumerError>;

    async fn get_context_states_by_filter(
        &self,
        request: GetContextStatesByFilter,
    ) -> Result<GetContextStatesByFilterResponse, ConsumerError>;

    async fn get_context_states_by_identification(
        &self,
        request: GetContextStatesByIdentification,
    ) -> Result<GetContextStatesByIdentificationResponse, ConsumerError>;

    // SetService
    async fn set_value(&self, request: SetValue) -> Result<Arc<T>, ConsumerError>;

    async fn set_string(&self, request: SetString) -> Result<Arc<T>, ConsumerError>;

    async fn activate(&self, request: Activate) -> Result<Arc<T>, ConsumerError>;

    async fn set_alert_state(&self, request: SetAlertState) -> Result<Arc<T>, ConsumerError>;

    async fn set_component_state(
        &self,
        request: SetComponentState,
    ) -> Result<Arc<T>, ConsumerError>;

    async fn set_context_state(&self, request: SetContextState) -> Result<Arc<T>, ConsumerError>;

    async fn set_metric_state(&self, request: SetMetricState) -> Result<Arc<T>, ConsumerError>;

    // localization service
    async fn get_localized_text(
        &self,
        request: GetLocalizedText,
    ) -> Result<GetLocalizedTextResponse, ConsumerError>;

    async fn get_supported_languages(
        &self,
        request: GetSupportedLanguages,
    ) -> Result<GetSupportedLanguagesResponse, ConsumerError>;

    async fn disconnect(&mut self) -> Result<(), ConsumerError>;
}

#[derive(Debug, Error)]
pub enum ConsumerError {
    #[error(transparent)]
    TransportError(anyhow::Error),

    #[error("The service required for this rpc is not available")]
    ServiceUnavailable,
}

async fn execute_sco<ST, SSA>(
    sco: Option<&SSA>,
    request: AbstractSetOneOf,
) -> Result<Arc<ST>, ConsumerError>
where
    ST: ScoTransaction + Send + Sync,
    SSA: SetServiceAccess<ST> + Send + Sync + Clone,
{
    let mut set = sco.ok_or(ConsumerError::ServiceUnavailable)?.clone();

    set.invoke(request)
        .await
        .map_err(|err| ConsumerError::TransportError(err.into()))
}

#[async_trait]
impl<U, HOSTING, HOSTED, S, HS, SSA, ST, EHF> SdcConsumer<U, ST>
    for SdcRemoteDevice<U, HOSTING, HOSTED, S, HS, SSA, ST, EHF>
where
    U: RemoteMdibAccess + Send + Sync + Clone,
    HOSTING: HostingService<HOSTED, S, HS> + Send + Sync,
    HOSTED: HostedService<HS> + Send + Sync,
    S: SoapClient + Send + Sync,
    HS: SoapClient + Send + Sync + Clone,
    SSA: SetServiceAccess<ST> + Send + Sync + Clone,
    ST: ScoTransaction + Send + Sync,
    EHF: ExtensionHandlerFactory + Send + Sync + 'static,
{
    async fn remote_mdib(&self) -> U {
        self.remote_mdib.clone()
    }

    async fn get_mdib(&self) -> Result<GetMdibResponse, ConsumerError> {
        let hosting = self.hosting_service.lock().await;
        services::get_mdib(&*hosting)
            .await
            .map_err(|err| ConsumerError::TransportError(err.into()))
    }

    async fn get_md_state(
        &self,
        handles: Vec<String>,
    ) -> Result<GetMdStateResponse, ConsumerError> {
        let hosting = self.hosting_service.lock().await;
        services::get_md_state(&*hosting, &handles)
            .await
            .map_err(|err| ConsumerError::TransportError(err.into()))
    }

    async fn get_md_description(
        &self,
        handles: Vec<String>,
    ) -> Result<GetMdDescriptionResponse, ConsumerError> {
        let hosting = self.hosting_service.lock().await;
        services::get_md_description(&*hosting, &handles)
            .await
            .map_err(|err| ConsumerError::TransportError(err.into()))
    }

    async fn get_context_states(
        &self,
        handles: Vec<String>,
    ) -> Result<GetContextStatesResponse, ConsumerError> {
        let hosting = self.hosting_service.lock().await;
        services::get_context_states(&*hosting, &handles)
            .await
            .map_err(|err| ConsumerError::TransportError(err.into()))
    }

    async fn get_context_states_by_filter(
        &self,
        _request: GetContextStatesByFilter,
    ) -> Result<GetContextStatesByFilterResponse, ConsumerError> {
        Err(ConsumerError::TransportError(anyhow::anyhow!(
            "get_context_states_by_filter is underspecified and unsupported"
        )))
    }

    async fn get_context_states_by_identification(
        &self,
        _request: GetContextStatesByIdentification,
    ) -> Result<GetContextStatesByIdentificationResponse, ConsumerError> {
        Err(ConsumerError::TransportError(anyhow::anyhow!(
            "get_context_states_by_identification is underspecified and unsupported"
        )))
    }

    async fn set_value(&self, request: SetValue) -> Result<Arc<ST>, ConsumerError> {
        execute_sco(
            self.set_service.as_ref(),
            AbstractSetOneOf::SetValue(request),
        )
        .await
    }

    async fn set_string(&self, request: SetString) -> Result<Arc<ST>, ConsumerError> {
        execute_sco(
            self.set_service.as_ref(),
            AbstractSetOneOf::SetString(request),
        )
        .await
    }

    async fn activate(&self, request: Activate) -> Result<Arc<ST>, ConsumerError> {
        execute_sco(
            self.set_service.as_ref(),
            AbstractSetOneOf::Activate(request),
        )
        .await
    }

    async fn set_alert_state(&self, request: SetAlertState) -> Result<Arc<ST>, ConsumerError> {
        execute_sco(
            self.set_service.as_ref(),
            AbstractSetOneOf::SetAlertState(request),
        )
        .await
    }

    async fn set_component_state(
        &self,
        request: SetComponentState,
    ) -> Result<Arc<ST>, ConsumerError> {
        execute_sco(
            self.set_service.as_ref(),
            AbstractSetOneOf::SetComponentState(request),
        )
        .await
    }

    async fn set_context_state(&self, request: SetContextState) -> Result<Arc<ST>, ConsumerError> {
        execute_sco(
            self.set_service.as_ref(),
            AbstractSetOneOf::SetContextState(request),
        )
        .await
    }

    async fn set_metric_state(&self, request: SetMetricState) -> Result<Arc<ST>, ConsumerError> {
        execute_sco(
            self.set_service.as_ref(),
            AbstractSetOneOf::SetMetricState(request),
        )
        .await
    }

    async fn get_localized_text(
        &self,
        request: GetLocalizedText,
    ) -> Result<GetLocalizedTextResponse, ConsumerError> {
        let hosting = self.hosting_service.lock().await;
        services::get_localized_text_msg(&*hosting, request)
            .await
            .map_err(|err| ConsumerError::TransportError(err.into()))
    }

    async fn get_supported_languages(
        &self,
        _request: GetSupportedLanguages,
    ) -> Result<GetSupportedLanguagesResponse, ConsumerError> {
        let hosting = self.hosting_service.lock().await;
        services::get_supported_languages(&*hosting)
            .await
            .map_err(|err| ConsumerError::TransportError(err.into()))
    }

    async fn disconnect(&mut self) -> Result<(), ConsumerError> {
        SdcRemoteDevice::disconnect(self)
            .await
            .map_err(|it| ConsumerError::TransportError(it.into()))
    }
}

#[async_trait]
impl<T, U, V, ST> SdcConsumer<U, ST>
    for protosdc::consumer::remote_device::SdcRemoteDevice<T, U, V, ST>
where
    T: Consumer
        + Send
        + Sync
        + LocalizationServiceConsumer
        + protosdc::consumer::grpc_consumer::GetServiceConsumer,
    U: RemoteMdibAccess + Send + Sync + Clone,
    V: SetServiceAccess<ST> + Send + Sync + Clone,
    ST: ScoTransaction + Send + Sync,
{
    async fn remote_mdib(&self) -> U {
        self.get_mdib_access().await
    }
    async fn get_mdib(&self) -> Result<GetMdibResponse, ConsumerError> {
        protosdc::consumer::remote_device::SdcRemoteDevice::get_mdib(self)
            .await
            .map_err(|err| ConsumerError::TransportError(err.into()))
    }

    async fn get_md_state(
        &self,
        handles: Vec<String>,
    ) -> Result<GetMdStateResponse, ConsumerError> {
        protosdc::consumer::remote_device::SdcRemoteDevice::get_md_state(self, handles)
            .await
            .map_err(|err| ConsumerError::TransportError(err.into()))
    }

    async fn get_md_description(
        &self,
        handles: Vec<String>,
    ) -> Result<GetMdDescriptionResponse, ConsumerError> {
        protosdc::consumer::remote_device::SdcRemoteDevice::get_md_description(self, handles)
            .await
            .map_err(|err| ConsumerError::TransportError(err.into()))
    }

    async fn get_context_states(
        &self,
        handles: Vec<String>,
    ) -> Result<GetContextStatesResponse, ConsumerError> {
        protosdc::consumer::remote_device::SdcRemoteDevice::get_context_states(self, handles)
            .await
            .map_err(|err| ConsumerError::TransportError(err.into()))
    }

    async fn get_context_states_by_filter(
        &self,
        _request: GetContextStatesByFilter,
    ) -> Result<GetContextStatesByFilterResponse, ConsumerError> {
        Err(ConsumerError::TransportError(anyhow::anyhow!(
            "get_context_states_by_filter is underspecified and unsupported"
        )))
    }

    async fn get_context_states_by_identification(
        &self,
        _request: GetContextStatesByIdentification,
    ) -> Result<GetContextStatesByIdentificationResponse, ConsumerError> {
        Err(ConsumerError::TransportError(anyhow::anyhow!(
            "get_context_states_by_filter is underspecified and unsupported"
        )))
    }

    async fn set_value(&self, request: SetValue) -> Result<Arc<ST>, ConsumerError> {
        execute_sco(
            self.set_service().await.as_ref(),
            AbstractSetOneOf::SetValue(request),
        )
        .await
    }

    async fn set_string(&self, request: SetString) -> Result<Arc<ST>, ConsumerError> {
        execute_sco(
            self.set_service().await.as_ref(),
            AbstractSetOneOf::SetString(request),
        )
        .await
    }

    async fn activate(&self, request: Activate) -> Result<Arc<ST>, ConsumerError> {
        execute_sco(
            self.set_service().await.as_ref(),
            AbstractSetOneOf::Activate(request),
        )
        .await
    }

    async fn set_alert_state(&self, request: SetAlertState) -> Result<Arc<ST>, ConsumerError> {
        execute_sco(
            self.set_service().await.as_ref(),
            AbstractSetOneOf::SetAlertState(request),
        )
        .await
    }

    async fn set_component_state(
        &self,
        request: SetComponentState,
    ) -> Result<Arc<ST>, ConsumerError> {
        execute_sco(
            self.set_service().await.as_ref(),
            AbstractSetOneOf::SetComponentState(request),
        )
        .await
    }

    async fn set_context_state(&self, request: SetContextState) -> Result<Arc<ST>, ConsumerError> {
        execute_sco(
            self.set_service().await.as_ref(),
            AbstractSetOneOf::SetContextState(request),
        )
        .await
    }

    async fn set_metric_state(&self, request: SetMetricState) -> Result<Arc<ST>, ConsumerError> {
        execute_sco(
            self.set_service().await.as_ref(),
            AbstractSetOneOf::SetMetricState(request),
        )
        .await
    }

    async fn get_localized_text(
        &self,
        request: GetLocalizedText,
    ) -> Result<GetLocalizedTextResponse, ConsumerError> {
        self.consumer()
            .get_localized_text(request)
            .await
            .map_err(|err| ConsumerError::TransportError(err.into()))
    }

    async fn get_supported_languages(
        &self,
        _request: GetSupportedLanguages,
    ) -> Result<GetSupportedLanguagesResponse, ConsumerError> {
        self.consumer()
            .get_supported_languages()
            .await
            .map_err(|err| ConsumerError::TransportError(err.into()))
    }

    async fn disconnect(&mut self) -> Result<(), ConsumerError> {
        self.disconnect()
            .await
            .map_err(|it| ConsumerError::TransportError(it.into()))
    }
}
