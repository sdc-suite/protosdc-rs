pub mod common {
    pub mod scopes {
        pub mod production_specification;
    }
}
pub mod provider {
    pub mod dual_stack_provider;
    pub mod plugin;
    pub mod sdc_provider;
}

pub mod consumer {
    pub mod sdc_consumer;
}
